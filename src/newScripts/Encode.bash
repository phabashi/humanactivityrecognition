#!/bin/bash

if [ $# -lt 1 ]; then
	echo "Usage 1: $0 <2DTrajectoryFolder>"
	echo "OR"
	echo "Usage 2: $0 <inputFifoName> [outputFifoName]"

	exit 1
fi

if [ -d $1 ]; then
	folderName=$1
else 
	if [ -e $1 ]; then 
		folderName=`cat $1`
	else 
		mkfifo $1
		folderName=`cat $1`
	fi
fi

 
encoding=${encoding:-"NormDiff"}
fileList=`ls $folderName`

outputFolder="$folderName/../$encoding/features"
mkdir -p $outputFolder
bc=0
for file in $fileList; do
	if [ ! -f "$outputFolder/$file" ]; then
		bc=$(( bc + 1 ))
		echo "encode: $file with $encoding and save it to $outputFolder/$file"
		./bin/encode -e $encoding -i "$folderName/$file" -o "$outputFolder/$file" & 
	else 
		#echo "File alreasy exists: $outputFolder/$file"
		echo -n "."
	fi
	if [ $bc == 7 ]; then
		wait
		bc=0
	fi 
done 

#send the parameter to next program
if [ "$2" != "" ]; then
        echo send the parameter to next program through \"$2\"
        if [ ! -e "$2" ]; then
                mkfifo "$2"
        fi
        echo "$outputFolder/../" > $2
fi

