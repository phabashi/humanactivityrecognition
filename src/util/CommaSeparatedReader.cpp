/*
 * CommaSeparatedReader.cpp
 *
 *  Created on: Nov 17, 2015
 *      Author: Pejman
 *      Copyright (c) 2015 Pejman. All rights reserved.
 */

#include "CommaSeparatedReader.h"

#include "../Exceptions/CommaSeparatedReadingException.h"
#include "../util/util.h"
#include "../Logger/Logger.h"

#include <string>
#include <iostream>
#include <fstream>
#include <vector>

#include "UniqueSequenceNumberGenerator.h"

using namespace std;
using namespace cv;
using namespace Pejman::Logger;

namespace HAR {

char CommaSeparatedReader::delimiter = ',';
std::string CommaSeparatedReader::delimiters = ", \t\n";

CommaSeparatedReader::CommaSeparatedReader() {

}

//cv::Mat CommaSeparatedReader::ReadFloatMatrix() {
//	return ReadFloatMatrix(cin);
//}

//cv::Mat CommaSeparatedReader::ReadFloatMatrix(std::string fileName) {
//	ifstream input(fileName);
//	return ReadFloatMatrix(input);
//}

cv::Mat CommaSeparatedReader::ReadFloatMatrix(std::istream& stream) {
	Logger::Log(LogLevel::VERBOSE, "Read Float Matrix.");
	int rowsToReadEachTime = 10000;
	int D;
	int i;
	//TODO Important : This way, we through away the first line (sometimes the first line contain data, not a header
	//Reading the first line to get the dimension D
	string str;
	getline(stream, str);
	while (str == "") {
		//read the input stream until we find a non empty line
		getline(stream, str);
		if (!stream) {
			//We reached the end of stream without reaching a non empty line
			//throw CommaSeparatedReadingException(); Bug Fix: This will cause the Crash of pipeline in encode" application,
			return cv::Mat();// if an empty file provided, an empty matrix makes more sense!
		}
		//exit(-1);
	}
	stringstream temp(str);
	//char delimiter = ',';
	auto tokens=split(str, delimiters);

	D = tokens.size(); // initialized Dimension based on first line
	Mat tempData(rowsToReadEachTime, D, CV_32F);
	Mat inputData;
	int lineNumber = 0;
	i = 0;
	try {
		for( uint j=0; j<tokens.size() ;j++){
			tempData.at<float>(i, j) = stringTo<float>(tokens[j]);
		}
		i++;
	} catch (ConversionException& e) {
		Logger::Log(LogLevel::EXCEPTION,
				"Non float value! detected in line %d  content = %s '",
				i, str.c_str());
	}

	lineNumber++;
	while (stream) {
		while (i < rowsToReadEachTime) {
			if (! stream ) break;
			string str;
			getline(stream, str);
			if(!stream)
				break;
			//if(str == "") break;
			str = trim( str );
			if(str == "")
				continue; //silently skip empty lines!
			auto tokens = split(str, delimiters);
			if(tokens.size() != D) {//wrong number of items in the line!
				Logger::Log(LogLevel::ERROR,
						"Error reading line from Stream! expecting %d items while see %d items in line %d",
						D, tokens.size(), i);
			} else{
				uint j;
				try {
					for( j=0; j<tokens.size(); j++){
						tempData.at<float>(i, j) = stringTo<float>(tokens[j]);
					}

				} catch (ConversionException& e) {
					Logger::Log(LogLevel::EXCEPTION,
							"Non float value! detected in line %d column: %d  content = %s '",
							i, j, str.c_str());
				}

				i++;
			}
		}
		if (i < rowsToReadEachTime) {
			inputData.push_back(tempData(Rect(0, 0, D, i)));
		} else {
			inputData.push_back(tempData);
		}
		lineNumber += i;
		i = 0; //move to next line
	}
	return inputData;
}

void CommaSeparatedReader::WriteFloatMatrix(cv::Mat mat) {
	WriteFloatMatrix(cout, mat);
}

void CommaSeparatedReader::WriteFloatMatrix(const std::string& fileName, cv::Mat mat) {
	ofstream out(fileName);
	WriteFloatMatrix(out, mat);
}

void CommaSeparatedReader::WriteFloatMatrix(std::ostream& ostream, cv::Mat mat) {
	if(mat.cols == 0) return;
	for (int i = 0; i < mat.rows; i++) {
		for (int j = 0; j < mat.cols - 1; j++)
			ostream << mat.at<float>(i, j) << ", ";
		ostream << mat.at<float>(i, mat.cols - 1) << endl;
	}
}

std::vector<float> CommaSeparatedReader::ReadLineFloat() {
	return ReadLineFloat(cin);
}

std::vector<float> CommaSeparatedReader::ReadLineFloat(std::istream& input) {

	std::vector<float> result;
	int j;
	string str;
	getline(input, str);
	while (str == "") {
		//read the input stream until we find a non empty file
		getline(input, str);
		if (!input) {
			return result;
		}
	}
	stringstream temp(str);
	//char delimiter = ',';
	j = 0;
	while (temp) {
		string valueStr;
		getline(temp, valueStr, delimiter);
		valueStr = trim(valueStr);
		if (valueStr == "") {
			//maybe something goes wrong, maybe we reach the end of string.
			// TODO find out what happened and how to manage it!
			break;
		}

		result.push_back(stringTo<float>(valueStr));
		j++;
	}

	return result;
}

CommaSeparatedReader::~CommaSeparatedReader() {
	// TODO Auto-generated destructor stub
}

cv::Mat CommaSeparatedReader::ReadFloatLabeledMatrix(std::string fileName,
		std::string lebelDefinitionFileName) {
	ifstream input(fileName);
	return ReadFloatLabeledMatrix(input, lebelDefinitionFileName);
}

cv::Mat CommaSeparatedReader::ReadFloatLabeledMatrix(std::istream& input,
		std::string lebelDefinitionFileName) {
	//We assume each row has a label and again we assume the label will present in the first row
	Mat data;
	//We read the labels from file
	UniqueSequenceNumberGenerator sg(lebelDefinitionFileName);
	string line;
//	int i =1;
	while (input) {
//		std::cout << "readin line " << i++ << endl;
		getline(input, line);
		if (!input)
			break;
		vector<string> tokens = split(line, ',');
		int cols = tokens.size();
		Mat theLine(1, cols, CV_32F);
		Logger::Log(VERBOSE, "The label to find index for : %s",
				tokens[0].c_str());
		theLine.at<float>(0, 0) = sg.getLabelIndex(tokens[0]);
		for (int i = 1; i < cols; i++)
			theLine.at<float>(0, i) = stringTo<float>(tokens[i]);
		data.push_back(theLine);
	}
	return data;
}

} /* namespace HAR */

