/*
 * Display.cpp
 *
 *  Created on: Jan 7, 2015
 *      Author: Pejman
 *      Copyright (c) 2015 Pejman. All rights reserved.
 */

#include "Display.h"
#include "../Logger/Logger.h"
#include <sstream>
using namespace Pejman::Logger;
using namespace HAR::Video::Features;
using namespace std;

namespace HAR {
namespace Video {

Display::Display() {
	// TODO Auto-generated constructor stub

}

void Display::Show(const std::string& windowName, const cv::Mat& image) {
	if(image.empty())
	{
		Logger::Log(LogLevel::WARNING, "Empty Frame To show!! Exiting now!\n" );
		return;
	}
	cv::imshow(windowName, image);
}
void Display::Show(cv::Mat imageL, cv::Mat imageR){
	if(imageL.empty())
	{
		Logger::Log(LogLevel::WARNING, "Empty Left Frame To show!! Exiting now!\n");
		return;
	}
	if(imageL.empty())
	{
		Logger::Log(LogLevel::WARNING, "Empty Right Frame To show!! Exiting now!\n");
		return;
	}
	cv::imshow("Test Left", imageL);
	cv::imshow("Test Right", imageR);
}
void Display::Show(cv::Mat image) {
	if(image.empty())
	{
		Logger::Log(LogLevel::WARNING, "Empty Frame To show!! Exiting now!\n");
		return;
	}
	cv::imshow("Test", image);
}

void Display::Show(cv::Mat image, double scale) {
	if(image.empty())
	{
		Logger::Log(LogLevel::WARNING, "Empty Frame To show!! Exiting now!\n");
		return;
	}
	cv::Mat temp;
	cv::resize(image, temp, cv::Size(image.cols * scale, image.rows*scale));
	cv::imshow("Test", temp);
}

void Display::Show(const cv::Mat originalImage, const FrameFeaturePoints* featuePoint) {
	cv::Mat displayImage;
	featuePoint->DrawFeaturePointsOnImage(originalImage, displayImage);
	Show(displayImage);
}

void Display::Show(Video originalVideo, const VideoFeaturePoints* featuePoints) {
	int i = 0;
	cv::Mat inputFrame = originalVideo.getFrame(i);
	int keycode = 0;
	while(inputFrame.data && keycode != 27)
	{
		FrameFeaturePoints* currentFrameFeatures = featuePoints->getFrameFeaturePoints(i++);
		//		FrameFeaturePoints* currentFrameFeatures = (*featuePoints)[i++];
		Display::Show(inputFrame, currentFrameFeatures);
		keycode = Display::waitKey(30);
		inputFrame = originalVideo.getNextFrame();
	}
}


int Display::waitKey(int delay) {
	static clock_t oldCall = clock();
	if(delay <=0)
		return cv::waitKey(delay);

	//delay in second
	double waistedTime =  ( clock() - oldCall ) / (double) CLOCKS_PER_SEC * 1000;//To convert it to millisecond
	//    		std::cout << "waisted time = " << waistedTime << "ms." << std::endl;
	int waitTime = delay - waistedTime;
	int result;
	if(waitTime > 0)
		result = cv::waitKey(waitTime);
	else
		result = cv::waitKey(1);
	oldCall = clock();
	return result;
}

void Display::printOutData(const FrameFeaturePoints* featuePoint) {
	// Add results to image and save.
	const std::vector<cv::KeyPoint>& keypoints  = featuePoint->getKeyPoints();
	int i =0;
	for(auto it = keypoints.begin(); it != keypoints.end(); it++)
	{
		const cv::KeyPoint& ti = (*it);
		stringstream sstr;
		sstr << std::endl << "P("<< ++i <<") = " <<  ti.pt << std::endl;
		sstr << "Response: " <<  ti.response << std::endl;
		sstr << "angle: " << ti.angle << std::endl;
		sstr << "Class ID: " << ti.class_id << std::endl;
		sstr << "Size: " << ti.size << std::endl;
		Logger::Log(LogLevel::INFO, sstr.str().c_str());

	}
}
void Display::Show(DisplayableVideo& left, DisplayableVideo& right)
{
	float waitTime= 1000.0 / left.getFramePerSecond();
	left.reset();
	right.reset();
	while(! left.isFinished() && ! right.isFinished())
	{
		bool isPaused = left.isPaused() ||  right.isPaused();
		if(! isPaused)
		{
			left.moveToNextDisplayabelFrame();//frameNumber++;
			right.moveToNextDisplayabelFrame();//frameNumber++;
		}

		if(left.getDisplayableFrame().empty() || right.getDisplayableFrame().empty())
		{
			Logger::Log(INFO,"Reaching the end of file.  The last frame number was %d.", left.getDisplayableFrameNumber() - 1);
			break;
		}

		cv::Mat combined = Display::Combine(left.getDisplayableFrame(), right.getDisplayableFrame());
		Display::Show( combined );
		int key = Display::waitKey(left.isPaused()?0:waitTime);
		Logger::Log(INFO, "The displayed frame is %d the pressed key is %d.",
				left.getDisplayableFrameNumber(), key);
		Logger::Log(INFO, "The displayed frame is %d the pressed key is %d.",
				right.getDisplayableFrameNumber(), key);
		left.UpdateWithKey(key);
		right.UpdateWithKey(key);
	}
}
void Display::Show(const cv::Mat left, const cv::Mat right, const std::vector<cv::KeyPoint>& keypoints1, const std::vector<cv::KeyPoint>& keypoints2, const std::vector<cv::DMatch>& matches)
{
	cv::Mat displayImage;
	cv::drawMatches(left, keypoints1, right, keypoints2,matches, displayImage);
	Display::Show(displayImage);
	Display::waitKey(1);
}

void Display::Show(const std::string& windowName, const cv::Mat left, const cv::Mat right,
		const FrameFeaturePoints& keypoints1, const FrameFeaturePoints& keypoints2,
		const StereoFrameMatches& matches)
{
	cv::Mat displayImage;
	cv::drawMatches(left, keypoints1.getKeyPoints(),
			right, keypoints2.getKeyPoints(),
			matches.getMatches(), displayImage);
	Display::Show(windowName, displayImage);

}

void Display::Show(const cv::Mat image, const std::vector<cv::Point2f>& ipLocations)
{
	Display::Show("Interest Points", image, ipLocations);
}

Display::~Display() {
	// TODO Auto-generated destructor stub
}

void Display::Show(Video& video) {
	cv::Mat inputFrame = video.getFrame(0);
	if(! inputFrame.data)
		Logger::Log( LogLevel::WARNING, "Empty Video...\n" );
	int keycode = 0;
	int timeToWait = 1000 / video.getFramePerSecond();//in millisecond
	while(inputFrame.data && keycode != 27)
	{
		Display::Show(inputFrame);
		keycode = Display::waitKey(timeToWait);
		inputFrame = video.getNextFrame();
	}


}

//void Display::Show(StereoAugmentedVideo video, std::string featureType)
//{
//	Logger::Log(LogLevel::VERBOSE, "Display ridemoon show!");
//	cv::Mat left, right;
//	int frameNumber = 0;
//	bool readSuccess = video.getFrame(frameNumber, left, right);

//	if(!readSuccess)
//		Logger::Log( LogLevel::WARNING, "Empty Video...\n");
//	int keycode = 0;
//	cv::Mat displayFrame;
//	while(readSuccess && keycode != 27)
//	{
//		if(featureType != "")
//		{
//			cv::Mat dleft, dright;
//			FrameFeaturePoints leftFeaturePoints = video.getLeftFrameFeaturePoints(frameNumber, featureType);
//			FrameFeaturePoints rightFeaturePoints = video.getRightFrameFeaturePoints(frameNumber, featureType);
//			leftFeaturePoints.DrawFeaturePointsOnImage(left, dleft);
//			rightFeaturePoints.DrawFeaturePointsOnImage(right, dright);
//			displayFrame = Combine(dleft, dright);
//		}
//		else
//		{
//			displayFrame = Combine(left, right);
//		}

//		Display::Show(displayFrame);
//		keycode = Display::waitKey(30);
//		readSuccess = video.getNextFrame(left, right); frameNumber ++;
//	}
//}

cv::Mat Display::Combine(const cv::Mat left, const cv::Mat right)
{
	int lw = left.size().width;
	int lh = left.size().height;
	int rw = right.size().width;
	int rh = right.size().height;

	cv::Size resultSize(lw + 2 + rw ,lh);

	cv::Mat currentStereoFrame = cv::Mat(resultSize, left.type());
	cv::Mat roiLeft = currentStereoFrame(cv::Rect(0,0, lw, lh));
	cv::Mat roiMargin = currentStereoFrame(cv::Rect(lw, 0, 2, lh));
	cv::Mat roiRight = currentStereoFrame(cv::Rect(lw + 2, 0, rw, rh));

	left.copyTo(roiLeft);
	right.copyTo(roiRight);
	roiMargin = cv::Scalar(250, 100, 100);

	return currentStereoFrame.clone();
}

bool Display::Check(const cv::Mat image)
{
	stringstream tmp;
	tmp << "Width = " << image.cols << std::endl;
	tmp << "Height = " << image.rows << std::endl;
	//	std::cout << "Data = " << image.data << std::endl;
	tmp << "isEmpty = " << image.empty() << std::endl;
	Logger::Log( LogLevel::WARNING, tmp.str().c_str());
	return (image.data != 0);
}

void Display::Show(const StereoFrame image, bool dispayFeaturePoints)
{
	Display::Show("No-Name", image, dispayFeaturePoints);
}


void Display::Show(const std::string& windowName, const StereoFrame image, bool dispayFeaturePoints)
{
	if( image.getLeft().empty() || image.getRight().empty())
	{
		//		cout << "Can not display and empty image..." << endl;
		return;
	}
	if( dispayFeaturePoints )
	{
		Display::Show(windowName, image.getLeft(), image.getRight(),
				image.getLeftFeaturePoints(), image.getRightFeaturePoints(), image.getStereoMatches());
	}
	else
	{
		Display::Show(windowName, Combine(image.getLeft(), image.getRight()));
	}
}

void Display::Show(std::string displayWindowName, const cv::Mat image,
		const std::vector<cv::Point2f>& ipLocations)
{
	cv::Mat disPlayImage = image.clone();
	int radious = 2;
	auto temp = ipLocations;
	for(auto it = temp.begin(); it != temp.end(); it ++ )
	{
		try
		{
			cv::circle(disPlayImage, *it, radious, cv::Scalar(255,255,0));
			//			cv::Mat roi = disPlayImage( cv::Rect(it->x - halfPointSize, it->y - halfPointSize,  halfPointSize, halfPointSize));
			//			roi = cv::Scalar(255, 0, 0);
		}
		catch(cv::Exception& )
		{

		}
	}

	Display::Show(displayWindowName, disPlayImage);
}

void Display::Show(Video theVideo, const MyVideoDescrptor& descriptor)
{

	unsigned int micromovementIndex = 0;
	unsigned int currentFrame = 0;
	//std::cout << descriptor.ToString() << std::endl;
	unsigned int oldDescriptorStart = 0, backup = 0;
	bool play_pause = true;

	for (cv::Mat displayImage = theVideo.getFrame(0); ! displayImage.empty(); displayImage = theVideo.getNextFrame(), currentFrame++)
	{
		if(!descriptor.getDescriptors().empty() &&  oldDescriptorStart < descriptor.getDescriptors().size())
		{
			for(micromovementIndex = oldDescriptorStart;
					micromovementIndex < descriptor.getDescriptors().size()
            														&& descriptor.getDescriptors()[micromovementIndex].getStartingFrame() < currentFrame;
					micromovementIndex++)
			{
				Display::DrawMicromovementOnImage(displayImage, descriptor.getDescriptor(micromovementIndex));
				if (micromovementIndex > 1)
					if(descriptor.getDescriptors()[micromovementIndex].getStartingFrame() !=
							descriptor.getDescriptors()[micromovementIndex -1].getStartingFrame())
					{
						backup = micromovementIndex;
					}
			}
			if (micromovementIndex > 1)
				if( descriptor.getDescriptors()[micromovementIndex - 1].getStartingFrame()
						!= descriptor.getDescriptors()[oldDescriptorStart].getStartingFrame() )
				{
					oldDescriptorStart = backup;
				}
		}
		Display::Show("Extracted Features", displayImage);
		int key = Display::waitKey(play_pause ? 30 : 0);
		if(key == 27) break;
		switch(key){
		case 'p':
		case 'P':
		case ' '://space bar
			play_pause = !play_pause;
			break;

		default:
			if(key != -1)
				Logger::Log( LogLevel::INFO, "Unknown key: keycode = %d\n", key);
		}
	}
}

void Display::DrawMicromovementOnImage(cv::Mat image,
		const MyDescriptor& micromovement)
{
	cv::Point2f oldPoint(0,0);
	for(auto dd : micromovement.getMicroMovement())
	{
		cv::Point2f newPoint = cv::Point2f(dd.x, dd.y);
		if (oldPoint == cv::Point2f(0,0))
		{
			oldPoint = newPoint;
			continue;
		}
		//		cv::line(image, oldPoint ,cv::Point2f(dd.x, dd.y), cv::Scalar(255/dd.z));
		//		std::cout << dd.z << " ";
		double c = (dd.z - 70) ;
		cv::line(image, oldPoint ,cv::Point2f(dd.x, dd.y), cv::Scalar(c, c, c), 2);
		oldPoint = newPoint;
	}
}
void Display::Show(DisplayableVideo& displayableVideo) {
	Logger::Log(LogLevel::VERBOSE, "Display ridemoon andar ridemoon show!");
	float waitTime= 1000.0 / displayableVideo.getFramePerSecond();
	displayableVideo.reset();
	while(! displayableVideo.isFinished())
	{
		if(! displayableVideo.isPaused())
		{
			displayableVideo.moveToNextDisplayabelFrame();//frameNumber++;
		}

		if(displayableVideo.getDisplayableFrame().empty())
		{
			Logger::Log(INFO,"Reaching the end of file.  The last frame number was %d.", displayableVideo.getDisplayableFrameNumber() - 1);
			break;
		}

		Display::Show( displayableVideo.getDisplayableFrame() );
		int key = Display::waitKey(displayableVideo.isPaused()?0:waitTime);
		Logger::Log(INFO, "The displayed frame is %d the pressed key is %d.",
				displayableVideo.getDisplayableFrameNumber(), key);
		displayableVideo.UpdateWithKey(key);
	}
}

void Display::DrawLineOnImage(cv::Mat& inputImage, double a, double b, double c) {
	float width = inputImage.cols;
	float height = inputImage.rows;

	double m = - a / b;
	if(abs(m) < (height / width)){// line has small slope
		cv::Point2f p0(0, - c / b);
		cv::Point2f p1(width, width * ( - a / b ) - c / b);
		cv::line(inputImage,p0,p1,cv::Scalar(255,0,0));
	} else {//line has big slope
		cv::Point2f p0(- c / a , 0);
		cv::Point2f p1( height * ( - b / a ) - c / a , height);
		cv::line(inputImage, p0, p1,cv::Scalar(255,0,0));
	}
}

void Display::DrawEpipolarLines(cv::Mat input, const cv::Mat fundamentalMat) {
	int n = 10;//number of points
	cv::Mat points(3, n, CV_64FC1);

	cout << "F = " << fundamentalMat << endl;
	for(int i =0; i< n; i++){
		points.at<double>(0, i) = rand() % input.cols;
		points.at<double>(1, i) = rand() % input.rows;
		points.at<double>(2, i) = 1;
	}
	cv::Mat lines = fundamentalMat * points;
	for(int i =0; i< n; i++){
		double a = lines.at<double>(0, i);
		double b = lines.at<double>(1, i);
		double c = lines.at<double>(2, i);
		DrawLineOnImage(input, a,b,c);
	}
}

} /* namespace Video */
} /* namespace HAR */

