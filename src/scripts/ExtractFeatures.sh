#!/bin/bash

if [ $# -ne 2 ]; then
	echo "Usage: $0 length interesPointAlgorithm "
	exit 1
fi
echo "This script will go trough all the clips in ./clips folder, search for Left vidoe and run extract featues on it and its corresponding right vido. "

#setting
interestPointAlgorithm=$2
descriptorAlgorithm="SIFT"
matchingAlgorithm="BruteForce"
ratio="1"
removeHomogenousAreas="0" 
featureFolderName="./$interestPointAlgorithm/$descriptorAlgorithm/$matchingAlgorithm/ratio_$ratio/rha_$removeHomogenousAreas"

featureLengthes=(3 5 7 9 11 13 15 17 19 21 23)

leftVideoFileNames=($(ls ./clips/ | grep Left))

#for length in ${featureLengthes[@]};do
length=$1

completeFeatureFileName="$featureFolderName/Length_$length/features"
mkdir -p $completeFeatureFileName

for i in ${leftVideoFileNames[@]}; do
	leftVideo="./clips/$i"
	rightVideo="./clips/${i/Left/Right}"
	name=${i/_Left}
	name="${name/avi/fet}"
	featureFileName="$completeFeatureFileName/$name"	
	if [ -e $featureFileName ]; then
		echo "File Exsists, skipping $featureFileName..." 
	else
		echo "Extracting features of '$leftVideo' and '$rightVideo' with lenght $length and save it to '$featureFileName'"
		./bin/ExtractFeatures -l $leftVideo -r $rightVideo -o $featureFileName -dl $length -ip $interestPointAlgorithm -ipd $descriptorAlgorithm -ma $matchingAlgorithm -ratio $ratio -rha $removeHomogenousAreas
	fi
done
#done


