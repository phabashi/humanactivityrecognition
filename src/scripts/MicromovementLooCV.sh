#!/bin/bash

if [ $# != 1 ]; then
	echo "You should provide the feature set you want: (HOG, HOF, Trajectory, MBHx, MBHy) "
	exit
fi
#Configuration Section
ExternalFeature=$1
Activities="ActivitySet_15.txt"
ActorList=(S01 S02 S03 S04 S05 S06 S07 S08 S09 S10 S11) # I assumend the list is always constant during my tests
numberOfWords=40

# Folder definition Section
ExternalFeaturesFolder="./featuresx/" #This folder is input

folderPrefix="./test-data/$(echo $Activities | cut -d . -f 1)/$ExternalFeature/n-$numberOfWords-words" # find the file name part of $Activities

csvDataFolder="./$folderPrefix/data/csv"
SVMDataFolder="./$folderPrefix/data/svm"
SVMModelFolder="./$folderPrefix/models/svm"
KMeansModelFolder="./$folderPrefix/models/kmeans"
resultFolder="./$folderPrefix/results/"

# Automated generating parameters section, should not be modified usually.

ActivityList="./lists/$Activities" 
externalFeatureFlag=true

SVMLearningParameters="-s 0 -t 3 -d 3 -c 500"

if [ $ExternalFeature = "Trajectory" ]; then
	FeatureNumbers='11-40' # Trajectory Features
else 
	if [ $ExternalFeature = "HOG" ]; then
		FeatureNumbers='41-136' #HOG Features'
	else 
		if [ $ExternalFeature = "HOF" ]; then
			FeatureNumbers='137-244' #HOF Features'
		else 
			if [ $ExternalFeature = "MBHx" ]; then
				FeatureNumbers='245-340' #MBHx Features'
			else
				if [ $ExternalFeature = "MBHy" ]; then
					FeatureNumbers='341-436' #MBHy Features'
				else
					externalFeatureFlag=false # we are dealing with a Micromovement
					if [ $ExternalFeature = "Micromovement-11" ]; then
						
						FeatureNumbers='2-34' #Micromovemetn with length 11
					else
						echo "The External Feature can not be found!"
						exit
					fi
				fi
			fi
		fi
	fi
fi


# Creating the folders if they are not exists already:
mkdir -p $csvDataFolder
mkdir -p $SVMDataFolder
mkdir -p $SVMModelFolder
mkdir -p $KMeansModelFolder
mkdir -p $resultFolder

echo "Setting : Feature = $ExternalFeature , FeatureNumbers = $FeatureNumbers , NumberOfWords = $numberOfWords "
doFoldExternal()
{
	i=$1
	echo "Initialize system, making folds...." 
	testingSet=`ls $ExternalFeaturesFolder | grep -e "Left" | grep -f $ActivityList | grep $i`
	trainingSet=`ls $ExternalFeaturesFolder | grep -e "Left" | grep -f $ActivityList | grep -v $i`
	modelFileName="$KMeansModelFolder/Fold-$i.yml"
	csvTrainFile="$csvDataFolder/Fold-$i.csv.train"
	csvTestFile="$csvDataFolder/Fold-$i.csv.test"
	svmTrainFile="$SVMDataFolder/Fold-$i.svm.train"
	svmTestFile="$SVMDataFolder/Fold-$i.svm.test"
	svmModelFileName="$SVMModelFolder/Fold-$i.svm.model"
	svmOutputFile="$SVMDataFolder/Fold-$i.svm.output"

	#make a KMeans model for this fold
	echo "Make KMeans Model for fold $i"
        echo $trainingSet | tr ' ' '\n'  | ./bin/Merge -f $ExternalFeaturesFolder | tr '\t' ',' | cut -d ',' -f $FeatureNumbers | ./bin/KMeansClustering -s -c $numberOfWords -o $modelFileName


	#Count The Words for this fold
	echo "Count the words for trainig in fold $i"
	echo $trainingSet | tr ' ' '\n' | ./bin/CountTheWords -m $modelFileName -f $ExternalFeaturesFolder -p  4 -c $csvTrainFile -d tab -sel $FeatureNumbers -lab $ActivityList

	echo "Count the words for testing in fold $i"
	echo $testingSet   | tr ' ' '\n' | ./bin/CountTheWords -m $modelFileName -f $ExternalFeaturesFolder -p  4 -c $csvTestFile -d tab -sel $FeatureNumbers -lab $ActivityList
       
	#Prepare data to be learned by libSVM
	echo "Prepare data to be used by libSVM"
	./bin/csv2svm -i $csvTrainFile $csvTestFile -o $svmTrainFile $svmTestFile -n -lab $ActivityList

	#Make a svm-model for the current data
	echo "Making SVM Model for the fold $i" 
	./svmbin/svm-train $SVMLearningParameters $svmTrainFile $svmModelFileName

	#Measure the accuracy of svm-model for calssification
	echo "Measuring the accuracy for fold $i"
        ./svmbin/svm-predict $svmTestFile $svmModelFileName $svmOutputFile

}

makeReportForFold()
{
	i=$1

	expected="$resultFolder/Fold-$i.expected"
	aquired="$resultFolder/Fold-$i.out"
	svmTestFile="$SVMDataFolder/Fold-$i.svm.test"
	svmOutputFile="$SVMDataFolder/Fold-$i.svm.output"
	
	#echo "Making report for fold $i :"
	cat $svmTestFile | cut -d " " -f 1 > $expected
	cp $svmOutputFile $aquired
	#./bin/MakeReport -e $expected -o $aquired -lab $ActivityList
	cat $expected >> $expectedResult
	cat $aquired >> $outputResult
}

doFold()
{
	i=$1
	echo "Initialize system, making folds...." 
	testingSet=`ls $FeaturesFolder | grep -e "_L11" | grep -f $ActivityList | grep $i`
	trainingSet=`ls $FeaturesFolder | grep -e "_L11" | grep -f $ActivityList | grep -v $i`
	modelFileName="$KMeansModelFolder/MM-w$numberOfWords-Fold-$i.yml"
	csvTrainFile="$CSVDataFolder/MM-w$numberOfWords-Fold-$i.csv.train"
	csvTestFile="$CSVDataFolder/MM-w$numberOfWords-Fold-$i.csv.test"
	svmTrainFile="$SVMDataFolder/MM-w$numberOfWords-Fold-$i.svm.train"
	svmTestFile="$SVMDataFolder/MM-w$numberOfWords-Fold-$i.svm.test"
	svmModelFileName="$SVMModelFolder/MM-w$numberOfWords-Fold-$i.svm.model"
	svmOutputFile="$SVMDataFolder/Fold-$i.svm.output"

	#make a KMeans model for this fold
	echo "Make KMeans Model for fold $i"
        echo $trainingSet | tr ' ' '\n'  | ./bin/Merge -f $FeaturesFolder | cut -d ',' -f $FeaturesList | ./bin/KMeansClustering -s -c $numberOfWords -o $modelFileName

	#Count The Words for this fold
	echo "Count the words for trainig in fold $i"
	echo $trainingSet | tr ' ' '\n' | ./bin/CountTheWords -m $modelFileName -f $FeaturesFolder -p  3 -c $csvTrainFile -sel $FeaturesList -lab $ActivityList

	echo "Count the words for testing in fold $i"
	echo $testingSet  | tr ' ' '\n' | ./bin/CountTheWords -m $modelFileName -f $FeaturesFolder -p  3 -c $csvTestFile -sel $FeaturesList -lab $ActivityList
       
	#Prepare data for libSVM
	echo "Preparing data for learning/testing with libSVM"
	./bin/csv2svm -i $csvTrainFile $csvTestFile -o $svmTrainFile $svmTestFile -n -lab $ActivityList

	#Make a svm-model for the current data
	echo "Making SVM Model for the fold $i" 
	./svmbin/svm-train $SVMLearningParameters $svmTrainFile $svmModelFileName

	#Measure the accuracy of svm-model for calssification
	echo "Measuring the accuracy for fold $i"
        ./svmbin/svm-predict $svmTestFile $svmModelFileName $svmOutputFile
}

if ["$externalFeatureFlag" = true]; then
	for i in ${ActorList[@]}; do doFoldExternal "$i" & done
else
	for i in ${ActorList[@]}; do doFoldMicromovement "$i" & done
fi
wait

#Make a report

echo "Making Report: "

expectedResult="$SVMDataFolder/all.expected"
outputResult="$SVMDataFolder/all.actual"

rm -f $expectedResult
rm -f $outputResult

for i in ${ActorList[@]}; do makeReportForFold "$i" ; done

./bin/MakeReport -e $expectedResult -o $outputResult -lab $ActivityList


