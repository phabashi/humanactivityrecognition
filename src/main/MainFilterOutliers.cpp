/*
 * MainFilterOutliers.cpp
 *
 *  Created on: May 1, 2015
 *      Author: Pejman
 *      Copyright (c) 2015 Pejman. All rights reserved.
 */

#include "../util/util.h"
#include <iostream>
#include "../Video/Descriptor/MyVideoDescrptor.h"

using namespace std;
using namespace HAR;


int main(int argc, char** argv)
{
    string intputfn, outputfn;
    double minEnergy = -1, maxEnergy = -1;
    bool removeDagamedInstances = false;

//    intputfn = "Descriptors.txt";
//    outputfn = "f.txt";
//    minEnergy = 0;
//    maxEnergy = -1;
//    removeDagamedInstances = true;
//


    if (argc == 6)
    {
        intputfn = argv[1];
        outputfn = argv[2];
        minEnergy = stringTo<double>(argv[3]);
        maxEnergy = stringTo<double>(argv[4]);

        if( string( argv[5]) == string("removeDamaged"))
            removeDagamedInstances = true;
        else
        {
            cout << "Unknown Option : " << argv[5]  << endl;
            cout << "Did You Mean : " << "removeDamaged" << endl;
            return -1;
        }
    }else if (argc == 5)
    {
        intputfn = argv[1];
        outputfn = argv[2];
        minEnergy = stringTo<double>(argv[3]);
        maxEnergy = stringTo<double>(argv[4]);

    } else if (argc == 4)
    {
        intputfn = argv[1];
        outputfn = argv[2];
        minEnergy = stringTo<double>(argv[3]);
    }
    else
    {
        cout << "Usage: FilterOutliears inpputDesctiptors outputDescriptors minEnergy [maxEnergy [removeDamaged] ] " <<endl;
        return -1;
    }

    int linesRead = 0;
    int linesToReadInEachStep = 10000;
    std::ifstream inFile(intputfn);
    uint theDescriptorLength = 0;
    while(inFile)
    {
    	MyVideoDescrptor descriptor;
    	descriptor.Load(inFile, linesToReadInEachStep);

    	MyVideoDescrptor outDescriptor = descriptor.filterOutLiers(minEnergy, maxEnergy, theDescriptorLength);
    	outDescriptor.Save(outputfn, (linesRead == 0));
    	int r = descriptor.getDescriptors().size();
    	int w = outDescriptor.getDescriptors().size();
    	linesRead += linesToReadInEachStep;
    	cout << "Read: \t" << r << "\t Wrote: \t" << w << "\t = " << 100.0 * w / r << "% "<< endl;

    }

    return 0;
}
