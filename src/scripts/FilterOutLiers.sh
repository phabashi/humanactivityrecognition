#!/bin/bash

#Setings
low=0
high=40000

#defining folders
Executable="./bin/FilterOutliers"
clipsFolder='./clips'
#featureFolder="./SIFT/SIFT/BruteForce/ratio_1/rha_0/Length_9/features/" # './features' # 
featureFolder='./featuresNew' # 
#filteredFolder="./SIFT/SIFT/BruteForce/ratio_1/rha_0/Length_9/filtered-m$low-M$high/" # './filtered' # 
filteredFolder='./filteredNew' # 

#making required output folders
mkdir -p $filteredFolder

#if (( $# == 0 )); then
#	echo 'You should provide the file list.'
#	echo "Usage: $0 fileNameList"
#	exit 1
#fi

#len=$( more $1 | wc -l )
lineNumber=0
#if [ ! -z $2 ]; then
#	lineNumber=$2
#	echo "Starting from $2 line among $len lines. Emmiting the previous lines."
#	len=`expr $len - $2`
#	echo "$len liens left to be prossesed."
#fi

#list=(`more $1 | tail -n $len`)
list=`ls $featureFolder` # | tail -n $len`

for fileName in ${list[@]}; do
	lineNumber=`expr $lineNumber + 1`
	echo -n "$lineNumber: Filtering features of $fileName. "
	
	newFileName=${fileName/.fet/.fil}
	if [[ ! -f $filteredFolder/$newFileName ]]; then
		$Executable $featureFolder/$fileName $filteredFolder/$newFileName $low $high removeDamaged 
	fi
		
	echo "Done."
done
