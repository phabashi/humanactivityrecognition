
FOLDERS = ExtractFeatures CountTheWords DisplayDescriptors 

.PHONY: ExtractFeatures
.PHONY: CountTheWords
.PHONY: DisplayDescriptors
.PHONY: CalculateMicroMovements 
.PHONY: FilterOutliers 
.PHONY: KMeansClustering 
.PHONY: LabelInstances
.PHONY: LabelVideoFrames
.PHONY: MotionDetection 
.PHONY: ReadMicroMovements
.PHONY: ShowVideoFrames
.PHONY: SplitVideo
.PHONY: SynchronizeVideos
.PHONY: Randomize
.PHONY: Merge
.PHONY: CSV2SVM
.PHONY: MakeReport

all: ExtractFeatures CountTheWords DisplayDescriptors CalculateMicroMovements \
	FilterOutliers KMeansClustering LabelInstances LabelVideoFrames \
	MotionDetection ReadMicroMovements ShowVideoFrames ShowVideoFrames \
	SplitVideo SynchronizeVideos Randomize Merge CSV2SVM MakeReport

MakeReport:
	$(MAKE) -C MakeReport
CSV2SVM:
	$(MAKE) -C CSV2SVM
Merge:
	$(MAKE) -C Merge
	
Randomize:
	$(MAKE) -C Randomize
	
ExtractFeatures:
	$(MAKE) -C ExtractFeatures

CountTheWords:
	$(MAKE) -C CountTheWords

DisplayDescriptors:
	$(MAKE) -C DisplayDescriptors

CalculateMicroMovements:
	$(MAKE) -C CalculateMicroMovements

FilterOutliers:
	$(MAKE) -C FilterOutliers

KMeansClustering:
	$(MAKE) -C KMeansClustering
       
LabelInstances:
	$(MAKE) -C LabelInstances
       
LabelVideoFrames:
	$(MAKE) -C LabelVideoFrames

MotionDetection: 
	$(MAKE) -C MotionDetection

ReadMicroMovements: 
	$(MAKE) -C ReadMicroMovements

ShowVideoFrames:
	$(MAKE) -C ShowVideoFrames

ShowVideoFrames:
	$(MAKE) -C ShowVideoFrames

SplitVideo:
	$(MAKE) -C SplitVideo

SynchronizeVideos:
	$(MAKE) -C LabelVideoFrames


clean:
	$(MAKE) -C ExtractFeatures clean
	$(MAKE) -C CountTheWords clean
	$(MAKE) -C DisplayDescriptors clean  
	$(MAKE) -C CalculateMicroMovements clean
	$(MAKE) -C FilterOutliers clean
	$(MAKE) -C KMeansClustering clean
	$(MAKE) -C LabelInstances clean
	$(MAKE) -C LabelVideoFrames clean
	$(MAKE) -C MotionDetection clean
	$(MAKE) -C ReadMicroMovements clean
	$(MAKE) -C ShowVideoFrames clean
	$(MAKE) -C ShowVideoFrames clean
	$(MAKE) -C SplitVideo clean
	$(MAKE) -C SynchronizeVideos clean
	$(MAKE) -C Randomize clean
	$(MAKE) -C Merge clean
	$(MAKE) -C CSV2SVM clean
	$(MAKE) -C MakeReport clean

