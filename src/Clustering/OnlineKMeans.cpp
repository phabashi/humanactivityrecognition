/*
 * OnlineKMeans.cpp
 *
 *  Created on: Jan 17, 2016
 *      Author: habashi
 */

#include "OnlineKMeans.h"
#include "DistanceCalculator.h"
#include <stdlib.h>
#include <math.h>
//#include "util.h"
#include <stdio.h>
#include <iostream>
#include "DistanceCalculator.h"

#include "../Exceptions/ArgumentParseException.h"
using namespace HAR::Exceptions;

namespace HAR{

OnlineKMeans::OnlineKMeans(int numberOfClasses, std::vector<float> sample) {
	counts = std::vector<int>(numberOfClasses, 0); //initialize count to zero

	this->numberOfClusters = numberOfClasses;
	dimension = sample.size();

	auto maxe = std::max(sample.begin(), sample.end());
	auto mine = std::min_element(sample.begin(), sample.end());

	double max = *maxe;
	double min = *mine;

	//std::cout << "Min = " << min << std::endl << "Max = " << max;

	double range = 1 * (max - min);

	centers = std::vector<std::vector<float> >(numberOfClasses, std::vector<float> (dimension, 0) );

	for(int i=0; i<numberOfClasses; i++ ){
		for(unsigned int j=0; j<dimension; j++){
			double r = static_cast<double>( rand()) / static_cast<float> (RAND_MAX);//r is between 0 and 1
			r = r*2 - 1; // r is in range -1 and +1
			centers[i][j] = r*(range) ;
		}
	}
}

OnlineKMeans::~OnlineKMeans() {
	// TODO Auto-generated destructor stub
}


int OnlineKMeans::Update(std::vector<float> observation) {
	int nearestCluster = -1;
	try{
		nearestCluster= getNearestCenter(observation);
	}
	catch(ArgumentParseException& exp ){
		std::cerr<< " Error: " <<  exp.what() << std::endl;
	}
	counts[nearestCluster] ++;

	//update centers
	for(unsigned int i=0; i<dimension; i++){
		centers[nearestCluster][i] += (1/counts[nearestCluster]) * (observation[i]-centers[nearestCluster][i]);
	}
	return nearestCluster;
}

int OnlineKMeans::getNearestCenter(std::vector<float> observation) {

	if ( observation.size() != dimension){
		std::stringstream p;
		p << "Observation size is not the same as dimension size. |observation| = "
			<< observation.size() << " dimension = " << dimension << std::endl;
		throw ArgumentParseException(p.str());
	}
	int minIndex = 0;
	float min = DistanceCalculator::EucledeanDistance(centers[0], observation);

	for(int i=1; i< numberOfClusters; i++){
		float temp = DistanceCalculator::EucledeanDistance(centers[i], observation);
		if(temp < min){
			min = temp;
			minIndex = i;
		}
	}
	return minIndex;
}

cv::Mat OnlineKMeans::getCentersInMatrixFormat() {
	cv::Mat result(numberOfClusters, dimension, CV_32FC1);
	for(int i=0; i<numberOfClusters; i++)
		for(unsigned int j=0; j<dimension; j++){
			result.at<float>(i,j) = centers[i][j];
		}
	return result;
}

std::vector<int> OnlineKMeans::getDistribution() {
	return counts;
}


}
