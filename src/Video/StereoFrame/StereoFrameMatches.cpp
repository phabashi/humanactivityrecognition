/*
 * StereoFrameMatches.cpp
 *
 *  Created on: Feb 5, 2015
 *      Author: Pejman
 *      Copyright (c) 2015 Pejman. All rights reserved.
 */

#include "StereoFrameMatches.h"

namespace HAR {
namespace Video {

StereoFrameMatches::StereoFrameMatches(const std::vector<cv::DMatch>& initialMatches,
		const FrameFeaturePoints& originalSourceKeypoints,
		const FrameFeaturePoints& originalDestinationKeypoints) {
	singleMatches = initialMatches;
	for(auto match : singleMatches)
	{
		from.push_back(originalSourceKeypoints.getKeyPoint(match.queryIdx));
		to.push_back(originalDestinationKeypoints.getKeyPoint(match.trainIdx));
	}
}

StereoFrameMatches::~StereoFrameMatches() {
	// TODO Auto-generated destructor stub
}

} /* namespace Video */
} /* namespace HAR */
