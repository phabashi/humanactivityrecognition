/*
 * DistanceCalculator.h
 *
 *  Created on: Feb 14, 2016
 *      Author: Pejman
 *      Copyright (c) 2016 pejman. All rights reserved.
 */

#ifndef SRC_COUNTTHEWORDS_DISTANCECALCULATOR_H_
#define SRC_COUNTTHEWORDS_DISTANCECALCULATOR_H_

#include <exception>
#include <string>
#include <vector>
#include <opencv2/opencv.hpp>

namespace HAR {

class DistanceCalculatorException : public std::exception {
public:
	DistanceCalculatorException(std::string msg) : msg(msg) {}
	virtual const char* what() {
		return ("DistanceCalculatorException : Exception " + msg).c_str();
	}

private:
	std::string msg;
};

class DistanceCalculator {
public:
	DistanceCalculator();

	static float EucledeanDistance(const std::vector<float>& a, const std::vector<float>& b);
	static float EucledeanDistance(const cv::Point2f& p1, const cv::Point2f& p2);
	inline static float EucledeanDistance(const cv::Mat& a, const cv::Mat& b);

	static double EucledeanDistance(const std::vector< double >& a, const std::vector< double >& b);

	static float ManhattanDistance(const cv::Point2f& p1, const cv::Point2f& p2);

	static float DTWDistance(const std::vector<float>& a, const std::vector<float>& b);

	float virtual getDistance() = 0;

	virtual ~DistanceCalculator();
};

inline float DistanceCalculator::EucledeanDistance(const cv::Mat& a,const cv::Mat& b) {
	float d = 0;
	assert(a.rows ==1 && b.rows == 1);
	assert(a.cols == b.cols);
	/*if (a.rows != 1 || b.rows != 1) {
		Logger::Log(LogLevel::EXCEPTION,
				"The matrices should have only one row! (They should represent a vector.");
		throw DistanceCalculatorException(
				"The matrices should have only one row! (They should represent a vector.");
	}
	if (a.cols != b.cols) {
		Logger::Log(LogLevel::EXCEPTION,
				"The matrices should have the same size of cols A.cols= %d, B.cols= %d", a.cols, b.cols);
		throw DistanceCalculatorException(
				"The matrices should have the same size of cols");
	}*/
	/*for (int i = 0; i < a.cols; i++) {
		float dx = a.at<float>(0, i) - b.at<float>(0, i);
		d += dx * dx;
	}*/
	const float *ap = a.ptr<float>(0);
	 const float *bp = b.ptr<float>(0);
	 for(int i=0; i< a.cols; i++ ) {
	 //assert(ap[i] == a.at<float>(0, i));
	 //assert(bp[i] == b.at<float>(0, i));
	 float dx = ap[i] - bp[i];
	 d += dx*dx;
	 }

	return sqrt(d);
}

} /* namespace HAR */

#endif /* SRC_COUNTTHEWORDS_DISTANCECALCULATOR_H_ */
