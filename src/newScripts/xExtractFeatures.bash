#!/bin/bash

#ls clips/ | grep -f ./lists/Actor_set3.txt | grep -f ./lists/ActivitySet_15.txt | grep Left

source ~/HAR/newScripts/xdefault.bash
#setting
settingFile="./SampleSetting.ini"
inputFileList="./lists/Windsor-short.txt"
inputVideoFolder="./dataset/Windsor/short/"
# Sample making file List:
# inputFileList=`ls ./clips/ | grep -e "Left" | grep -f ./lists/ActivitySet_10.txt | grep -f ./lists/Actor_set3.txt | grep -f ./lists/Instances_5.txt > ./lists/VideoFileList_Actor03_Activity_10_I5.txt` 
halfcores=8

#length=15

featureFolderName="./xtest-data/Length_$length/"

mkdir -p $featureFolderName
#featureLengthes=( 3 )

leftVideoFileNames=(`cat $inputFileList`)

completeFeatureFileName="$featureFolderName/features"
mkdir -p $completeFeatureFileName


count=${#leftVideoFileNames[@]}
fileNumber="1"
for i in ${leftVideoFileNames[@]}; do
	#ExtractFeatures $i &
	fileNumber=$(( $fileNumber + 1 ))
	#echo "Running $fileNumber/ $count = [$(( fileNumber * 100 / count ))%]"
	leftVideo="${inputVideoFolder}/$i"
	rightVideo="${inputVideoFolder}/${i/Left/Right}"
	lname="${i/avi/fet}"
	lfeatureFileName="$completeFeatureFileName/$lname"
	rname=${lname/_Left/_Right}
	rfeatureFileName="$completeFeatureFileName/$rname"
	if [ -e $lfeatureFileName ]; then
		echo "File Exsists, skipping $lfeatureFileName..."
	else
		echo "[$(( fileNumber * 100 / count ))%] Extracting features of '$leftVideo' and save it to '$lfeatureFileName'"
		./xbin/DenseTrack $leftVideo -L $length> $lfeatureFileName &
	fi
	if [ -e $rfeatureFileName ]; then
		echo "File Exsists, skipping $rfeatureFileName..."
	else
		echo "[$(( fileNumber * 100 / count ))%] Extracting features of '$rightVideo' and save it to '$rfeatureFileName'"
		./xbin/DenseTrack $rightVideo -L $length > $rfeatureFileName &
	fi
	if (( $fileNumber % $halfcores == 0 )); then
		echo "Waiting for childs!"
		wait
	fi
done
