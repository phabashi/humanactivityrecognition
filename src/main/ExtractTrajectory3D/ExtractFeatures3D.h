/*
 * ExtractFeatures3D.h
 *
 *  Created on: Jan 1, 2017
 *      Author: dhawal
 */

#ifndef SRC_MAIN_EXTRACTTRAJECTORY3D_EXTRACTFEATURES3D_H_
#define SRC_MAIN_EXTRACTTRAJECTORY3D_EXTRACTFEATURES3D_H_

#include "../../Video/Frame/FrameFeatureDescriptors.h"
#include "../../Feature/FeatureDescriptorMatcher.h"
#include "../../Video/motion/MotionDetection.h"
#include "../../Video/Display.h"
#include "../../Feature/FeatureDetector.h"
#include "../../Feature/FeatureDescriptorExtractor.h"
#include "../../OpticalFlow/IOpticalFlow.h"
#include "../../Logger/Logger.h"
#include "../../Pejman/GlobalSetting.h"
#include "Arguments.h"
#include "../../Video/TrajectoryEncoding/IEncoding.h"

using namespace cv;
using namespace std;
using namespace HAR;
using namespace Pejman::Logger;
using namespace Pejman::Application;
using namespace HAR::NewExtractFeatures3D;
using namespace HAR::Exceptions;
using namespace HAR::Video;
using namespace HAR::Video::Motion;
using namespace HAR::Video::OpticalFlow;

 namespace HAR{
 namespace ExtractFeatures3D{
 class ExtractFeatures3D{
 public:

	 //extracts features from left and right video
	 void ExtractFeatures(AugmentedVideoBuffer& leftVideo, AugmentedVideoBuffer& rightVideo, const Arguments& args);

 };
 }
 }

#endif /* SRC_MAIN_EXTRACTTRAJECTORY3D_EXTRACTFEATURES3D_H_ */
