/*
 * FundamentalMatrixFinder.h
 *
 *  Created on: Mar 12, 2017
 *      Author: pejman
 */

#ifndef SRC_VIDEO_FUNDAMENTALMATRIXFINDER_H_
#define SRC_VIDEO_FUNDAMENTALMATRIXFINDER_H_

#include<opencv2/opencv.hpp>
#include <vector>

namespace HAR {
namespace Video {

class FundamentalMatrixFinder {
public:
	FundamentalMatrixFinder();

	static cv::Mat findFundamentalMatrix(
			const std::vector<cv::Point2f>& leftImagePoints,
			const std::vector<cv::Point2f>& rightImagePoints);

	static cv::Mat findFundamentalMatrix(
			const std::vector<cv::Point2f>& leftImagePoints,
			const std::vector<cv::Point2f>& rightImagePoints,
			std::vector<uchar>& mask);

	~FundamentalMatrixFinder();

private:
	static cv::Mat findTransformationForNomalization(
			const std::vector<cv::Point2f>& points);
	static std::vector<cv::Point2f> applyTransformation(const std::vector<cv::Point2f>& points,
			cv::Mat transformation);

private:

};

} /* namespace Video */
} /* namespace HAR */

#endif /* SRC_VIDEO_FUNDAMENTALMATRIXFINDER_H_ */
