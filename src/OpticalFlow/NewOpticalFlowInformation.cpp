/*
 * NewOptialFlowInformation.cpp
 *
 *  Created on: May 21, 2015
 *      Author: Pejman
 *      Copyright (c) 2015 Pejman. All rights reserved.
 */

#include "NewOpticalFlowInformation.h"

namespace HAR {

NewOpticalFlowInformation::NewOpticalFlowInformation() {
	// TODO Auto-generated constructor stub
	maxIndex = -1;
}

void NewOpticalFlowInformation::setPointsForTracking(const FrameFeaturePoints& points)
{
	for(unsigned int i=0; i < points.getKeyPoints().size(); i++)
	{
		previousPoints[i] = points.getKeyPoint(i).pt;
        validIndexes.push_back(true);
	}
	maxIndex = points.getKeyPoints().size();
}

void NewOpticalFlowInformation::UpdatePoints(
		std::vector<cv::Point2f>& newLocation, std::vector<uchar>& status,
		const std::vector<float>& error)
{
	for (unsigned int i=0; i<newLocation.size(); i++)
		if(status[i])//if the point tracked successfully
		{
			nextPoints[i] = newLocation[i];
            validIndexes[i] = true;
		}
    else
        validIndexes[i] = false;
}

void NewOpticalFlowInformation::ReplaceOldPointsWithNewOnes()
{
	previousPoints = nextPoints;
	nextPoints.clear();
}

std::vector<cv::Point2f> NewOpticalFlowInformation::getPreviousPoints()
{
	std::vector<cv::Point2f> result;
	result.reserve(previousPoints.size());
	for(auto it = previousPoints.begin(); it != previousPoints.end(); it ++)
	{
		result.push_back(it->second);
	}
	return result;
}

NewOpticalFlowInformation::~NewOpticalFlowInformation() {
	// TODO Auto-generated destructor stub
}

} /* namespace HAR */
