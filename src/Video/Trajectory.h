/*
 * Trajectory.h
 *
 *  Created on: Feb 14, 2016
 *      Author: Pejman
 *      Copyright (c) 2016 Pejman. All rights reserved.
 */

#ifndef SRC_VIDEO_TRAJECTORY_H_
#define SRC_VIDEO_TRAJECTORY_H_
#include <unordered_map>
#include <opencv2/opencv.hpp>
#include "TrajectoryEncoding/IEncoding.h"

namespace HAR {
namespace Video {

class Trajectory {
public:
	Trajectory(const Trajectory& other);//copy constructor
	Trajectory(Trajectory&& other);//move constructor

	Trajectory& operator=(Trajectory other);

	Trajectory(const cv::Mat& oneRowTrajectoryMat);

	friend void swap(Trajectory& first, Trajectory& second){
		std::swap(first.startFrame, second.startFrame);
		std::swap(first.points, second.points);
	}

	Trajectory() : startFrame{0} { }
	Trajectory(int startFrame, cv::Point2f startPoint) : startFrame{startFrame} { points.push_back(startPoint); }
	void Add(cv::Point2f thePoint) { points.push_back(thePoint); }
	int length() const { return points.size(); }
	int getStartFrame() const { return startFrame; }
	const std::vector<cv::Point2f>& getAllPoints() const { return points; }
	cv::Point2f getLastPoint() const { return *(points.rbegin()); }
	cv::Point2f getFirstPoint() const { return *(points.begin()); }
	cv::Point2f getPoint(int frameNumber) const{
		if(frameNumber < startFrame || frameNumber > startFrame + length() )
			return cv::Point2f(-1,-1);
		return points[frameNumber - startFrame];
	}

	std::string Serialize( const cv::Size* frameSize = NULL, bool includeFrameNumber = true ) const;
	std::string Serialize(TrajectoryEncoding::TEncoding& encodingAlgorithm) const;

//	std::string SerializeDeferentiation(bool includeFrameNumber, bool normalize) const;

	bool isActiveOnFrame(int frameNumber) const {
		return (frameNumber >= startFrame && frameNumber < startFrame + length() );
	}

	bool operator==(const Trajectory& other) const {
		//Note: Two tracks are equal if their starting frame and location is equal.
		//+ which means we can not have tracks starting from same location going to different directions!!
		//+ which means we assumed ideal tracking
		//		if(points.size() == 0)
		//			std::cout << "ALARM ! ALARM ! ZeroLength Track Detected!" << std::endl;
		return (startFrame == other.startFrame
				&& points[0] == other.points[0]
		);
	}

	static float getEnergy(const std::vector<cv::Point2f>& points);

	float getEnergy() const;
	float getNormalizedEnergy() const { return (getEnergy() / length()); }

	//TODO Check the result of dynamic array on hash table!
	std::size_t getHashCode() const {
		return (std::hash<int>() ( startFrame ) << 5) ^
				 (std::hash<int>() ( points.size() )) ^
				 (std::hash<float>() (points[0].x )) ^
				 (std::hash<float>() (points[0].y ) << 2);
	}
private:
	int startFrame;
	std::vector<cv::Point2f> points;

};
} /*namespace Video*/
} /*namespace HAR*/

namespace std {
template<>
struct hash<HAR::Video::Trajectory>{
	std::size_t operator()(const HAR::Video::Trajectory& k) const
	{
		return k.getHashCode();
	}
};

} /* namespace std */

#endif /* SRC_VIDEO_TRAJECTORY_H_ */
