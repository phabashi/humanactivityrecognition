#!/bin/bash

ExternalFeature="Trajectory"

csvDataFolder="./data/csv"
SVMDataFolder="./data/svm"
SVMModelFolder="./models/svm"
KMeansModelFolder="./models/kmeans"
ExternalFeaturesFolder="./featuresx/"

ActivityList="./lists/ActivitySet_3.txt"

ActorList=(S01 S02 S03 S04 S05 S06 S07 S08 S09 S10 S11)
if [ $ExternalFeature = "Trajectory" ]; then
	FeatureNumbers='11-40' # Trajectory Features
else 
	if [ $ExternalFeature = "HOG" ]; then
		FeatureNumbers='41-136' #HOG Features'
	else 
		if [ $ExternalFeature = "HOF" ]; then
			FeatureNumbers='137-244' #HOF Features'
		else 
			if [ $ExternalFeature = "MBHx" ]; then
				FeatureNumbers='245-340' #MBHx Features'
			else
				if [ $ExternalFeature = "MBHy" ]; then
					FeatureNumbers='341-436' #MBHy Features'
				else
					echo "The External Feature can not be found!"
					exit
				fi
			fi
		fi
	fi
fi
numberOfWords=40

echo "Setting : Feature = $ExternalFeature , FeatureNumbers = $FeatureNumbers , NumberOfWords = $numberOfWords "
doFold()
{
	i=$1
	echo "Initialize system, making folds...." 
	testingSet=`ls $ExternalFeaturesFolder | grep -e "Left" | grep -f $ActivityList | grep $i`
	trainingSet=`ls $ExternalFeaturesFolder | grep -e "Left" | grep -f $ActivityList | grep -v $i`
	modelFileName="$KMeansModelFolder/$ExternalFeatur-w$numberOfWords-Fold-$i.yml"
	csvTrainFile="$csvDataFolder/$ExternalFeature-w$numberOfWords-Fold-$i.csv.train"
	csvTestFile="$csvDataFolder/$ExternalFeature-w$numberOfWords-Fold-$i.csv.test"
	svmTrainFile="$SVMDataFolder/$ExternalFeature-w$numberOfWords-Fold-$i.svm.train"
	svmTestFile="$SVMDataFolder/$ExternalFeature-w$numberOfWords-Fold-$i.svm.test"
	svmModelFileName="$SVMModelFolder/$ExternalFeature-w$numberOfWords-Fold-$i.svm.model"

	#make a KMeans model for this fold
	#echo "Make KMeans Model for fold $i"
        #echo $trainingSet | tr ' ' '\n'  | ./bin/Merge -f $ExternalFeaturesFolder | tr '\t' ',' | cut -d ',' -f $FeatureNumbers | ./bin/KMeansClustering -s -c $numberOfWords -o $modelFileName


	#Count The Words for this fold
	#echo "Count the words for trainig in fold $i"
	#echo $trainingSet | tr ' ' '\n' | ./bin/CountTheWords -m $modelFileName -f $ExternalFeaturesFolder -p  4 -c $csvTrainFile -d tab -sel $FeatureNumbers

	#echo "Count the words for testing in fold $i"
	#echo $testingSet | tr ' ' '\n' | ./bin/CountTheWords -m $modelFileName -f $ExternalFeaturesFolder -p 4  -c $csvTestFile -d tab -sel $FeatureNumbers
       
	#Prepare data to be learned by libSVM
	echo "Prepare data to be used by libSVM"
	./bin/csv2svm -i $csvTrainFile $csvTestFile -o $svmTrainFile $svmTestFile -n

	#Make a svm-model for the current data
	echo "Making SVM Model for the fold $i" 
	./svmbin/svm-train -s 0 -t 2 -d 3 -c 500 $svmTrainFile $svmModelFileName

	#Measure the accuracy of svm-model for calssification
	echo "Measuring the accuracy for fold $i"
	svmOutputFile="$SVMDataFolder/$ExternalFeatures-w$numberOfWords-Fold-$i.svm.output"
        ./svmbin/svm-predict $svmTestFile $svmModelFileName $svmOutputFile
}

for i in ${ActorList[@]}; do doFold "$i" ; done

wait

