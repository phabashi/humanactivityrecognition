/*
 * MainCalculateMicorMovements.cpp
 *
 *  Created on: May 1, 2015
 *      Author: Pejman
 *      Copyright (c) 2015 Pejman. All rights reserved.
 */

#include "../util/util.h"
#include <iostream>
#include "../Video/Descriptor/MyVideoDescrptor.h"

using namespace std;
using namespace HAR;


int main(int argc, char** argv)
{
    string inputDescriptorFileName, outputMicromovementFileName;
    if (argc == 3)
    {
        inputDescriptorFileName = argv[1];
        outputMicromovementFileName = argv[2];
    }
    else
    {
        cout << "Usage: CalculateMicromovements inputFeaturePoints outputMicromovements" <<endl;
        return -1;
    }

    MyVideoDescrptor descriptor;

    descriptor.Load(inputDescriptorFileName);

    descriptor.SaveMicroMovements(outputMicromovementFileName);

    return 0;
}
