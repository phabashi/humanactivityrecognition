/*
 * SpatialEncoding.cpp
 *
 *  Created on: Dec 28, 2016
 *      Author: user
 */

#include "SpatialEncoding.h"

namespace HAR {
namespace Video {
namespace TrajectoryEncoding {

SpatialEncoding::SpatialEncoding() {
	// TODO Auto-generated constructor stub

}

std::vector<float> SpatialEncoding::encode(const std::vector<cv::Point2f>& points) const {
	std::vector<float> result;
	result.reserve((points.size()- 1) *2);
	for (uint i = 1; i < points.size(); i++) {//Removes the starting location of trajectory only
		result.push_back(points[i].x - points[0].x);
		result.push_back(points[i].y - points[0].y);
	}
	return result;
}

SpatialEncoding::~SpatialEncoding() {
	// TODO Auto-generated destructor stub
}

} /* namespace TrajectoryEncoding */
} /* namespace Video */
} /* namespace HAR */
