/*
 * ThirdOrder.h
 *
 *  Created on: Sep 26, 2016
 *      Author: pejman
 */

#ifndef SRC_VIDEO_TRAJECTORYENCODING_THIRDORDER_H_
#define SRC_VIDEO_TRAJECTORYENCODING_THIRDORDER_H_

#include "IEncoding.h"

namespace HAR {
namespace Video {
namespace TrajectoryEncoding {

class ThirdOrder: public IEncoding {
public:
	ThirdOrder();

	virtual std::vector<float> encode(const std::vector<cv::Point2f>& points) const;

	virtual ~ThirdOrder();
};

} /* namespace TrajectoryEncoding */
} /* namespace Video */
} /* namespace HAR */

#endif /* SRC_VIDEO_TRAJECTORYENCODING_THIRDORDER_H_ */
