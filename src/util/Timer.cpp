//
//  Timer.cpp
//  Disparity
//
//  Created by pejman on 2014-10-19.
//  Copyright (c) 2014 pejman. All rights reserved.
//

#include "Timer.h"

namespace HAR
{

Timer::Timer()
{
    oldTime = clock();
    passedTime = 0;
    isActive = false;
}

void Timer::Start()
{
    if(isActive)
        return;
    passedTime = 0;
    oldTime = clock();
    isActive = true;
}

void Timer::pause()
{
    if(!isActive)
        return;
    clock_t end = clock();
    passedTime += double(end - oldTime);
    isActive = false;
}

void Timer::Resume()
{
    if(isActive)
        return;
    oldTime = clock();
    isActive = true;
}

double Timer::getPassedTime()
{
	if(isActive){
		clock_t end = clock();
		passedTime += double(end - oldTime);
		oldTime = clock();
	}
    return (passedTime / CLOCKS_PER_SEC);
}

void Timer::Stop()
{
    if(!isActive)
        return;
    clock_t end = clock();
    passedTime += double(end - oldTime);
    isActive = false;
}

clock_t Timer::getTicks()
{
	//TODO The clock is the CPU clock not the real clock!
	return clock();
}

Timer::~Timer()
{
    
}

}//end of namespace GameEngine
