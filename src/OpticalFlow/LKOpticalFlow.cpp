/*
 * LKOpticalFlow.cpp
 *
 *  Created on: Apr 30, 2016
 *      Author: pejman
 */

#include "LKOpticalFlow.h"
#include "../Pejman/GlobalSetting.h"
#include "../Logger/Logger.h"

using namespace Pejman::Logger;
using namespace Pejman::Application;

namespace HAR {
namespace Video {
namespace OpticalFlow {

LKOpticalFlow::LKOpticalFlow() {
	// TODO Auto-generated constructor stub
	//TODO Parameters to be optimized!
	winSize = std::move(cv::Size(
			GlobalSetting::GetOptionAs<int>("OpticalFlowLK", "WindowSizeX", 15),
			GlobalSetting::GetOptionAs<int>("OpticalFlowLK", "WindowSizeY", 15)));

	level = 5;
	termcrit = cv::TermCriteria(cv::TermCriteria::COUNT | cv::TermCriteria::EPS,
			GlobalSetting::GetOptionAs<int>("OpticalFlowLK", "TermCritCount", 20),
			GlobalSetting::GetOptionAs<float>("OpticalFlowLK", "TermCritEpsilon", 0.003));
//	float jumptreshold = GlobalSetting::GetOptionAs<float>("OpticalFlowLK",
//			"jumpThreshold", 0); // This parameter optimization happen in the first of the main loop.
	flags = GlobalSetting::GetOptionAs<int>("OpticalFlowLK", "flags", 0);
	minimumEigenThreshold = GlobalSetting::GetOptionAs<float>(
			"OpticalFlowLK", "minimumEigenThreshold", 0.001);
}

void LKOpticalFlow::ExtractOpticalFlow(AugmentedVideoBuffer& video, int length) {
	video.ClearTrajectories();

	cv::Mat preImage;
	preImage = video.getFrame(0);
	cv::Size frameSize(preImage.cols, preImage.rows);

	std::vector<Trajectory> survivedTracking;  //Contains ongoing tracks
	std::vector<Trajectory> activeTracks;  //Contains ongoing tracks
	std::vector<Trajectory> finishedTracks;
	//	std::unordered_set<Track> finishedTracks;//Contains finished tracks

	//TODO The frame will never be used during the loop, it might be faster to replace actual reading with a simulation
	for (cv::Mat frame = video.getFrame(1); frame.data;
			frame = video.getNextFrame()) {
//		if (treshold < 1) {  // we have not calculated the threshold yet!
//			//Optimize! Now it is fixed to 800 for 640x400 frame size
//			float dx = (frame.cols / 32.0);
//			float dy = (frame.rows / 20.0);
//			jumptreshold = dx * dx + +dy * dy; // Example 20x20  for 640 and 400 => jump (10*10 + 10*10) = 200
//		}
//		const std::vector<cv::Point_<float> >& previousFramePointsOfInteres = video.getPreviousFrameInformation().getPointsOfInterest();
		const std::vector<cv::Point_<float> >& thisFramePointsOfInteres =
				video.getCurrentFrameInformation().getPointsOfInterest();

		std::vector<cv::Point2f> activeTrackingPoints;
		//Add old tracking point for further tracking
		for (uint i = 0; i < activeTracks.size(); i++)
			activeTrackingPoints.push_back(activeTracks[i].getLastPoint());
		//Add new Tracking points for tracking
		for (const cv::Point2f p : thisFramePointsOfInteres) {
			activeTrackingPoints.push_back(p);
			activeTracks.push_back(
					Trajectory(video.getCurrentFrameNumber(), p));
		}

		//Here activeTacks and activeTracking points have a one-to-one relationship, based on index

		if (activeTrackingPoints.empty())
			continue;

		std::vector<cv::Point2f> nextPoints;
		std::vector<uchar> status;
		std::vector<float> error;
		cv::calcOpticalFlowPyrLK(preImage,
				frame, // new Image
				activeTrackingPoints, nextPoints, status, error, winSize, level,
				termcrit, flags, minimumEigenThreshold);
		preImage = frame.clone();
		std::vector<Trajectory> survivedTracking;

		for (uint i = 0; i < nextPoints.size(); i++) {
			if (status[i]) { //if the point correctly tracked
				Trajectory t = activeTracks[i];
//				cv::Point2f old = t.getLastPoint();
//				float dx = old.x - nextPoints[i].x;
//				float dy = old.y - nextPoints[i].y;
//				if ((dx * dx + dy * dy) < jumptreshold)
				{
					t.Add(nextPoints[i]);
					if (t.length() == length) // if we had reached the desired length
						video.AddTrajectory(t);
					else
						survivedTracking.push_back(t);
				}
			}
		}

		activeTracks = survivedTracking;
	}
}

LKOpticalFlow::~LKOpticalFlow() {
	// TODO Auto-generated destructor stub
}

} /* namespace OpticalFlow */
} /* namespace Video */
} /* namespace HAR */
