/*
 * MainShowVideoFrames.cpp
 *
 *  Created on: May 1, 2015
 *      Author: Pejman
 *      Copyright (c) 2015 Pejman. All rights reserved.
 */

#include <ncurses.h>
#include "../../util/util.h"
#include <iostream>
#include "../../Video/Display.h"

#include "LabelFrameVideoModel.h"
#include "LabelVideoFrameView.h"
#include "LabelVideoFramesController.h"

using namespace std;
using namespace HAR;
using namespace HAR::Video;


int main(int argc, char** argv)
{
    int startingFrame = 0;
    string labelFileName;
 	string inFileName;
    string labelDefinitionFileName;
	double scale = 1.0;

    if (argc >= 6)
    {
        startingFrame = stringTo<int>(argv[5]);
    }
    
	if (argc >= 5)
	{
		scale = stringTo<double>(argv[4]);
		//cout << "Set scale to" << scale << endl;
        //getch();
	}
    
    if (argc >= 4)
	{
        labelFileName = argv[3];
        labelDefinitionFileName = argv[2];
		inFileName = argv[1];
	}
	else
	{
		cout << "Usage: LabelVideoFrames inFileName existingLabelFileName outputLabelFileName [scale [startingFrame]]";
		return -1;
	}
    
    LabelVideoModel model(inFileName, labelDefinitionFileName, labelFileName, scale);
    if(startingFrame != 0) model.setFrameNumber(startingFrame);
    LabelVideoView view(model);
    LabelVideoController controller(model, view);

    while(! model.isPlayingFinished())
	{
		if(! model.isPlayigPaused())
		{
            model.getNextFrame();
		}

		if(model.getCurrentFrame().empty())
		{
            char temp [1000];
            sprintf(temp, "Reaching the end of file. The last frame number was: %d ", model.getCurrentFrameNumber() -1);
			model.setErrorMsg(temp);
			break;
		}

        view.Refresh();
		int key = Display::waitKey(model.isPlayigPaused()?0:30);

        controller.Update(key);
    }

	return 0;
}
