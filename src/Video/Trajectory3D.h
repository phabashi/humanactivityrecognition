/*
 * Trajectory3D.h
 *
 *  Created on: Feb 13, 2017
 *      Author: pejman
 */

#ifndef SRC_VIDEO_TRAJECTORY3D_H_
#define SRC_VIDEO_TRAJECTORY3D_H_

#include "Trajectory.h"

namespace HAR {
namespace Video {

class Trajectory3D {
public:
	Trajectory3D(Trajectory left, Trajectory right);
	Trajectory3D();

	int getStartFrame() const { return leftTrajectory.getStartFrame(); }
	cv::Point2f getLeftFP() const { return leftTrajectory.getFirstPoint(); }
	cv::Point2f getRightFP() const { return rightTrajectory.getFirstPoint(); }

	std::size_t getHashCode() const;

	virtual ~Trajectory3D();

	void Combine(const std::vector<bool>& isValid, const std::vector<cv::Mat>& h1, const std::vector<cv::Mat>& h2);
	const std::vector<cv::Point3f>& get3DPoints() const { return trajectory3D; }

private:
	Trajectory leftTrajectory;
	Trajectory rightTrajectory;

	std::vector<cv::Point3f> trajectory3D;
};

} /* namespace Video */
} /* namespace HAR */

namespace std {
template<>
struct hash<HAR::Video::Trajectory3D>{
	std::size_t operator()(const HAR::Video::Trajectory3D& k) const
	{
		return k.getHashCode();
	}
};
} /* namespace std */

#endif /* SRC_VIDEO_TRAJECTORY3D_H_ */
