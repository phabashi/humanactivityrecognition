#!/bin/bash

if [ $# < 1 ]; then
	echo "Dataset Name is missing!"
	echo "Usage: $0 DatasetName [fifoName]"
	echo "fifoName parameter is used to save the path to extracted features in the named fifo provided."
	exit 1
fi

datasetFolder="./dataset/Windsor/selectedClips/"

echo "This script will go trough all the clips in ./$datasetFolder folder, search for Left vidoe and run extract featues on it and its corresponding right vido. "

#ls clips/ | grep -f ./lists/Actor_set3.txt | grep -f ./lists/ActivitySet_15.txt | grep Left
datasetName=$1;
#setting
settingFile="./SampleSetting.ini"
inputFileList="./lists/$datasetName.txt"

if [ ! -f $inputFileList ]; then
	echo "The dataset '$1' should have a file list in '$inputFileList'"
	echo "This means either you mistyped the dataset name of the list is not created yet!"
	exit 2
fi

# Sample making file List:
# inputFileList=`ls ./clips/ | grep -e "Left" | grep -f ./lists/ActivitySet_10.txt | grep -f ./lists/Actor_set3.txt | grep -f ./lists/Instances_5.txt > ./lists/VideoFileList_Actor03_Activity_10_I5.txt` 
cores=16

source ~/HAR/newScripts/default.bash

if [[ $detectMotion == "No" ]]; then
	motionOption="-dm"
	motionFolderName="Disabled"
else
	motionOption="-em"
	motionFolderName="Enabled"
fi

#encoding="NormDiff"
#normalized="-nd"
#foldn="Not"
#if [[ $normalizeOutput == "Yes" ]]; then
#	normalized="-ne"
#	foldn=""
#fi

if [ $MULTISCALE == "" ]; then 
	scaleFolderName="SingleScale"
else
	scaleFolderName="MultiScale"
fi

featureFolderName="./test-data/$datasetName/${motionFolderName}Motion/$opticalFlowAlgorithm/$interestPointAlgorithm/$descriptorAlgorithm/$scaleFolderName/scale_$scale/ss_${samplingScale}/"

mkdir -p $featureFolderName

leftVideoFileNames=(`cat $inputFileList`)

completeFeatureFileName="$featureFolderName/Length_$length/2DTrajectories"
mkdir -p $completeFeatureFileName

count=${#leftVideoFileNames[@]}
fileNumber="1"
for i in ${leftVideoFileNames[@]}; do
	#ExtractFeatures $i &
	fileNumber=$(( $fileNumber + 1 ))
	#echo "Running $fileNumber/ $count = [$(( fileNumber * 100 / count ))%]"
	leftVideo="./$datasetFolder/$i"
	echo $leftVideo 
	rightVideo="./$datasetFolder/${i/Left/Right}"
	name=${i}
	name="${name/avi/fet}"
	featureFileName="$completeFeatureFileName/$name"
	if [ -e $featureFileName ]; then
		echo "File Exsists, skipping $featureFileName..."
	else
		echo "[$(( fileNumber * 100 / count ))%] Extracting features of '$leftVideo' with lenght $length and save it to '$featureFileName'"
		#extract Trajectories without encoding
		./bin/newExtractFeatures -l $leftVideo -o $featureFileName -dl $length -ip $interestPointAlgorithm -ipd $descriptorAlgorithm -ofa $opticalFlowAlgorithm -s $scale $MULTISCALE -te Off $motionOption -minE $minEnergy -ss $samplingScale as $settingFile& 
		if [[ $? != 0 ]]; then
			echo "Some error occured! Exiting "
			exit 1
		fi
	fi
	#sleep 1 &
	#echo $fileNumber &
	#wait %1
	if (( $fileNumber % $cores == 0 )); then
		echo "Waiting for childs!"
		wait
	fi
done

#send the parameter to next program
if [ "$2" != "" ]; then
	echo send the parameter to next program through \"$2\"
	if [ ! -e "$2" ]; then
        	mkfifo "$2"
	fi
	echo  $completeFeatureFileName > $2
fi                                
