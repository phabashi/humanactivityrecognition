/*
 * DifferentiationEncoding.h
 *
 *  Created on: May 31, 2016
 *      Author: pejman
 */

#ifndef SRC_VIDEO_TRAJECTORYENCODING_DIFFERENTIATIONENCODING_H_
#define SRC_VIDEO_TRAJECTORYENCODING_DIFFERENTIATIONENCODING_H_

#include "IEncoding.h"

namespace HAR {
namespace Video {
namespace TrajectoryEncoding {

class DifferentiationEncoding: public IEncoding {
public:
	DifferentiationEncoding();

	virtual std::vector<float> encode(const std::vector<cv::Point2f>& points) const;

	virtual ~DifferentiationEncoding();
};

} /* namespace TrajectoryEncoding */
} /* namespace Video */
} /* namespace HAR */

#endif /* SRC_VIDEO_TRAJECTORYENCODING_DIFFERENTIATIONENCODING_H_ */
