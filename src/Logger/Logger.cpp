/*
 * Logger.cpp
 *
 *  Created on: Mar 6, 2016
 *      Author: Pejman
 *      Copyright (c) 2016 Pejman. All rights reserved.
 */

#include "Logger.h"
#include <iostream>
#include <stdio.h>
#include <cstdio>
#include <cstdarg>
#include <ctime>
#include <vector>

using namespace std;

namespace Pejman {
namespace Logger {

Logger::Logger(LogLevel level) {
	// TODO Auto-generated constructor stub
	logLevel = level;
	//The order is important in the following five lines
	levelName[0] = "FATAL";
	levelName[1] = "ERROR";
	levelName[2] = "EXCEPTION";
	levelName[3] = "WARNING";
	levelName[4] = "INFO";
	levelName[5] = "VERBOSE";
}

void Logger::LogFormattedString(LogLevel type, const char* fmt, ...) {
	va_list tmp;
	va_start(tmp, fmt);
	LogFormattedString(type, fmt, tmp);
	va_end(tmp);
}

void Logger::LogFormattedString(LogLevel type, const char* fmt, va_list args1) {
	if(type > logLevel) return;
	std::time_t t = std::time(nullptr);
	char time_buf[100];
	std::strftime(time_buf, sizeof time_buf, "%D %T", std::gmtime(&t));
	//va_list args1;
	//va_start(args1, fmt);
	va_list args2;
	va_copy(args2, args1);
	std::vector<char> buf(1 + std::vsnprintf(NULL, 0, fmt, args1));
	va_end(args1);
	std::vsnprintf(buf.data(), buf.size(), fmt, args2);
	va_end(args2);
	std::printf("%s [%s]: %s\n", levelName[type].c_str() ,time_buf, buf.data());
}

Logger::~Logger() {
	// TODO Auto-generated destructor stub
}

} /* namespace Logger */
} /* namespace Pejman */
