/*
 * FBOpticalFlow.h
 *
 *  Created on: Apr 30, 2016
 *      Author: pejman
 */

#ifndef SRC_OPTICALFLOW_FBOPTICALFLOW_H_
#define SRC_OPTICALFLOW_FBOPTICALFLOW_H_

#include "IOpticalFlow.h"

namespace HAR {
namespace Video {
namespace OpticalFlow {

class FBOpticalFlow: public IOpticalFlow {
public:
	FBOpticalFlow();

	virtual void ExtractOpticalFlow(AugmentedVideoBuffer& video, int length);

	virtual ~FBOpticalFlow();

private:
	int windowSize;
	int level;
	float scale;
	int iteration;
	int poly_n;
	double poly_sigma;
	int flags;
	cv::TermCriteria termcrit;
};

} /* namespace OpticalFlow */
} /* namespace Video */
} /* namespace HAR */

#endif /* SRC_OPTICALFLOW_FBOPTICALFLOW_H_ */
