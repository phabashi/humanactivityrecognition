#!/bin/bash

#setting

#Sample Generating file list
#ls ./test-data/IP/FAST/SIFT/scale_1/Normalized/Length_9/features/ | grep -e "Left" | grep -f ./lists/ActivitySet_10.txt | grep -f ./lists/Actor_set3.txt | grep -f ./lists/Instances_5.txt

source ~/HAR/newScripts/KTH/default.bash

# inputFileList=`ls ./clips/ | grep -e "Left" | grep -f ./lists/ActivitySet_10.txt | grep -f ./lists/Actor_set3.txt | grep -f ./lists/Instances_5.txt > ./lists/VideoFileList_Actor03_Activity_10_I5.txt` 
inputFileList="./lists/KTH/FeatureFileList_Actor25_Activity_10_I5.txt"
SVMLearningParameters="-s 0 -t 2 -c 1500 -q"

MicromovementFeaturesFolder=$featureFolderName/features/

leftVideoFileNames=(`cat $inputFileList`)
Actors=`cat $inputFileList | cut -f 1 -d "_" | uniq `

folderPrefix="./$featureFolderName/learning/n-$numberOfWords-words" # find the file name part of $Activities

csvDataFolder="./$folderPrefix/data/csv"
SVMDataFolder="./$folderPrefix/data/svm"
SVMModelFolder="./$folderPrefix/models/svm"
KMeansModelFolder="./$folderPrefix/models/kmeans"
resultFolder="./$folderPrefix/results/"

ActivityList="./lists/KTH/ActivitySet.txt"

mkdir -p $csvDataFolder
mkdir -p $SVMDataFolder
mkdir -p $SVMModelFolder
mkdir -p $KMeansModelFolder
mkdir -p $resultFolder

i=0

for Actor in ${Actors[@]}; do
	i=$(( i+1 ))
	echo "Do fold $i for actor $Actor"
	trainingSet=`cat $inputFileList | grep -v $Actor`
	testingSet=`cat $inputFileList | grep $Actor`

	modelFileName="$KMeansModelFolder/Fold-$Actor.yml"
	csvTrainFile="$csvDataFolder/Fold-$Actor.csv.train"
	csvTestFile="$csvDataFolder/Fold-$Actor.csv.test"
	svmTrainFile="$SVMDataFolder/Fold-$Actor.svm.train"
	svmTestFile="$SVMDataFolder/Fold-$Actor.svm.test"
	svmModelFileName="$SVMModelFolder/Fold-$Actor.svm.model"
	svmOutputFile="$SVMDataFolder/Fold-$Actor.svm.output"

	#make a KMeans model for this fold
	if [ ! -f $modelFileName ]; then
		echo "Make KMeans Model for fold $Actor"
		echo $trainingSet | tr ' ' '\n'  | ./bin/Merge -f $MicromovementFeaturesFolder -s 100 | tr " " "," | ./bin/KMeansClustering -c $numberOfWords -o $modelFileName as $settingFile #-online 

		if [ $? != 0 ] ; then
			exit 1
		fi
	else
		echo "The moded file Exist! Skipping the clustering part!"
		echo 
		echo
		echo "Exists: $modelFileName"
	fi

	#Count The Words for this fold
	if [ ! -f $csvTrainFile ]; then
		echo "Count the words for trainig in fold $i"
		echo $trainingSet | tr ' ' '\n' | ./bin/CountTheWords -m $modelFileName -f $MicromovementFeaturesFolder -p 1 -c $csvTrainFile -lab $ActivityList -d " " &
		if [ $? != 0 ] ; then
			exit 2
		fi 
	fi

	if [ ! -f $csvTestFile ]; then
		echo "Count the words for testing in fold $i"
		echo $testingSet  | tr ' ' '\n' | ./bin/CountTheWords -m $modelFileName -f $MicromovementFeaturesFolder -p 1 -c $csvTestFile -lab $ActivityList -d " " &
		if [ $? != 0 ] ; then
			exit 3 
		fi 
	fi

	wait #wait for the CountTheWords to return
	#Prepare data for libSVM
	if [ ! -f $svmTrainFile ] || [ ! -f $svmTestFile ]; then
		echo "Preparing data for learning/testing with libSVM"
		./bin/csv2svm -i $csvTrainFile $csvTestFile -o $svmTrainFile $svmTestFile -n -lab $ActivityList
		if [ $? != 0 ] ; then
			echo "Erro Executing the following Command: "
			echo
			echo "./bin/csv2svm -i $csvTrainFile $csvTestFile -o $svmTrainFile $svmTestFile -n -lab $ActivityList"
			exit 4
		fi 
	fi

	#Make a svm-model for the current data
	#if [ ! -f $svmModelFileName ]; then
		echo "Making SVM Model for the fold $i"
		./svmbin/svm-train $SVMLearningParameters $svmTrainFile $svmModelFileName
		if [ $? != 0 ] ; then
			exit 5
		fi 
	#fi

	#Measure the accuracy of svm-model for calssification
	#if [ ! -f $svmOutputFile ]; then
		echo "Measuring the accuracy for fold $i"
		./svmbin/svm-predict $svmTestFile $svmModelFileName $svmOutputFile
		if [ $? != 0 ] ; then
			exit 6
		fi 
	#fi 

done

echo "Making Report: "

expectedResult="$SVMDataFolder/all.expected"
outputResult="$SVMDataFolder/all.actual"

rm -f $expectedResult
rm -f $outputResult
j=0

makeReportForFold()
{
	i=$1
  j=$(( j + 1 ))

	expected="$resultFolder/Fold-$i.expected"
	aquired="$resultFolder/Fold-$i-out"
	svmTestFile="$SVMDataFolder/Fold-$i.svm.test"
	svmOutputFile="$SVMDataFolder/Fold-$i.svm.output"

	#echo "Making report for fold $i :"
	cat $svmTestFile | cut -d " " -f 1 > $expected
	cp $svmOutputFile $aquired
	#./bin/MakeReport -e $expected -o $aquired -lab $ActivityList
	cat $expected >> $expectedResult
	cat $aquired >> $outputResult
}

for i in ${Actors[@]}; do
	makeReportForFold "$i" ; 
done

./bin/MakeReport -e $expectedResult -o $outputResult -lab $ActivityList
echo "$folderPrefix"
