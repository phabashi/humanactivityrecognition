/*
 * MainCountTheWords.cpp
 *
 *  Created on: May 1, 2015
 *      Author: Pejman
 *      Copyright (c) 2015 Pejman. All rights reserved.
 */

#include "../util/util.h"
#include <unordered_map>
#include <opencv2/opencv.hpp>
#include <fstream>
#include <exception>
#include <algorithm> //Contain replace

#include "../Exceptions/ArgumentParseException.h"
#include "../Exceptions/CommaSeparatedReadingException.h"
#include "../util/CommaSeparatedReader.h"
#include "../Clustering/ClusterModel.h"
#include "../util/UniqueSequenceNumberGenerator.h"
#include "../Logger/Logger.h"

using namespace cv;
using namespace std;
using namespace HAR;
using namespace HAR::Exceptions;
using namespace Pejman::Logger;

class Arguments {
public:
	Arguments() :
			positionOfClassLabel(3) {
	}
	Arguments(int argc, char* argv[]);
	string getTheModelFileName() {
		return theModelFileName;
	}
	string getFolderName() {
		return folderName;
	}
	int getPositionOfClassLabel() {
		return positionOfClassLabel;
	}
	string getCSVOutputFileName() {
		return csvOutputFileName;
	}
	string getSVMOutputFileName() {
		return svmOutputFileName;
	}
	bool getCommaSeparatedOutput() {
		return (svmOutputFileName == "");
	}
	int getFeaturesLow() {
		return featuresLow;
	}
	int getFeaturesHigh() {
		return featuresHigh;
	}
	char getDelimiter() {
		return delimiter;
	}
	string getLabelDefinitionFileName() {
		return labelDefinitionFileName;
	}
	char isNumericalEnabled() {
		return numericalEnabled;
	}

private:
	string theModelFileName;
	string folderName;
	string csvOutputFileName;
	string svmOutputFileName;
	string labelDefinitionFileName;
	int positionOfClassLabel;
	char delimiter = ',';
	int featuresLow = -1;
	int featuresHigh = -1;
	bool numericalEnabled = false;
};

//int getClassID(string classLabel);

int main(int argc, char* argv[]) {
	Logger::getInstance().ChangeLevel(LogLevel::ERROR);
	Arguments args;
	try {
		args = Arguments(argc, argv);
	} catch (ArgumentParseException& exp) {
		cout << exp.what() << endl;
		cout << "Usage: " << argv[0] << " [options]" << endl
				<< "\t-m <theModelFileName>\tThis option is obligatory." << endl
				<< "\t-f <theFolderName>\tSpecifies the input folder." << endl
				<< "\t-p <positionIndex>\tSpecifies zero based index of class name label in the filename (file name parts separated with '_')"
				<< endl
				<< "\t-c <CSV File Name>\tSpecifies the name of output csv file (if provided the file will be written in csv format))."
				<< endl
				<< "\t-s <SVM File Name>\tSpecifies the name of output file in the SVM format (to be used by libSVM)."
				<< endl
				<< "\t-d <delimiter>    \tSpecified the 'delimiter' used to separate cells in the input file, Default is ','"
				<< endl
				<< "\t-sel low-high     \tSelects the features to be used. (E.g: 11-41 -> Trajectory)"
				<< endl
				<< "\t-n                \tIn CSV output file mode, this switch will enable the writing of numerical class names, instead of string."
				<< endl;

		exit(1);
	}

	Mat centers;

	FileStorage fs;
	if (!fs.open(args.getTheModelFileName(), FileStorage::READ)) {
		Logger::Log(LogLevel::FATAL,
				"ERROR: The input model file '%s' does not exists, or we do not have access to open it for reading!",
				args.getTheModelFileName().c_str());
		exit(1);
	}
	//read the centers
	fs["Centers"] >> centers;

	if (centers.rows == 0 || centers.cols == 0) {
		Logger::Log(LogLevel::FATAL,
				"ERROR: The file '%s' does not contain the Model!",
				args.getTheModelFileName().c_str());
		Logger::Log(LogLevel::INFO, "r = %d c = %d", centers.rows,
				centers.cols);
		exit(1);
	}

	//cout << "Centers read successfully. makin a model over them!" << endl;
	ClusterModel model(centers);

	//Initialize SVM and CSV output stream

	ofstream csvFileStream(args.getCSVOutputFileName());
	ofstream svmFileStream(args.getSVMOutputFileName());

	if (args.getCommaSeparatedOutput()) {
		//TODO Make the header (To be used by WEKA or a human reader)
		//throw std::runtime_error("Not implemented functionality");
		Logger::Log(LogLevel::WARNING,
				"WARNING: Not Implemented Feature! Comma Separated files will generate without a human readable header.");
	}

	CommaSeparatedReader::setDelimiter(args.getDelimiter());
	UniqueSequenceNumberGenerator* sequenceGenerator = NULL;
	if ((args.isNumericalEnabled() && args.getCSVOutputFileName() != "")
			|| args.getSVMOutputFileName() != "") {
		sequenceGenerator = new UniqueSequenceNumberGenerator(
				args.getLabelDefinitionFileName());
	}
	//int i=0;
	while (cin) {
		//cout << "Readin file "<< ++i << endl;
		string fileName;
		cin >> fileName;
		if (!cin)
			break;
		Logger::Log(LogLevel::VERBOSE, "Reading the Content of File '%s'",
				fileName.c_str());
		Mat temp;
		try {
			temp = CommaSeparatedReader::ReadFloatMatrix(
					args.getFolderName() + fileName);
		} catch (CommaSeparatedReadingException& err) {
			Logger::Log(LogLevel::ERROR, "Error reading from file '%s'",
					(args.getFolderName() + fileName).c_str());
			Logger::Log(LogLevel::EXCEPTION, err.what());
		} catch (exception& e) {
			Logger::Log(LogLevel::ERROR, "Totally another type of error when reading from comma separated file!!!");
		}

		Logger::Log(LogLevel::INFO, "The matrix read successfully!");

		Mat selectedColumns;
		//TODO select the rows if it is appropriate
		try {
			selectedColumns =
					(args.getFeaturesLow() == -1) ?
							temp :
							temp(
									cv::Rect(args.getFeaturesLow() - 1, 0,
											args.getFeaturesHigh()
													- args.getFeaturesLow() + 1,
											temp.rows));
		} catch (exception& e) {
			Logger::Log(LogLevel::ERROR, "Can not select the requested rows: " );
			cv::Rect r = cv::Rect(args.getFeaturesLow() - 1, 0,
					args.getFeaturesHigh() - args.getFeaturesLow() + 1,
					temp.rows);
			Logger::Log(LogLevel::INFO, "More information: \n temp.size() = (%d, %d) \n r = [x=%d, y=%d, W=%d, H=%d]"
					, temp.size().width, temp.size().height, r.x, r.y, r.width, r.height);
			Logger::Log(LogLevel::EXCEPTION, e.what());
		}
		// Minus one because it should turn to zero based !

		//		for(int r=0; r<temp.rows; r++)
		//			cout << model.getCluster(temp.row(r)) << endl;

		//cout << "Count the words for the matrix! " << endl;
		Mat ctw = model.CountTheWords(selectedColumns);
		vector<string> tokens = split(fileName, '_');

		//std::replace(fileName.begin(), fileName.end(), '_', ',');
		if (args.getCommaSeparatedOutput()) {
			if (args.getCSVOutputFileName() == "") {
				cout << tokens[args.getPositionOfClassLabel()] << ", ";
				CommaSeparatedReader::WriteMatrix<int>(ctw);
			} else {
				// CSV file
				if (args.isNumericalEnabled()) {
					csvFileStream
							<< sequenceGenerator->getLabelIndex(
									tokens[args.getPositionOfClassLabel()])
							<< ", ";
				} else {
					csvFileStream << tokens[args.getPositionOfClassLabel()]
							<< ", ";
				}
				CommaSeparatedReader::WriteMatrix<int>(csvFileStream, ctw);
			}
		} else {
			//SVM file output
			int classId = sequenceGenerator->getLabelIndex(
					tokens[args.getPositionOfClassLabel()]);
			svmFileStream << classId << " ";
			CommaSeparatedReader::WriteMatrixWithoutClassLabelInSVMFormat<int>(
					svmFileStream, ctw);
		}
	}

	if (sequenceGenerator != NULL) {
		delete sequenceGenerator;
	}
	return 0;
}

Arguments::Arguments(int argc, char* argv[]) :
		theModelFileName("") {
	positionOfClassLabel = -1;
	positionOfClassLabel = 0; //it is not the most probable but it prevent illegal access to memory
	csvOutputFileName = "";
	svmOutputFileName = "";
	for (int i = 1; i < argc; i++) {
		if (strcmp(argv[i], "-f") == 0) {
			if (i < argc - 1)
				folderName = argv[++i];
			else
				throw ArgumentParseException(
						string(argv[i])
								+ ": Model folder name is missing after -f option.");
		} else if (strcmp(argv[i], "-m") == 0) {
			if (i < argc - 1)
				theModelFileName = argv[++i];
			else
				throw ArgumentParseException(
						string(argv[i]) + ": ModelFileName is missing");
		} else if (strcmp(argv[i], "-p") == 0) {
			if (i < argc - 1)
				positionOfClassLabel = atoi(argv[++i]);
			else
				throw ArgumentParseException(
						string(argv[i])
								+ ": the PositionOfClassLabel is missing");
		} else if (strcmp(argv[i], "-v") == 0) {
			Logger::getInstance().ChangeLevel(LogLevel::VERBOSE);
		} else if (strcmp(argv[i], "-c") == 0) {
			if (i < argc - 1)
				csvOutputFileName = string(argv[++i]);
			else
				throw ArgumentParseException(
						string(argv[i])
								+ ": the CSV output file name is missing");
		} else if (strcmp(argv[i], "-s") == 0) {
			if (i < argc - 1)
				svmOutputFileName = string(argv[++i]);
			else
				throw ArgumentParseException(
						string(argv[i])
								+ ": the SVM output file name is missing");
		} else if (strcmp(argv[i], "-d") == 0) {
			if (i < argc - 1) {
				if (strlen(argv[i + 1]) == 1) {
					delimiter = argv[++i][0];
				} else if (strcmp(argv[i + 1], "tab") == 0) {
					delimiter = '\t';
					i++;
				} else if (strcmp(argv[i + 1], "newLine") == 0) {
					delimiter = '\n';
					i++;
				} else if (strcmp(argv[i + 1], "CarriageReturn") == 0) {
					delimiter = '\r';
					i++;
				} else {
					stringstream temp;
					temp << strlen(argv[i + 1]);
					throw ArgumentParseException(
							string(argv[i]) + " " + string(argv[i + 1])
									+ ": Unknown delimiter string\""
									+ argv[i + 1] + "\"! + with len = "
									+ temp.str());
				}
			} else
				throw ArgumentParseException(
						string(argv[i])
								+ ": the SVM output file name is missing");
		} else if (strcmp(argv[i], "-sel") == 0) {
			if (i < argc - 1) {
				string temp = argv[++i];
				vector<string> k = split(temp, '-');
				featuresLow = atoi(k[0].c_str());
				featuresHigh = atoi(k[1].c_str());
			} else
				throw ArgumentParseException(
						string(argv[i])
								+ ": the SVM output file name is missing");
		} else if (strcmp(argv[i], "-lab") == 0) {
			if (i < argc - 1)
				labelDefinitionFileName = string(argv[++i]);
			else
				throw ArgumentParseException(
						"switch " + string(argv[i])
								+ ": the label file name is missing");
		} else if (strcmp(argv[i], "-n") == 0) {
			numericalEnabled = true;
		} else { //Unknown Argument
			throw ArgumentParseException(
					string("Unknown Argument! '") + argv[i] + "'");
		}
	}
	if (positionOfClassLabel == -1)
		throw ArgumentParseException(
				"thePositionOfClassLabel is not specified");
	if (theModelFileName == "")
		throw ArgumentParseException("the modelFileName is not specified!");
	if (svmOutputFileName != "" and csvOutputFileName != "")
		throw ArgumentParseException(
				"You may use either svm or csv file name, not both!");
	if ((numericalEnabled && (csvOutputFileName != ""))
			|| (svmOutputFileName != "")) {
		// then label file name should be set
		if (labelDefinitionFileName == "")
			throw ArgumentParseException(
					"You should provide label Definition File name when you are trying to convert string labels to numberc ones.");
	}

}

//int getClassID(string classLabel){
//	static unordered_map<string, int> labelMapper;
//	static int counter = 0;
//	auto labelID = labelMapper.find(classLabel);
//	if( labelID == labelMapper.end()){
//		//New label have been found
//		labelMapper[classLabel] = ++counter;
//		return labelMapper[classLabel];
//	} else {
//		return labelID->second;
//	}
//}
