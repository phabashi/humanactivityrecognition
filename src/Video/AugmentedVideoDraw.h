/*
 * AugmentedVideoDraw.h
 *
 *  Created on: May 8, 2016
 *      Author: Pejman
 *      Copyright (c) 2016 Pejman. All rights reserved.
 */

#ifndef SRC_VIDEO_AUGMENTEDVIDEODRAW_H_
#define SRC_VIDEO_AUGMENTEDVIDEODRAW_H_

#include <opencv2/opencv.hpp>
#include <unordered_set>
#include "Trajectory.h"

namespace HAR {
namespace Video {

class AugmentedVideoDraw {
public:
	AugmentedVideoDraw(cv::Mat theFrame);

	void DrawRegionsOfInteresOnFrame(const std::vector<cv::Rect_<float> >& rois);
	void DrawInterestPointsOnFrame(const std::vector<cv::Point_<float> >& interesPoints);
	std::unordered_set<Trajectory> DrawTrajectoriesOnFrame(
			const std::pair<std::unordered_multimap<int, Trajectory>::const_iterator,
			std::unordered_multimap<int, Trajectory>::const_iterator>& range,
			int frameNumber);

	virtual ~AugmentedVideoDraw();

	cv::Mat getDisplayFrame() const { return displayFrame; }
private:
	cv::Mat displayFrame;
};

} /* namespace Video */
} /* namespace HAR */

#endif /* SRC_VIDEO_AUGMENTEDVIDEODRAW_H_ */
