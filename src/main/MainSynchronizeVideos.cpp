/*
 * MainSynchronizeVideos.cpp
 *
 *  Created on: May 1, 2015
 *      Author: Pejman
 *      Copyright (c) 2015 Pejman. All rights reserved.
 */

#include "../util/util.h"
#include "../Video/Display.h"
#include "../Video/StereoAugmentedVideo.h"

using namespace std;
using namespace cv;
using namespace HAR;

int main(int argc, char** argv)
{
    string lvFileName, rvFileName;
    double scale;

    if(argc == 4)
    {
        lvFileName = argv[1];
        rvFileName = argv[2];
        scale = stringTo<double>(argv[3]);
        cout << "Scale set to: " << scale << endl;
    }
    else if (argc == 3)
    {
        lvFileName = argv[1];
        rvFileName = argv[2];
        scale = 1.0;
    }
    else
    {
        cout << "Usage: SynchronizeVideos LeftVideo rightVideo [scale]" << endl;

        return -1;
    }

    cout << "Usage information:" << endl
    		 << "Space            \tStop/start video playback" << endl
		 << "Up/Down Arrow    \tChange left/right synchronizatoin" << endl
		 << "Left/Right Arrow \tFast forward/backward" << endl
    		 << "Any other key    \tGo to next frame" << endl;

    bool pause = false;
    bool endOfVideo = false;
    Mat left, right;
    int synchronizationParameter = 0, frameNumber = 0;
    Video::StereoAugmentedVideo svideo(lvFileName, rvFileName, synchronizationParameter);

    svideo.setFrameSynchronization(synchronizationParameter, 0);
    while (! endOfVideo) {
        endOfVideo = ! svideo.getNextFrame(left, right);
        if( endOfVideo ) break;
        frameNumber++;

        Video::StereoFrame temp(left, right);

        temp.Scale(scale);

        Video::Display::Show(temp);
        int key = Video::Display::waitKey(pause?0:30);
        switch (key) {
            case 27:
                endOfVideo = true;
                break;
            case 32:
            case 'p':
            case 'P':
                pause = ! pause;
                break;
            case 63232://  ^
                synchronizationParameter++;
                svideo.setFrameSynchronization(synchronizationParameter, frameNumber-1);
                cout << "synchronizationParameter = " << synchronizationParameter << endl;
                break;
            case 63233:// \/
                synchronizationParameter--;
                svideo.setFrameSynchronization(synchronizationParameter, frameNumber-1);
                cout << "synchronizationParameter = " << synchronizationParameter << endl;
                break;
            case 63234:// <-
                frameNumber -= 300;
                if(frameNumber < 0) frameNumber =0;
                svideo.setFrameSynchronization(synchronizationParameter,frameNumber);
                break;
            case 63235:// ->
                frameNumber += 300;
                svideo.setFrameSynchronization(synchronizationParameter,frameNumber);
                break;
            default:
                if(key!= -1)
                    cout << "code = " << key << endl;
                break;
        }
    }
    return 0;
}
