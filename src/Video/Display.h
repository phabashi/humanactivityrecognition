/*
 * Display.h
 *
 *  Created on: Jan 7, 2015
 *      Author: Pejman
 *      Copyright (c) 2015 Pejman. All rights reserved.
 */

#ifndef DISPLAY_H_
#define DISPLAY_H_

#include <opencv2/opencv.hpp>
#include "DisplayableVideo.h"
#include "Frame/FrameFeaturePoints.h"
#include "../Feature/VideoFeaturePoints.h"
#include "Frame/FrameMatches.h"
#include "AugmentedVideoBuffer.h"
#include "Descriptor/MyVideoDescrptor.h"
#include "StereoAugmentedVideo.h"
#include "StereoFrame/StereoFrame.h"
#include "Video.h"

namespace HAR {
namespace Video{
class Display {
public:
    static void Show(const cv::Mat image);
    static void Show(const cv::Mat image, double scale);
    static void Show(const cv::Mat image, const std::vector<cv::Point2f>& ipLocations);
    static void Show(std::string displayWindowName, const cv::Mat image, const std::vector<cv::Point2f>& ipLocations);
    static void Show(const std::string& windowName, const cv::Mat& image);
    static void Show(const StereoFrame image, bool dispayFeaturePoints = false);
    static void Show(const std::string& windowName, const StereoFrame image, bool dispayFeaturePoints = false);
	static void Show(const cv::Mat originalImage,const FrameFeaturePoints* featuePoint);
	static void Show(Video originalVideo, const Features::VideoFeaturePoints* featuePoint);
	static void Show(Video& video);
	//static void Show(StereoAugmentedVideo video, std::string featureType = "");
	static void Show(DisplayableVideo& left, DisplayableVideo& right);
	static void Show(cv::Mat imageL, cv::Mat imageR);
	static void Show(const cv::Mat left, const cv::Mat right, const std::vector<cv::KeyPoint>& keypoints1, const std::vector<cv::KeyPoint>& keypoints2, const std::vector<cv::DMatch>& matches);
    static void Show(const std::string& windowName, const cv::Mat left, const cv::Mat right, const FrameFeaturePoints& keypoints1, const FrameFeaturePoints& keypoints2, const StereoFrameMatches& matches);

    static void Show(Video tehVideo, const MyVideoDescrptor& descriptor);

    //static void Show(AugmentedVideoBuffer& video);
	static int waitKey(int delay = 0);
	static void printOutData(const FrameFeaturePoints* featuePoint);

	static bool Check(const cv::Mat image);
	static cv::Mat Combine(cv::Mat left, cv::Mat right);

	static void Show(DisplayableVideo& displayableVideo);
	static void DrawEpipolarLines(cv::Mat input, const cv::Mat fundamentalMat);
	static void DrawLineOnImage(cv::Mat& inputImage, double a, double b, double c);
private:
	Display();
	virtual ~Display();

private:
	static void DrawMicromovementOnImage(cv::Mat image, const MyDescriptor& micromovement);
};

} /* namespace Video */
} /* namespace HAR */

#endif /* DISPLAY_H_ */
