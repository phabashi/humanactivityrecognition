/*
 * MainDisplayFeatures.cpp
 *
 *  Created on: May 1, 2015
 *      Author: Pejman
 *      Copyright (c) 2015 Pejman. All rights reserved.
 */



//#include "util.h"
#include <iostream>

#include "../Video/Display.h"

using namespace std;
using namespace HAR;

int main(int argc, char** argv)
{
    string inputVideoFileName, inputDescriptorFileName;
    if (argc == 3)
    {
        inputVideoFileName = argv[1];
        inputDescriptorFileName = argv[2];
    }
    else
    {
        cout << "Usage: DisplayDescriptors inputVideo inputFeatures" <<endl;
        return -1;
    }

    Video::Video temp(inputVideoFileName);
    temp.setGrayscale(false);

    MyVideoDescrptor descriptor;

    descriptor.Load(inputDescriptorFileName);
    HAR::Video::Display::Show(temp, descriptor);
    return 0;
}
