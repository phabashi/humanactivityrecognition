/*
 * AugmentedVideoBuffer.h
 *
 *  Created on: Feb 4, 2016
 *      Author: Pejman
 *      Copyright (c) 2016 Pejman. All rights reserved.
 */

#ifndef AUGMENTEDVIDEOBUFFER_H_
#define AUGMENTEDVIDEOBUFFER_H_

#include <unordered_map>

#include "Frame/FrameInformation.h"
#include "Trajectory.h"
#include "VideoBuffer.h"
#include "DisplayableVideo.h"
#include "AugmentedVideoDraw.h"
#include "../Logger/Logger.h"
#include "TrajectoryEncoding/IEncoding.h"

//using namespace Pejman::Logger;

namespace HAR {
namespace Video {

class AugmentedVideoBuffer: public VideoBuffer, public DisplayableVideo {
public:
	AugmentedVideoBuffer(std::string inputFile, int bufferSize = -1,
			bool grayscale = false);

	FrameInformation& getFrameInformation(int frameNumber);

	std::string SerializeInfo() const;
	void DeserializeInfo(std::string str);

	FrameInformation& getCurrentFrameInformation() {
		return info[getCurrentFrameNumber()];
	}
	FrameInformation& getPreviousFrameInformation() {
		return info[getPreviousFrameNumber()];
	}
	std::vector<cv::Mat> getCurrentFrameRegionsOfInterest();

	virtual ~AugmentedVideoBuffer();

public:
	void ClearTrajectories() {
		trajectories.clear();
		frameTrajectories.clear();
	}

	void AddTrajectory(const Trajectory& track) {
		trajectories.insert(track);
		std::unordered_multimap<int, Trajectory>::value_type pair(
				track.getStartFrame(), track);
		frameTrajectories.insert(pair);
	}

	//Remove low energy Trajectories and return the number of deleted Trajectories
	int RemoveLowEnergyTrajectories(const double minEnergy);

	const std::pair<std::unordered_multimap<int, Trajectory>::const_iterator,
			std::unordered_multimap<int, Trajectory>::const_iterator> getCurrentFrameTrajectoriesRange() const {
		return frameTrajectories.equal_range(getCurrentFrameNumber());
	}

	const std::unordered_set<Trajectory> getAllTrajectories() const {
		return trajectories;
	}

	const std::pair<std::unordered_multimap<int, Trajectory>::const_iterator,
	std::unordered_multimap<int, Trajectory>::const_iterator> getFrameTrajectories(int frameNumber) const {
		return frameTrajectories.equal_range(frameNumber);
	}

//	std::string SerializeTrajectories() const;
//	std::string SerializeTrajectoriesDeferentiation(bool normalize) const;
	std::string SerializeTrajectories(TrajectoryEncoding::TEncoding& encodingAlgorithm) const;

	virtual cv::Mat getDisplayableFrame(int frameNumber);

	virtual int getFramePerSecond() {
		int fps = ((Video*) this)->getFramePerSecond();
		if (fps <= 0 ) fps = 25;
		return fps;
	}

private:
	std::unordered_map<int, FrameInformation> info; // map frame number to the information related to that frame
	std::unordered_set<Trajectory> trajectories; //Set of all motion Trajectories
	std::unordered_multimap<int, Trajectory> frameTrajectories; // map frame number to the trajectories starting from that frame
};

} /* namespace Video */
} /* namespace HAR */

#endif /* AUGMENTEDVIDEOBUFFER_H_ */
