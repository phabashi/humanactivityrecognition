/*
 * StereoFrame.h
 *
 *  Created on: Jan 28, 2015
 *      Author: Pejman
 *      Copyright (c) 2015 Pejman. All rights reserved.
 */

#ifndef STEREOFRAME_H_
#define STEREOFRAME_H_

#include <opencv2/core/core.hpp>

#include "../Frame/FrameFeatureDescriptors.h"
#include "../Frame/FrameFeaturePoints.h"
#include "../Frame/FrameMatches.h"
#include "../../OpticalFlow/OpticalFlowInformation.h"
#include "../../OpticalFlow/NewOpticalFlowInformation.h"
#include "../StereoFrame/StereoFrameMatches.h"

namespace HAR {
namespace Video {

class StereoFrame {
public:
    StereoFrame() { min = max = 0;}
	StereoFrame(cv::Mat& left, cv::Mat& right);
    
	void FindFeaturePoints(std::string algorithm);
    void UpdateFeaturePoints(const OpticalFlowInformation& leftFlowInfo, const OpticalFlowInformation& rightFlowInfo, const StereoFrameMatches& oldMatches);
	//void ExtractDescriptors(std::string algorithm, std::vector<cv::Point2f> interestPoints);
	void ExtractDescriptors(std::string algorithm);
	void MatchDescriptors(std::string algorithm);
    void DoubleMatchDescriptors(std::string algorithm);

    void RemoveHomogenousAreaKeypoints();
    
	const StereoFrameMatches& KeepBestMatches(float ratio = 0.8); // As in Lowe's paper; can be tuned
    const StereoFrameMatches& RemoveOutliersBasedOnTheirDistance(cv::Vec2f mean, cv::Vec2f var, double coeff);

	const cv::Mat& getLeft() const { return left; }
	const cv::Mat& getRight() const { return right; }

	void setLeftFeaturePoints(const FrameFeaturePoints& lfp) { leftFeaturePoints = lfp; }
	void setRightFeaturePoints(const FrameFeaturePoints& rfp) { rightFeaturePoints = rfp; }

	const FrameFeaturePoints& getLeftFeaturePoints() const { return leftFeaturePoints; }
	const FrameFeaturePoints& getRightFeaturePoints() const { return rightFeaturePoints; }

	const FrameFeatureDescriptors& getLeftFeatureDescriptor() const { return leftFeatureDescriptors; }
	const FrameFeatureDescriptors& getRightFeatureDescriptor() const { return rightFeatureDescriptors; }

	const FrameMatches& getMatches() const { return matches; }
	const StereoFrameMatches& getStereoMatches() const { return goodMatches; }

	void setStereoMatches(const StereoFrameMatches& newGoodMatches) { goodMatches = newGoodMatches; }

	const std::vector<double>& getDisparity() { return disparity; }
	cv::Vec2f  calculateAllMatchesMean() const;
	cv::Vec2f  calculateAllMatchesVar(cv::Vec2f ) const;

	const cv::Mat& getDisparityApproximationImage();
	void calculateDisparityApproximationImage();
	void calculateDisparity();

	void CalculateDisparityOF();
	void calculateDisparityApproximationImageOF();
	void Scale(double scale);

	virtual ~StereoFrame();

private:
	cv::Mat left;
	cv::Mat right;

	FrameFeaturePoints leftFeaturePoints;
	FrameFeaturePoints rightFeaturePoints;

	FrameFeatureDescriptors leftFeatureDescriptors;
	FrameFeatureDescriptors rightFeatureDescriptors;

	FrameMatches matches;
	StereoFrameMatches goodMatches;

	//Disparity Definitions
	cv::Mat disparityImage;
	std::vector<double> disparity;
    std::unordered_map<int, double> disparityOF;

    int max;
	int min;

	//TODO Remove the folowing variable
	NewOpticalFlowInformation info;
};

} /* namespace Video */
} /* namespace HAR */

#endif /* STEREOFRAME_H_ */
