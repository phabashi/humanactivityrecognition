/*
 * OptocalFlow.cpp
 *
 *  Created on: Feb 8, 2015
 *      Author: Pejman
 *      Copyright (c) 2016 Pejman. All rights reserved.
 */

#include <vector>
#include <unordered_set>

#include "OpticalFlow.h"
#include "../Clustering/DistanceCalculator.h"
#include "../Video/Display.h"
#include "../Logger/Logger.h"
#include "../Pejman/GlobalSetting.h"

using namespace Pejman::Logger;
using namespace Pejman::Application;

namespace HAR {
namespace Video {
namespace OpticalFlow {

OpticalFlow::OpticalFlow(cv::Mat statringImage) {
	preImage = statringImage;
}

void OpticalFlow::Update(cv::Mat nextImage, OpticalFlowInformation& info) {
	if (info.getPreviousPoints().empty()) {
		return;
	}

	cv::Size winSize(31, 31);
	cv::TermCriteria termcrit(cv::TermCriteria::COUNT | cv::TermCriteria::EPS,
			20, 0.03);

	std::vector<cv::Point2f> nextPoints;
	std::vector<uchar> status;
	std::vector<float> error;

	cv::calcOpticalFlowPyrLK(preImage, nextImage, info.getPreviousPoints(),
			nextPoints, status, error, winSize, 3, termcrit, 0, 0.001);

	info.ReplaceOldPointsWithNewOnes(nextPoints, status, error);
	preImage = nextImage;
}

void OpticalFlow::Update(cv::Mat newImage, NewOpticalFlowInformation& info) {
	std::vector<cv::Point2f> previousPoints = info.getPreviousPoints();
	if (previousPoints.empty())
		return;
	cv::Size winSize(31, 31); //TODO Optimize
	cv::TermCriteria termcrit(cv::TermCriteria::COUNT | cv::TermCriteria::EPS,
			20, 0.03);

	std::vector<cv::Point2f> nextPoints;
	std::vector<uchar> status;
	std::vector<float> error;
	int level = 5; //TODO Optimize

	cv::calcOpticalFlowPyrLK(preImage, newImage, previousPoints, nextPoints,
			status, error, winSize, level, termcrit, 0, 0.001);

	info.UpdatePoints(nextPoints, status, error);
	preImage = newImage;
}

void OpticalFlow::ExtractOpticalFlowOfInterestPointsFarnBack(
		AugmentedVideoBuffer& video, int length) {
	video.ClearTrajectories();
	//TODO Parameters to be optimized!
//	int length = GlobalSetting::GetOptionAs<int>("OpticalFlowFarnBack",
//			"length", 15);
	int windowSize = GlobalSetting::GetOptionAs<int>("OpticalFlowFarnBack",
			"windowSize", 5);
	int level = GlobalSetting::GetOptionAs<int>("OpticalFlowFarnBack", "level",
			5);
	float scale = GlobalSetting::GetOptionAs<float>("OpticalFlowFarnBack",
			"scale", 0.5);
	int iteration = GlobalSetting::GetOptionAs<int>("OpticalFlowFarnBack",
			"iteration", 5);
	int poly_n = GlobalSetting::GetOptionAs<int>("OpticalFlowFarnBack",
			"poly_n", 5);
	double poly_sigma = GlobalSetting::GetOptionAs<double>(
			"OpticalFlowFarnBack", "poly_sigma", 1.2);
	int flags = GlobalSetting::GetOptionAs<int>("OpticalFlowFarnBack", "flags",
			0);
//	float jumptreshold = GlobalSetting::GetOptionAs<float>(
//			"OpticalFlowFarnBack", "jumptreshold", 0); // This parameter optimization happen in the first of the main loop.
	cv::TermCriteria termcrit(cv::TermCriteria::COUNT | cv::TermCriteria::EPS,
			GlobalSetting::GetOptionAs<int>("OpticalFlowFarnBack",
					"TermCritCount", 20),
			GlobalSetting::GetOptionAs<float>("OpticalFlowFarnBack",
					"TermCritEpsilon", 0.003));
	//TODO ---------------------------
	cv::Mat preImage;
	preImage = video.getFrame(0);
	cv::Size frameSize(preImage.cols, preImage.rows);

	std::vector<Trajectory> activeTracks; //Contains ongoing tracks
	std::vector<Trajectory> finishedTracks;

	for (cv::Mat frame = video.getFrame(1); frame.data;
			frame = video.getNextFrame()) {
//		if (jumptreshold < 1) { // we have not calculated the threshold yet!
//			//Optimize! Now it is fixed to 800 for 640x400 frame size
//			float dx = (frame.cols / 32.0);
//			float dy = (frame.rows / 20.0);
//			jumptreshold = dx * dx + +dy * dy; // Example 20x20  for 640 and 400 => jump (10*10 + 10*10) = 200
//		}
		Logger::Log(LogLevel::INFO, "Extracting Frame %d ",
				video.getCurrentFrameNumber());
//				<< std::endl;
//		const std::vector<cv::Point_<float> >& previousFramePointsOfInteres = video.getPreviousFrameInformation().getPointsOfInterest();
		Logger::Log(LogLevel::VERBOSE,
				"Try accessing this frame interest points.");
		const std::vector<cv::Point_<float> >& thisFramePointsOfInteres =
				video.getCurrentFrameInformation().getPointsOfInterest();

		Logger::Log(LogLevel::VERBOSE,
				"Creating new trajectories base on this frame feature points.");
		//Add new Tracking points for tracking
		for (const cv::Point2f p : thisFramePointsOfInteres) {
			activeTracks.push_back(
					Trajectory(video.getCurrentFrameNumber(), p)); // Add new point to tracking
		}

		if (activeTracks.empty()) { //If there is no point to find next points
			Logger::Log(LogLevel::WARNING,
					"Empty tracking set! skipping frame\n");
			continue;
		}
		cv::Mat flow;

		Logger::Log(LogLevel::VERBOSE,
				"Calculating farnback optical flow field");
		cv::calcOpticalFlowFarneback(preImage, frame, flow, scale, level,
				windowSize, iteration, poly_n, poly_sigma, flags);

		std::vector<cv::Mat> flows;
		cv::split(flow, flows);

		Logger::Log(LogLevel::VERBOSE,
				"Adding the result of farnback to the end of trajectories.");
		std::vector<Trajectory> notFinished;
		for (unsigned int i = 0; i < activeTracks.size(); i++) {
			Trajectory& t = activeTracks[i];
			const cv::Point2f& point = t.getLastPoint();
			cv::Mat dX = flows[0];
			cv::Mat dY = flows[1];
			float dx = dX.at<float>(point);
			float dy = dY.at<float>(point);
			cv::Point2f thePoint(point.x + dx, point.y + dy);
			if ( //((dx * dx + dy * dy) < jumptreshold)
			thePoint.x >= 0 && thePoint.x < frame.cols && thePoint.y >= 0
					&& thePoint.y < frame.rows) {
				t.Add(thePoint);
				if (t.length() == length) // if we had reached the desired length
					video.AddTrajectory(t);
				else
					notFinished.push_back(t);
			}
		}

		activeTracks = notFinished;
	}
}

void OpticalFlow::ExtractOpticalFlowOfInteresPointsLK(
		AugmentedVideoBuffer& video, int length) {
	video.ClearTrajectories();
	//TODO Parameters to be optimized!
	cv::Size winSize(
			GlobalSetting::GetOptionAs<int>("OpticalFlowLK", "WindowSizeX", 15),
			GlobalSetting::GetOptionAs<int>("OpticalFlowLK", "WindowSizeY", 15));

	int level = 5;
	cv::TermCriteria termcrit(cv::TermCriteria::COUNT | cv::TermCriteria::EPS,
			GlobalSetting::GetOptionAs<int>("OpticalFlowLK", "TermCritCount",
					20),
			GlobalSetting::GetOptionAs<float>("OpticalFlowLK",
					"TermCritEpsilon", 0.003));
//	float jumptreshold = GlobalSetting::GetOptionAs<float>("OpticalFlowLK",
//			"jumpThreshold", 0); // This parameter optimization happen in the first of the main loop.
	int flags = GlobalSetting::GetOptionAs<int>("OpticalFlowLK", "flags", 0);
	float minimumEigenThreshold = GlobalSetting::GetOptionAs<float>(
			"OpticalFlowLK", "minimumEigenThreshold", 0.001);
	//TODO ---------------------------
	cv::Mat preImage;
	preImage = video.getFrame(0);
	cv::Size frameSize(preImage.cols, preImage.rows);

	std::vector<Trajectory> survivedTracking;  //Contains ongoing tracks
	std::vector<Trajectory> activeTracks;  //Contains ongoing tracks
	std::vector<Trajectory> finishedTracks;
	//	std::unordered_set<Track> finishedTracks;//Contains finished tracks

	//TODO The frame will never be used during the loop, it might be faster to replace actual reading with a simulation
	for (cv::Mat frame = video.getFrame(1); frame.data;
			frame = video.getNextFrame()) {
//		if (treshold < 1) {  // we have not calculated the threshold yet!
//			//Optimize! Now it is fixed to 800 for 640x400 frame size
//			float dx = (frame.cols / 32.0);
//			float dy = (frame.rows / 20.0);
//			jumptreshold = dx * dx + +dy * dy; // Example 20x20  for 640 and 400 => jump (10*10 + 10*10) = 200
//		}

//		const std::vector<cv::Point_<float> >& previousFramePointsOfInteres = video.getPreviousFrameInformation().getPointsOfInterest();
		const std::vector<cv::Point_<float> >& thisFramePointsOfInteres =
				video.getCurrentFrameInformation().getPointsOfInterest();

		std::vector<cv::Point2f> activeTrackingPoints;
		//Add old trackign point for further tracking
		for (uint i = 0; i < activeTracks.size(); i++)
			activeTrackingPoints.push_back(activeTracks[i].getLastPoint());
		//Add new Tracking points for tracking
		for (const cv::Point2f p : thisFramePointsOfInteres) {
			activeTrackingPoints.push_back(p);
			activeTracks.push_back(
					Trajectory(video.getCurrentFrameNumber(), p));
		}
		//Here activeTacks and activeTracking points have a one-to-one relationship, based on index

		if (activeTrackingPoints.empty())
			continue;

		std::vector<cv::Point2f> nextPoints;
		std::vector<uchar> status;
		std::vector<float> error;
		cv::calcOpticalFlowPyrLK(preImage,
				frame, // new Image
				activeTrackingPoints, nextPoints, status, error, winSize, level,
				termcrit, flags, minimumEigenThreshold);

		std::vector<Trajectory> survivedTracking;

		for (uint i = 0; i < nextPoints.size(); i++) {
			if (status[i]) { //if the point correctly tracked
				Trajectory t = activeTracks[i];
//				cv::Point2f old = t.getLastPoint();
//				float dx = old.x - nextPoints[i].x;
//				float dy = old.y - nextPoints[i].y;
//				if ((dx * dx + dy * dy) < jumptreshold)
				{
					t.Add(nextPoints[i]);
					if (t.length() == length) // if we had reached the desired length
						video.AddTrajectory(t);
					else
						survivedTracking.push_back(t);
				}
			}
		}

		activeTracks = survivedTracking;
	}
}

void OpticalFlow::ExtractOpticalFlowOfInteresPointsBasedOnDescriptors(
		AugmentedVideoBuffer& video, int length) {

	video.ClearTrajectories();
	//TODO Parameters to be optimized!
	float thresholdA = GlobalSetting::GetOptionAs<float>("OpticalFlowIP",
			"thresholdA", 20); //pixels
	Logger::Log(VERBOSE, "TresholdA = %2.2f", thresholdA);
	float distanceBetweenBestMatchAndSecondBest = GlobalSetting::GetOptionAs<
			float>("OpticalFlowIP", "distanceBetweenBestMatchAndSecondBest",
			0.96);
	cv::Mat preImage;
	preImage = video.getFrame(0);
	cv::Size frameSize(preImage.cols, preImage.rows);

	std::unordered_set<Trajectory> survivedTracking; //Contains ongoing tracks
	std::unordered_set<Trajectory> activeTracks; //Contains ongoing tracks
	//	std::unordered_set<Track> finishedTracks;//Contains finished tracks

	//TODO The frame will never be used during the loop, it might be faster to replace actual reading with a simulation
	for (cv::Mat frame = video.getFrame(1); frame.data;
			frame = video.getNextFrame()) {
		const std::vector<cv::Point_<float> >& previousFramePointsOfInteres =
				video.getPreviousFrameInformation().getPointsOfInterest();
		const std::vector<cv::Point_<float> >& thisFramePointsOfInteres =
				video.getCurrentFrameInformation().getPointsOfInterest();

		std::unordered_set<cv::Point2f> PointsThatAreCurrentlyTracking;
		//add new location of interest points to the tracking points
		for (Trajectory track : activeTracks)
			PointsThatAreCurrentlyTracking.insert(track.getLastPoint());

		//Look if new feature points are different from tracked ones, create new trackings for them
		for (cv::Point2f newPoint : previousFramePointsOfInteres) {
			if (PointsThatAreCurrentlyTracking.find(newPoint)
					== PointsThatAreCurrentlyTracking.end()) {
				//Create New tracking for the current point from previous frame (we are still looking in previous frames
				activeTracks.insert(
						Trajectory(video.getPreviousFrameNumber(), newPoint));
			}
		}

		if (activeTracks.empty()) {
			Logger::Log(LogLevel::WARNING, "Empty Tracks set!\n");
			continue;
		}

		for (Trajectory track : activeTracks) {
			cv::Point2f oldPoint = track.getLastPoint();
			cv::Mat descriptor1;
			try {
				descriptor1 = video.getPreviousFrameInformation().getDescriptor(
						oldPoint);
			} catch (indexNotFoundException& excp) {
				Logger::Log(LogLevel::WARNING,
						"The (%f, %f) point could not be located in the existing frame informations point of previous frame, skipping it.\n",
						oldPoint.x, oldPoint.y);
				continue;	//skip this track
			}
			float min = FLT_MAX;
			float secondMin = FLT_MAX;
			cv::Point2f bestMatch(-1, -1);
			cv::Point2f secondBestMatch(-1, -1);

			for (cv::Point2f newPoint : thisFramePointsOfInteres) {
				if (DistanceCalculator::ManhattanDistance(oldPoint, newPoint)
						< thresholdA) {
					cv::Mat descriptor2;
					try {
						descriptor2 =
								video.getCurrentFrameInformation().getDescriptor(
										newPoint);
					} catch (indexNotFoundException& exc) {
						Logger::Log(LogLevel::WARNING,
								"The (%f, %f) point could not be located in the existing frame informations point of previous frame, skipping it.\n",
								newPoint.x, newPoint.y);
						continue;
					}

					try {
						float distance = DistanceCalculator::EucledeanDistance(
								descriptor1, descriptor2);
						if (distance < min) {
							secondMin = min;
							secondBestMatch = bestMatch;

							min = distance;
							bestMatch = newPoint;
						} else if (distance < secondMin) {
							secondMin = distance;
							secondBestMatch = newPoint;
						}
					} catch (DistanceCalculatorException& exp) {
						Logger::Log(LogLevel::ERROR, exp.what());
					}
				}
			}
//			Logger::Log(LogLevel::VERBOSE, "min = %f,  smin = %f \n", min,
//					secondMin);

			if ((bestMatch != cv::Point2f(-1, -1))
					&& (min <= distanceBetweenBestMatchAndSecondBest * secondMin)) {
				//We could find a good match!
				track.Add(bestMatch);
				if (track.length() == length)
					video.AddTrajectory(track);
				else
					survivedTracking.insert(track);

			} else {
				//Best match did not found, assuming the interest point is not moving or deleting it from tracking
				Logger::Log(LogLevel::WARNING,
						"The best match did not found! Ignoring the trajectory!\n");
//				Logger::Log(LogLevel::INFO,
//						"The best match did not found! Adding point (%3.2f, %3.2f) to track!\n",
//						oldPoint.x, oldPoint.y);
//				track.Add(oldPoint);
//				if (track.length() == length)
//					video.AddTrajectory(track);
//				else
//					survivedTracking.insert(track);
			}

		}
		//replace survivedTracking with actual tracking.
		Logger::Log(LogLevel::VERBOSE, "Survived = %d ALL = %d \n",
				survivedTracking.size(), video.getAllTrajectories().size());
		activeTracks = survivedTracking;
		survivedTracking.clear();
	}	//main for loop
}

OpticalFlow::~OpticalFlow() {

}
// TODO Auto-generated destructor stub
} /* namespace OpticalFlow */
} /* namespace Video */
} /* namespace HAR */
