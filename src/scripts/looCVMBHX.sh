#!/bin/bash

SVMDataFolder="./svmdata"
SVMModelFolder="./models/svm"
KMeansModelFolder="./models/kmeans"
ExternalFeaturesFolder="./featuresx/"
ActorList=(S01 S02 S03 S04 S05 S06 S07 S08 S09 S10 S11)

MBHxFeatures='245-340'
numberOfWords=40

for i in ${ActorList[@]}; do
	testingSet=`ls $ExternalFeaturesFolder | grep $i`
	trainingSet=`ls $ExternalFeaturesFolder | grep -v $i`
	modelFileName="$KMeansModelFolder/MBHx-w$numberOfWords-Fold-$i.yml"
	svmTrainFile="$SVMDataFolder/MBHx-w$numberOfWords-Fold-$i.svm.train"
	svmTestFile="$SVMDataFolder/MBHx-w$numberOfWords-Fold-$i.svm.test"
	svmModelFileName="$SVMModelFolder/MBHx-w$numberOfWords-Fold-$i.svm.model"
	#make a KMeans model for this fold
        echo $trainingSet | tr ' ' '\n'  | ./bin/Merge -f $ExternalFeaturesFolder | tr '\t' ',' | cut -d ',' -f $MBHxFeatures |  ./bin/KMeansClustering -s -c $numberOfWords -o $modelFileName

	#Count The Words for this fold
	echo $trainingSet | tr ' ' '\n' | tr '\t' ',' | cut -d ',' -f $MBHxFeatures | ./bin/CountTheWords -m $modelFileName -f $ExternalFeaturesFolder -p  3 -s $svmTrainFile

	echo $testSet | tr ' ' '\n' | tr '\t' ',' | cut -d ',' -f $MBHxFeatures | ./bin/CountTheWords -m $modelFileName -f $ExternalFeaturesFolder -p  3 -s $svmTestFile
       
	#Make a svm-model for the current data
	./svmbin/svm-train -s 0 -t 3 -d 3 -c 500 $svmTrainFile $svmModelFileName

	#Measure the accuracy of svm-model for calssification
	svmOutputFile="$SVMDataFolder/MBHx-w$numberOfWords-Fold-$i.svm.output"
        ./svmbin/svm-predict $svmTestFile $svmModelFileName $svmOutputFile

done

