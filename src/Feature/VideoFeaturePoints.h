/*
 * VideoFeaturePoints.h
 *
 *  Created on: Dec 30, 2014
 *      Author: Pejman
 *      Copyright (c) 2014 Pejman. All rights reserved.
 */

#ifndef VIDEOFEATUREPOINTS_H_
#define VIDEOFEATUREPOINTS_H_

#include <vector>
#include <string>
#include "../Video/Frame/FrameFeaturePoints.h"

namespace HAR {
namespace Video {
namespace Features {

class VideoFeaturePoints {
public:
	VideoFeaturePoints();
	VideoFeaturePoints(std::string keypointType);
	void addFrameFeaturePoints(unsigned int frameNumber, FrameFeaturePoints* frameFeaturePoints);
	FrameFeaturePoints* operator[](int index);
	FrameFeaturePoints* getFrameFeaturePoints(unsigned int frameNumber) const;

	bool Write(std::string fileName, std::string channel);

	bool Read(std::string fileName, std::string channel);
	bool ReadVideoFromFileNode(cv::FileNode node);

	bool empty() { return (videoFeaturePoints.empty()); }
	virtual ~VideoFeaturePoints();

private:
	std::vector<FrameFeaturePoints*> videoFeaturePoints;
	std::string keypointType;
};

} /* namespace Features */
} /* namespace Video */
} /* namespace HAR */

#endif /* VIDEOFEATUREPOINTS_H_ */
