/*
 * MyDescriptor.h
 *
 *  Created on: Feb 12, 2015
 *      Author: Pejman
 *      Copyright (c) 2015 Pejman. All rights reserved.
 */

#ifndef MYDESCRIPTOR_H_
#define MYDESCRIPTOR_H_

#include <vector>
#include <opencv2/opencv.hpp>

#include <string>
#include <fstream>

namespace HAR {

class MyDescriptor {
public:
    MyDescriptor(const std::string stringToParse) { terminated = false; Parse(stringToParse); }
	MyDescriptor(int StartingFrame);
	MyDescriptor(std::vector<cv::Point3f> mincroMovement, int startingFrame );
	void Update(cv::Point3f newLocation);
	void Terminate() { terminated = true; } //No further update is accepted.
	bool isTerminated() { return terminated; }
	virtual ~MyDescriptor();
	unsigned int getStartingFrame() const { return startingFrame; }
    unsigned long getLength() { return microMovement.size(); }

	const std::vector<cv::Point3f>& getMicroMovement() const { return microMovement; }

    void Save(std::ofstream& outFile) const;
	std::string ToString() const;
    bool Parse(std::string input);
    
    double CalculateEnergy();
    std::string getHeader();
    std::string ToMicromovementString();
    
private:
	/*
	 * Each microMovement contains several frames (approximately 20) of movement. For each frame
	 * a microMovement contains the information of x,y adn disparity of that movement.
	 */
	std::vector<cv::Point3f> microMovement;
	unsigned int startingFrame;//The starting frame of micromovement
	bool terminated;
};

} /* namespace HAR */

#endif /* MYDESCRIPTOR_H_ */
