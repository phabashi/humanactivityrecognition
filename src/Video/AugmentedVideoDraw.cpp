/*
 * AugmentedVideoDraw.cpp
 *
 *  Created on: May 8, 2016
 *      Author: Pejman
 *      Copyright (c) 2016 Pejman. All rights reserved.
 */

#include "AugmentedVideoDraw.h"

namespace HAR {
namespace Video {

AugmentedVideoDraw::AugmentedVideoDraw(cv::Mat frame) {
	if( frame.channels() == 1)
		cv::cvtColor(frame, displayFrame, cv::COLOR_GRAY2BGR);
	else
		displayFrame = frame.clone();
}

AugmentedVideoDraw::~AugmentedVideoDraw() {
	// TODO Auto-generated destructor stub
}

void AugmentedVideoDraw::DrawRegionsOfInteresOnFrame(
		const std::vector<cv::Rect_<float> >& rois) {
	for (auto r = rois.begin(); r != rois.end(); r++) {
//		cv::Rect rect(r->x * frame.cols, r->y * frame.rows,
//				r->width * frame.cols, r->height * frame.rows);
		cv::Rect rect(r->x , r->y,
				r->width , r->height );
		rectangle(displayFrame, rect, cv::Scalar(0, 255, 0), 1);
	}
}

void AugmentedVideoDraw::DrawInterestPointsOnFrame( const std::vector<cv::Point_<float> >& interesPoints) {
	for (auto r = interesPoints.begin(); r != interesPoints.end(); r++) {
		cv::Point position = *r;
		cv::circle(displayFrame, position, 1, cv::Scalar(255, 0, 0), -1);
	}
}

std::unordered_set<Trajectory> AugmentedVideoDraw::DrawTrajectoriesOnFrame(
		const std::pair<std::unordered_multimap<int, Trajectory>::const_iterator,
				std::unordered_multimap<int, Trajectory>::const_iterator>& range, int frameNumber) {

	static std::unordered_map<Trajectory, cv::Scalar> color;
	static std::unordered_set<Trajectory> toDisplay;

	std::unordered_set<Trajectory> toDeleteSet;
	for (auto tr = toDisplay.begin(); tr != toDisplay.end(); tr++) {
		if (!tr->isActiveOnFrame(frameNumber))
			toDeleteSet.insert(*tr);
	}
	for (Trajectory tr : toDeleteSet)
		toDisplay.erase(tr);
	for (auto tr = range.first; tr != range.second; tr++) {
		if (tr->second.isActiveOnFrame(frameNumber))
			toDisplay.insert(tr->second);
	}

	for (Trajectory tr : toDisplay) {
		//If this is a new trajectory, assign to it a new color.
		if(color.find(tr) == color.end() ) {
			int r = rand() % 256;
			int g = rand() % 256;
			int b = rand() % 256;
			color[tr] = cv::Scalar(r,g,b);
		}
		for (int i = frameNumber; i > tr.getStartFrame() + 1;
				i--)
			cv::line(displayFrame, tr.getPoint(i), tr.getPoint(i - 1), color[tr], 1);
	}
	return toDeleteSet;
}

} /* namespace Video */
} /* namespace HAR */
