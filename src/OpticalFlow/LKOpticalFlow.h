/*
 * LKOpticalFlow.h
 *
 *  Created on: Apr 30, 2016
 *      Author: pejman
 */

#ifndef SRC_OPTICALFLOW_LKOPTICALFLOW_H_
#define SRC_OPTICALFLOW_LKOPTICALFLOW_H_

#include "IOpticalFlow.h"

namespace HAR {
namespace Video {
namespace OpticalFlow {

class LKOpticalFlow : public IOpticalFlow {
public:
	LKOpticalFlow();

	virtual void ExtractOpticalFlow(AugmentedVideoBuffer& video, int length);

	virtual ~LKOpticalFlow();
private:
	cv::Size winSize;
	int level;
	int flags;
	float minimumEigenThreshold;
	cv::TermCriteria termcrit;
};

} /* namespace OpticalFlow */
} /* namespace Video */
} /* namespace HAR */

#endif /* SRC_OPTICALFLOW_LKOPTICALFLOW_H_ */
