/*
 * FeatureExtractor.h
 *
 *  Created on: Dec 30, 2014
 *      Author: Pejman
 *      Copyright (c) 2014 Pejman. All rights reserved.
 */

#ifndef FEATUREDETECTOR_H_
#define FEATUREDETECTOR_H_

#include <opencv2/core/core.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <vector>
#include <unordered_map>

#include "../Video/AugmentedVideoBuffer.h"
#include "../Video/Frame/FrameFeaturePoints.h"
#include "../Video/Video.h"
#include "VideoFeaturePoints.h"

namespace HAR {
namespace Video {
namespace Features {

class FeatureDetector {
public:
	static FeatureDetector& getInstance(std::string featureType);
	static void removeInstances();

	FrameFeaturePoints ExtractFeatureFromFrame(const cv::Mat input);
	VideoFeaturePoints* ExtractFeaturesFromVideo(Video inputVideo);
	void ExtractFeatures(AugmentedVideoBuffer& inputVideo,  bool includeOnlyMotionArea, int samplingScale = 1);
	std::string getType() { return featureType; }
	virtual ~FeatureDetector();

private://Hide constructors as well as copy constructor && assignment operator
	FeatureDetector();
	FeatureDetector(FeatureDetector&);
	FeatureDetector& operator=(FeatureDetector&);

private:
	std::string featureType;
	cv::Ptr<cv::FeatureDetector> featureDetector;

private:
	static std::unordered_map<std::string, FeatureDetector*> instances;

};

} /* namespace Features */
} /* namespace Video */
} /* namespace HAR */

#endif /* FEATUREDETECTOR_H_ */
