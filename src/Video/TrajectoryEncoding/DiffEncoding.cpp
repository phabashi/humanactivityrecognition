/*
 * DiffEncoding.cpp
 *
 *  Created on: Dec 28, 2016
 *      Author: user
 */

#include "DiffEncoding.h"
#include "../Trajectory.h"

namespace HAR {
namespace Video {
namespace TrajectoryEncoding {

DiffEncoding::DiffEncoding(int N, bool normalize): order(N), normalize(normalize) {

}

std::vector<float> DiffEncoding::encode(const std::vector<cv::Point2f>& points) const {
	std::vector<float> result;
	if (order < 0) return result;
	std::vector<cv::Point2f> diff;

	int resultSize = (points.size() * order - (order * (order + 1) / 2.0) ) *  2;
	result.reserve(resultSize);
	diff.reserve(resultSize/2);
	//result.reserve((3*(points.size()- 1) - 3) * 2);

	float m = Trajectory::getEnergy(points);

	if (m == 0)
		return result;

	double sum = 0;
	if( order == 0 ){
		for (uint i = 1; i < points.size(); i++) {//Zero order
			cv::Point2f thePoint = points[i] - points[0];//The difference to starting point
			result.push_back(thePoint.x);
			result.push_back(thePoint.y);
			sum += thePoint.x * thePoint.x + thePoint.y * thePoint.y;
		}
		return ((normalize)? normalizeVector(result, sqrt(sum)) : result);
	}

	for (uint i = 1; i < points.size(); i++) {//first order
		cv::Point2f thePoint = points[i] - points[i - 1];
		result.push_back(thePoint.x);
		result.push_back(thePoint.y);
		sum += thePoint.x * thePoint.x + thePoint.y * thePoint.y;
		diff.push_back(thePoint);
	}

	int diffLocation = 0;
	for (int o=2; o <= order; o++) {
		for (uint i = 1; i < points.size() - o + 1; i++) {//first order
			cv::Point2f thePoint = diff[diffLocation + i] - diff[diffLocation + i - 1];
			result.push_back(thePoint.x);
			result.push_back(thePoint.y);
			sum += thePoint.x * thePoint.x + thePoint.y * thePoint.y;
			diff.push_back(thePoint);
		}
		diffLocation  += points.size() - o + 1;
	}
//
//	for (uint i = 1; i < first.size(); i++) {//second order
//		cv::Point2f thePoint = first[i]-first[i-1];
//		result.push_back(thePoint.x);
//		result.push_back(thePoint.y);
//		second.push_back(thePoint);
//	}
//
//	for (uint i = 1; i < second.size(); i++) {//second order
//		cv::Point2f thePoint = second[i]-second[i-1];
//		result.push_back(thePoint.x);
//		result.push_back(thePoint.y);
//		//third.push_back(thePoint);
//	}

	return ((normalize)? normalizeVector(result, sqrt(sum)) : result);

}

float DiffEncoding::get3DEnergy(const std::vector<cv::Point3f>& trajectory3D) {
	float energy = 0;
	for (uint i = 1; i < trajectory3D.size(); i++) {
		float dx = trajectory3D[i].x - trajectory3D[i - 1].x;
		float dy = trajectory3D[i].y - trajectory3D[i - 1].y;
		float dz = trajectory3D[i].z - trajectory3D[i - 1].z;
		energy += dx * dx + dy * dy  + dz * dz;
	}
	return sqrt(energy);
}

std::vector<float> DiffEncoding::encode3D(const std::vector<cv::Point3f>& trajectory3D) const {
		std::vector<float> result;
		if (order < 0) return result;
		std::vector<cv::Point3f> diff;

		int resultSize = (trajectory3D.size() * order - (order * (order + 1) / 2.0) ) *  2;
		result.reserve(resultSize);
		diff.reserve(resultSize/2);
		//result.reserve((3*(points.size()- 1) - 3) * 2);

		float m = get3DEnergy(trajectory3D);

		if (m == 0)
			return result;

		double sum = 0;
		if( order == 0 ){
			for (uint i = 1; i < trajectory3D.size(); i++) {//Zero order
				cv::Point3f thePoint = trajectory3D[i] - trajectory3D[0];//The difference to starting point
				result.push_back(thePoint.x);
				result.push_back(thePoint.y);
				result.push_back(thePoint.z);
				sum += thePoint.x * thePoint.x + thePoint.y * thePoint.y + thePoint.z * thePoint.z;
			}
			return ((normalize)? normalizeVector(result, sqrt(sum)) : result);
		}

		for (uint i = 1; i < trajectory3D.size(); i++) {//first order
			cv::Point3f thePoint = trajectory3D[i] - trajectory3D[i - 1];
			result.push_back(thePoint.x);
			result.push_back(thePoint.y);
			result.push_back(thePoint.z);
			sum += thePoint.x * thePoint.x + thePoint.y * thePoint.y + thePoint.z * thePoint.z;
			diff.push_back(thePoint);
		}

		int diffLocation = 0;
		for (int o=2; o <= order; o++) {
			for (uint i = 1; i < trajectory3D.size() - o + 1; i++) {//first order
				cv::Point3f thePoint = diff[diffLocation + i] - diff[diffLocation + i - 1];
				result.push_back(thePoint.x);
				result.push_back(thePoint.y);
				result.push_back(thePoint.z);
				sum += thePoint.x * thePoint.x + thePoint.y * thePoint.y + thePoint.z * thePoint.z;
				diff.push_back(thePoint);
			}
			diffLocation  += trajectory3D.size() - o + 1;
		}

		return ((normalize)? normalizeVector(result, sqrt(sum)) : result);
}


DiffEncoding::~DiffEncoding() {
	// TODO Auto-generated destructor stub
}

std::vector<float> DiffEncoding::normalizeVector(std::vector<float>& input, double vectorSize) {
	for(int i=0; i < input.size(); i++)
		input[i] /= vectorSize;
	return input;
}

} /* namespace TrajectoryEncoding */
} /* namespace Video */
} /* namespace HAR */
