/*
 * DirectionEncoding.h
 *
 *  Created on: Jun 1, 2016
 *      Author: pejman
 */

#ifndef SRC_VIDEO_TRAJECTORYENCODING_DIRECTIONENCODING_H_
#define SRC_VIDEO_TRAJECTORYENCODING_DIRECTIONENCODING_H_

#include "IEncoding.h"

namespace HAR {
namespace Video {
namespace TrajectoryEncoding {

class DirectionEncoding: public IEncoding {
public:
	DirectionEncoding();

	virtual std::vector<float> encode(const std::vector<cv::Point2f>& points) const;

	virtual ~DirectionEncoding();
};

} /* namespace TrajectoryEncoding */
} /* namespace Video */
} /* namespace HAR */

#endif /* SRC_VIDEO_TRAJECTORYENCODING_DIRECTIONENCODING_H_ */
