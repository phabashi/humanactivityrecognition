#!/bin/bash

echo "This script will go trough all the clips in ./clips folder, search for Left vidoe and run extract featues on it and its corresponding right vido. "

#ls clips/ | grep -f ./lists/Actor_set3.txt | grep -f ./lists/ActivitySet_15.txt | grep Left

#setting
settingFile="./SampleSetting.ini"
inputFileList="./lists/VideoFileList.txt"
opticalFlowAlgorithm="IP"
interestPointAlgorithm="FAST"
descriptorAlgorithm="SIFT"
ratio="1"
scale="1" #scale the original video to half size in each direction
featureFolderName="./test-data/$opticalFlowAlgorithm/$interestPointAlgorithm/$descriptorAlgorithm/$matchingAlgorithm/scale_$scale/rha_$removeHomogenousAreas"

mkdir -p $featureFolderName
featureLengthes=(3 5 7 11 13 15)

leftVideoFileNames=(`cat $inputFileList`)

function ExtractFeatures {
	i=$1
	fileNumber=$(( $fileNumber + 1 ))
	#echo "Running $fileNumber/ $count = [$(( fileNumber * 100 / count ))%]"
	leftVideo="./clips/$i"
	rightVideo="./clips/${i/Left/Right}"
	name=${i/_Left}
	name="${name/avi/fet}"
	featureFileName="$completeFeatureFileName/$name"
	if [ -e $featureFileName ]; then
		echo "File Exsists, skipping $featureFileName..."
	else
		echo "[$(( fileNumber * 100 / count ))%] Extracting features of '$leftVideo' with lenght $length and save it to '$featureFileName'"
		./bin/newExtractFeatures -l $leftVideo -o $featureFileName -dl $length -ip $interestPointAlgorithm -ipd $descriptorAlgorithm -ofa $opticalFlowAlgorithm -s $scale as $settingFile
	fi
}

for length in ${featureLengthes[@]};do

completeFeatureFileName="$featureFolderName/Length_$length/features"
mkdir -p $completeFeatureFileName

count=${#leftVideoFileNames[@]}
fileNumber="1"
for i in ${leftVideoFileNames[@]}; do
	#ExtractFeatures $i &
	fileNumber=$(( $fileNumber + 1 ))
	#echo "Running $fileNumber/ $count = [$(( fileNumber * 100 / count ))%]"
	leftVideo="./clips/$i"
	rightVideo="./clips/${i/Left/Right}"
	name=${i/_Left}
	name="${name/avi/fet}"
	featureFileName="$completeFeatureFileName/$name"
	if [ -e $featureFileName ]; then
		echo "File Exsists, skipping $featureFileName..."
	else
		echo "[$(( fileNumber * 100 / count ))%] Extracting features of '$leftVideo' with lenght $length and save it to '$featureFileName'"
		./bin/newExtractFeatures -l $leftVideo -o $featureFileName -dl $length -ip $interestPointAlgorithm -ipd $descriptorAlgorithm -ofa $opticalFlowAlgorithm -s $scale as $settingFile&
	fi
	#sleep 1 &
	#echo $fileNumber &
	#wait %1
	if (( $fileNumber % 8 == 0 )); then
		echo "Waiting for childs!"
		wait
	fi
done
done
