/*
 * ApplicationSetting.cpp
 *
 *  Created on: Mar 15, 2016
 *      Author: Pejman
 *      Copyright (c) 2016 Pejman. All rights reserved.
 */

#include "GlobalSetting.h"

#include <fstream>
#include "../Logger/Logger.h"
#include <stdexcept>

using namespace std;
using namespace Pejman::Logger;

namespace Pejman {
namespace Application {

bool GlobalSetting::LoadConfigurationFromIniFile(std::string fileName) {
	ifstream input(fileName);
	if (!input)
		return false;
	string line;
	string currentCategory;
	getline(input, line);

	while (input) {

		line = trim(line);
		if (line[0] == ';') {
			//This line is a comment! skip it!
		} else if (line[0] == '[') {
			currentCategory = line.substr(1, line.length() - 2);
		} else {
			vector<string> val = split(line, '=');
			if (val.size() != 0) { //if we could not find an equal sign, just skip that line!
				if (val.size() != 2)
					return false; //if there is more than one equal sign, complain!
				string key = trim(val[0]);
				string value = trim(val[1]);
				if (key != "" and value != "") {
					GlobalSetting::getInstance().categories[toLowerCase(
							currentCategory)][toLowerCase(key)] = value;
				}
			}
		}

		getline(input, line);
	};

	return true;
}

GlobalSetting::GlobalSetting() {
	// TODO Auto-generated constructor stub

}

GlobalSetting::~GlobalSetting() {
	// TODO Auto-generated destructor stub
}

GlobalSetting::GlobalSetting(GlobalSetting&) {
	throw std::runtime_error("Not Implemented!");
}

GlobalSetting& GlobalSetting::operator =(GlobalSetting&) {
	throw std::runtime_error("Not Implemented!");
	return *this;
}

} /* namespace Application */
} /* namespace Pejman */
