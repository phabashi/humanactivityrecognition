#!/bin/bash

echo "Running test on Different Configurations"
i=0
printf " ##    OFA   length  Normalized  Motion-Detected    NOW     scale    Accuracy\n"
for OFA in IP LK FB; do
	export OFA
	for length in 5 7 9 11 13 15 17; do
		export length
		for Norm in Yes; do
			export Norm
			for DM in No Yes; do
				export DM
				#. ./newScripts/test.bash
				#for NOW in 4000; do
					for scale in "5" ; do

						i=$(( i + 1 ))
						printf  "%3d     %2s     %-4s       %-3s         %-8s      %5s    %-6s " $i $OFA $length $Norm $DM $scale $NOW
						./bin/ExtractTrajectory3D.bash Windsor-short ./rawTrajectoryFolder &
						./bin/Encode3D.bash ./rawTrajectoryFolder ./encodedTrajectories &
						wait
					done
				#done
			done
		done
	done
done
