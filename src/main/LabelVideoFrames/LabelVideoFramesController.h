//
//  LabelVideoFramesController.h
//  HARLinked
//
//  Created by pejman on 2015-07-08.
//  Copyright (c) 2015 pejman. All rights reserved.
//

#ifndef __HARLinked__LabelVideoFramesController__
#define __HARLinked__LabelVideoFramesController__

#include <stdio.h>


#include "LabelFrameVideoModel.h"
#include "LabelVideoFrameView.h"

class LabelVideoController
{
public:
    LabelVideoController(LabelVideoModel& model, LabelVideoView& view);
    
    void Update(int keyCode);//update the model with the input
    
    std::string getKeyBindingHelp();
    bool setKeyBindings(int startIndex);
private:
    LabelVideoModel& theModel;
    LabelVideoView& view;
    
    std::map<int, int> keyBinding;
    int si;
    
private://for now
    int getKeyBindings(int i);
    const std::map<int, int> getKeyBinding() { return keyBinding; }
    
    void updateKeyBinding(std::pair<int,int> newPair)
    { keyBinding[newPair.first] = newPair.second; }
};

#endif /* defined(__HARLinked__LabelVideoFramesController__) */
