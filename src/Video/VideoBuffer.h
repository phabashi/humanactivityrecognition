/*
 * VideoBuffer.h
 *
 *  Created on: Feb 3, 2016
 *      Author: Pejman
 *      Copyright (c) 2016 Pejman. All rights reserved.
 */

#ifndef VIDEOBUFFER_H_
#define VIDEOBUFFER_H_

#include <map>
#include <unordered_map>
#include "Video.h"

namespace HAR {
namespace Video{

class VideoBuffer : public Video {
public:
	VideoBuffer(std::string inputVideoFileName, int bufferSize = -1 , bool isGrayscale = false);

	virtual const cv::Mat& getFrame(unsigned long frameNumber);
	virtual const cv::Mat& getNextFrame();
	virtual const cv::Mat& getCurrentFrame();

	cv::Size getFrameSize() const { return frameSize; }

	virtual void Reset() { currentFrameNumber = 0; }

	virtual ~VideoBuffer();

private:
	virtual void InvalidateBuffer();

private:
	bool LoadBuffer(int frameOfinterest);//It had no interest to users, they should not know about it's existence

private:
	///This is the number of frames allowed t load into memory at once. A -1 value means load all video into memory
	int maxBufferSize;
	int bufferStartFrame; // The frame number of starting frame in buffer
    std::vector<cv::Mat> buffer;
	int currentFrameNumber;
	cv::Size frameSize;

public:
	int getCurrentFrameNumber() const { return currentFrameNumber; }
	int getPreviousFrameNumber() const { return currentFrameNumber - 1; }
private:
	const cv::Mat emptyFrame;

};

} /* namespace Video */
} /* namespace HAR */

#endif /* VIDEOBUFFER_H_ */
