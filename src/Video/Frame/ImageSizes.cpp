/*
 * ImageSizes.cpp
 *
 *  Created on: Feb 10, 2015
 *      Author: Pejman
 *      Copyright (c) 2015 Pejman. All rights reserved.
 */

#include "ImageSizes.h"

namespace HAR {

ImageSizes::ImageSizes(float width, float height)
{
	w = width;
	h = height;
}

bool ImageSizes::isInside(cv::Point2f p)
{
	return (p.x < w && p.x >= 0 && p.y < h && p.y >=0);
}

bool ImageSizes::isInside(cv::Rect r)
{
	return (r.x >= 0 && r.x + r.width < w && r.y > 0 && r.y + r.height < h);
}

ImageSizes::~ImageSizes() {
	// TODO Auto-generated destructor stub
}

} /* namespace HAR */

HAR::ImageSizes::ImageSizes(cv::Mat image)
{
	w = image.cols;
	h = image.rows;
}
