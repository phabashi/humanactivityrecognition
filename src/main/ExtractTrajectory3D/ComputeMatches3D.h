/*
 * ComputeMatches3D.h
 *
 *  Created on: Jan 1, 2017
 *      Author: dhawal
 */

#ifndef SRC_MAIN_COMPUTEMATCHES3D_COMPUTEMATCHES3D_H_
#define SRC_MAIN_COMPUTEMATCHES3D_COMPUTEMATCHES3D_H_

#include "../../Video/AugmentedVideoBuffer.h"
#include "../../Feature/FeatureDescriptorMatcher.h"
#include "../../Video/Display.h"
#include "../../Feature/FeatureDetector.h"
#include "../../Feature/FeatureDescriptorExtractor.h"
#include "../../Logger/Logger.h"
#include "Arguments.h"

using namespace cv;
using namespace std;
using namespace HAR::NewExtractFeatures3D;
using namespace HAR;
using namespace Pejman::Logger;
using namespace HAR::Exceptions;
using namespace HAR::Video;


namespace HAR {
namespace ComputeMatches {
class ComputeMatches {
public:
	//this function computes the matches between videos
	void ComputeMatches3D(AugmentedVideoBuffer& leftVideo,
			AugmentedVideoBuffer& rightVideo, const Arguments& args);
	//this functions extracts keyPoints and saves the corresponding trajectories
	void getTrajKeyPoints(AugmentedVideoBuffer& video,  vector<const Trajectory*>* Trajectory , std::vector<KeyPoint>* keyPoints);
	//show the matches if the program is run with -sm option
	void showMatches(Mat lframe,vector<KeyPoint> keyPointsL, Mat rframe,vector<KeyPoint> keyPointsR, vector<DMatch>frameGoodMatches);
	//returns good matches
	std::vector<std::vector<cv::DMatch> > getGoodMatches() { return goodMatches; }
	 //returns survived trajectories for left video
	std::vector<std::vector<const Trajectory*>> getLeftTrajectories() { return SurvivedTrajectoryL; }
	//returns survived trajectories for right video
	std::vector<std::vector<const Trajectory*>> getRightTrajectories() { return SurvivedTrajectoryR; }
	std::vector<std::vector<cv::Point2f>> getLeftMatchedPoints() {return leftMatchedPoints;}
	std::vector<std::vector<cv::Point2f>> getRightMatchedPoints() {return rightMatchedPoints;}

private:
	std::vector<std::vector<cv::DMatch> > goodMatches;
	std::vector<std::vector<std::vector<cv::DMatch>>> matches;
	std::vector<std::vector<const Trajectory*>> SurvivedTrajectoryL,  SurvivedTrajectoryR;
	std::vector<std::vector<cv::Point2f>> leftMatchedPoints, rightMatchedPoints;
};

}
}

#endif /* SRC_MAIN_COMPUTEMATCHES3D_COMPUTEMATCHES3D_H_ */
