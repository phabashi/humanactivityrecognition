/*
 * SplitVideo.cpp
 *
 *  Created on: Sep 27, 2015
 *      Author: habashi
 */

#include <iostream>
#include <opencv2/core/core.hpp>
#include "../Video/Video.h"
#include <fstream>
#include <string>
#include <unordered_map>
#include "../util/util.h"

using namespace std;

int main(int argc, char* argv[])
{
	string inputVideoFileName;
	string inputLabelFileName;
	int offset = 0;
	if(argc == 4)
	{
		inputVideoFileName = string(argv[1]);
		inputLabelFileName = string(argv[2]);
		offset = stringTo<int>( string(argv[3]) );
	}
	else if(argc ==3)
	{
		inputVideoFileName = string(argv[1]);
		inputLabelFileName = string(argv[2]);
		offset = 0;
	}
	else
	{
		cout << "Usage: SplitVidoe inputVideoFileName inputLabelFileName [offset]" << endl;
		return -1;
	}

	cout << "The synchronization factor is : " << offset << endl;

	HAR::Video::Video theVideo;

	theVideo.setGrayscale(false);
	theVideo.LoadFromFile(inputVideoFileName);
	ifstream labelStream;
	labelStream.open(inputLabelFileName.c_str());
	if( ! labelStream )
	{
		cout << "Can not open the input label file: """ << inputLabelFileName << """" <<  endl;
		return -2;
	}

	int startFrameNumber = 0;
	int endFrameNumber = 0;
	//unordered_map<string, int> instanceNumber;
	map<string, int> instanceNumber;
	while(labelStream)
	{
		startFrameNumber = endFrameNumber;

		string label;
		labelStream >> endFrameNumber;
		labelStream >> label;

		endFrameNumber += offset;

		cout << "Cutting the content between " << startFrameNumber << " and " << endFrameNumber << " with label " << label << ", length = " << endFrameNumber - startFrameNumber <<endl;

		stringstream fileNameStream;
		instanceNumber[label] += 1;
		int lios = inputVideoFileName.find_last_of('/') +1;
		string fileN = inputVideoFileName.substr(lios, inputVideoFileName.find_last_of('.') - lios);
		fileNameStream << "./clips2/" << fileN << "_L_" << label << "_I_" << instanceNumber[label] << ".avi";

		string subVideoFileName = fileNameStream.str() ;
		cout << "File Name = " << subVideoFileName << endl;
		theVideo.saveSubVideoToFile(subVideoFileName, startFrameNumber, endFrameNumber);

	}

}
