/*
 * NormalizedDifferentiationEncoding.h
 *
 *  Created on: May 31, 2016
 *      Author: pejman
 */

#ifndef SRC_VIDEO_TRAJECTORYENCODING_NORMALIZEDDIFFERENTIATIONENCODING_H_
#define SRC_VIDEO_TRAJECTORYENCODING_NORMALIZEDDIFFERENTIATIONENCODING_H_

#include "IEncoding.h"

namespace HAR {
namespace Video {
namespace TrajectoryEncoding {

class NormalizedDifferentiationEncoding: public IEncoding {
public:
	NormalizedDifferentiationEncoding();

	virtual std::vector<float> encode(const std::vector<cv::Point2f>& points) const;

	virtual ~NormalizedDifferentiationEncoding();
};

} /* namespace TrajectoryEncoding */
} /* namespace Video */
} /* namespace HAR */

#endif /* SRC_VIDEO_TRAJECTORYENCODING_NORMALIZEDDIFFERENTIATIONENCODING_H_ */
