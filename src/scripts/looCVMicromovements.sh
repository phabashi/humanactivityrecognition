#!/bin/bash

CSVDataFolder="./data/csv"
SVMDataFolder="./data/svm"
SVMModelFolder="./models/svm"
KMeansModelFolder="./models/kmeans"
FeaturesFolder="./microMovements/"

ActivityList="./lists/ActivitySet_3.txt"
FeaturesList="2-34" # for length == 11 
ActorList=(S01 S02 S03 S04 S05 S06 S07 S08 S09 S10 S11)
numberOfWords=40
doFold()
{
	i=$1
	echo "Initialize system, making folds...." 
	testingSet=`ls $FeaturesFolder | grep -e "_L11" | grep -f $ActivityList | grep $i`
	trainingSet=`ls $FeaturesFolder | grep -e "_L11" | grep -f $ActivityList | grep -v $i`
	modelFileName="$KMeansModelFolder/MM-w$numberOfWords-Fold-$i.yml"
	csvTrainFile="$CSVDataFolder/MM-w$numberOfWords-Fold-$i.csv.train"
	csvTestFile="$CSVDataFolder/MM-w$numberOfWords-Fold-$i.csv.test"
	svmTrainFile="$SVMDataFolder/MM-w$numberOfWords-Fold-$i.svm.train"
	svmTestFile="$SVMDataFolder/MM-w$numberOfWords-Fold-$i.svm.test"
	svmModelFileName="$SVMModelFolder/MM-w$numberOfWords-Fold-$i.svm.model"

	#make a KMeans model for this fold
	echo "Make KMeans Model for fold $i"
        echo $trainingSet | tr ' ' '\n'  | ./bin/Merge -f $FeaturesFolder | cut -d ',' -f $FeaturesList | ./bin/KMeansClustering -s -c $numberOfWords -o $modelFileName

	#Count The Words for this fold
	#echo "Count the words for trainig in fold $i"
	#echo $trainingSet | tr ' ' '\n' | ./bin/CountTheWords -m $modelFileName -f $FeaturesFolder -p  3 -c $csvTrainFile -sel $FeaturesList

	#echo "Count the words for testing in fold $i"
	#echo $testingSet  | tr ' ' '\n' | ./bin/CountTheWords -m $modelFileName -f $FeaturesFolder -p  3 -c $csvTestFile -sel $FeaturesList
       
	#Prepare data for libSVM
	echo "Preparing data for learning/testing with libSVM"
	./bin/csv2svm -i $csvTrainFile $csvTestFile -o $svmTrainFile $svmTestFile -n

	#Make a svm-model for the current data
	echo "Making SVM Model for the fold $i" 
	./svmbin/svm-train -s 0 -t 3 -d 3 -c 500 $svmTrainFile $svmModelFileName

	#Measure the accuracy of svm-model for calssification
	echo "Measuring the accuracy for fold $i"
	svmOutputFile="$SVMDataFolder/MM-w$numberOfWords-Fold-$i.svm.output"
        ./svmbin/svm-predict $svmTestFile $svmModelFileName $svmOutputFile
}

for i in ${ActorList[@]}; do doFold "$i" & done

wait

