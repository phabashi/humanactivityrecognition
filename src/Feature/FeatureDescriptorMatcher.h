/*
 * FeatureDescriptorMatcher.h
 *
 *  Created on: Jan 27, 2015
 *      Author: Pejman
 *      Copyright (c) 2015 Pejman. All rights reserved.
 */

#ifndef FEATUREDESCRIPTORMATCHER_H_
#define FEATUREDESCRIPTORMATCHER_H_

#include <string>

#include <unordered_map>
#include "../Video/Frame/FrameFeatureDescriptors.h"
#include "../Video/Frame/FrameMatches.h"

namespace HAR {
namespace Video {
namespace Features {

class FeatureDescriptorMatcher {

public:
	static FeatureDescriptorMatcher& getInstance(std::string featureType);
	static void removeInstances();

public:
	std::vector< std::vector<cv::DMatch> > MatchDescriptors(
			const cv::Mat& descriptor1,
			const cv::Mat& descriptor2);

	FrameMatches MatchDescriptors(const FrameFeatureDescriptors& descriptor1, const FrameFeatureDescriptors& descriptors2);
	std::string getType() { return featureType; }
	virtual ~FeatureDescriptorMatcher();

private:
	FeatureDescriptorMatcher();
	FeatureDescriptorMatcher(FeatureDescriptorMatcher&);
	FeatureDescriptorMatcher& operator=(FeatureDescriptorMatcher&);

private:
	std::string featureType;
	cv::Ptr<cv::DescriptorMatcher> descriptorMatcher;

private:
	static std::unordered_map<std::string, cv::Ptr<FeatureDescriptorMatcher> > instances;
};

} /* namespace Feature */
} /* namespace Video */
} /* namespace HAR */

#endif /* FEATUREDESCRIPTORMATCHER_H_ */
