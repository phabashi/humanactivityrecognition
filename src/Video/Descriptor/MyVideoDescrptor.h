/*
 * MyVideoDescrptor.h
 *
 *  Created on: Feb 12, 2015
 *      Author: Pejman
 *      Copyright (c) 2015 Pejman. All rights reserved.
 */

#ifndef MYVIDEODESCRPTOR_H_
#define MYVIDEODESCRPTOR_H_

#include "MyDescriptor.h"

namespace HAR {

class MyVideoDescrptor {
public:
	MyVideoDescrptor();
    void Clear() { descriptors.clear(); }
	void Add(MyDescriptor newDescriptor);
	void Add(std::vector<MyDescriptor> newDescriptor);
	virtual ~MyVideoDescrptor();
    
    const std::vector<MyDescriptor>& getDescriptors() const { return descriptors; }
    const MyDescriptor& getDescriptor(int index) const { return descriptors[index]; }

    void Save(const std::string& fileName, bool overWright = true);
    
    bool Load(std::ifstream& inFile, int numberOfLinesToRead);
    bool Load(const std::string& fileName);
	std::string ToString();
    
    MyVideoDescrptor filterOutLiers(double minEnergy, double maxEnergy, uint& desiredLengths);
    
    void SaveMicroMovements(std::string outputMicromovementFileName);


private:
	std::vector<MyDescriptor> descriptors;
};

} /* namespace HAR */

#endif /* MYVIDEODESCRPTOR_H_ */
