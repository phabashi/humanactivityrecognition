/*
 * MainRegionOfInterest.cpp
 *
 *  Created on: Jan 28, 2016
 *      Author: Pejman
 */

#include <iostream>
#include <string>
#include <queue>
#include <opencv2/opencv.hpp>
//#include "util.h"

#include "../Video/motion/MotionDetection.h"
#include "../Video/VideoBuffer.h"
#include "../Video/StereoAugmentedVideo.h"
#include "../Video/Display.h"
#include "../Exceptions/ArgumentParseException.h"

using namespace std;
using namespace cv;
using namespace HAR;
using namespace HAR::Video;
using namespace HAR::Exceptions;

class Arguments{
public:
	Arguments():isStereoVideo(false) {}
	Arguments(int argc, char* argv[]);
	void printHelp();

	const bool getIsStereoVideo() const { return isStereoVideo; }
	const string& getLeftVideoInputFileName() const { return leftVideoInputFileName; }
	const string& getOuputFileName() const {	return ouputFileName; }
	const string& getRightVideoInputFileName() const { return rightVideoInputFileName; }
	const bool getDisplauResult() const { return displayResult; }

private:
	string leftVideoInputFileName;
	string rightVideoInputFileName;
	bool isStereoVideo;
	string ouputFileName;
	bool displayResult;
};

int main(int argc, char* argv[]){
	Arguments args;
	try{
		args = Arguments(argc, argv);
	} catch(ArgumentParseException& exp) {
		cout << exp.what() << endl;
		exit(1);
	}

	if(args.getIsStereoVideo()){
		throw std::runtime_error("Not implemented yet !");
		StereoAugmentedVideo sav(args.getLeftVideoInputFileName(), args.getRightVideoInputFileName());

		Display::Show(sav);

	} else { // Single video mode
		AugmentedVideoBuffer input(args.getLeftVideoInputFileName());
		input.enableResize(cv::Size(640, 400));
		HAR::Video::Motion::MotionDetection md;
		cout << "Detecting Motion..." << endl;
		md.DetectMotion(input);
		cout << "Done!" << endl;

		cout << input.SerializeInfo() << endl;
		if(args.getDisplauResult())
			Display::Show((DisplayableVideo&)input);
	}

}

void Arguments::printHelp(){
	cout << "Usage: RegionOfInterest -i <inputFile> [-o <outFile>]" << endl
			<< "OR:    RegionOfInterest -l leftInputFile -r <rightInputFile> [-o <outFile>]" << endl
			<< "You may also use -d to display the result " << endl;
}

Arguments::Arguments(int argc, char* argv[]){

	// Initialize settings
	leftVideoInputFileName = "";
	rightVideoInputFileName = "";
	isStereoVideo = "";
	displayResult = false;
	ouputFileName = "";

	for(int i=1; i< argc; i++)
	{
		if( strcmp(argv[i], "-l") == 0) {
			isStereoVideo = true;
			if(i < argc -1)
				leftVideoInputFileName = argv[++i];
			else
				throw ArgumentParseException(string(argv[i]) + ": Left video file name is missing after -l option.");
		} else if( strcmp(argv[i], "-r") == 0) {
			if(i < argc -1)
				rightVideoInputFileName = argv[++i];
			else
				throw ArgumentParseException(string(argv[i]) + ": Right video file name is missing after -r option.");
		} else if(strcmp(argv[i], "-i") == 0) {
			isStereoVideo = false;
			if(i < argc -1)
				leftVideoInputFileName = argv[++i];
			else
				throw ArgumentParseException(string(argv[i]) + ": Model folder name is missing after -f option.");
		}else if( strcmp(argv[i], "-d" ) == 0){
			displayResult = true;
		}else {//Unknown Argument
			throw ArgumentParseException(string("Unknown Argument! '") + argv[i] + "'");
		}
	}

	//Check the correct initialization of input parameters
	if(leftVideoInputFileName == "")
		throw ArgumentParseException("You should provide the input file name with -i or -l option.");
	if(isStereoVideo && rightVideoInputFileName == "")
		throw ArgumentParseException("You should provide the right input file name with -r option.");

}
