/*
 * AugmentedVideoBuffer.cpp
 *
 *  Created on: Feb 4, 2016
 *      Author: Pejman
 *      Copyright (c) 2016 Pejman. All rights reserved.
 */

#include "AugmentedVideoBuffer.h"

#include "../util/util.h"
#include <sstream>

using namespace std;
using namespace Pejman::Logger;

namespace HAR {
namespace Video {

AugmentedVideoBuffer::AugmentedVideoBuffer(std::string inputFile,
		int bufferSize, bool grayscale) :
				VideoBuffer(inputFile, bufferSize, grayscale) {
}

FrameInformation& AugmentedVideoBuffer::getFrameInformation(int frameNumber) {
	//TODO TO SEE if it is required or not
	//	if(info.find(frameNumber) == info.end() ){
	//		//This is the first time that someone access the information
	//	} else
	return info[frameNumber];
}

AugmentedVideoBuffer::~AugmentedVideoBuffer() {
}

std::string AugmentedVideoBuffer::SerializeInfo() const {
	std::stringstream result;
	for (uint i = 0; i < info.size(); i++) {
		auto item = info.find(i);
		if (item != info.end())
			result << item->second.Serialize() << '\n';
	}

	return result.str();
}

void AugmentedVideoBuffer::DeserializeInfo(std::string str) {
	std::vector<std::string> lines = split(str, '\n');
	for (uint i = 0; i < lines.size(); i++) {
		info[i].deSerialize(lines[i]);
	}
}

std::vector<cv::Mat> AugmentedVideoBuffer::getCurrentFrameRegionsOfInterest() {
	std::vector<cv::Mat> regionsOfInterest;
	auto roiList = getCurrentFrameInformation().getRegionsOfInterest();

	for (auto r = roiList.begin(); r != roiList.end(); r++) {
		cv::Mat frame = getCurrentFrame();
		cv::Rect rect(r->x * frame.cols, r->y * frame.rows,
				r->width * frame.cols, r->height * frame.rows);
		regionsOfInterest.push_back(frame(rect));
	}
	return regionsOfInterest;
}

//std::string AugmentedVideoBuffer::SerializeTrajectories() const {
//	stringstream ss;
//	bool includeFrameNumber = false;
//	for (auto trajectory : trajectories)
//		ss << trajectory.Serialize(includeFrameNumber) << "\n";
//	return ss.str();
//}

std::string AugmentedVideoBuffer::SerializeTrajectories(
		TrajectoryEncoding::TEncoding& encodingAlgorithm) const {
	stringstream ss;
	//	bool includeFrameNumber = false;
	for (auto trajectory : trajectories)
		ss << trajectory.Serialize(encodingAlgorithm) << "\n";
	return ss.str();
}

//std::string AugmentedVideoBuffer::SerializeTrajectoriesDeferentiation(bool normalize) const {
//	stringstream ss;
//	bool includeFrameNumber = false;
//	for (Trajectory trajectory : trajectories)
//		ss << trajectory.SerializeDeferentiation(includeFrameNumber, normalize) << "\n";
//	return ss.str();
//}

int AugmentedVideoBuffer::RemoveLowEnergyTrajectories(const double minEnergy) {
	unordered_set<Trajectory> survived;
	for(Trajectory t : trajectories){
		if(t.getNormalizedEnergy() >= minEnergy)
			survived.insert(t);
	}
	int removedItems = trajectories.size() - survived.size();
	trajectories = std::move(survived);
	return removedItems;
}

cv::Mat AugmentedVideoBuffer::getDisplayableFrame(int frameNumber) {

	cv::Mat theFrame=getFrame(frameNumber);
	Logger::Log(INFO, "theFrame is %s", (theFrame.empty())?"EMPTY": "OK");
	AugmentedVideoDraw displayFrame( theFrame );

	const std::vector<cv::Rect_<float> >& rois = this->getCurrentFrameInformation().getRegionsOfInterest();
	displayFrame.DrawRegionsOfInteresOnFrame(rois);

	const std::vector<cv::Point_<float> >& interesPoints=this->getCurrentFrameInformation().getPointsOfInterest();
	displayFrame.DrawInterestPointsOnFrame(interesPoints);

	const std::pair<std::unordered_multimap<int, Trajectory>::const_iterator,
	std::unordered_multimap<int, Trajectory>::const_iterator> range = this->getCurrentFrameTrajectoriesRange();

	std::unordered_set<Trajectory> toDeleteSet = displayFrame.DrawTrajectoriesOnFrame(
			range, this->getCurrentFrameNumber());
	Logger::Log(INFO, "displayFrame is %s", (displayFrame.getDisplayFrame().empty())? "EMPTY": "OK");
	return displayFrame.getDisplayFrame();
}

} /* namespace Video */
} /* namespace HAR */
