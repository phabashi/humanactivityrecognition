/*
 * MainExtractTrajectory3D.cpp
 *
 *  Created on: Nov 20, 2016
 *      Author: Dhawal Rank
 */

#include "ComputeMatches3D.h"
#include "ExtractFeatures3D.h"
#include "../../Video/TrajectoryEncoding/I3DEncoding.h"

std::string getEncodedTrajectories(
		std::vector<std::vector<const Trajectory*>> SurvivedTrajectoryL,
		std::vector<std::vector<const Trajectory*>> SurvivedTrajectoryR,
		Arguments args, cv::Size);

int main(int argc, char** argv) {
	Logger::getInstance().ChangeLevel(LogLevel::ERROR);
	Arguments args;
	try {
		args = Arguments(argc, argv);
	} catch (ArgumentParseException& exp) {
		Logger::Log(LogLevel::FATAL, "Error in parsing of the arguments!");
		args.printHelp();
		exit(1);
	}
	if (!GlobalSetting::LoadConfigurationFromIniFile(args.getApplicationSettingFileName())) {
		Logger::Log(LogLevel::FATAL, "Can not load the configuration file '%s'.", args.getApplicationSettingFileName().c_str());
		exit(1);
	} else {
		Logger::Log(LogLevel::INFO, "Setting File '%s' loaded successfully.", args.getApplicationSettingFileName().c_str());
	}
	int bufferSize = GlobalSetting::GetOptionAs<int>("Video", "BufferSize", -1); //-1 load whole video into the buffer
	bool isGrayscale = GlobalSetting::GetOptionAs<int>("Video", "isGrayscale", (args.getTrackingAlgorithm() == "FB"));
	if(args.IsMultiscaleEnabled()) {
		int n = args.getScale();
		if (n <= 1) {
			Logger::Log(LogLevel::FATAL, "You are running algorithm for multiscale and you should provide the number of scales with -s parameter.");
			exit(1);
		}
		float scale= args.getInitialScale();
		for(int i=0; i<n; i++) {
			AugmentedVideoBuffer leftVideo(args.getInputFileLeft(), bufferSize, isGrayscale);
			if (!leftVideo.isValid()) {
				Logger::Log(FATAL, "Can not open the requested Left Video file : '%s'", args.getInputFileLeft().c_str());
				exit(1);
			}
			AugmentedVideoBuffer rightVideo(args.getInputFileRight(), bufferSize, isGrayscale);
			if (!rightVideo.isValid()) {
				Logger::Log(FATAL, "Can not open the requested Right Video file : '%s'", args.getInputFileRight().c_str());
				exit(1);
			}
			leftVideo.enableResize(scale);
			rightVideo.enableResize(scale);

			scale *= 1/sqrt(2);
			ExtractFeatures3D::ExtractFeatures3D FeatureExtractor3D;
			FeatureExtractor3D.ExtractFeatures(leftVideo, rightVideo, args);
			ComputeMatches::ComputeMatches matchGenerator;
			matchGenerator.ComputeMatches3D(leftVideo, rightVideo, args);
			std::vector<std::vector<const Trajectory*>> SurvivedTrajectoryL = matchGenerator.getLeftTrajectories(), SurvivedTrajectoryR = matchGenerator.getRightTrajectories();
			if(args.getOutputTrajectoryFile() != ""){
				Logger::Log(VERBOSE, "Saving trajectories to file.");
				std::unique_ptr<ofstream> outfile = std::unique_ptr < ofstream
						> (new ofstream(args.getOutputTrajectoryFile(),
								std::ofstream::app));
				(*outfile) << getEncodedTrajectories(SurvivedTrajectoryL, SurvivedTrajectoryR, args,leftVideo.getFrameSize());
				outfile->close();
			}
			else{
				Logger::Log(VERBOSE, "Printing out the trajectories.");
				cout << getEncodedTrajectories(SurvivedTrajectoryL, SurvivedTrajectoryR, args, leftVideo.getFrameSize());
			}
			if(args.getOutputFeatureFile() != ""){
				Logger::Log(VERBOSE, "Saving Features to file.");
				std::unique_ptr<ofstream>outfile = std::unique_ptr<ofstream>(new ofstream(args.getOutputFeatureFile(),
						std::ofstream::app));
				std::vector<std::vector<cv::Point2f>> leftPoints = matchGenerator.getLeftMatchedPoints();
				std::vector<std::vector<cv::Point2f>> rightPoints = matchGenerator.getRightMatchedPoints();
				for(auto frameLeftPoints = leftPoints.begin(), frameRightPoints = rightPoints.begin();
						frameLeftPoints != leftPoints.end() && frameRightPoints != rightPoints.end(); ++frameLeftPoints, ++frameRightPoints){
					for(auto it = frameLeftPoints->begin(), it1= frameRightPoints->begin();
							it != frameLeftPoints->end() && it1 != frameRightPoints->end(); ++it, ++it1){
						(*outfile) << it->x << "," << it->y << "\n" <<it1->x << "," << it1->y <<"\n";
					}
				}
				outfile->close();
			}
		}
	} else { //Run program in single scale mode!
		AugmentedVideoBuffer leftVideo(args.getInputFileLeft(), bufferSize, isGrayscale);
		if (!leftVideo.isValid()) {
			Logger::Log(FATAL, "Can not open the requested Left Video file : '%s'", args.getInputFileLeft().c_str());
			exit(1);
		}
		AugmentedVideoBuffer rightVideo(args.getInputFileRight(), bufferSize, isGrayscale);
		if (!leftVideo.isValid()) {
			Logger::Log(FATAL, "Can not open the requested Right Video file : '%s'", args.getInputFileRight().c_str());
			exit(1);
		}
		leftVideo.enableResize(args.getScale());
		rightVideo.enableResize(args.getScale());
		ExtractFeatures3D::ExtractFeatures3D FeatureExtractor3D;
		FeatureExtractor3D.ExtractFeatures(leftVideo, rightVideo, args);
		ComputeMatches::ComputeMatches matchGenerator;
		matchGenerator.ComputeMatches3D(leftVideo, rightVideo, args);
		std::vector<std::vector<const Trajectory*>> SurvivedTrajectoryL = matchGenerator.getLeftTrajectories(), SurvivedTrajectoryR = matchGenerator.getRightTrajectories();
		if(args.getOutputTrajectoryFile() != ""){
			Logger::Log(VERBOSE, "Saving trajectories to file.");
			std::unique_ptr<ofstream>outfile = std::unique_ptr<ofstream>(new ofstream(args.getOutputTrajectoryFile()));
			(*outfile) << getEncodedTrajectories(SurvivedTrajectoryL, SurvivedTrajectoryR, args, leftVideo.getFrameSize());
			outfile->close();
		}
		else{
			Logger::Log(VERBOSE, "Printing out the trajectories.");
			cout << getEncodedTrajectories(SurvivedTrajectoryL, SurvivedTrajectoryR, args, leftVideo.getFrameSize());
		}
		if(args.getOutputFeatureFile() != ""){
			std::unique_ptr<ofstream>outfile = std::unique_ptr<ofstream>(new ofstream(args.getOutputFeatureFile()));
			std::vector<std::vector<cv::Point2f>> leftPoints = matchGenerator.getLeftMatchedPoints();
			std::vector<std::vector<cv::Point2f>> rightPoints = matchGenerator.getRightMatchedPoints();
			Logger::Log(VERBOSE, "Saving Features to file.");
			for(auto frameLeftPoints = leftPoints.begin(), frameRightPoints = rightPoints.begin();
					frameLeftPoints != leftPoints.end() && frameRightPoints != rightPoints.end(); ++frameLeftPoints, ++frameRightPoints){
				for(auto it = frameLeftPoints->begin(), it1= frameRightPoints->begin();
						it != frameLeftPoints->end() && it1 != frameRightPoints->end(); ++it, ++it1){
					(*outfile)  << it->x << "," << it->y << "\n" <<it1->x << "," << it1->y <<"\n";
				}
			}
			outfile->close();
		}
	}
	return 0;
}

std::string getEncodedTrajectories(
		std::vector<std::vector<const Trajectory*>> SurvivedTrajectoryL,
		std::vector<std::vector<const Trajectory*>> SurvivedTrajectoryR,
		Arguments args,
		cv::Size imgSize) {
	stringstream result;
//	TrajectoryEncoding::T3DEncoding encodingAlgorithm =
//			TrajectoryEncoding::I3DEncoding::get3DEncoding(
//					args.getTrajectoryEncoding());

	for (auto leftFramesTrajectory = SurvivedTrajectoryL.begin(), rightFrameTrajectories =
			SurvivedTrajectoryR.begin();
			leftFramesTrajectory != SurvivedTrajectoryL.end() && rightFrameTrajectories != SurvivedTrajectoryR.end();
			++leftFramesTrajectory, ++rightFrameTrajectories) {
		//We are entering new frame, update rectify information!
//		encodingAlgorithm->UpdateRectifyInformation(*it, *it1, imgSize);
		for (unsigned i = 0; i < leftFramesTrajectory->size(); ++i) {
			//output trajectories without any specific encoding into a file.
			result << ( (*leftFramesTrajectory)[i])->Serialize(&imgSize) << std::endl;
			result << ( (*rightFrameTrajectories)[i])->Serialize(&imgSize) << std::endl;
//			std::vector<cv::Point2f> leftPoints =
//					(it->data()[i])->getAllPoints();
//			std::vector<cv::Point2f> rightPoints =
//					(it1->data()[i])->getAllPoints();
//			std::stringstream ss;
//			auto vec = encodingAlgorithm->Combine3D(leftPoints, rightPoints);
//			if(vec.size() != 0){
//				for (auto it = vec.begin(); it != vec.end(); it++) {
//					ss << it->x << ", " << it->y << ", " << it->z << ", ";//<< "\n";
//				}
//				//ss << "\n";
//				result << ss.str().substr(0, ss.str().length() - 2) << "\n";
//			}
		}
	}
	return result.str();
}

