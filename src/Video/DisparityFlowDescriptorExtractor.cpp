/*
 * DisparityFlowDescriptorExtractor.cpp
 *
 *  Created on: Feb 12, 2015
 *      Author: Pejman
 *      Copyright (c) 2015 Pejman. All rights reserved.
 */

#include "DisparityFlowDescriptorExtractor.h"

#include "Display.h"
#include "../OpticalFlow/OpticalFlow.h"

#include <opencv2/nonfree/nonfree.hpp>

namespace HAR {
namespace Video {

DisparityFlowDescriptorExtractor::DisparityFlowDescriptorExtractor(
		StereoAugmentedVideo theSynchronizedVideo) :theVideo(theSynchronizedVideo)
{
	cv::initModule_features2d();
	cv::initModule_nonfree();
}

void DisparityFlowDescriptorExtractor::AddMicroMovementsToResult(
		std::vector<MyDescriptor>& micromovements,
		MyVideoDescrptor& result) {
	//TODO Save micromovements

}

void DisparityFlowDescriptorExtractor::InitializeMicromovements(
		unsigned long size, int startingFrame,
		std::vector<MyDescriptor>& micromovements) {
	// Start micor movements
	//	 = aFrame.getLeftFeaturePoints().getKeyPoints().size();
	micromovements.clear();
	micromovements.reserve(size);
	for (unsigned int i = 0; i < size; i++)
		micromovements.push_back(MyDescriptor(startingFrame));
}

void DisparityFlowDescriptorExtractor::UpdateMicroMovements(
		OpticalFlowInformation& flowInfoLeft,
		std::vector<MyDescriptor>& micromovements, MyVideoDescrptor& result) {
	//TODO RMEOVE THIS FUNCTION AS WELL!
}

void DisparityFlowDescriptorExtractor::AddPointsToMicromovement(
		const OpticalFlowInformation& flowInfoLeft,
		std::vector<MyDescriptor>& micromovements, StereoFrame& aFrame) {
	//TODO REMOVE THIS FUNCTION
}

MyVideoDescrptor DisparityFlowDescriptorExtractor::ExtractMicroMovementDescriptors (
		unsigned int frameLength, bool& videoReachedEnd,
		unsigned int startFrame , unsigned int length,
		std::string FeaturePointExtractionMethod,
		std::string FeaturePointDescriptionMethod,
		std::string FeaturePointMatchingAlgorithm,
		double theRatioToSelectBestMatches,
		bool removeHemogenousArea )
{
	//length == 0 means go until the end of file!

//	std::string FeaturePointExtractionMethod = "Dense"; // "Dense", "FAST", "SIFT", "SURF"
//	std::string FeaturePointDescriptionMethod = "SIFT"; // "Dense", "FAST", "SIFT", "SURF" ????
//	std::string FeaturePointMatchingAlgorithm = "BruteForce"; // "BruteForce", ????
//	double theRatioToSelectBestMatches = 0.95; // The ratio of distance between best and second best match in order to keep it

	std::vector<MyDescriptor> micromovements;
	MyVideoDescrptor result;
	videoReachedEnd = false;
	int key =0;
	bool pause = false;
	unsigned int frameCount = 0;
	StereoFrame aFrame, oldFrame;
	cv::Mat left, right;
	theVideo.getFrame(startFrame, left, right);//Set the start position and Synchronizes left and right frames
	aFrame = StereoFrame(left, right);
	//frameCount ++;
	OpticalFlow::OpticalFlow leftTracker(aFrame.getLeft());
	OpticalFlow::OpticalFlow rightTracker(aFrame.getRight());

	OpticalFlowInformation flowInfoLeft;
	OpticalFlowInformation flowInfoRight;

	//    flowInfo.getPreviousPoints().push_back(cv::Point2f(100,100));
	bool shouldRefreshPoints = true;
	StereoFrameMatches oldMatches;
	FrameFeaturePoints leftFP, rightFP;
	while(key != 27)
	{
		oldFrame = aFrame;
		if(! theVideo.getNextFrame(aFrame)){ videoReachedEnd = true; break; }
		frameCount ++;

		if(frameCount % frameLength == 0)
		{
			shouldRefreshPoints = true;
			//Save micromovements
			result.Add(micromovements);
			//            micromovements.clear(); ---> ????
		}

		if( shouldRefreshPoints)
		{
			aFrame.FindFeaturePoints(FeaturePointExtractionMethod);

			if(removeHemogenousArea)
				aFrame.RemoveHomogenousAreaKeypoints();

			aFrame.ExtractDescriptors(FeaturePointDescriptionMethod);
			aFrame.MatchDescriptors(FeaturePointMatchingAlgorithm);

			aFrame.KeepBestMatches(theRatioToSelectBestMatches);

			leftFP = aFrame.getLeftFeaturePoints();
			rightFP = aFrame.getRightFeaturePoints();
			oldMatches = aFrame.getStereoMatches();

			flowInfoLeft.setPointsForTracking(leftFP);
			flowInfoRight.setPointsForTracking(rightFP);
			//flowInfo.getPreviousPoints() = aFrame.getLeftFeaturePoints().CalculateAndGetLocations();
			shouldRefreshPoints = false;
			//Start Micro-movements
			int patchedStartingFrame = (frameCount == 1)? startFrame :  startFrame + frameCount;//To fix the first run!
			InitializeMicromovements(aFrame.getStereoMatches().getSize(), patchedStartingFrame, micromovements);
		}
		else{
			aFrame.setLeftFeaturePoints(oldFrame.getLeftFeaturePoints());
			aFrame.setRightFeaturePoints(oldFrame.getRightFeaturePoints());
			//            aFrame.setStereoMatches(oldFrame.getStereoMatches());
			aFrame.UpdateFeaturePoints(flowInfoLeft, flowInfoRight, oldMatches);

			//            aFrame.setLeftFeaturePoints(leftFP);
			//            aFrame.setRightFeaturePoints(rightFP);


			//            aFrame.setStereoMatches(oldMatches);

			//                    aFrame.setLeftFeaturePoints(FrameFeaturePoints( flowInfoLeft.getPreviousKeyPoints() ));
			//                    aFrame.setRightFeaturePoints(FrameFeaturePoints( flowInfoRight.getPreviousKeyPoints() ));
			//        			aFrame.setStereoMatches(oldMatches);

		}

		leftTracker.Update(aFrame.getLeft(), flowInfoLeft);
		rightTracker.Update(aFrame.getRight(), flowInfoRight);

		//        aFrame.UpdateFeaturePoints(FrameFeaturePoints( flowInfoLeft.getPreviousKeyPoints() ), FrameFeaturePoints( flowInfoRight.getPreviousKeyPoints() ));
		//        aFrame.ExtractDescriptors("SIFT");
		//        aFrame.MatchDescriptors("BruteForce");
		//
		//        aFrame.KeepBestMatches(0.84);

		aFrame.calculateDisparity();

		std::vector<cv::DMatch> stereoMatches = aFrame.getStereoMatches().getMatches();

		//        std::cout << "FlowKeypoinys: " << flowInfoLeft.getPreviousKeyPoints().size()
		//        << " DisparityPoints: " << aFrame.getDisparity().size()
		//        << " Micromovements size: " << micromovements.size()
		//        << " SteeoMatches size: " << stereoMatches.size()
		//        << std::endl;

		std::vector<MyDescriptor> survivedTracking;
		unsigned long size = micromovements.size();
		survivedTracking.clear();
		survivedTracking.reserve(size);
		for (unsigned int i = 0; i < size; i++) {
			if (!(flowInfoLeft.getStatus()[i])) {
				micromovements[i].Terminate();
				survivedTracking.push_back(micromovements[i]);
				//                result.Add(micromovements[i]);
			} else {

				cv::Point3f newLocation;
				//                newLocation.x = leftFP.getKeyPoints()[stereoMatches[i].trainIdx].pt.x;
				//                newLocation.y = leftFP.getKeyPoints()[stereoMatches[i].trainIdx].pt.y;

				//                newLocation.x = aFrame.getLeftFeaturePoints().getKeyPoints()[stereoMatches[i].trainIdx].pt.x;
				//                newLocation.y = aFrame.getLeftFeaturePoints().getKeyPoints()[stereoMatches[i].trainIdx].pt.y;
				newLocation.x = aFrame.getLeftFeaturePoints().getKeyPoints()[stereoMatches[i].queryIdx].pt.x;
				newLocation.y = aFrame.getLeftFeaturePoints().getKeyPoints()[stereoMatches[i].queryIdx].pt.y;

				//                flowInfoLeft.getPreviousKeyPoints()[stereoMatches[i].trainIdx].pt.y;
				//                stereoMatches
				newLocation.z = aFrame.getDisparity()[i];

				micromovements[i].Update(newLocation);

				survivedTracking.push_back(micromovements[i]);
			}
		}

		micromovements = survivedTracking;


		//        UpdateMicroMovements(flowInfoLeft, micromovements, result);
		bool displayFeatures = false;
		if(displayFeatures)
		{
			cv::Mat leftIm;
			cv::Mat rightIm;

			cv::cvtColor(aFrame.getLeft(), leftIm, CV_GRAY2RGB);
			cv::cvtColor(aFrame.getRight(), rightIm, CV_GRAY2RGB);

			Display::Show("L", leftIm, flowInfoLeft.getPreviousPoints());
			Display::Show("R", rightIm, flowInfoRight.getPreviousPoints());


			aFrame.calculateDisparityApproximationImage();
			Display::Show("Disparity", aFrame.getDisparityApproximationImage() );


			key = Display::waitKey(pause? 0:30);
			//        cout << "Key = " << key;
			switch (key)
			{
			case 63235: //RIGHT ARROW KEY
				//TODO Implement
				break;

			case 63234: //LEFT ARROW KEY
				//TODO Implement
				break;
			case 32://Space
				pause = !pause;
				break;
			case 27://Esc
				break;
			case 'r' :
				shouldRefreshPoints = true;
				break;

			default:
				//	                if(key>0)
				//	                    cout << "Unknown Key: " << key << endl;
				break;
			}
		}
		if(length!=0 and frameCount > length ) break;
	}

	return result;
}

MyVideoDescrptor DisparityFlowDescriptorExtractor::ExtractDescriptors(
		int frameLength)
{
	//	std::vector<MyDescriptor> micromovements;
	MyVideoDescrptor result;
	//	int key =0;
	//	bool pause = false;
	//	int frameCount = 0;
	//	StereoFrame aFrame;
	//
	//	theVideo.getNextFrame(aFrame);
	//	frameCount ++;
	//	OpticalFlow leftTracker(aFrame.getLeft());
	//	OpticalFlow rightTracker(aFrame.getRight());
	//
	//	OpticalFlowInformation flowInfoLeft;
	//	OpticalFlowInformation flowInfoRight;
	//
	//	//    flowInfo.getPreviousPoints().push_back(cv::Point2f(100,100));
	//	bool shouldRefreshPoints = true;
	//	while(key != 27)
	//	{
	//		if(! theVideo.getNextFrame(aFrame)) break;
	//		frameCount ++;
	//
	//		if(frameCount % frameLength == 0)
	//		{
	//			shouldRefreshPoints = true;
	//			//Save micromovements
	//			result.Add(micromovements);
	//            micromovements.clear();
	//		}
	//
	//		if( shouldRefreshPoints)
	//		{
	//			//            aFrame.FindFeaturePoints("Dense");
	//			aFrame.FindFeaturePoints("FAST");
	//			aFrame.RemoveHomogenousAreaKeypoints();
	//
	//            aFrame.ExtractDescriptors("SIFT");
	//            aFrame.MatchDescriptors("BruteForce");
	//
	//            aFrame.KeepBestMatches(0.84);
	//
	//			flowInfoLeft.setPointsForTracking(aFrame.getLeftFeaturePoints());
	//			flowInfoRight.setPointsForTracking(aFrame.getRightFeaturePoints());
	//			//            flowInfo.getPreviousPoints() = aFrame.getLeftFeaturePoints().CalculateAndGetLocations();
	//			shouldRefreshPoints = false;
	//			//TODO Start micor movements
	//			InitializeMicromovements(aFrame.getLeftFeaturePoints().getKeyPoints().size(), frameCount, micromovements);
	//		}
	//
	//		aFrame.setLeftFeaturePoints(FrameFeaturePoints( flowInfoLeft.getPreviousKeyPoints() ));
	//		aFrame.setRightFeaturePoints(FrameFeaturePoints( flowInfoRight.getPreviousKeyPoints() ));
	//
	//
	//
	//
	///*
	// * 		Some commened Code. not relevant
	//		        Vec2f matchMean = aFrame.calculateAllMatchesMean();
	//		        Vec2f matchVar = aFrame.calculateAllMatchesVar(matchMean);
	//
	//		        Vec2f leftKeypointMean = aFrame.getLeftFeaturePoints().CalculateMean();
	//		        Vec2f rightKeypointMean = aFrame.getRightFeaturePoints().CalculateMean();
	//
	//		        Vec2f leftKeypointVar = aFrame.getLeftFeaturePoints().CalculateVar(leftKeypointMean);
	//		        Vec2f rightKeypointVar = aFrame.getRightFeaturePoints().CalculateVar(rightKeypointMean);
	//
	//		        Vec2f deltaMean = rightKeypointMean - leftKeypointMean;
	//		        Vec2f deltaVar = Vec2f(1000, 100);//(rightKeypointVar + leftKeypointVar) /2;
	//
	//		        cout << "MM = " << matchMean << "MV = "<< matchVar << endl;
	//		        cout << "DM = " << deltaMean << "DV = "<< deltaVar << endl;
	//
	//		        aFrame.RemoveOutliersBasedOnTheirDistance(deltaMean, deltaVar, 1);
	//		        */
	///*
	// *
	//	Again some commented code
	//		        Display::Show("FrameMatches", aFrame, true);
	//		        cout << "Number of the survived matches = " << aFrame.getStereoMatches().getMatches().size() << endl;
	//*/
	//
	//        leftTracker.Update(aFrame.getLeft(), flowInfoLeft);
	//        rightTracker.Update(aFrame.getRight(), flowInfoRight);
	//
	//
	//        aFrame.calculateDisparity();
	////        AddPointsToMicromovement(flowInfoLeft, micromovements, aFrame);
	//
	//        std::map<long, cv::DMatch> stereoMatches;
	//        for (int i=0; i<aFrame.getStereoMatches().getSize();  i++) {
	//            cv::DMatch value = aFrame.getStereoMatches().getMatches(i);
	//            stereoMatches[value.trainIdx] = value ;
	//        }
	////        aFrame.getStereoMatches().getMatches();
	///*
	// *
	//        //        if (stereoMatches.size() != aFrame.getDisparity().size())
	////            std::cout << "Definitely something is going wrong!" << std::endl;
	////
	////        for (int i = 0; i < stereoMatches.size(); i++) {
	////            cv::Point3f newLocation;
	////            newLocation.x =
	////            flowInfoLeft.getPreviousKeyPoints()[stereoMatches[i].trainIdx].pt.x;
	////            newLocation.y =
	////            flowInfoLeft.getPreviousKeyPoints()[stereoMatches[i].trainIdx].pt.y;
	////            newLocation.z = aFrame.getDisparity()[i];
	////            micromovements[i].Update(newLocation);
	////        }
	//
	//
	////
	// */
	//
	//        std::cout << "FlowKeypoinys: " << flowInfoLeft.getPreviousKeyPoints().size()
	//            << " DisparityPoints: " << aFrame.getDisparity().size()
	//         << " Micromovements size: " << micromovements.size() << std::endl;
	//        std::vector<MyDescriptor> survivedTracking;
	//        unsigned long size = micromovements.size();
	//        survivedTracking.clear();
	//        survivedTracking.reserve(size);
	//        for (int i = 0; i < size; i++) {
	//            if (!(flowInfoLeft.getStatus()[i])) {
	//                result.Add(micromovements[i]);
	//            } else {
	//
	//                //TODO the i here
	//                cv::Point3f newLocation;
	//                newLocation.x =flowInfoLeft.getPreviousKeyPoints()[i].pt.x;
	////                flowInfoLeft.getPreviousKeyPoints()[stereoMatches[i].trainIdx].pt.x;
	//                newLocation.y = flowInfoLeft.getPreviousKeyPoints()[i].pt.y;
	////                flowInfoLeft.getPreviousKeyPoints()[stereoMatches[i].trainIdx].pt.y;
	////                stereoMatches
	//                newLocation.z = 0;                micromovements[i].Update(newLocation);
	//                //TODO may be different from the i here!!!
	//
	//                survivedTracking.push_back(micromovements[i]);
	//            }
	//        }
	//        micromovements = survivedTracking;
	//
	//
	////        UpdateMicroMovements(flowInfoLeft, micromovements, result);
	//
	//        Display::Show("L", aFrame.getLeft(), flowInfoLeft.getPreviousPoints());
	//        Display::Show("R", aFrame.getRight(), flowInfoRight.getPreviousPoints());
	//
	//
	//        aFrame.calculateDisparityApproximationImage();
	//		Display::Show("Disparity", aFrame.getDisparityApproximationImage() );
	//
	//
	//
	//		key = Display::waitKey(pause? 0:30);
	//		//        cout << "Key = " << key;
	//		switch (key)
	//		{
	//		case 63235: //RIGHT ARROW KEY
	//			//TODO Implement Fast forward in Video class
	//			for (int i=0; i<100; i++)
	//				{
	//					if(! theVideo.getNextFrame(aFrame)) break;
	//					frameCount ++;
	//				}
	//				break;
	//
	//		case 63234: //LEFT ARROW KEY
	//			//TODO Implement Fast Revind in Video class
	//			//TODO Implement
	//			break;
	//		case 32://Space
	//			pause = !pause;
	//			break;
	//		case 27://Esc
	//			break;
	//		case 'r' :
	//			shouldRefreshPoints = true;
	//			break;
	//
	//		default:
	//			//	                if(key>0)
	//			//	                    cout << "Unknown Key: " << key << endl;
	//			break;
	//			}
	//
	//	}
	//	//		cout << "Done!" << endl;
	//
	return result;
}

DisparityFlowDescriptorExtractor::~DisparityFlowDescriptorExtractor() {
	// TODO Auto-generated destructor stub
}

} /* namespace Video */
} /* namespace HAR */
