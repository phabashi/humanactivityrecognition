/*
 * Arguments.cpp
 *
 *  Created on: Mar 6, 2016
 *      Author: Pejman
 *      Copyright (c) 2016 Pejman. All rights reserved.
 */

#include "Arguments.h"
#include "../../util/util.h"
#include <iostream>
#include <string.h>
#include "../../Logger/Logger.h"

using namespace std;
using namespace HAR::Exceptions;

namespace HAR {
namespace NewExtractFeatures {

void Arguments::printHelp() {
	cout
			<< "Usage: ExtractFeatures -l <infileL> -r <inFileR> -o outFile=<outputFeatueFile> -fs frames -fl length " << endl
			<< "-l <inFileL>        : is the name of left input video file: (i.e. L.avi)" << endl
			<< "-r <inFileR>        : is the name of right input video file: (i.e. R.avi)" << endl
			<< "-o <outFileName>    : to save the output descriptors to a file."
			<< "-fs <frameSync>     : is the value of difference between frame 0 of right video and left video measured in the number of frames. (e.g. -4) (default = 0)" << endl
			<< "-dl <featureLength> : is the length of descriptors that we are willing to extract. (default = 9)" << endl
			<< "-ip <AlgorithmName> : The Interest point extractor algorithm name: (Allowed: 'FAST' 'SIFT' 'Dense' 'SURF' ???? ) (default = 'FAST') " << endl
			<< "-ipd <AlgorithmName>: The Interest point descriptor Algorithm name: (Allowed: ??? ) (default = 'SIFT') " << endl
			<< "-ma <AlgorithmName> : The matching algorithm name. (Allowed: ??? ) (default = 'BruteForce')" << endl
			<< "-ratio <ratio>      : The ratio of selecting best matches to second best matches between 0 and 1 (default = 0.84)." << endl
			<< "-s <value>          : The scale factor of input video, it is used to reduce the size of input video. (default = 1)." << endl
			<< "-rha [0,1]          : Either 0 or 1 and it specifies if the features in homogeneous areas should be removed (default 1 = true)" << endl
			<< "-ofa <Algorithm>    : The optical flow algorithm to use [default->FB (Farnback), IP (Interest Point), LK (Lucas-Kanade)]" << endl
			<< "-v                  : verbose " << endl
			//<< "-ne/-nd             : enable/disable output normalization. Normalization is disabled by default." << endl
//			<< "-t <trajectoryFN>   : Save Trajectories into trajectoryFN " << endl
			<< "-em/-dm             : enable/disable motion detection algorithm, if enabled, only the motion area will be used for making trajectories(enabled by default)." << endl
			<< "-minE <minenergy>	: A float value to indicate the minimum energy of trajectories" << endl
			<< "-multiscale	        : To enable multiscale Extraction mode instead of single scale mode" << endl
			<< "-te <encoding>      : Set the trajectory encoding algorithm, default is Off (No encoding). " << endl
			<< "-ss <samplingScale> : Sets the step number for sampling. 1 means sample every frame, 5 means sample one in five frame." << endl
			<< "-is <initialScale>  : Sets the initial scale value in multiscale version. Default is one." << endl
			<< "-h                  : Show this help." << endl

			<< endl;
}

Arguments::Arguments(int argc, char* argv[]) {

	inputFileLeft = "";
	inputFileRight = "";
	outputFeatureFile = "";
	frameSynchronizationDifference = 0;
	descriptorLength = 9;
	interestPointExtractor = "FAST";
	interestPointDescriptor = "SIFT";
	matchingAlgorithm = "BruteForce";
	theRatioToSelectBestMatches = 0.84;
	scale = 1.0;
	removeHemogenousArea = true;
	trackingAlgorithm = "FB";
	displayEnabled = false;
//	normalizeOutput = false;
	detectMotion = true;
	minEnergy = -1;
	isMultiscaleEnabled= false;
	trajectoryEncoding = "Off";
	samplingScale=1;
	initialScale=1;

	for (int i = 1; i < argc; i++) {
		if (strcmp(argv[i], "-v") == 0) {
			Pejman::Logger::Logger::getInstance().ChangeLevel(
					Pejman::Logger::LogLevel::VERBOSE);
			Pejman::Logger::Logger::Log(Pejman::Logger::LogLevel::VERBOSE,
					"The logger set to verbose!");
		} else if (strcmp(argv[i], "-l") == 0) {
			if (i < argc - 1) {
				inputFileLeft = argv[++i];
			} else {
				throw new ArgumentParseException(
						"Ill formated input: switch '-l' The left input file name is missing.");
			}
		} else if (strcmp(argv[i], "-r") == 0) {
			if (i < argc - 1) {
				inputFileRight = argv[++i];
			} else {
				throw new ArgumentParseException(
						"Ill formated input: switch '-r' The right input file name is missing.");
			}
		} else if (strcmp(argv[i], "-o") == 0) {
			if (i < argc - 1) {
				outputFeatureFile = argv[++i];
			} else {
				throw new ArgumentParseException(
						"Ill formated input: switch '-o' The ouput feature file name is missing.");
			}
		}
//		else if (strcmp(argv[i], "-t") == 0) {
//			if (i < argc - 1) {
//				trajectoryFileName = argv[++i];
//			} else {
//				throw new ArgumentParseException(
//						"Ill formated input: switch '-t' The output trajectory file name is missing.");
//			}
//		}
		else if (strcmp(argv[i], "-fs") == 0) {
			if (i < argc - 1) {
				frameSynchronizationDifference = stringTo<int>(argv[++i]);
			} else {
				throw new ArgumentParseException(
						"Ill formated input: switch '-fs' The frame synchronizatoin value is missing.");
			}
		} else if (strcmp(argv[i], "-dl") == 0) {
			if (i < argc - 1) {
				descriptorLength = stringTo<int>(argv[++i]);
			} else {
				throw new ArgumentParseException(
						"Ill formated input: switch '-dl' The descriptor length is missing.");
			}
		} else if (strcmp(argv[i], "-s") == 0) {
			if (i < argc - 1) {
				scale = stringTo<float>(argv[++i]);
			} else {
				throw new ArgumentParseException(
						"Ill formated input: switch '-s' The scale factor is missing.");
			}
		} else if (strcmp(argv[i], "-is") == 0) {
			if (i < argc - 1) {
				initialScale = stringTo<float>(argv[++i]);
			} else {
				throw new ArgumentParseException(
						"Ill formated input: switch '-is' The initial scale value is missing.");
			}
		} else if (strcmp(argv[i], "-ip") == 0) {
			if (i < argc - 1) {
				interestPointExtractor = argv[++i];
			} else {
				throw new ArgumentParseException(
						"Ill formated input: switch '-ip' The interest point extraction algorithm name is missing.");
			}
		} else if (strcmp(argv[i], "-ipd") == 0) {
			if (i < argc - 1) {
				interestPointDescriptor = argv[++i];
			} else {
				throw new ArgumentParseException(
						"Ill formated input: switch '-ipd' The name of interest point descriptor algorithm is missing.");
			}
		} else if (strcmp(argv[i], "-ma") == 0) {
			if (i < argc - 1) {
				matchingAlgorithm = argv[++i];
			} else {
				throw new ArgumentParseException(
						"Ill formated input: switch '-ma' The matching algorithm name is missing.");
			}
		} else if (strcmp(argv[i], "-ratio") == 0) {
			if (i < argc - 1) {
				theRatioToSelectBestMatches = stringTo<double>(argv[++i]);
			} else {
				throw new ArgumentParseException(
						"Ill formated input: switch '-ratio' The ratio value is missing.");
			}
		} else if (strcmp(argv[i], "-rha") == 0) {
			if (i < argc - 1) {
				removeHemogenousArea =
						(stringTo<int>(argv[++i]) == 1) ? true : false;
			} else {
				throw new ArgumentParseException(
						"Ill formated input: switch '-ratio' The ratio value is missing.");
			}
		} else if (strcmp(argv[i], "-minE") == 0) {
			if (i < argc - 1) {
				minEnergy = stringTo<float>(argv[++i]);
			} else {
				throw new ArgumentParseException(
						"Ill formated input: switch '-minE' minimum energy value is missing!");
			}
		} else if (strcmp(argv[i], "-ss") == 0) {
			if (i < argc - 1) {
				samplingScale = stringTo<int>(argv[++i]);
			} else {
				throw new ArgumentParseException(
						"Ill formated input: switch '-ss' the sampling scale value is missing!");
			}
		} else if (strcmp(argv[i], "-ofa") == 0) {
			if (i < argc - 1) {
				trackingAlgorithm = string(argv[++i]);
			} else {
				throw new ArgumentParseException(
						"Ill formated input: switch '-ofa' The optical flow algorithm name is missing.");
			}
		}
		// Use trajectory encoding instead of normalization enabling and disabling
//		else if (strcmp(argv[i], "-ne") == 0) {
//			normalizeOutput = true;
//		}
//		else if (strcmp(argv[i], "-nd") == 0) {
//			normalizeOutput = false;
//		}
		else if (strcmp(argv[i], "-em") == 0) {
			detectMotion = true;
		} else if (strcmp(argv[i], "-multiscale") == 0) {
			isMultiscaleEnabled = true;
		} else if (strcmp(argv[i], "-dm") == 0) {
			detectMotion = false;
		} else if (strcmp(argv[i], "as") == 0) {
			if (i < argc - 1) {
				applicationSettingFileName = string(argv[++i]);
			} else {
				throw new ArgumentParseException(
						"Ill formated input: switch '-ofa' The optical flow algorithm name is missing.");
			}
		} else if (strcmp(argv[i], "-te") == 0) {
			if (i < argc - 1) {
				trajectoryEncoding = string(argv[++i]);
			} else {
				throw new ArgumentParseException(
						"Ill formated input: switch '-te' The trajectory encoding name is missing.");
			}
		} else if (strcmp(argv[i], "-d") == 0) {
			displayEnabled = true;
		} else if (strcmp(argv[i], "-h") == 0) {
			printHelp();
			exit(1);
		} else {
			cout << "Unknown parameter ! " << argv[i] << endl << endl;
			printHelp();
			exit(1);
		}
	}
	//Check if minimum parameters have been provided:
	if (inputFileLeft == "")
		throw ArgumentParseException(
				"You should provide at least one input video file name using -l switch.");
//	if (inputFileRight == "")
//		throw ArgumentParseException(
//				"You should provide right input file name with -r.");
//	if (outputFeatureFile == "")
//		throw ArgumentParseException(
//				"You should provide output file name with -o.");
}

} /* namespace NewExtractFeatures */
} /* namespace HAR */
