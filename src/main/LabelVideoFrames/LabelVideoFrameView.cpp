//
//  LabelVideoFrameView.cpp
//  HARLinked
//
//  Created by pejman on 2015-07-08.
//  Copyright (c) 2015 pejman. All rights reserved.
//

#include "LabelVideoFrameView.h"

#include <ncurses.h>
#include "../../Video/Display.h"


LabelVideoView::LabelVideoView(LabelVideoModel& modelToDisplay):theModel(modelToDisplay)
{
    //initialize Screen
    initscr();
    raw();
    
    if(has_colors() == FALSE)
    {	endwin();
        printf("Your terminal does not support color\n");
        exit(1);
    }
    
    start_color();			/* Start color */
    init_pair(1, COLOR_WHITE, COLOR_BLUE);
    init_pair(2, COLOR_WHITE, COLOR_RED);
    init_pair(3, COLOR_WHITE, COLOR_GREEN);
    init_pair(4, COLOR_WHITE, COLOR_MAGENTA);
    init_pair(5, COLOR_WHITE, COLOR_CYAN);
    init_pair(6, COLOR_BLACK, COLOR_WHITE);
    getmaxyx(stdscr, height, width);
    
    
    errorMsgLocation = cv::Point2i(0, 0);
    
    frameNumberLocation = cv::Point2i(1, 1);
    fileNameLocation = cv::Point2i(1, 2);
    lastPressedKeyCodeLocation = cv::Point2i(1, 3);
    statusLocation = cv::Point2i(1, 4);
    scaleLocation = cv::Point2i(1, 5);
    
    informationWindow = create_newwin(10, 0.5*width, 0, 0.3*width);
    errorWindow = create_newwin(1, width, height-1, 0);
    int hWinHight = 7;
    helpWindow = create_newwin(hWinHight, 0.5 * width, height - hWinHight - 1, 0.3 * width);
    labelDefinitionsWindow = create_newwin(height-1, 0.3 * width, 0, 0);
    outputWindow = create_newwin(height -1, 0.2*width, 0, width - 0.2*width);
}

void* LabelVideoView::create_newwin(int height, int width, int starty, int startx)
{
    WINDOW* local_win;
    local_win = newwin(height, width, starty, startx);
    box(local_win, 0 , 0);		/* 0, 0 gives default characters
                                 * for the vertical and horizontal
                                 * lines			*/
    wrefresh(local_win);		/* Show that box 		*/
    
    return local_win;
}

void LabelVideoView::destroy_win(void* window)
{
    WINDOW *local_win = (WINDOW*) window;
    /* box(local_win, ' ', ' '); : This won't produce the desired
     * result of erasing the window. It will leave it's four corners
     * and so an ugly remnant of window.
     */
    wborder(local_win, ' ', ' ', ' ',' ',' ',' ',' ',' ');
    /* The parameters taken are
     * 1. win: the window on which to operate
     * 2. ls: character to be used for the left side of the window
     * 3. rs: character to be used for the right side of the window
     * 4. ts: character to be used for the top side of the window
     * 5. bs: character to be used for the bottom side of the window
     * 6. tl: character to be used for the top left corner of the window
     * 7. tr: character to be used for the top right corner of the window
     * 8. bl: character to be used for the bottom left corner of the window
     * 9. br: character to be used for the bottom right corner of the window
     */
    wrefresh(local_win);
    delwin(local_win);
}

void LabelVideoView::Refresh()
{
    printInformation();
    printHelp();
    printLabelDefinitions();
    printOutputLabels();
    printError();
    
    HAR::Video::Display::Show(theModel.getCurrentFrame(), theModel.getScale());
    
    refresh();
}

void LabelVideoView::printInformation()
{
    WINDOW* infoW = (WINDOW*)informationWindow;
    wattron(infoW, COLOR_PAIR(1));
    wbkgd(infoW, COLOR_PAIR(1));
    box(infoW, 0 , 0);
    const int w = 25;
    char format[10];
    sprintf(format, "%%-%ds", w);
    char buff[256];
    wmove(infoW, frameNumberLocation.y, frameNumberLocation.x);
    sprintf(buff, "Frame Number: %10d", theModel.getCurrentFrameNumber());
    wprintw(infoW,format, buff);
    
    wmove(infoW, fileNameLocation.y, fileNameLocation.x);
    sprintf(buff, "File: %s", theModel.getInputFileName().c_str());
    wprintw(infoW, buff);
    
    wmove(infoW, lastPressedKeyCodeLocation.y, lastPressedKeyCodeLocation.x);
    sprintf(buff, "Last Key Code : %8d", theModel.getLastPressedKeyCode());
    wprintw(infoW, buff);
    
    wmove(infoW, statusLocation.y, statusLocation.x);
    sprintf(buff, "Status : %15s", (theModel.isPlayingFinished())? "Finished" : (theModel.isPlayigPaused())? "Paused" : "Playing");
    wprintw(infoW, buff);
    
    wmove(infoW, scaleLocation.y, scaleLocation.x);
    sprintf(buff, "Scale : %16.2f", theModel.getScale());
    wprintw(infoW, buff);
    wrefresh(infoW);
    wattroff(infoW, COLOR_PAIR(1));
}

void LabelVideoView::printError()
{
    WINDOW* errWin= (WINDOW*) errorWindow;
    if(theModel.getLastErrorMsg() != "")
        wattron(errWin, COLOR_PAIR(2));
    wmove(errWin, errorMsgLocation.y, errorMsgLocation.x);
    char formatting[10];
    sprintf(formatting, "%%-%ds", width);
    wprintw(errWin, formatting, theModel.getLastErrorMsg().c_str());
    wrefresh(errWin);
    wattroff(errWin, COLOR_PAIR(2));
}

void LabelVideoView::printHelp()
{
    WINDOW* hWin = (WINDOW*) helpWindow;
    wbkgd(hWin, COLOR_PAIR(3));
    int i =1, j = 1;
//    wmove(hWin, i++, j);
    wmove(hWin, i++, j);
    wprintw(hWin, "'P' or Space      : Play/Pause the video" );
    wmove(hWin, i++, j);
    wprintw(hWin, "'<-' '<' or ','   : Rewind" );
    wmove(hWin, i++, j);
    wprintw(hWin, "'->' '>' or '.'   : Fast Forward"  );
    wmove(hWin, i++, j);
    wprintw(hWin, "'Up/Down Arrow'   : Change video size" );
    wmove(hWin, i++, j);
    wprintw(hWin, "%-50s", theModel.getKeyBindingHelp());
    
    wrefresh(hWin);
}

void LabelVideoView::printLabelDefinitions()
{
    WINDOW* labWin = (WINDOW*) labelDefinitionsWindow;
    wbkgd(labWin, COLOR_PAIR(4));
    
    uint i =1, x = 1;
    const std::vector< std::string> labelList = theModel.getLabelDefinitions();
    int maxx, maxy;
    getmaxyx(labWin, maxy, maxx);
    for (i = 0; i < labelList.size(); i++) {
        int y = (i% (maxy - 2)) +1;
        wmove(labWin, y, x);
        if(y == maxy - 2) x+= 12;
        wprintw(labWin, "%2d- %s", i, labelList[i].c_str() );
    }
    
    wrefresh(labWin);
}

void LabelVideoView::printOutputLabels()
{
    WINDOW* outWin = (WINDOW*) outputWindow;
    wbkgd(outWin, COLOR_PAIR(5));
    //wclear(outWin);
    const std::vector< std::string> labelList = theModel.getCurrentLabels();
    int maxx, maxy;
    getmaxyx(outWin, maxy, maxx);
    int start = ((int) labelList.size()) - maxy + 3 ;
    //wprintw(outWin, "m = %d, S= %d, s = %d", maxy ,(int)labelList.size(), start);
    if(start < 0) start = 0;
    int y = 1;
    for (uint i = start ; i< labelList.size(); i++) {
        wmove(outWin, y++, 1);
        wprintw(outWin, "%4d- %-15s", i, labelList[i].c_str());
    }
    
    for (;y < maxy - 2; y++) {
        wmove(outWin, y, 1);
        wprintw(outWin, "~ %-20s", "");
    }
    
    wrefresh(outWin);
}

std::string LabelVideoView::showStringInput(std::string message)
{
    WINDOW* ginW = (WINDOW *) create_newwin(5, 0.5 * width, 11, 0.3 * width);
    wbkgd(ginW, COLOR_PAIR(6));
    wmove(ginW, 1, 1);
    char value[100];
    wprintw(ginW, message.c_str());
    char temp[10];
    strcpy(temp, "%s");
    wscanw(ginW, temp, value);
    wrefresh(ginW);
    wclear(ginW);
    wbkgd(ginW, COLOR_PAIR(0));
    destroy_win(ginW);
    return std::string(value);
}

std::pair<int, int> LabelVideoView::showSetNewMapping()
{
    WINDOW* ginW = (WINDOW *) create_newwin(5, 0.5 * width, 11, 0.3 * width);
    wbkgd(ginW, COLOR_PAIR(6));
    wmove(ginW, 1, 1);
    int a,b;
    wprintw(ginW,"Enter numpad key:");
    wrefresh(ginW);
    a = getch();
    wprintw(ginW,"Enter label index:");
    wrefresh(ginW);
    char temp[10];
    strcpy(temp,"%d");
    wscanw(ginW, temp, &b);
    wclear(ginW);
    wbkgd(ginW, COLOR_PAIR(0));
    destroy_win(ginW);
    return std::pair<int, int>(a,b);
}

void LabelVideoView::showString(std::string message)
{
    WINDOW* ginW = (WINDOW *) create_newwin(5, 0.5 * width, 11, 0.3 * width);
    wbkgd(ginW, COLOR_PAIR(6));
    wmove(ginW, 1, 1);
    wprintw(ginW, message.c_str());
    wrefresh(ginW);
    getch();
    wclear(ginW);
    wbkgd(ginW, COLOR_PAIR(0));
    destroy_win(ginW);
}

LabelVideoView::~LabelVideoView()
{
    //Free Window Allocations
    destroy_win(informationWindow);
    destroy_win(errorWindow);
    destroy_win(helpWindow);
    destroy_win(labelDefinitionsWindow);
    destroy_win(outputWindow);
    
    //Shut down ncurses
    attroff(COLOR_PAIR(1));
    echo();
    //getch();
    endwin();
}
