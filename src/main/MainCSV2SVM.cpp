/*
 * MainCSV2SVM.cpp
 *
 *  Created on: Dec 23, 2015
 *      Author: Pejman
 *      Copyright (c) 2015 Pejman. All rights reserved.
 */
#include "../util/CommaSeparatedReader.h"

#include <iostream>
#include <string>
#include <opencv2/opencv.hpp>

#include "../util/util.h"
#include "../Exceptions/ArgumentParseException.h"
#include "../Logger/Logger.h"

using namespace std;
using namespace HAR;
using namespace cv;
using namespace HAR::Exceptions;
using namespace Pejman::Logger;

class Arguments{
public:
	Arguments() :
		inputTrainFileName(""),
		outputTrainFileName(""),
		inputTestFileName(""),
		outputTestFileName(""),
		isNormalizeRequired(false),
		isBinarizeRequired(false),
		inputNormalizationModelFileName(""),
		outputNormalizationModelFileName(""),
		labelDefinitionFileName("") { }

	Arguments(int argc, const char* argv[]);
	string getInputTrainFileName() { return inputTrainFileName; }
	string getOutputTrainFileName() { return outputTrainFileName; }
	string getInputTestFileName() { return inputTestFileName; }
	string getOutputTestFileName() { return outputTestFileName; }
	bool isNormalizeIsSet() { return isNormalizeRequired; }
	bool isBinarizeSet() { return isBinarizeRequired; }
	string getInputNormalizationFileName() { return inputNormalizationModelFileName; }
	string getOutputNormalizationFileName() { return outputNormalizationModelFileName; }
	string getLabelDefinitionFileName() { return labelDefinitionFileName; }

	void printHelp() {
		cout << "Usage : CSV2SVM -i <inputTrainFileName> <inputTestFileName> -o <outputTrainFileName> <outputTestFileName> -lab <labelFileName> [ -n | -b ]" << endl
				<< "    -n            To indicate Normalizing is needed                        " << endl
				//<< "    -b            To indicate Binarizing is needed                         " << endl
				//				 << "    -l <fileName> Load Normalization Parameters from file <fileName>       " << endl
				//				 << "    -s <fileName> Save Normalization Parameters to file <fileName>         " << endl
				<< endl
				<< "-v     : Verbose print out many more massages."
				<< " It assumes that the first column contains the class label (string) "
				"and the rest is simple float values" << endl;

	}
private:
	string inputTrainFileName;
	string outputTrainFileName;
	string inputTestFileName;
	string outputTestFileName;
	bool isNormalizeRequired;
	bool isBinarizeRequired;
	string inputNormalizationModelFileName;
	string outputNormalizationModelFileName;
	string labelDefinitionFileName;


};

void CalculateMinMax(const Mat& data, vector<float>& min, vector<float>& max);
void CalculateMeanVariance(const Mat& data, vector<float>& mean, vector<float>& var);
void NormalizeDataMinMax(Mat& data, const vector<float>& min, const vector<float>& max);
void NormalizeDataDiagonally(Mat& data, const vector<float>& mean, const vector<float>& stddev);
void BinarizeData(Mat& data, const vector<float>& min, const vector<float>& max);

int main(int argc,const char* argv[]){
	Logger::getInstance().ChangeLevel(LogLevel::ERROR);
	Arguments args;
	try{
		args = Arguments(argc, argv);
	}
	catch(ArgumentParseException& exp)
	{
		cerr << exp.what() << endl << endl;
		args.printHelp();
		exit(1);
	}

	Logger::Log(VERBOSE, "Readint the matrix from csv file.");
	Mat trainData = CommaSeparatedReader::ReadFloatLabeledMatrix(args.getInputTrainFileName(), args.getLabelDefinitionFileName());
	Mat testData = CommaSeparatedReader::ReadFloatLabeledMatrix(args.getInputTestFileName(), args.getLabelDefinitionFileName());

	if (args.isNormalizeIsSet()){

		Logger::Log(VERBOSE, "Normalizign the matrix.");
		vector<float> min, max;
		//vector<float> mean, var;

		CalculateMinMax(trainData, min, max);
		//CalculateMeanVariance(trainData, mean, var);

		//cout << "min = [ ";
		//for(uint i=1; i < min.size() ;i++)
		//	cout << min[i] << " ";
		//cout << "]" << endl;
		//cout << "max = [ ";
		//for(uint i=1; i < max.size() ;i++)
		//	cout << max[i] << " ";
		//cout << "]" << endl;

		//vector<float> stddev(var.size(),0);
		//for(uint i =0; i< var.size(); i++){
		//	stddev[i]=sqrt(var[i]);
		//}

		//BinarizeData(trainData, min, max);
		//BinarizeData(testData, min, max);

		NormalizeDataMinMax(trainData, min, max);
		NormalizeDataMinMax(testData, min, max);

		//NormalizeDataDiagonally(trainData, mean, stddev);
		//NormalizeDataDiagonally(testData, mean, stddev);
	}

	//if (args.isBinarizeSet()) BinarizeData(trainData);

	ofstream trainOutput(args.getOutputTrainFileName());
	ofstream testOutput(args.getOutputTestFileName());
	Logger::Log(VERBOSE, "Saving the content into file.");
	CommaSeparatedReader::WriteMatrixWithClassLabelInSVMFormat<float>(trainOutput, trainData);
	CommaSeparatedReader::WriteMatrixWithClassLabelInSVMFormat<float>(testOutput, testData);
	return 0;
}

void CalculateMinMax(const Mat& data, vector<float>& min, vector<float>& max){
	min.reserve(data.cols);
	max.reserve(data.cols);
	for (int i = 0; i < data.cols; i++) {
		min.push_back(data.at<float>(0, i));
		max.push_back(data.at<float>(0, i));
	}

	for(int i=0; i<data.cols; i++){
		for(int j =0; j< data.rows; j++){
			float a = data.at<float>(j,i);
			min[i] = ( min[i] < a )? min[i] : a ;
			max[i] = ( max[i] > a )? max[i] : a ;
		}
	}

}

void CalculateMeanVariance(const Mat& data, vector<float>& mean, vector<float>& var){
	mean.resize(data.cols);
	var.resize(data.cols);
	//	for (int i = 0; i < data.cols; i++) {
	//		min.push_back(data.at<float>(0, i));
	//		max.push_back(data.at<float>(0, i));
	//	}

	for(int i=0; i<data.cols; i++){
		float sum = 0;
		for(int j =0; j< data.rows; j++){
			sum += data.at<float>(j,i);
		}
		mean[i]= sum /data.rows;
	}

	for(int i=0; i<data.cols; i++){
		float sum=0;
		for(int j =0; j< data.rows; j++){
			float d = data.at<float>(j,i) - mean[i];
			sum += d*d;
		}
		var[i] = sum / data.rows;
	}
}

void NormalizeDataMinMax(Mat& data, const vector<float>& min, const vector<float>& max) {
	for(int i=0; i<data.cols; i++){
		for(int j =0; j< data.rows; j++){
			//the range is zero! there is not normalizations
			if(max[i] == min[i]){
				data.at<float>(j,i) = 0;
				continue;
			}
			float a = data.at<float>(j,i);
			if (i!=0)//i == 0 is the labels and should not be considered for normalization
				//data.at<float>(j,i) = (a - min[i]) / (max[i] - min[i]);
				data.at<float>(j,i) = 2 * ((a - min[i]) / (max[i] - min[i])) -1;
		}
	}
}

void NormalizeDataDiagonally(Mat& data, const vector<float>& mean, const vector<float>& stddev){
	for(int i=0; i<data.cols; i++){
		for(int j =0; j< data.rows; j++){
			//the range is zero! there is not normalizations
			if(stddev[i] == 0){//very unlikely!
				data.at<float>(j,i) = 0;
				continue;
			}
			float a = data.at<float>(j,i);
			if (i!=0)//i == 0 is the labels and should not be considered for normalization
				data.at<float>(j,i) = ((a - mean[i]) / stddev[i] + 1) /2;
			//data.at<float>(j,i) = 2*((a - min[i]) / (max[i] - min[i])) -1;
		}
	}
}

void BinarizeData(Mat& data, const vector<float>& min, const vector<float>& max) {
	for(int i=0; i<data.cols; i++){
		for(int j =0; j< data.rows; j++){
			//the range is zero! there is not normalizations
			float a = data.at<float>(j,i);
			if (i!=0){ //i == 0 is the labels and should not be considered for normalization
				int step = 2;
				float norm = step * ((a - min[i]) / (max[i] - min[i]));
				int n = (int) norm ;
				data.at<float>(j,i) = 2.0 * n / (step) - 1;

				//data.at<float>(j,i) = 2*((a - min[i]) / (max[i] - min[i])) -1;
			}
		}
	}

}

Arguments::Arguments(int argc, const char* argv[]){
	inputTrainFileName = outputTrainFileName = "";
	isNormalizeRequired = isBinarizeRequired = false;

	for(int i=1; i< argc; i++) {
		if( strcmp(argv[i], "-o") == 0) {
			if(i < argc-2){
				outputTrainFileName = argv[++i];
				outputTestFileName = argv[++i];
			} else {
				throw new ArgumentParseException("Ill formated input: switch '-o' The output file name is missing.");
			}
		} else if( strcmp(argv[i], "-i") == 0) {
			if(i < argc-2) {
				Logger::Log(INFO, "Setting input Training file name to %s", argv[i+1]);
				inputTrainFileName = argv[++i];
				inputTestFileName = argv[++i];
			} else {
				throw new ArgumentParseException( "Ill formated input: switch '-i' The input file name is missing." );
			}
		} else if( strcmp(argv[i], "-v") == 0) {
			Logger::getInstance().ChangeLevel(LogLevel::VERBOSE);
		} else if( strcmp(argv[i], "-n") == 0) {
			//Normalize
			if(isBinarizeRequired) {
				throw new ArgumentParseException("You can not both binarize and normalize data! run it is separate executions.");
			} else {
				isNormalizeRequired = true;
			}
		} else if( strcmp(argv[i], "-b") == 0 ) {
			//Binarize
			if(isNormalizeRequired) {
				throw new ArgumentParseException("You can not both binarize and normalize data! run it is separate executions.");
			} else {
				isBinarizeRequired = true;
			}
		} else if( strcmp(argv[i], "-l") == 0) {
			if(i < argc-1) {
				inputNormalizationModelFileName = argv[++i];
			} else {
				throw new ArgumentParseException( "Ill formated input: switch '-l' The input nomralization file name is missing." );
			}
		} else if( strcmp(argv[i], "-s") == 0) {
			if(i < argc-1) {
				outputNormalizationModelFileName = argv[++i];
			} else {
				throw new ArgumentParseException( "Ill formated input: switch '-s' The output normalizations file name is missing." );
			}
		}else if( strcmp(argv[i], "-lab") == 0) {
			if(i < argc-1) {
				labelDefinitionFileName = argv[++i];
			} else {
				throw new ArgumentParseException( "Ill formated input: switch '-lab' The label Definition file name is missing." );
			}
		} else if (strcmp (argv[i], "-h") == 0){
			printHelp();
			exit(1);
		}
	}
	//Check if minimum parameters have been provided:
	if(inputTrainFileName == "")
		throw ArgumentParseException("You should provide input file name with -i.");
	if (outputTrainFileName == "")
		throw ArgumentParseException("You should provide output file name with -o.");
	if(labelDefinitionFileName == "")
		throw ArgumentParseException("You should provide the label definition file name with -lab.");
}
