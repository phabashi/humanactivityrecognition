#!/bin/bash

#setting
settingFile="./SampleSetting.ini"
inputFileList="./lists/VideoFileList_Actor03_Activity_10_I5.txt"
cores=16

source ~/HAR/newScripts/default.bash

if [[ $detectMotion == "No" ]]; then
	motionOption="-dm"
	motionFolderName="Disabled"
else
	motionOption="-em"
	motionFolderName="Enabled"
fi
encoding="Cluster"

OffFolderName="./test-data/${motionFolderName}Motion/$opticalFlowAlgorithm/$interestPointAlgorithm/$descriptorAlgorithm/$matchingAlgorithm/scale_$scale/Off_Encoding/ss_${samplingScale}/"

featureFolderName="./test-data/${motionFolderName}Motion/$opticalFlowAlgorithm/$interestPointAlgorithm/$descriptorAlgorithm/$matchingAlgorithm/scale_$scale/${encoding}_Encoding/ss_${samplingScale}/"

mkdir -p $featureFolderName

leftVideoFileNames=(`cat $inputFileList`)

#for length in ${featureLengthes[@]};do

OffFeatureFileName="$OffFolderName/Length_$length/features"
completeFeatureFileName="$featureFolderName/Length_$length/features"
mkdir -p $completeFeatureFileName

fileNumber=0
for file in `ls $OffFeatureFileName`; do
	fileNumber=$(( fileNumber + 1 ))
	echo "$fileNumber : Counting the word for video $OffFeatureFileName/$file and save to $completeFeatureFileName/$file"
	cat "$OffFeatureFileName/$file" | ./bin/encode -e $encoding | ./bin/TrajectoryClassifier > "$completeFeatureFileName/$file" & 

	if (( $fileNumber % $cores == 0 )); then
		echo "Waiting for childs!"
		wait
	fi
done
