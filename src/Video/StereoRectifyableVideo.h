/*
 * StereoRectifyableVideo.h
 *
 *  Created on: Mar 5, 2017
 *      Author: pejman
 */

#ifndef SRC_VIDEO_STEREORECTIFYABLEVIDEO_H_
#define SRC_VIDEO_STEREORECTIFYABLEVIDEO_H_

#include "DisplayableVideo.h"
#include "AugmentedVideoBuffer.h"
#include "Trajectory3D.h"

namespace HAR {
namespace Video {

class StereoRectifyableVideo: public DisplayableVideo {
public:
	StereoRectifyableVideo(std::string leftFileName, std::string rightFileName);
	StereoRectifyableVideo(AugmentedVideoBuffer& left, AugmentedVideoBuffer& right);
	virtual ~StereoRectifyableVideo();

	cv::Mat getFrame(unsigned long frameNumber);
	virtual cv::Mat getDisplayableFrame(int frameNumber);
	virtual int getFramePerSecond() { return 0; }

	void UpdateRectifyInformation(int frameNumber,
			const std::vector<cv::Point2f>& leftPoints,
			const std::vector<cv::Point2f>& rightPoints,
			cv::Size imgSize);
	//use starting of trajectories for rectification : Not so good in tests!
	void RectifyVideo(std::vector<std::vector<Trajectory3D> > allTrajectories,
			int numberOfFrames, cv::Size imgSize);

	//Use all image interest points for revtification
	void RectifyVideo();

private:
	AugmentedVideoBuffer left;
	AugmentedVideoBuffer right;

	int frameApart;

	std::vector<cv::Mat> H1;
	std::vector<cv::Mat> H2;
	std::vector<bool> rectifiedCorrectly;
	std::vector<cv::Mat> fundamentalMatrix;
};

} /* namespace Video */
} /* namespace HAR */

#endif /* SRC_VIDEO_STEREORECTIFYABLEVIDEO_H_ */
