/*
 * LabelNotFoundException.h
 *
 *  Created on: Jan 14, 2016
 *      Author: Pejman
 *      Copyright (c) 2016 Pejman. All rights reserved.
 */

#ifndef SRC_EXCEPTIONS_LABELNOTFOUNDEXCEPTION_H_
#define SRC_EXCEPTIONS_LABELNOTFOUNDEXCEPTION_H_
#include <exception>
#include <string>

namespace HAR {

class LabelNotFoundException : public std::exception {
public:
	LabelNotFoundException(std::string msg) { label = msg; }
	virtual const char* what() const throw()
	{
		return ("Unknown label:  " + label ).c_str();
	}
	virtual ~LabelNotFoundException();

private:
	std::string label;
};

} /* namespace HAR */

#endif /* SRC_EXCEPTIONS_LABELNOTFOUNDEXCEPTION_H_ */
