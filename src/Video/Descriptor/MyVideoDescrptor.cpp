/*
 * MyVideoDescrptor.cpp
 *
 *  Created on: Feb 12, 2015
 *      Author: Pejman
 *      Copyright (c) 2015 Pejman. All rights reserved.
 */

#include "MyVideoDescrptor.h"

namespace HAR {

MyVideoDescrptor::MyVideoDescrptor() {
	// TODO Auto-generated constructor stub

}

void MyVideoDescrptor::Add(MyDescriptor newDescriptor)
{
	newDescriptor.Terminate();
	descriptors.push_back(newDescriptor);
}

MyVideoDescrptor::~MyVideoDescrptor() {
	// TODO Auto-generated destructor stub
}

void MyVideoDescrptor::Add(std::vector<MyDescriptor> newDescriptor)
{
	for (unsigned int i = 0; i < newDescriptor.size(); i++) {
		newDescriptor[i].Terminate();
			Add(newDescriptor[i]);
		}
}

    MyVideoDescrptor MyVideoDescrptor::filterOutLiers(double minEnergy, double maxEnergy, uint& desiredLength)
    {
    		/* desiredLength
    		 * -1 : Not need to remove Damaged Instances
    		 *  0 : Calculate maximum
    		 *  other: stick to it
    		 * */

    		bool findMax = (desiredLength == 0);

        MyVideoDescrptor result;
        unsigned long maxSize = 0;
        for (auto descriptor: descriptors)
        {
            auto theLength = descriptor.getLength();
            if(findMax)
            {
                if( theLength > maxSize)
                {
                    maxSize = theLength;
                    result.Clear();//To clear incomplete instances possibly added earlier
                    desiredLength = maxSize;
                }
            }
            
            if( desiredLength > 0 && theLength < desiredLength) continue;
            double descriptorEnergy = descriptor.CalculateEnergy();
            if( minEnergy >= 0 &&  descriptorEnergy < minEnergy)
                continue;
            if( maxEnergy >= 0 && descriptorEnergy > maxEnergy)
                continue;
            result.Add(descriptor);
        }
        return result;
    }
    
    void MyVideoDescrptor::Save(const std::string& fileName, bool overWright)
    {
        std::ofstream outFile(fileName, ( overWright ) ? std::ofstream::out : std::ofstream::app);
        for(unsigned int i =0; i< descriptors.size(); i++)
        {
            std::string r = descriptors[i].ToString();
            if(r != "")
                outFile << r << "\r\n" ;
        }
    }
    
    bool MyVideoDescrptor::Load(std::ifstream& inFile, int numberOfLinesToRead)
    {
    	descriptors.clear();
    	//std::ifstream inFile(fileName);
    	if(!inFile) return false;

    	std::string temp;
    	int numberOfLineRead = 0;
    	while (inFile && (numberOfLineRead < numberOfLinesToRead)) {
    		std::getline(inFile, temp);numberOfLineRead ++;
    		if( temp.find_first_of(',') == std::string::npos) continue;//empty line detected
    		MyDescriptor aDescriptor(temp);
    		descriptors.push_back(aDescriptor);
    	}

    	return (numberOfLineRead == numberOfLinesToRead)? true : false;
    }

    bool MyVideoDescrptor::Load(const std::string& fileName)
    {
        descriptors.clear();
        std::ifstream inFile(fileName);
        if(!inFile) return false;
        
        std::string temp;
        while (inFile) {
            std::getline(inFile, temp);
            if( temp.find_first_of(',') == std::string::npos) continue;//empty line detected
            MyDescriptor aDescriptor(temp);
            descriptors.push_back(aDescriptor);
        }
        
        return true;
    }
    
std::string MyVideoDescrptor::ToString()
{
	std::string temp = "[";
	for(unsigned int i =0; i< descriptors.size(); i++)
	{
        std::string r = descriptors[i].ToString();
        if(r != "")
            temp += r + ";" ;
	}
	temp += "] \n";
	return temp;
}
    
    void MyVideoDescrptor::SaveMicroMovements(std::string outputMicromovementFileName )
    {
        if(descriptors.size() == 0) return;//nothing to save!
        std::ofstream outFile(outputMicromovementFileName);
     
        outFile << descriptors[0].getHeader();
        for(unsigned int i =0; i< descriptors.size(); i++)
        {
            std::string r = descriptors[i].ToMicromovementString();
            if(r != "")
                outFile << r << "\r\n" ;
        }
    }



} /* namespace HAR */
