/*
 * FundamentalMatrixFinder.cpp
 *
 *  Created on: Mar 12, 2017
 *      Author: pejman
 */

#include "FundamentalMatrixFinder.h"

using namespace std;
using namespace cv;

namespace HAR {
namespace Video {

FundamentalMatrixFinder::FundamentalMatrixFinder() {
}

cv::Mat FundamentalMatrixFinder::findFundamentalMatrix(
		const std::vector<cv::Point2f>& leftImagePoints,
		const std::vector<cv::Point2f>& rightImagePoints,
		std::vector<uchar>& mask) {
	auto Tl = findTransformationForNomalization(leftImagePoints);
	auto Tr = findTransformationForNomalization(rightImagePoints);

	auto mappedPointsL = applyTransformation(leftImagePoints, Tl);
	auto mappedPointsR = applyTransformation(rightImagePoints, Tr);

	cout << "mpL size = " << mappedPointsL.size() << endl;
	cout << "mpR size = " << mappedPointsR.size() << endl;

	cout << " cv::findFundamentalMat()" << endl;cout.flush();
	cv::Mat normalizedFundamental = cv::findFundamentalMat(mappedPointsL,
			mappedPointsR, mask);
	cout << "done..." << endl;cout.flush();
	cv::Mat f = Tr.t() * normalizedFundamental * Tl;
	cout << "f = " << f << endl;
//	cin.get();
	return f;
}

FundamentalMatrixFinder::~FundamentalMatrixFinder() {
}

cv::Mat FundamentalMatrixFinder::findTransformationForNomalization(
		const std::vector<cv::Point2f>& points) {
	int numberOfPoints = points.size();
	cv::Scalar center = cv::mean(points);
	vector<Point2f> centralizedPoints(numberOfPoints);
	cv::subtract(points, center, centralizedPoints);

	double distance = 0;
	for (int i = 0; i < numberOfPoints; i++)
		distance += cv::norm(centralizedPoints[i]); //sqrt(cp.x^2 + cp.y^2)
	double meanDistance = distance / numberOfPoints;

	double scale = sqrt(2) / meanDistance;
	double initialValue[9] = { scale, 0, -scale * center.val[0], 0, scale,
			-scale * center.val[1], 0, 0, 1 };
	cv::Mat T = cv::Mat(3, 3, CV_64FC1, initialValue);
	cout << "Transformation = " << T << endl;
	return T.clone(); //important! unless the initialvalue memory used will be freed after this functoin!
}

cv::Mat FundamentalMatrixFinder::findFundamentalMatrix(
		const std::vector<cv::Point2f>& leftImagePoints,
		const std::vector<cv::Point2f>& rightImagePoints) {
	std::vector<uchar> mask;
	return findFundamentalMatrix(leftImagePoints, rightImagePoints, mask);
}

std::vector<cv::Point2f> FundamentalMatrixFinder::applyTransformation(
		const std::vector<cv::Point2f>& points, cv::Mat transformation) {

	cv::Mat input(points.size(), 3,CV_64FC1);
	for(int i =0; i < points.size(); i++){
		input.at<double>(i, 0) = points[i].x;
		input.at<double>(i, 1) = points[i].y;
		input.at<double>(i, 2) = 1;
	}
	cv::Mat output = (transformation * input.t()).t();
//	cv::Mat output = input * transformation;
//	cv::transform(input.t(), output, transformation);
	cout << "T ++ == " << transformation << endl;
	//cout << points.size() << " => " << output.size() << " under " << transformation.size() << ", ch = " << output.channels() << ", Under = " << transformation.size() << endl;

	std::vector<cv::Point2f> mappedPoints;
	for(int i =0; i < points.size(); i++){
//		cv::Point3f p(output.at<float>(i,0));
		cv::Point3f p(output.at<double>(i,0) , output.at<double>(i,1) , output.at<double>(i,2));
		//cout << " Salam Poin:  " << p  << ", va salam be " << output.at<double>(i,0) << " " <<output.at<double>(i,1) << " " << output.at<double>(i,2) << endl;
//		cin.get();
 		mappedPoints.push_back(
				cv::Point2f(p.x / p.z, p.y / p.z)
				);
	}

	return mappedPoints;

	//std::vector<cv::Point3f> homPoints;
	//cv::convertPointsToHomogeneous(points, homPoints);

	//int numberOfPoints = points.size();;
	//cv::Mat homPointsMatrix(numberOfPoints, 3, CV_64FC1);
	//for (int i = 0; i < numberOfPoints; i++) {
	//	homPointsMatrix.at<double>(i, 0) = homPoints[i].x;
	//	homPointsMatrix.at<double>(i, 1) = homPoints[i].y;
	//	homPointsMatrix.at<double>(i, 2) = homPoints[i].z;
	//}
	//
	//cv::Mat m = transformation * homPointsMatrix;

	//std::vector<cv::Point3f> transformedH(numberOfPoints);
	//for (int i = 0; i < numberOfPoints; i++) {
	//	cv::Point3f temp;
	//	if (m.at<double>(i, 2) != 0) {
	//		temp.x = m.at<double>(i, 0);
	//		temp.y = m.at<double>(i, 1);
	//		temp.z = m.at<double>(i, 2);
	//	}
	//	transformedH.push_back(temp);
	//}

	//std::vector<cv::Point2f> result(numberOfPoints);
	//cv::convertPointsFromHomogeneous(transformedH, result);
	//return result;
}

} /* namespace Video */
} /* namespace HAR */
