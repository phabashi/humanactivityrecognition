/*
 * IOpticalFlow.h
 *
 *  Created on: Apr 30, 2016
 *      Author: pejman
 */

#ifndef SRC_OPTICALFLOW_IOPTICALFLOW_H_
#define SRC_OPTICALFLOW_IOPTICALFLOW_H_

#include "../Video/AugmentedVideoBuffer.h"
#include <memory>

namespace HAR {
namespace Video {
namespace OpticalFlow {

class IOpticalFlow {
public:
	static std::unique_ptr<IOpticalFlow> getOpticalFlowAlgorithm(std::string algorithmName);

protected:
	IOpticalFlow();

public:
	virtual void ExtractOpticalFlow(AugmentedVideoBuffer& video, int length) = 0;

	virtual ~IOpticalFlow();
};

} /* namespace OpticalFlow */
} /* namespace Video */
} /* namespace HAR */

#endif /* SRC_OPTICALFLOW_IOPTICALFLOW_H_ */
