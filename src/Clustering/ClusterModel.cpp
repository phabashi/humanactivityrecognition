/*
 * ClusterModel.cpp
 *
 *  Created on: Nov 17, 2015
 *      Author: Pejman
 *      Copyright (c) 2015 Pejman. All rights reserved.
 */

#include "ClusterModel.h"
#include <iostream>
#include "../Logger/Logger.h"

using namespace std;
using namespace Pejman::Logger;

namespace HAR {

ClusterModel::ClusterModel(cv::Mat centers) {
	this->centers = centers;
}

cv::Mat ClusterModel::CountTheWords(cv::Mat samples) const {
	int numberOfClasses = centers.rows;
	cv::Mat result(1, numberOfClasses, CV_32SC1);
	result = cv::Scalar(0);
	#pragma omp parallel for
	for(int r=0; r<samples.rows; r++){
		//cout << "Row " << r << endl;
		int cluster =  this->getCluster(samples.row(r));
		#pragma omp atomic
		result.at<int>(0, cluster) ++;
	}
	//#pragma omp parallel
	//{
	//	int a[numberOfClasses];//private for each thread
	//	for(int i=0; i < numberOfClasses; i++)
	//		a[i] = 0;
	//	#pragma omp for //openmp parallelism
	//	for(int r=0; r<samples.rows; r++){
	//		//cout << "Row " << r << endl;
	//		int cluster =  this->getCluster(samples.row(r));
	//		#pragma omp atomic
	//		a[cluster] ++;
	//	}

	//	#pragma omp critical //openmp parallelism
	//	for(int i=0; i<numberOfClasses; i++)
	//		result.at<int>(0, i) += a[i];
			//accumulate all counts
	//}
	return result;
}

//double ClusterModel::euclideanDistance(const cv::Mat& a, const cv::Mat& b) const {
//	double sum =0;
//	if(a.cols != b.cols || a.rows != 1 || b.rows != 1)
//		throw ArgumentParseException("A and B should be row vectors of the same size");
//	//cout << " COLS = " << a.cols << " b.cols =  " << b.cols<< endl;
//	//cout << "Comparing A with B" << endl;
//	//cout << "A= " << a << endl;
//	//cout << "B= " << b << endl;
//	//cout << "BB= [ ";
//	//for(int i=0; i<b.cols; i++)
//	//	cout << b.at<float>(0,i) << ", ";
//	//cout << " ] " << endl;
//	for(int i=0; i<a.cols; i++){
//		//cout << " i = " << i << endl;
//		/*if( i == 60) {
//			cout << " i = " << i << endl;
//			cout <<" a[i] = " << a.at<float>(0,i) << endl;
//			cout << " b.cols = " << b.cols << endl;
//			cout << " b[i] = " << endl;
//			cout << b.at<float>(0,i) << endl;
//		}*/
//		double x = a.at<float>(0,i) - b.at<float>(0,i);
//		sum += x * x;
//	}
//	//cout << " Return " << endl;
//	return sqrt(sum);
//}

ClusterModel::~ClusterModel() {
	// TODO Auto-generated destructor stub
}

} /* namespace HAR */
