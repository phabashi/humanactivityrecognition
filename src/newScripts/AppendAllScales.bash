#!/bin/bash

#This is a script to run and append all different scale data into one file

echo "This script will go trough all the clips in ./clips folder, search for Left vidoe and run extract featues on it and its corresponding right vido. "

#ls clips/ | grep -f ./lists/Actor_set3.txt | grep -f ./lists/ActivitySet_15.txt | grep Left

#setting
settingFile="./SampleSetting.ini"
inputFileList="./lists/VideoFileList_Actor03_Activity_10_I5.txt"
# Sample making file List:
# inputFileList=`ls ./clips/ | grep -e "Left" | grep -f ./lists/ActivitySet_10.txt | grep -f ./lists/Actor_set3.txt | grep -f ./lists/Instances_5.txt > ./lists/VideoFileList_Actor03_Activity_10_I5.txt` 
opticalFlowAlgorithm=${OFA:-"IP"}
interestPointAlgorithm="FAST"
descriptorAlgorithm="SIFT"
normalizeOutput=${Norm:-"Yes"}
detectMotion=${DM:-"No"}
ratio="1"
scale=${scale:-"1"} #scale the original video to half size in each direction

if [[ $detectMotion == "No" ]]; then
	motionOption="-dm"
	motionFolderName="Disabled"
else
	motionOption="-em"
	motionFolderName="Enabled"
fi
normalized="-nd"
foldn="Not"
if [[ $normalizeOutput == "Yes" ]]; then
	normalized="-ne"
	foldn=""
fi

featureFolderName="./test-data/${motionFolderName}Motion/$opticalFlowAlgorithm/$interestPointAlgorithm/$descriptorAlgorithm/$matchingAlgorithm/scale_$scale/${foldn}Normalized"

mkdir -p $featureFolderName

leftVideoFileNames=(`cat $inputFileList`)

length=${length:-11}
completeFeatureFileName="$featureFolderName/Length_$length/features"
mkdir -p $completeFeatureFileName

count=${#leftVideoFileNames[@]}
fileNumber="1"
for i in ${leftVideoFileNames[@]}; do
	#ExtractFeatures $i &
	fileNumber=$(( $fileNumber + 1 ))
	#echo "Running $fileNumber/ $count = [$(( fileNumber * 100 / count ))%]"
	leftVideo="./clips/$i"
	rightVideo="./clips/${i/Left/Right}"
	name=${i/_Left}
	name="${name/avi/fet}"
	featureFileName="$completeFeatureFileName/$name"
	AllScaleFeatureFileName="${featureFileName/Scale_$scale/Scale_ALL}"
	if [ -e $featureFileName ]; then
		echo "File Exsists, skipping $featureFileName..."
	else
		echo "[$(( fileNumber * 100 / count ))%] Extracting features of '$leftVideo' with lenght $length and save it to '$featureFileName'"
		./bin/newExtractFeatures -l $leftVideo -o $featureFileName -dl $length -ip $interestPointAlgorithm -ipd $descriptorAlgorithm -ofa $opticalFlowAlgorithm -s $scale $normalized $motionOption as $settingFile
		if [[ $? != 0 ]]; then
			echo "Some error occured! Exiting "
			exit 1
		fi
	fi
	cat $featureFileName >> $AllScaleFeatureFileName;	
done

