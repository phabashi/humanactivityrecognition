#!/bin/bash

echo "This script will go trough all the clips in ./clips folder, search for Left vidoe and run extract featues on it and its corresponding right vido. "

#ls clips/ | grep -f ./lists/Actor_set3.txt | grep -f ./lists/ActivitySet_15.txt | grep Left

#setting
settingFile="./SampleSetting.ini"
inputFileList="./lists/VideoFileList_Actor03_Activity_10_I5.txt"
# Sample making file List:
# inputFileList=`ls ./clips/ | grep -e "Left" | grep -f ./lists/ActivitySet_10.txt | grep -f ./lists/Actor_set3.txt | grep -f ./lists/Instances_5.txt > ./lists/VideoFileList_Actor03_Activity_10_I5.txt` 
cores=7

source ~/HAR/newScripts/default.bash

if [[ $detectMotion == "No" ]]; then
	motionOption="-dm"
	motionFolderName="Disabled"
else
	motionOption="-em"
	motionFolderName="Enabled"
fi
encoding="Cluster"
#encoding="NormDiff"
#normalized="-nd"
#foldn="Not"
#if [[ $normalizeOutput == "Yes" ]]; then
#	normalized="-ne"
#	foldn=""
#fi

featureFolderName="./test-data/${motionFolderName}Motion/$opticalFlowAlgorithm/$interestPointAlgorithm/$descriptorAlgorithm/$matchingAlgorithm/scale_$scale/${encoding}_Encoding/ss_${samplingScale}/"

mkdir -p $featureFolderName

leftVideoFileNames=(`cat $inputFileList`)

#for length in ${featureLengthes[@]};do

completeFeatureFileName="$featureFolderName/Length_$length/features"
mkdir -p $completeFeatureFileName

count=${#leftVideoFileNames[@]}
fileNumber="1"
for i in ${leftVideoFileNames[@]}; do
	#ExtractFeatures $i &
	fileNumber=$(( $fileNumber + 1 ))
	#echo "Running $fileNumber/ $count = [$(( fileNumber * 100 / count ))%]"
	leftVideo="./clips/$i"
	rightVideo="./clips/${i/Left/Right}"
	name=${i/_Left}
	name="${name/avi/fet}"
	featureFileName="$completeFeatureFileName/$name"
	if [ -e $featureFileName ]; then
		echo "File Exsists, skipping $featureFileName..."
	else
		echo "[$(( fileNumber * 100 / count ))%] Extracting features of '$leftVideo' with lenght $length and save it to '$featureFileName'"
		./bin/newExtractFeatures -l $leftVideo -dl $length -ip $interestPointAlgorithm -ipd $descriptorAlgorithm -ofa $opticalFlowAlgorithm -s $scale $MULTISCALE -te $encoding $motionOption -minE $minEnergy -ss $samplingScale as $settingFile | ./bin/TrajectoryClassifier >  $featureFileName & 
		if [[ $? != 0 ]]; then
			echo "Some error occured! Exiting "
			exit 1
		fi
	fi
	#sleep 1 &
	#echo $fileNumber &
	#wait %1
	if (( $fileNumber % $cores == 0 )); then
		echo "Waiting for childs!"
		wait
	fi
done
#done
