/*
 * MotionDetection.cpp
 *
 *  Created on: Jan 30, 2016
 *      Author: Pejman
 *      Copyright (c) 2016 Pejman. All rights reserved.
 */

#include "MotionDetection.h"
#include "../../Video/Display.h"
#include <iostream>
#include "../../Logger/Logger.h"
#include "../../Pejman/GlobalSetting.h"
#include "RectangleSet.h"
using namespace Pejman::Application;
using namespace Pejman::Logger;
namespace HAR {
namespace Video {
namespace Motion {

MotionDetection::MotionDetection() {
	int numberOfMixtures = GlobalSetting::GetOptionAs<int>("MotionDetection",
			"NumberOfMixtures", 2);

	int numberOfShadowDetection = GlobalSetting::GetOptionAs<int>(
			"MotionDetection", "numberOfShadowDetection", 0);
	bg.set("nmixtures", numberOfMixtures);
	bg.set("nShadowDetection", numberOfShadowDetection);
}

void MotionDetection::DetectMotion(AugmentedVideoBuffer& video) {
	cv::Rect mean;
	video.Reset();

	//TODO This function should not decide on closeness! Move the following definition to the outside of this function
	double closeness = GlobalSetting::GetOptionAs<double>("MotionDetection",
			"Closeness", 50.0 / 400);

	for (int frameNumber = 0;; frameNumber++) {

		frame = video.getNextFrame();

		if (frame.empty())
			break;
		bg.operator()(frame, fore);
		bg.getBackgroundImage(back);
		cv::erode(fore, fore, cv::Mat());
		cv::dilate(fore, fore, cv::Mat(), cv::Point(-1, -1), 1);
		//	      imshow("fore", fore);
		cv::findContours(fore, contours, CV_RETR_EXTERNAL,
				CV_CHAIN_APPROX_NONE);

		RectangleSet rects;
		for (auto contour = contours.begin(); contour != contours.end();
				contour++) {
			cv::Rect temp = cv::boundingRect(*contour);
			rects.AddRectangle(temp);
		}

		rects.MergeCloseRectangles(closeness * frame.rows);

		auto finalRects = rects.getFinalRectangles();

		video.getFrameInformation(frameNumber).ClearRegionsOfInterest();
		for (auto r = finalRects.begin(); r != finalRects.end(); r++) {
			video.getFrameInformation(frameNumber).AddToRegionsOFInterest(*r);
		}
	}
}

MotionDetection::~MotionDetection() {
	// TODO Auto-generated destructor stub
}

} /* namespace Motion */
} /* namespace Video */
} /* namespace HAR */
