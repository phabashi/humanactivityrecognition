/*
 * StereoRectifyableVideo.cpp
 *
 *  Created on: Mar 5, 2017
 *      Author: pejman
 */

#include "StereoRectifyableVideo.h"

#include "../Logger/Logger.h"
#include <iostream>
#include "Display.h"

#include "VideoRectifier.h"
#include "FundamentalMatrixFinder.h"

using namespace Pejman::Logger;
using namespace std;
using HAR::Video::Display;

namespace HAR {
namespace Video {

StereoRectifyableVideo::StereoRectifyableVideo(AugmentedVideoBuffer& left, AugmentedVideoBuffer& right) :
		left(left),
		right(right),
		frameApart(0) {

	int n = std::max(left.getNumberOfFrames(), right.getNumberOfFrames());
	H1 = std::vector<cv::Mat>(n);
	H2 = std::vector<cv::Mat>(n);
	rectifiedCorrectly = std::vector<bool>(n, false);
	fundamentalMatrix = std::vector<cv::Mat>(n);
}

StereoRectifyableVideo::~StereoRectifyableVideo() {
	// TODO Auto-generated destructor stub
}

cv::Mat StereoRectifyableVideo::getFrame(unsigned long frameNumber) {
	Logger::Log(LogLevel::VERBOSE,
			" StereoAugmentedVideo::getFrame(unsigned long frameNumber)");
	cv::Mat nextLeftFrame;
	cv::Mat nextRightFrame;
	if (frameApart < 0) {
		nextLeftFrame = left.getFrame(frameNumber - frameApart);
		nextRightFrame = right.getFrame(frameNumber);
	} else {
		nextLeftFrame = left.getFrame(frameNumber);
		nextRightFrame = right.getFrame(frameNumber + frameApart);
	}

	cv::Size size(nextLeftFrame.cols, nextLeftFrame.rows);

	if (rectifiedCorrectly.size() > frameNumber) {
		if (rectifiedCorrectly[frameNumber]) {
			Logger::Log(LogLevel::INFO,
					"Rectified ! Try to unwarp the image! image size = (%d, %d)",
					size.height, size.width);
			cv::warpPerspective(nextLeftFrame, nextLeftFrame, H1[frameNumber],
					size);
			cv::warpPerspective(nextRightFrame, nextRightFrame, H2[frameNumber],
					size);

			Logger::Log(LogLevel::INFO, "Draw epipolar lines!");
			Display::DrawEpipolarLines(nextLeftFrame,
					fundamentalMatrix[frameNumber]);
			cv::Mat fprime = fundamentalMatrix[frameNumber].t();
			Display::DrawEpipolarLines(nextRightFrame, fprime);
			Logger::Log(LogLevel::INFO, "WE ARE BACK");

			Logger::Log(LogLevel::INFO, "Printing Hs");
			cout << "H1 = " << H1[frameNumber] << "\nH2 = " << H2[frameNumber]
					<< endl;
		}
	}
	return Display::Combine(nextLeftFrame, nextRightFrame);
}

cv::Mat StereoRectifyableVideo::getDisplayableFrame(int frameNumber) {
	cv::Mat theFrame = getFrame(frameNumber);
	Logger::Log(INFO, "theFrame is %s", (theFrame.empty()) ? "EMPTY" : "OK");
	AugmentedVideoDraw displayFrame(theFrame);

	Logger::Log(INFO, "displayFrame is %s",
			(displayFrame.getDisplayFrame().empty()) ? "EMPTY" : "OK");
	return displayFrame.getDisplayFrame();
}

StereoRectifyableVideo::StereoRectifyableVideo(std::string leftFileName,
		std::string rightFileName) :
		left(AugmentedVideoBuffer(leftFileName)),
		right(AugmentedVideoBuffer(rightFileName)),
		frameApart(0) {
	int n = left.getNumberOfFrames();
	H1 = std::vector<cv::Mat>(n);
	H2 = std::vector<cv::Mat>(n);
	rectifiedCorrectly = std::vector<bool>(n);
	fundamentalMatrix = std::vector<cv::Mat>(n);

	for(int i=0; i< n; i++)
		rectifiedCorrectly[i] = false;
}

void StereoRectifyableVideo::UpdateRectifyInformation(int frameNumber,
		const std::vector<cv::Point2f>& leftPoints,
		const std::vector<cv::Point2f>& rightPoints, cv::Size imgSize) {

	Logger::Log(LogLevel::VERBOSE, "ls = %d, rs = %d", leftPoints.size(),
			rightPoints.size());

	if (leftPoints.size() < 8) {
		rectifiedCorrectly[frameNumber] = false;
		return;
	}
	//if ( rectifiedCorrectly[frameNumber] ) return;//if it is already rectified
	Logger::Log(LogLevel::VERBOSE,
			"Rectifying frame %d with image size = (%d, %d)", frameNumber,
			imgSize.height, imgSize.width);

	fundamentalMatrix[frameNumber] = FundamentalMatrixFinder::findFundamentalMatrix(leftPoints, rightPoints);
			//cv::findFundamentalMat(leftPoints, rightPoints, CV_FM_RANSAC_ONLY, 3, 0.99);
	//TODO correctMatches() Improve the location by using fundamental matrix!
	rectifiedCorrectly[frameNumber] = cv::stereoRectifyUncalibrated(leftPoints,
			rightPoints, fundamentalMatrix[frameNumber], imgSize,
			H1[frameNumber], H2[frameNumber]);
	cout << "Points Left =  [ ";
	 for(int i=0; i< leftPoints.size(); i++){
	 cout << "[ "<< leftPoints[i].x << "," << leftPoints[i].y <<", 1 ], ";
	 }
	 cout << "]" << endl;

	 cout << "Points Right =  [ ";
	 for(int i=0; i< rightPoints.size(); i++){
	 cout << "[ "<< rightPoints[i].x << "," << rightPoints[i].y << ", 1 ], ";
	 }
	 cout << "]" << endl;
	 cout << "H1["<< frameNumber <<"] = " << H1[frameNumber] << endl;
	 cout << "H2["<< frameNumber <<"] = " << H2[frameNumber] << endl;
	 cout << "F["<< frameNumber <<"] = " << fundamentalMatrix[frameNumber] << endl;
	 cout << "imgSize=" << imgSize << endl;
	 //exit(1);
}

//void StereoRectifyableVideo::setRectificationInformation(
//		const std::vector<bool> * rectifiedCorrectly,
//		const std::vector<cv::Mat>* H1,
//		const std::vector<cv::Mat>* H2, const std::vector<cv::Mat>* F) {
//	this->rectifiedCorrectly = rectifiedCorrectly;
//	this->H1 = H1;
//	this->H2 = H2;
//	this->F = F;
//}

void StereoRectifyableVideo::RectifyVideo(
		vector<vector<Trajectory3D> > allTrajectories, int numberOfFrames,
		cv::Size imgSize) {
	fundamentalMatrix = std::vector<cv::Mat>(numberOfFrames, cv::Mat());
	H1 = std::vector<cv::Mat>(numberOfFrames, cv::Mat::eye(3,3,CV_64FC1));
	H2 = std::vector<cv::Mat>(numberOfFrames, cv::Mat::eye(3,3,CV_64FC1));
	rectifiedCorrectly = std::vector<bool>(numberOfFrames, false);

	for (int frameNumber = 0; frameNumber < (int) allTrajectories.size();
			frameNumber++) {

		if (allTrajectories[frameNumber].size() < 30) {
			Logger::Log(LogLevel::WARNING,
					"Not enough correspondence points to calculate rectification!"
							" We only have %d point correspondence.",
					allTrajectories[frameNumber].size());
			//TODO We are ignoring these trajectories for now,
			//we might try to encode these trajectories based
			//on a nearby frame fundamental matrix
			continue;
		}
		std::vector<cv::Point2f> leftPoints;
		std::vector<cv::Point2f> rightPoints;
		leftPoints.reserve(allTrajectories[frameNumber].size());
		rightPoints.reserve(allTrajectories[frameNumber].size());
		for (int i = 0; i < (int) allTrajectories[frameNumber].size(); i++) {
			cv::Point2f lp = allTrajectories[frameNumber][i].getLeftFP();
			cv::Point2f rp = allTrajectories[frameNumber][i].getRightFP();
			lp.x = lp.x * imgSize.width;
			lp.y = lp.y * imgSize.height;
			rp.x = rp.x * imgSize.width;
			rp.y = rp.y * imgSize.height;
			leftPoints.push_back(lp);
			rightPoints.push_back(rp);
		}

		int r = rand() % leftPoints.size();
		Logger::Log(LogLevel::VERBOSE,
				"leftpoint Sample: (%f, %f) right point sample: (%f, %f)",
				leftPoints[r].x * imgSize.width,
				leftPoints[r].y * imgSize.height,
				rightPoints[r].x * imgSize.width,
				rightPoints[r].y * imgSize.height);

		UpdateRectifyInformation(frameNumber, leftPoints, rightPoints, imgSize);
	}
}

void StereoRectifyableVideo::RectifyVideo() {
	//initiailize rectification parameters

	int numberOfFrames = std::min(left.getNumberOfFrames(),
			right.getNumberOfFrames());
	int width = left.getWidth();
	int height = left.getHeight();

	Logger::Log(LogLevel::VERBOSE, "DAHANET SEERVICE: (%d, %d)", width, height);
	fundamentalMatrix = std::vector<cv::Mat>(numberOfFrames, cv::Mat());
	H1 = std::vector<cv::Mat>(numberOfFrames, cv::Mat());
	H2 = std::vector<cv::Mat>(numberOfFrames, cv::Mat());
	rectifiedCorrectly = std::vector<bool>(numberOfFrames, false);

	VideoRectifier videoRectifier(left, right);
	videoRectifier.ExtractFeaturePoints();
	videoRectifier.MatchFeaturePoints();

	cv::Size size(width, height);
	for (int i = 0; i < numberOfFrames; i++) {
		UpdateRectifyInformation(i, videoRectifier.getLeftPoints(i),
				videoRectifier.getRightPoints(i), size);
	}
}

} /* namespace Video */
} /* namespace HAR */
