#!/bin/bash

extractor="./xbin/DenseTrack"
clipsFolder='./clips'
featureFolder='./featuresx'

if (( $# == 0 )); then
	echo 'You should provide the file list.'
	echo "Usage: $0 fileNameList"
	exit 1
fi

len=$( more $1 | wc -l )
lineNumber=0
if [ ! -z $2 ]; then
	lineNumber=$2
	echo "Starting from $2 line among $len lines. Emmiting the previous lines."
	len=`expr $len - $2`
	echo "$len liens left to be prossesed."
fi

list=(`more $1 | tail -n $len`)

for fileName in ${list[@]}; do
	lineNumber=`expr $lineNumber + 1`
	echo -n "$lineNumber: Extracting feature from $fileName. "

	$extractor $clipsFolder/$fileName > $featureFolder/${fileName/avi/txt}
		
	echo "Done."
done
