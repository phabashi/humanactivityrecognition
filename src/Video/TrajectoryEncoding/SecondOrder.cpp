/*
 * SecondOrder.cpp
 *
 *  Created on: Sep 25, 2016
 *      Author: Pejman
 *      Copyright (c) 2016 Pejman. All rights reserved.
 */

#include "SecondOrder.h"

#include "../Trajectory.h"

namespace HAR {
namespace Video {
namespace TrajectoryEncoding {

SecondOrder::SecondOrder() {
	// TODO Auto-generated constructor stub

}

std::vector<float> SecondOrder::encode(const std::vector<cv::Point2f>& points) const {
	std::vector<float> result;
	std::vector<cv::Point2f> first;
	result.reserve((points.size()- 1) *4);

	float m = Trajectory::getEnergy(points);

	if (m == 0)
		return result;

	for (uint i = 1; i < points.size(); i++) {//first order
		cv::Point2f thePoint=points[i] - points[i - 1];
		result.push_back(thePoint.x);
		result.push_back(thePoint.y);
		first.push_back(thePoint);
	}

	for (uint i = 1; i < first.size(); i++) {//second order
		cv::Point2f thePoint = first[i]-first[i-1];
		result.push_back(thePoint.x);
		result.push_back(thePoint.y);
	}
	return result;
}

SecondOrder::~SecondOrder() {
	// TODO Auto-generated destructor stub
}

} /* namespace TrajectoryEncoding */
} /* namespace Video */
} /* namespace HAR */
