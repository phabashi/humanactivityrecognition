/*
 * mainReadMicromovements.cpp
 *
 *  Created on: Oct 31, 2015
 *      Author: Pejman
 *      Copyright (c) 2015 Pejman. All rights reserved.
 */


#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <sstream>

using namespace std;

int main(int argc, char* argv[])
{
	if (argc != 2) { cout << "Usage: " << argv[0] << " <inputFileName>" << endl; exit (1); }

	int i,j;
	i = j = 0;
	ifstream inputCSV(argv[1]);
	string line;
	getline( inputCSV, line);
	istringstream theLineInput(line);
	int N, D;
	N = D = 0;//unknown for now
	while(theLineInput)
	{
		string t;
		theLineInput >> t;
		// through away t
		j++;
	}
	D = j - 1;// IGUESS we have counted the end line character

	vector<string> theContent;
	while(inputCSV)
	{
		getline(inputCSV, line);
		istringstream content(line);
		ostringstream out;
		for(j=0; j<D; j++)
		{
			float v;
			string s;
			getline( content, s, ',');
			stringstream t(s);
			t>>v;
			out << v << "\t";
		}

		theContent.push_back(out.str());
		i++;
	}
	N =  i - 1;

	cout << N << " " << D << endl;
	for(auto it=theContent.begin(); it != theContent.end(); it ++)
		cout << *it << endl;
	return 0;
}


