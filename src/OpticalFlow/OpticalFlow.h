/*
 * OptocalFlow.h
 *
 *  Created on: Feb 8, 2015
 *      Author: Pejman
 *      Copyright (c) 2015 Pejman. All rights reserved.
 */

#ifndef OPTICALFLOW_H_
#define OPTICALFLOW_H_

#include <opencv2/opencv.hpp>

#include "NewOpticalFlowInformation.h"
#include "OpticalFlowInformation.h"
#include "../util/HashUtils.hpp"
#include "../Video/AugmentedVideoBuffer.h"


namespace HAR{
namespace Video{
namespace OpticalFlow{

class OpticalFlow {
public:
	OpticalFlow(cv::Mat statringImage);
	void Update(cv::Mat newImage, OpticalFlowInformation& info);
	void Update(cv::Mat newImage, NewOpticalFlowInformation& info);
	virtual ~OpticalFlow();

public:
	static void ExtractOpticalFlowOfInterestPointsFarnBack(AugmentedVideoBuffer& video, int length);
	static void ExtractOpticalFlowOfInteresPointsLK(AugmentedVideoBuffer& video, int length);
	static void ExtractOpticalFlowOfInteresPointsBasedOnDescriptors(AugmentedVideoBuffer& video, int length);

private:
	cv::Mat preImage;
};

} /* namespace OpticalFlow */
} /* namespace Video */
} /* namespace HAR */

#endif /* OPTICALFLOW_H_ */
