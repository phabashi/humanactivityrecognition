#!/bin/bash

if [ $# != 1 ]; then
	echo "Usage 1: $0 <EncodedFeatureFolderName> " 
	echo "OR"
	echo "Usage 2: $0 <inputFifoName> " 
	exit 1
fi

if [ -d $1 ]; then
	inputFolderName="$1"
else
	if [ -e $1 ]; then
		inputFolderName=`cat $1`
	else
		mkfifo $1
		inputFolderName=`cat $1`
	fi
fi


if [ ! -d $inputFolderName ]; then
	echo "Directory does not exists: $inputFolderName"
	exit 2
fi


#setting

#export LD_LIBRARY_PATH=~/git/humanactivityrecognition/HAR/:~/Development/vlfeat-0.9.20/bin/glnxa64/
source ~/HAR/newScripts/default.bash

activityFileName="$inputFolderName/Activities.txt"

SVMLearningParameters="-s 0 -t 0 -c 100 -q"

featureFolderName="$inputFolderName/features/"
inputFileList=`ls $featureFolderName`

ActivityList=`echo $inputFileList | tr " " "\n" | cut -f 7 -d "_" | sort | uniq`

echo $ActivityList | tr " " "\n" > $activityFileName

Actors=`echo $inputFileList | tr " " "\n" | cut -f 1 -d "_" | sort | uniq `

folderPrefix="./$inputFolderName/learning/n-${numberOfWords}-words"

csvDataFolder="./$folderPrefix/data/csv"
SVMDataFolder="./$folderPrefix/data/svm"
SVMModelFolder="./$folderPrefix/models/svm"
sampleFolder="./$folderPrefix/data/sample"
resultFolder="./$folderPrefix/results/"

mkdir -p $csvDataFolder
mkdir -p $SVMDataFolder
mkdir -p $SVMModelFolder
mkdir -p $sampleFolder
mkdir -p $resultFolder

i=0
sample="$sampleFolder/sampleFile.txt"
allFilesEncoded="$sampleFolder/AllFilesEncoded.txt"
if [ ! -f $sample ]; then
	echo "Creating a sample from all instances. Saving int to $sample"
	echo $inputFileList | tr " " "\n" | ./bin/Merge -f $featureFolderName -s $numberOfWords > $sample
else
	echo "The sample file \"$sample\" already existed, skipping..."	
fi
if [ ! -f $allFilesEncoded ]; then
	echo "Encoding all videos using vlfeat."
	echo "$inputFileList" | tr " " "\n" | ./bin/encodeVLfeat -f $featureFolderName -p 6 -sample $sample -nc $numberOfWords -g $allFilesEncoded -lab $activityFileName -d " "
fi
exit 0
for Actor in ${Actors[@]}; do
	i=$(( i+1 ))

	echo "Do fold $i for actor $Actor"
	
	trainingSet=`echo $inputFileList | tr " " "\n" | grep -v $Actor`
	testingSet=`echo $inputFileList | tr " " "\n" | grep $Actor`

	sampleFileName="$sampleFolder/sampleFile.txt"
	csvTrainFile="$csvDataFolder/Fold-$Actor.csv.train"
	csvTestFile="$csvDataFolder/Fold-$Actor.csv.test"
	svmTrainFile="$SVMDataFolder/Fold-$Actor.svm.train"
	svmTestFile="$SVMDataFolder/Fold-$Actor.svm.test"
	svmModelFileName="$SVMModelFolder/Fold-$Actor.svm.model"
	svmOutputFile="$SVMDataFolder/Fold-$Actor.svm.output"

	#make a KMeans model for this fold
	if [ ! -f $sampleFileName ]; then
		echo "Sampling for  $Actor"
		echo $trainingSet | tr ' ' '\n'  | ./bin/Merge -f $featureFolderName -s $numberOfWords | tr " " " " > $sampleFileName

		if [ $? != 0 ] ; then
			exit 1
		fi
	else
		echo "The sample file Exist! Skipping the sampling part!"
		echo 
		echo
		echo "Exists: $sampleFileName"
	fi

	#Count The Words for this fold
	if [[ ! -f $svmTrainFile || ! -f $svmTestFile ]]; then
		echo "encoding feature vectors of fold $i, using encodeVLFeat..."
		
		echo "TrainSamples $trainingSet TestSamples $testingSet" | tr ' ' '\n' | ./bin/encodeVLfeat -f $featureFolderName -p 5 -sample $sampleFileName -nc $numberOfWords -strain $svmTrainFile -stest $svmTestFile -lab $activityFileName -d " "
		if [ $? != 0 ] ; then
			exit 2
		fi 
	fi

	#Make a svm-model for the current data
	if [ ! -f $svmModelFileName ]; then
		echo "Making SVM Model for the fold $i"
		./svmbin/svm-train $SVMLearningParameters $svmTrainFile $svmModelFileName
		if [ $? != 0 ] ; then
			exit 5
		fi 
	fi

	#Measure the accuracy of svm-model for calssification
	if [ ! -f $svmOutputFile ]; then
		echo "Measuring the accuracy for fold $i"
		./svmbin/svm-predict -q $svmTestFile $svmModelFileName $svmOutputFile
		if [ $? != 0 ] ; then
			exit 6
		fi 
	fi 

done

echo "Making Report: "

expectedResult="$SVMDataFolder/all.expected"
outputResult="$SVMDataFolder/all.actual"

rm -f $expectedResult
rm -f $outputResult
j=0

makeReportForFold()
{
	i=$1
  j=$(( j + 1 ))

	expected="$resultFolder/Fold-$i.expected"
	aquired="$resultFolder/Fold-$i-out"
	svmTestFile="$SVMDataFolder/Fold-$i.svm.test"
	svmOutputFile="$SVMDataFolder/Fold-$i.svm.output"

	#echo "Making report for fold $i :"
	cat $svmTestFile | cut -d " " -f 1 > $expected
	cp $svmOutputFile $aquired
	cat $expected >> $expectedResult
	cat $aquired >> $outputResult
}

for i in ${Actors[@]}; do makeReportForFold "$i" ; done

date >> report.txt
echo "$folderPrefix" >> report.txt
./bin/MakeReport -e $expectedResult -o $outputResult -lab $activityFileName >> report.txt
echo ------------------------------------------------------------------ >> report.txt
