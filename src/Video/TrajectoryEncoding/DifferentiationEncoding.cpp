/*
 * DifferentiationEncoding.cpp
 *
 *  Created on: May 31, 2016
 *      Author: pejman
 */

#include "DifferentiationEncoding.h"

namespace HAR {
namespace Video {
namespace TrajectoryEncoding {

DifferentiationEncoding::DifferentiationEncoding() {
	// TODO Auto-generated constructor stub

}

std::vector<float> DifferentiationEncoding::encode(const std::vector<cv::Point2f>& points) const {
	std::vector<float> result;
	result.reserve((points.size()- 1) *2);
	for (uint i = 1; i < points.size(); i++) {
		result.push_back(points[i].x - points[i - 1].x);
		result.push_back(points[i].y - points[i - 1].y);
	}
	return result;
}

DifferentiationEncoding::~DifferentiationEncoding() {
	// TODO Auto-generated destructor stub
}

} /* namespace TrajectoryEncoding */
} /* namespace Video */
} /* namespace HAR */
