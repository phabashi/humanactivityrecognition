/*
 * FeatureDescriptorExtractor.cpp
 *
 *  Created on: Jan 26, 2015
 *      Author: Pejman
 *      Copyright (c) 2015 Pejman. All rights reserved.
 */

#include "FeatureDescriptorExtractor.h"
#include "../Logger/Logger.h"
using namespace Pejman::Logger;

namespace HAR {
namespace Video {
namespace Features {
std::unordered_map<std::string, cv::Ptr<FeatureDescriptorExtractor> > FeatureDescriptorExtractor::instances;

FeatureDescriptorExtractor& FeatureDescriptorExtractor::getInstance(
		std::string featureType)
{
	if(instances.find(featureType) == instances.end())
	{
		instances[featureType] = new FeatureDescriptorExtractor();
		instances[featureType]->descriptorExtractor = cv::DescriptorExtractor::create(featureType);
		if(instances[featureType]->descriptorExtractor == NULL){
			std::cout<<"Can not initialize Feature Descriptor type: " << featureType <<std::endl;}
//		assert(instances[featureType]->descriptorExtractor != NULL);
		instances[featureType]->featureType = featureType;
	}
	return (*instances[featureType]);
}

void FeatureDescriptorExtractor::removeInstances()
{
//	for(auto temp = instances.begin(); temp!= instances.end(); temp++)
//		delete (*temp).second;
	instances.clear();
}

FeatureDescriptorExtractor::FeatureDescriptorExtractor() {
	// TODO Auto-generated constructor stub

}

FeatureDescriptorExtractor::~FeatureDescriptorExtractor() {
	// TODO Auto-generated destructor stub
}

FeatureDescriptorExtractor::FeatureDescriptorExtractor(
		FeatureDescriptorExtractor& other) {
	this->featureType = other.featureType;
	this->descriptorExtractor = other.descriptorExtractor;
}

FrameFeatureDescriptors FeatureDescriptorExtractor::ExtractDescriptorsForFrame(
		const cv::Mat image, const FrameFeaturePoints& featurePoints)
{
    if(image.empty()) {
    	Logger::Log(ERROR, "The provided image is empty! returning empty frame feature descriptor.");
    	return FrameFeatureDescriptors();
    }
	std::vector<cv::KeyPoint> keypoints(featurePoints.getKeyPoints().begin(), featurePoints.getKeyPoints().end());
	cv::Mat descriptors;
	descriptorExtractor->compute(image, keypoints, descriptors);
	int interestPoints = featurePoints.getKeyPoints().size();
	int rejectedPoints = keypoints.size();
	int accepted = descriptors.rows;

	Logger::Log(INFO, "From %d keypoints %d were rejected %d accepted. (%6.2f%%)", interestPoints,rejectedPoints, accepted, (100.0f * accepted) /interestPoints);
	FrameFeatureDescriptors result(descriptors);
	return result;
}

void FeatureDescriptorExtractor::ExtractDescriptors(
		AugmentedVideoBuffer& inputVideo) {
	inputVideo.getCurrentFrameInformation().ClearPointsOfInterest();
	for( cv::Mat frame = inputVideo.getFrame(0); !frame.empty(); frame = inputVideo.getNextFrame()){
		std::vector<cv::KeyPoint> keyPoints = inputVideo.getCurrentFrameInformation().getKeypoints();
		cv::Mat descriptors;
		descriptorExtractor->compute(frame, keyPoints, descriptors);
		Logger::Log(LogLevel::INFO, "Keypoint size= %d,  dSize = %d , %d", keyPoints.size() ,descriptors.rows, descriptors.cols);
		inputVideo.getCurrentFrameInformation().setDescriptors(descriptors);
	}
}

FeatureDescriptorExtractor& FeatureDescriptorExtractor::operator=(
		FeatureDescriptorExtractor& other)
{
	this->featureType = other.featureType;
	this->instances = other.instances;
    return *this;
}

} /* namespace Features */
} /* namespace Video */
} /* namespace HAR */
