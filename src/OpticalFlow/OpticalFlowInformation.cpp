/*
 * OpticalFlowInformation.cpp
 *
 *  Created on: Feb 8, 2015
 *      Author: Pejman
 *      Copyright (c) 2015 Pejman. All rights reserved.
 */

#include "OpticalFlowInformation.h"

namespace HAR {

OpticalFlowInformation::OpticalFlowInformation() {
	// TODO Auto-generated constructor stub

}

void OpticalFlowInformation::ReplaceOldPointsWithNewOnes(
		std::vector<cv::Point2f>& nextFeaturePoints, std::vector<uchar>& status,
		const std::vector<float>& error) {
	nextKeyPoints = previousKeyPoints;
	this->nextPoints = nextFeaturePoints;
	this->status = status;
	this->error = error;
	previousKeyPoints.clear();
	previousPoints.clear();
	currentMapping.clear();
	std::vector<int> origIndex;
	origIndex.reserve(originalMapping.size());
	int j = 0;
	for (unsigned int i = 0; i < nextPoints.size(); i++)
		if (status[i]) {
			nextKeyPoints[i].pt = nextPoints[i];
			previousKeyPoints.push_back(nextKeyPoints[i]);
			previousPoints.push_back(nextPoints[i]);
			origIndex.push_back(originalMapping[i]);
			currentMapping[i] = j++;
		}
	originalMapping = origIndex;
//    previousKeyPoints = nextKeyPoints;
}

void OpticalFlowInformation::setPointsForTracking(
		const FrameFeaturePoints& featurePoints) {
	previousKeyPoints = featurePoints.getKeyPoints();
	previousPoints = featurePoints.CalculateAndGetLocations();

	originalMapping.clear();
	originalMapping.reserve(featurePoints.getKeyPoints().size());
	for (unsigned int i = 0; i < featurePoints.getKeyPoints().size(); i++)
		originalMapping.push_back(i);

	//this->originalMapping = originalIndex;
}

void OpticalFlowInformation::setPointsForTracking(
		const FrameFeaturePoints& featurePoints,
		std::vector<int> originalIndex) {
	previousKeyPoints = featurePoints.getKeyPoints();
	previousPoints = featurePoints.CalculateAndGetLocations();

	this->originalMapping = originalIndex;
}

OpticalFlowInformation::~OpticalFlowInformation() {
	// TODO Auto-generated destructor stub
}

int OpticalFlowInformation::getNewIndex(unsigned int oldIndex) const {
	auto item = currentMapping.find(oldIndex);
	if (item != currentMapping.end())
		return item->second;
	else
		return -1;
}

int OpticalFlowInformation::getOriginialIndex(unsigned int newIndex) const {
	if (newIndex >= originalMapping.size())
		return -1;
	else
		return originalMapping[newIndex];
}

} /* namespace HAR */

