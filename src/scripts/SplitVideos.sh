#!/bin/bash

echo "This script will split the videos into different clips"
echo "You should already synchronized the video and put the result in info/Syncronization.txt"

names=($( more "./info/Synchronization.txt" | cut -d " " -f 1 ) )
delays=($( more "./info/Synchronization.txt" | cut -d " " -f 2 ) )


for ((i=0; i< ${#names[@]};i++)); do
	leftVideo="./video/${names[i]}_Left.MOV"
	rightVideo="./video/${names[i]}_Right.MOV"
	labelFile="./lab/${names[i]}_Left.Lab"

	echo "Spliting $leftVideo with $labelFile at offset 0"
	./bin/SplitVideo $leftVideo $labelFile 0

	echo "Spliting $rightVideo with $labelFile at offset ${delays[i]}"
	./bin/SplitVideo $rightVideo $labelFile ${delays[i]}

done
