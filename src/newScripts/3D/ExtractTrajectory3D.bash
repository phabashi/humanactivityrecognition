#!/bin/bash

if [ $# < 1 ]; then
	echo "Dataset Name is missing!"
	echo "Usage: $0 DatasetName [fifoName]"
	echo "fifoName parameter is used to save the path to extracted features in the named fifo provided."
	exit 1
fi

datasetFolder="./dataset/Windsor/short/"

echo "This script will go trough all the clips in ./$datasetFolder folder, search for Left video and run extract featues on it and its corresponding right video. "

#ls clips/ | grep -f ./lists/Actor_set3.txt | grep -f ./lists/ActivitySet_15.txt | grep Left
datasetName=$1;
#setting
settingFile="./SampleSetting.ini"
inputFileList="./lists/${datasetName}.txt"

if [ ! -f $inputFileList ]; then
	echo "The dataset '$1' should have a file list in '$inputFileList'"
	echo "This means either you mistyped the dataset name of the list is not created yet!"
	exit 2
fi

# Sample making file List:
# inputFileList=`ls ./clips/ | grep -e "Left" | grep -f ./lists/ActivitySet_10.txt | grep -f ./lists/Actor_set3.txt | grep -f ./lists/Instances_5.txt > ./lists/VideoFileList_Actor03_Activity_10_I5.txt`
cores=8

source ~/HAR/newScripts/default.bash

if [[ $detectMotion == "No" ]]; then
	motionOption="-dm"
	motionFolderName="Disabled"
else
	motionOption="-em"
	motionFolderName="Enabled"
fi

#encoding="NormDiff"
#normalized="-nd"
#foldn="Not"
#if [[ $normalizeOutput == "Yes" ]]; then
#	normalized="-ne"
#	foldn=""
#fi

if [[ $MULTISCALE == "" ]]; then
	scaleFolderName="SingleScale"
else
	scaleFolderName="MultiScale"
fi

trajectoryFolderName="./test-datay/$datasetName/${motionFolderName}Motion/$opticalFlowAlgorithm/$interestPointAlgorithm/$descriptorAlgorithm/$scaleFolderName/scale_$scale/ss_${samplingScale}/"

mkdir -p "$trajectoryFolderName"

videoFileNames=(`cat $inputFileList`)

completeTrajectoryFileName="$trajectoryFolderName/Length_$length/trajectories"
mkdir -p "$completeTrajectoryFileName"

count=${#videoFileNames[@]}
fileNumber="1"
while read -r leftVideoFile;
do
	#echo "RUNNING LOOP ANOTHER TIME"
	rightVideoFile=${leftVideoFile/Left/Right}
	#ExtractFeatures $i &
	fileNumber=$(( $fileNumber + 1 ))
	#echo "Running $fileNumber/ $count = [$(( fileNumber * 100 / count ))%]"
	leftVideo="./$datasetFolder/$leftVideoFile"
	rightVideo="./$datasetFolder/$rightVideoFile"
	name=${leftVideoFile%.*}
	name=${name/Left/Both}
	trajectoryFileName="$completeTrajectoryFileName/$name.fet"
	if [ -e $trajectoryFileName ]; then
		echo "File Exsists, skipping $trajectoryFileName..."
	else
		echo "[$(( fileNumber * 100 / count ))%] Extracting trajectories of '$leftVideo' and '$rightVideo' with length $length and save it to '$trajectoryFileName'"
		#extract Trajectories without encoding
		echo "Running command ./ExtractTrajectory3D -l '$leftVideo' -r '$rightVideo' -otf '$trajectoryFileName' -dl '$length' -ip '$interestPointAlgorithm -ipd '$descriptorAlgorithm' -ofa '$opticalFlowAlgorithm' -s '$scale' '$MULTISCALE' -te Off $motionOption -minE '$minEnergy' -ss '$samplingScale' as '$settingFile' &"

		./bin/ExtractTrajectory3D -l $leftVideo -r $rightVideo -otf $trajectoryFileName -dl $length -ip $interestPointAlgorithm -ipd $descriptorAlgorithm -ofa $opticalFlowAlgorithm -s $scale $MULTISCALE -te Off $motionOption -minE $minEnergy -ss $samplingScale as $settingFile &
		if [[ $? != 0 ]]; then
			echo "Some error occured! Exiting "
			exit 1
		fi
	fi
	#sleep 1 &
	#echo $fileNumber &
	#wait %1
	if (( $fileNumber % $cores == 0 )); then
		echo "Waiting for childs!"
		wait
	fi
done < $inputFileList
echo "finito :D"
wait 
#send the parameter to next program

if [ "$2" != "" ]; then
	echo send the parameter to next program through \"$2\"
	if [ ! -e "$2" ]; then
        	mkfifo "$2"
	fi
	echo  $completeFeatureFileName > $2
fi
