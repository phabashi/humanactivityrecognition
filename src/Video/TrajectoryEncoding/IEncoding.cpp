/*
 * TrajectoryEncoding.cpp
 *
 *  Created on: May 30, 2016
 *      Author: pejman
 */

#include <memory>

#include "IEncoding.h"
#include "NoEncoding.h"
#include "DifferentiationEncoding.h"
#include "NormalizedDifferentiationEncoding.h"
#include "DirectionEncoding.h"
#include "ClusterEncoding.h"
#include "SecondOrder.h"
#include "ThirdOrder.h"
#include "SpatialEncoding.h"
#include "DiffEncoding.h"

#include "../../Logger/Logger.h"

using namespace Pejman::Logger;

namespace HAR {
namespace Video {
namespace TrajectoryEncoding {

std::unique_ptr<IEncoding> IEncoding::getEncoding(std::string algorithmName) {
	IEncoding* algorithm;

	if(algorithmName == "Off"){
		algorithm = new NoEncoding;
	} else if(algorithmName == "Diff") {
		algorithm = new DifferentiationEncoding;
	} else if(algorithmName == "NormDiff") {
		algorithm = new NormalizedDifferentiationEncoding;
	}  else if(algorithmName == "Direction") {
		algorithm = new DirectionEncoding;
	} else if(algorithmName == "Cluster") {
		algorithm = new ClusterEncoding;
	} else if(algorithmName == "SecondOrder") {
		algorithm = new SecondOrder;
	} else if(algorithmName == "ThirdOrder") {
			algorithm = new ThirdOrder;
	} else if(algorithmName == "Spatial") {
		algorithm = new SpatialEncoding();
	} else if(algorithmName == "4Order") {
		algorithm = new DiffEncoding(4);
	}  else if(algorithmName == "5Order") {
		algorithm = new DiffEncoding(5);
	}  else if(algorithmName == "6Order") {
		algorithm = new DiffEncoding(6);
	}  else if(algorithmName == "7Order") {
		algorithm = new DiffEncoding(7);
	} else {
		Logger::Log(LogLevel::ERROR,
				"The requested encoding algorithm %s is unknown! Using 'NormDiff' Instead.",
				algorithmName.c_str());
		algorithm = new NormalizedDifferentiationEncoding;
	}
	Logger::Log(LogLevel::VERBOSE,
					"The requested encoding algorithm %s Is set.",
					algorithmName.c_str());
	return std::unique_ptr<IEncoding>(algorithm);
}

IEncoding::IEncoding() {

}

IEncoding::~IEncoding() {
}

cv::Mat IEncoding::encode(const cv::Mat& trajectories) const {
	if(trajectories.rows == 0 || trajectories.cols < 2)//trajectory length should be greater than 2
		return cv::Mat();

	int start=1, end = trajectories.cols -1;
	if(trajectories.cols % 2 == 0) //if the frame number is not saved (2D trajectories have 2*l length)
		start = 0;
	Logger::Log(VERBOSE,"Start = %d,  end = %d", start, end);
	int trajectorylegth=(end - start)/2;

	std::vector<float> firstEncoded;
	int numberOfColumns = 0;
	int rowToRead=0;

	while(numberOfColumns == 0) {
		std::vector<cv::Point2f> points;
		points.reserve(trajectorylegth);
		for(int j=start; j <= end; j+=2){
			points.push_back(cv::Point2f(trajectories.at<float>(rowToRead,j), trajectories.at<float>(rowToRead,j+1)));
		}
		firstEncoded = encode(points);
		numberOfColumns = firstEncoded.size();
		rowToRead++;
	}
	cv::Mat result( trajectories.rows, numberOfColumns, CV_32FC1 );

	for(int j=0; j < numberOfColumns; j++ )
		result.at<float>(0,j) = firstEncoded[j];
	int rowToWrite=1;

	for(; rowToRead < trajectories.rows; rowToRead++){
		std::vector<cv::Point2f> points;
		points.reserve(trajectorylegth);
		for(int j=start; j <= end; j+=2){
			points.push_back(cv::Point2f(trajectories.at<float>(rowToRead,j), trajectories.at<float>(rowToRead,j+1) ));
		}
		std::vector<float> encoded = encode(points);

		if(encoded.size() != 0 ){
			for(int j=0; j < encoded.size(); j++ )
				result.at<float>(rowToWrite,j) = encoded[j];
			rowToWrite++;
		}
	}

	return result(cv::Range(0, rowToWrite), cv::Range(0, numberOfColumns));
}

} /* namespcae TrajectoryEncoding */
} /* namespace Video */
} /* namespace HAR */

