/*
 * FrameFeatureDescriptors.h
 *
 *  Created on: Jan 26, 2015
 *      Author: Pejman
 *      Copyright (c) 2015 Pejman. All rights reserved.
 */

#ifndef FRAMEFEATUREDESCRIPTORS_H_
#define FRAMEFEATUREDESCRIPTORS_H_

#include <opencv2/opencv.hpp>

namespace HAR {

class FrameFeatureDescriptors {
public:
	FrameFeatureDescriptors() {}
	FrameFeatureDescriptors(cv::Mat descriptors);
	const cv::Mat getImage() const { return descriptor; }
	virtual ~FrameFeatureDescriptors();

private:
	cv::Mat descriptor;
};

} /* namespace HAR */

#endif /* FRAMEFEATUREDESCRIPTORS_H_ */
