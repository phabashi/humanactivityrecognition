/*
 * ConfusionMatrix.cpp
 *
 *  Created on: Jan 10, 2016
 *      Author: Pejman
 *      Copyright (c) 2016 Pejman. All rights reserved.
 */

#include "ConfusionMatrix.h"
#include <fstream>
#include <vector>
#include <iostream>
#include <sstream>
#include <iomanip>
#include "../../Logger/Logger.h"

using namespace std;
using namespace Pejman::Logger;

namespace HAR {

ConfusionMatrix::ConfusionMatrix(UniqueSequenceNumberGenerator& labels) :
		knownLabels(labels) {
	accuracy = 0;

}

void ConfusionMatrix::makeConfusionMatrix(string expectedFileName,
		string actualFileName) {
	vector<int> expected;
	vector<int> actual;

	ifstream in1(expectedFileName);
	ifstream in2(actualFileName);
//	cout << "Readin the Files:" << endl;
	int i, j;
	in1 >> i;
	in2 >> j;
	while (in1 and in2) {
		expected.push_back(i);
		actual.push_back(j);
		in1 >> i;
		in2 >> j;
	}

	Logger::Log(LogLevel::VERBOSE, "%d and %d lines read from expected and actual.", expected.size(), actual.size());

	if (in1 || in2) {
		//TODO Make an Exception instead and remore include <iostream>
		cerr << "Expected and Outcome should have the same size!" << endl;
		exit(1);
	}

	makeConfusionMatrix(expected, actual);
}

void ConfusionMatrix::makeConfusionMatrix(vector<int> expected,
		vector<int> actual) {

	Logger::Log(LogLevel::INFO,
			"Making Confusion Matrix: expected size = %d, actual size=%d, knownLabels size=%d",
			expected.size(), actual.size(), knownLabels.size());
	int size = knownLabels.size();
	//initialize the matrix
	theMatrix = vector<vector<int> >(size, vector<int>(size, 0));

	for (unsigned int i = 0; i < expected.size(); i++) {
		if(expected[i] < 1 || expected[i] > size) {
			Logger::Log(LogLevel::ERROR,
					"Unknown Label Index in expected output: We expect index in range [1, %d] while we got %d",
					size, expected[i]);
		} else if (actual[i] < 1 || actual[i] > size){
			Logger::Log(LogLevel::ERROR,
					"Unknown Label Index in actual output: We expect index in range [1, %d] while we got %d",
					size, actual[i]);
		} else
			theMatrix[expected[i] - 1][actual[i] - 1] += 1;
	}

	CalculateStatistics();
}

string ConfusionMatrix::toString() {
	int n = theMatrix.size();
	stringstream out;
//out << " the Matrix size is:  " << theMatrix.size() <<  endl;
	const int distance = 3;
//TODO Fix the class name generator like a-z aa-zz aaa-zzz
	for (int i = 0; i < n; i++)
		out << setw(distance) << char('a' + i);
	out << " < identified as" << endl;

	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++) {
			out << setw(distance) << theMatrix[i][j];
		}
		out << " " << char('a' + i) << " = " << knownLabels.getLabel(i + 1)
				<< endl;
	}

	out << endl;
	out << "Precision " << endl;
	for (int i = 0; i < n; i++) {
		out << "\t " << knownLabels.getLabel(i + 1) << " \t = "
				<< precision[i] * 100 << "%" << endl;
	}
	out << endl;

	out << "Recall " << endl;
	for (int i = 0; i < n; i++) {
		out << "\t" << knownLabels.getLabel(i + 1) << " \t = "
				<< recall[i] * 100 << "%" << endl;
	}
	out << endl;

	out << "Accuracy = " << accuracy * 100 << "% " << endl;
	return out.str();
}

void ConfusionMatrix::CalculateStatistics() {
	int n = knownLabels.size();

//initialize statistics
	precision = vector<double>(n, 0);
	recall = vector<double>(n, 0);
	accuracy = 0;

	auto expectedInstancesInClass = vector<int>(n, 0); // This value is the number of instances that should go to a class
	auto actualInstanceInClass = vector<int>(n, 0); // This is the number of instances that assigned to a class.

	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++) {
			expectedInstancesInClass[i] += theMatrix[i][j]; //sum on rows
			actualInstanceInClass[i] += theMatrix[j][i]; //sum on columns
		}
	}

	int allInstances = 0; // Number of all instances
	int correctlyClassified = 0; //Number of Correctly Classified classes
	for (int i = 0; i < n; i++) {
		// for each class
		precision[i] = (double) theMatrix[i][i] / actualInstanceInClass[i]; // TP / (TP + FP)
		recall[i] = (double) theMatrix[i][i] / expectedInstancesInClass[i]; // TP / (TP + FN)
		correctlyClassified += theMatrix[i][i];
		allInstances += expectedInstancesInClass[i];
	}

	accuracy = (double) correctlyClassified / allInstances;
}

ConfusionMatrix::~ConfusionMatrix() {
// TODO Auto-generated destructor stub
}

ostream& operator <<(ostream& out, HAR::ConfusionMatrix& matrix) {
	out << matrix.toString();
	return out;
}

} /* namespace HAR */
