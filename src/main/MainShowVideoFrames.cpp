/*
 * MainShowVideoFrames.cpp
 *
 *  Created on: May 1, 2015
 *      Author: Pejman
 *      Copyright (c) 2015 Pejman. All rights reserved.
 */

#include "../util/util.h"
#include <iostream>
#include "../Video/Display.h"

using namespace std;
using namespace HAR;


int main(int argc, char** argv)
{
	string inFileName;
	double scale = 1.0;

	if (argc == 3)
	{
		inFileName = argv[1];
		scale = stringTo<double>(argv[2]);
		cout << "Set scale to" << scale << endl;
	}
	else if (argc == 2)
	{
		inFileName = argv[1];
	}
	else
	{
		cout << "Usage: ShowVideoFrames inFileName [scale]" << endl;
		return -1;
	}

	cout << "'T'          :To tell the current frame number." << endl
		<<  "'P' or Space :To pause the video." << endl
		<<  "'<-' '<' or ','   :Rewind..." << endl
		<<  "'->' '>' or '.'   :Fast Forward..." << endl
		<<  "Any key	      :To go to next frame."<< endl
		<< endl;

	bool finished = false, pause = false;
	cv::Mat frame;
	int frameNumber = 0;

	Video::Video temp(inFileName);
	while(! finished)
	{
		if(! pause)
		{
			frame = temp.getNextFrame();frameNumber++;
		}

		if(frame.empty())
		{
			cout << "Reaching the end of file. The last frame number was: " << frameNumber -1 << endl;
			break;
		}

		Video::Display::Show(frame, scale);
		int key = Video::Display::waitKey(pause?0:30);

		switch (key) {
		case 27:
			finished = true;
			break;
		case 'T':
		case 't':
			cout << "Frame Number = " << frameNumber << endl;
			break;

		case ' ':
		case 'P':
		case 'p':
			pause = !pause;
			if(pause)
				cout << "This Frame Number = " << frameNumber << endl;
			break;
		case 63234:// <-
			frameNumber -= 300;
			if(frameNumber < 0) frameNumber = 0;
			frame = temp.getFrame(frameNumber);//Set the frame number
			cout << "FB: Frame Number = " << frameNumber << endl;
			break;
		case 63235:// ->
			frameNumber += 300;
			frame = temp.getFrame(frameNumber);
			cout << "FF: Frame Number = " << frameNumber << endl;
			break;
		case '<' :
			frameNumber -= 10;
			frame = temp.getFrame(frameNumber);
			cout << "Tune Back: Frame Number = " << frameNumber << endl;
			break;

		case '>' :
			frameNumber += 10;
			frame = temp.getFrame(frameNumber);
			cout << "Tune Forward Frame Number = " << frameNumber << endl;
			break;

		case ',':
			frameNumber -= 1;
			frame = temp.getFrame(frameNumber);
			cout << "Previous Frame: " << frameNumber << endl;
			break;

		case 'g':
		case 'G':
			cout << "Enter Frame Number : ";
			cin >> frameNumber;
			frame = temp.getFrame(frameNumber);
			cout << "Got to Frame: " << frameNumber << endl;
			break;
		case '.':
			frameNumber += 1;
			frame = temp.getFrame(frameNumber);
			cout << "Next Frame: " << frameNumber << endl;
			break;
		case -1: break; //Do nothing
		default:
			cout << "Unknown Key = " << key << endl;
			break;
		}

	}
	return 0;
}
