/*
 * AugmentedVideo.cpp
 *
 *  Created on: Jan 16, 2015
 *      Author: Pejman
 *      Copyright (c) 2015 Pejman. All rights reserved.
 */

#include "AugmentedVideo.h"

#include "../Feature/FeatureDetector.h"

namespace HAR {
namespace Video {

AugmentedVideo::AugmentedVideo(std::string videoFileName, bool isGrayScale) {
	this->videoFileName = videoFileName;
	this->inputVideo = Video(videoFileName, isGrayScale);
	xmlFileName = videoFileName.substr(0, videoFileName.length() - 4) + ".xml";
}

//const VideoFeaturePoints& AugmentedVideo::getSiftFeaturePoints()
//{
//	if(siftFeaturePoints.empty())
//	{
//		//try to load feature points from file
//		if(! ReadSiftFeaturesFromXMLFile(xmlFileName))
//		{
//			CalculateSift();//if loading failed, calculate feature points from video
//			WriteSiftFeaturesToXMLFile(xmlFileName);//Save featurepoints for further reference
//		}
//	}
//
//	return siftFeaturePoints;
//}
//
//const VideoFeaturePoints& AugmentedVideo::getSurfFeaturePoints()
//{
//	if(surfFeaturePoints.empty())
//	{
//		//try to load feature points from file
//		if(! ReadSurfFeaturesFromXMLFile(xmlFileName))
//		{
//			CalculateSurf();//if loading failed, calculate feature points from video
//			WriteSurfFeaturesToXMLFile(xmlFileName);//Save feature points for further reference
//		}
//	}
//	return surfFeaturePoints;
//}
//
//
//void AugmentedVideo::CalculateSift()
//{
//	std::cout << "Calculating SIFT feature points for file '" << videoFileName << "'."<< std::endl <<"Please wait... " << std::endl;
//	SiftFeatureExtractor extractor;
//	siftFeaturePoints = *(extractor.ExtractFeaturesFromVideo(inputVideo));
//	inputVideo.getFrame(0);//To set the video pointer to first frame.
//	std::cout << "Done. " << std::endl;
//}
//
//void AugmentedVideo::CalculateSurf()
//{
//	std::cout << "Calculating SURF feature points for file '" << videoFileName << "'."<< std::endl <<"Please wait... " << std::endl;
//	SurfFeatureExtractor extractor;
//	surfFeaturePoints = *(extractor.ExtractFeaturesFromVideo(inputVideo));
//	inputVideo.getFrame(0);//To set the video pointer to first frame
//	std::cout << "Done. " << std::endl;
//}
//
//bool AugmentedVideo::ReadSiftFeaturesFromXMLFile(std::string xmlFileName)
//{
//	std::cout << "Reading SIFT feature points from file: '" << xmlFileName << "'." << std::endl << "Please wait... " << std::endl;
//	bool result = siftFeaturePoints.Read(xmlFileName, "SIFT");
//	std::cout << ((result)? "DONE." : "Failed") << std::endl;
//	return result;
//}
//
//bool AugmentedVideo::ReadSurfFeaturesFromXMLFile(std::string xmlFileName)
//{
//	std::cout << "Reading SURF feature points from file: '" << xmlFileName << "'." << std::endl << "Please wait... " << std::endl;
//	bool result = surfFeaturePoints.Read(xmlFileName, "SURF");
//	std::cout << ((result)? "DONE." : "Failed")  << std::endl;
//	return result;
//}
//
//bool AugmentedVideo::WriteSiftFeaturesToXMLFile(std::string xmlFileName)
//{
//	std::cout << "Writing SIFT feature points to file: '" << xmlFileName << "'." << std::endl << "Please wait... " << std::endl;
//	return siftFeaturePoints.Write(xmlFileName, "SIFT");
//	std::cout << "DONE." << std::endl;
//}
//
//bool AugmentedVideo::WriteSurfFeaturesToXMLFile(std::string xmlFileName)
//{
//	std::cout << "Writing SURF feature points to file: '" << xmlFileName << "'."<< std:: endl << "Please wait... " << std::endl;
//	return surfFeaturePoints.Write(xmlFileName, "SURF");
//	std::cout << "DONE." << std::endl;
//}

const Features::VideoFeaturePoints& AugmentedVideo::getFeaturePoints(
		std::string featureType)
{
	if(featurePoints.find(featureType) == featurePoints.end())
	{
			//try to load feature points from file
			if(! ReadFeaturesFromXMLFile(xmlFileName, featureType))
			{
			    CalculateFeatures(featureType);//if loading failed, calculate feature points from video
				WriteFeaturesToXMLFile(xmlFileName, featureType);//Save featurepoints for further reference
			}
	}

	return * (featurePoints[featureType]);
}

bool AugmentedVideo::ReadFeaturesFromXMLFile(std::string xmlFileName,
		std::string featureType)
{
	std::cout << "Reading " << featureType << " feature points from file: '" << xmlFileName << "'." << std::endl << "Please wait... " << std::endl;
	bool result = featurePoints[featureType]->Read(xmlFileName, featureType);
	std::cout << ((result)? "DONE." : "Failed")  << std::endl;
	return result;
}

bool AugmentedVideo::WriteFeaturesToXMLFile(std::string xmlFileName,
		std::string featureType)
{
	std::cout << "Writing " << featureType << " feature points to file: '" << xmlFileName << "'." << std::endl << "Please wait... " << std::endl;
	bool result = featurePoints[featureType]->Write(xmlFileName, featureType);
	std::cout << "DONE." << std::endl;
	return result;
}

void AugmentedVideo::CalculateFeatures(std::string featureType)
{
	std::cout << "Calculating " << featureType << " feature points for file '" << videoFileName << "'."<< std::endl <<"Please wait... " << std::endl;
	Features::FeatureDetector& extractor = Features::FeatureDetector::getInstance(featureType);
	featurePoints[featureType] = extractor.ExtractFeaturesFromVideo(inputVideo);
	inputVideo.getFrame(0);//To set the video pointer to first frame.
	std::cout << "Done. " << std::endl;
}

AugmentedVideo::~AugmentedVideo() {
	for(auto it=featurePoints.begin(); it != featurePoints.end(); it++)
		delete it->second;
}

} /* namespace Video */
} /* namespace HAR */
