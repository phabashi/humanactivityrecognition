/*
 * MainCountTheWords.cpp
 *
 *  Created on: May 1, 2015
 *      Author: Pejman
 *      Copyright (c) 2015 Pejman. All rights reserved.
 */

#include "util.h"
#include <unordered_map>
#include <opencv2/opencv.hpp>
#include <fstream>

using namespace cv;
using namespace std;
//using namespace HAR;

void resetClusterCount(map<string, int>& map, int numberOfClusters)
{
    for(int i=0; i<numberOfClusters; i++)
    {
        stringstream clusterName;
        clusterName << "cluster" << i ;
        map[clusterName.str()] = 0;
    }
}

int main(int argc, char** argv)
{
    int numberOfCluster;
    string csvFileName, csvOutFileName;
    if (argc == 4)
    {
        csvFileName = argv[1];
        csvOutFileName = argv[2];
        numberOfCluster = stringTo<int>(argv[3]);
    }
    else
    {
        cout << "Usage: CountTheWords csvFileName outFileName numberOfClusters" << endl;


        return -1;

//        csvFileName = "/Users/user/Documents/workspace/HumanActivityRecognition/bin/KMeansClustersSimplifiedWithFN.csv";
//        labelFileName = "/Users/user/Documents/workspace/HumanActivityRecognition/bin/Labels.lab";
//        csvOutFileName = "resutl.csv";
//        stringstream ns ("500");
//        ns >> numberOfCluster;
    }

    ifstream infile(csvFileName);
    ofstream outfile(csvOutFileName);

    string className;

    map<string, int> clusterCount; //match each cluster Name to its number of occurances
    resetClusterCount(clusterCount, numberOfCluster);

    string theLine;
    //copy the header
    getline(infile, theLine);

    for (int i=0; i<numberOfCluster; i++) {
        stringstream clusterName;
        clusterName << "cluster" << i ;
        outfile << clusterName.str() << ", " ;
    }

    outfile << "Instance, Class" ;


    outfile << endl;

    string currentInstance = "";
    string classText, clusterText;

    //int i =0;
    while (infile) {

        std::getline( infile, theLine);
        //cout << theLine << endl;
        stringstream temp(theLine);

        string thisInstance;
        getline(temp, thisInstance, ',');
        getline(temp, classText, ',');
        getline(temp, clusterText, ',');

        //cout << ++i << endl;

        chreplace(thisInstance, '\'', ' ');
        chreplace(classText, '\'', ' ');

        thisInstance = trim(thisInstance);
        classText = trim(classText);
        clusterText = trim(clusterText);

        if(currentInstance == "") currentInstance = thisInstance;
        if(currentInstance != thisInstance)
        {
            //TODO we should save the vector before clearing it
            for (int i=0; i<numberOfCluster; i++) {
                stringstream clusterName;
                clusterName << "cluster" << i ;
                outfile << clusterCount[clusterName.str()] << ", ";
            }
            outfile << currentInstance;
            outfile << ", " << classText;
            outfile << endl;

            resetClusterCount(clusterCount, numberOfCluster);
            currentInstance = thisInstance;
        }
        clusterCount[clusterText] += 1;
    }

    for (int i=0; i<numberOfCluster; i++) {
    	stringstream clusterName;
    	clusterName << "cluster" << i ;
    	outfile << clusterCount[clusterName.str()] << ", ";
    }
    outfile << currentInstance;
     outfile << ", " << classText;
    outfile << endl;

    return 0;
}
