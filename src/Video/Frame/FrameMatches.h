/*
 * FrameMatches.h
 *
 *  Created on: Jan 27, 2015
 *      Author: Pejman
 *      Copyright (c) 2015 Pejman. All rights reserved.
 */

#ifndef FRAMEMATCHES_H_
#define FRAMEMATCHES_H_

#include <vector>
#include <opencv2/opencv.hpp>

namespace HAR {

class FrameMatches {
public:
	FrameMatches(std::vector< std::vector<cv::DMatch> > initialMatches);
	FrameMatches() {}
    const std::vector<std::vector<cv::DMatch> > & getMatches() const { return multipleMatches; }

//	double getMean() const { return  mean;}
//	double getVar() const { return var;}

	virtual ~FrameMatches();


private:
//	FrameMatches(const std::vector<cv::DMatch>& initialMatches);
//	double calculateMean();
//	double calculateVar();

private:
	std::vector< std::vector<cv::DMatch> > multipleMatches;
//	std::vector<cv::DMatch> singleMatches;

//	double mean;
//	double var;
};

} /* namespace HAR */

#endif /* FRAMEMATCHES_H_ */
