/*
 * SpatialEncoding.h
 *
 *  Created on: Dec 28, 2016
 *      Author: user
 */

#ifndef SRC_VIDEO_TRAJECTORYENCODING_SPATIALENCODING_H_
#define SRC_VIDEO_TRAJECTORYENCODING_SPATIALENCODING_H_

#include "IEncoding.h"

namespace HAR {
namespace Video {
namespace TrajectoryEncoding {

class SpatialEncoding: public IEncoding {
public:
	SpatialEncoding();

	virtual std::vector<float> encode(const std::vector<cv::Point2f>& points) const;

	virtual ~SpatialEncoding();
};

} /* namespace TrajectoryEncoding */
} /* namespace Video */
} /* namespace HAR */

#endif /* SRC_VIDEO_TRAJECTORYENCODING_SPATIALENCODING_H_ */
