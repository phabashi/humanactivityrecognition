/*
 * ClusterEncoding.h
 *
 *  Created on: Jul 7, 2016
 *      Author: pejman
 */

#ifndef SRC_VIDEO_TRAJECTORYENCODING_CLUSTERENCODING_H_
#define SRC_VIDEO_TRAJECTORYENCODING_CLUSTERENCODING_H_

#include "IEncoding.h"

namespace HAR {
namespace Video {
namespace TrajectoryEncoding {

class ClusterEncoding: public IEncoding {
public:
	ClusterEncoding();
	virtual std::vector<float> encode(const std::vector<cv::Point2f>& points) const;
	int getCluster(const std::vector<cv::Point2f>& points) const;
	virtual ~ClusterEncoding();
};

} /* namespace TrajectoryEncoding */
} /* namespace Video */
} /* namespace HAR */

#endif /* SRC_VIDEO_TRAJECTORYENCODING_CLUSTERENCODING_H_ */
