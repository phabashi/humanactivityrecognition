/*
 * StereoAugmentedVideo.cpp
 *
 *  Created on: Jan 23, 2015
 *      Author: Pejman
 *      Copyright (c) 2015 Pejman. All rights reserved.
 */

#include "StereoAugmentedVideo.h"
#include "../Logger/Logger.h"

#include "Display.h"

//TODO REMOVE:
#include <iostream>
using namespace std;


using namespace Pejman::Logger;
namespace HAR {
namespace Video {

StereoAugmentedVideo::StereoAugmentedVideo(std::string leftVideoFileName,
		std::string rightVideoFileName, int synchronizationParameter) :
		left(leftVideoFileName), right(rightVideoFileName) {
	frameApart = synchronizationParameter;
//	rw = rh = lw = lh = -1;
}

bool StereoAugmentedVideo::getFrame(unsigned long frameNumber,
		cv::Mat& leftFrame, cv::Mat& rightFrame)
{
	cv::Mat nextLeftFrame;
	cv::Mat nextRightFrame;
	if(frameApart < 0)
	{
		nextLeftFrame = left.getVideo().getFrame(frameNumber - frameApart);
		nextRightFrame = right.getVideo().getFrame(frameNumber);
	}
	else
	{
		nextLeftFrame = left.getVideo().getFrame(frameNumber);
		nextRightFrame = right.getVideo().getFrame(frameNumber + frameApart);
	}

	leftFrame = nextLeftFrame;
	rightFrame = nextRightFrame;
	return ( ( ! leftFrame.empty() ) &&  (! rightFrame.empty() ));
}

bool StereoAugmentedVideo::getNextFrame(cv::Mat& leftFrame,
		cv::Mat& rightFrame)
{
	leftFrame = left.getVideo().getNextFrame();
	rightFrame = right.getVideo().getNextFrame();
	return ( ( ! leftFrame.empty() ) &&  (! rightFrame.empty() ));
}

FrameFeaturePoints StereoAugmentedVideo::getLeftFrameFeaturePoints(
		int frameNumber, std::string featureType)
{
	FrameFeaturePoints result;
	if(frameApart < 0)
		result = *(left.getFeaturePoints(featureType).getFrameFeaturePoints(frameNumber - frameApart));
	else
		result = *(left.getFeaturePoints(featureType).getFrameFeaturePoints(frameNumber));
	return result;
}

FrameFeaturePoints StereoAugmentedVideo::getRightFrameFeaturePoints(
		int frameNumber, std::string featureType)
{
	FrameFeaturePoints result;
	if(frameApart < 0)
		result = *(right.getFeaturePoints(featureType).getFrameFeaturePoints(frameNumber));
	else
		result = *(right.getFeaturePoints(featureType).getFrameFeaturePoints(frameNumber + frameApart));
	return result;
}

bool StereoAugmentedVideo::getNextFrame(StereoFrame& frame)
{
	cv::Mat leftFrame = left.getVideo().getNextFrame();
	cv::Mat rightFrame = right.getVideo().getNextFrame();
	frame = StereoFrame(leftFrame, rightFrame);
	return ( ( ! leftFrame.empty() ) &&  (! rightFrame.empty() ));
}

StereoAugmentedVideo::~StereoAugmentedVideo() {
	// TODO Auto-generated destructor stub
}



StereoFrame StereoAugmentedVideo::getNextFrame()
{
	Logger::Log(LogLevel::VERBOSE, " StereoAugmentedVideo::getNextFrame()");
	cv::Mat nextLeftFrame = left.getVideo().getNextFrame();
	cv::Mat nextRightFrame = right.getVideo().getNextFrame();
	return StereoFrame(nextLeftFrame, nextRightFrame);
}

cv::Mat StereoAugmentedVideo::getFrame(unsigned long frameNumber)
{
	Logger::Log(LogLevel::VERBOSE, " StereoAugmentedVideo::getFrame(unsigned long frameNumber)");
	cv::Mat nextLeftFrame;
	cv::Mat nextRightFrame;
	if(frameApart < 0)
	{
		nextLeftFrame = left.getVideo().getFrame(frameNumber - frameApart);
		nextRightFrame = right.getVideo().getFrame(frameNumber);
	}
	else
	{
		nextLeftFrame = left.getVideo().getFrame(frameNumber);
		nextRightFrame = right.getVideo().getFrame(frameNumber + frameApart);
	}

//	cv::Size size(nextLeftFrame.cols, nextLeftFrame.rows );
//
//	if( rectifiedCorrectly){
//		if( (*rectifiedCorrectly)[frameNumber]){
//			Logger::Log(LogLevel::INFO, "Rectified ! Try to unwarp the image! image size = (%d, %d)", size.height, size.width);
//			//cv::warpPerspective(nextLeftFrame, nextLeftFrame, (*H1)[frameNumber], size);
//			//cv::warpPerspective(nextRightFrame, nextRightFrame, (*H2)[frameNumber], size);
//			if(F == NULL) {
//				Logger::Log(LogLevel::INFO, "Empty Fundamental matrix");
//				cin.get();
//			}
//			else{
//				Logger::Log(LogLevel::INFO, "Draw epipolar lines!");
//				Display::DrawEpipolarLines(nextLeftFrame, (*F)[frameNumber]);
//				Display::DrawEpipolarLines(nextRightFrame, (*F)[frameNumber].t());
//				Logger::Log(LogLevel::INFO, "WE ARE BACK");
//			}
//			Logger::Log(LogLevel::INFO, "Printing Hs");
//			cout << "H1 = " << (*H1)[frameNumber] << "\nH2 = " << (*H2)[frameNumber] << endl;
//		}
//	}
	return Display::Combine(nextLeftFrame, nextRightFrame);
}

cv::Mat StereoAugmentedVideo::getDisplayableFrame(int frameNumber) {
	cv::Mat theFrame=getFrame(frameNumber);
	Logger::Log(INFO, "theFrame is %s", (theFrame.empty())?"EMPTY": "OK");
	AugmentedVideoDraw displayFrame( theFrame );



//	const std::vector<cv::Rect_<float> >& rois = this->getCurrentFrameInformation().getRegionsOfInterest();
//	displayFrame.DrawRegionsOfInteresOnFrame(rois);

//	const std::vector<cv::Point_<float> >& interesPoints=this->getCurrentFrameInformation().getPointsOfInterest();
//	displayFrame.DrawInterestPointsOnFrame(interesPoints);

//	const std::pair<std::unordered_multimap<int, Trajectory>::const_iterator,
//	std::unordered_multimap<int, Trajectory>::const_iterator> range = this->getCurrentFrameTrajectoriesRange();

//	std::unordered_set<Trajectory> toDeleteSet = displayFrame.DrawTrajectoriesOnFrame(
//			range, this->getCurrentFrameNumber());
	Logger::Log(INFO, "displayFrame is %s", (displayFrame.getDisplayFrame().empty())? "EMPTY": "OK");
	return displayFrame.getDisplayFrame();
}


} /* namespace Video */
} /* namespace HAR */

