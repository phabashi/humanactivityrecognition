/*
 * StereoAugmentedVideo.h
 *
 *  Created on: Jan 23, 2015
 *      Author: Pejman
 *      Copyright (c) 2015 Pejman. All rights reserved.
 */

#ifndef STEREOAUGMENTEDVIDEO_H_
#define STEREOAUGMENTEDVIDEO_H_

#include "AugmentedVideo.h"
#include "Frame/FrameFeaturePoints.h"
#include "StereoFrame/StereoFrame.h"
#include "DisplayableVideo.h"

namespace HAR {
namespace Video {

class StereoAugmentedVideo : public DisplayableVideo {
public:
	StereoAugmentedVideo(std::string leftVideoFileName, std::string rightVideoFileName, int synchronizationParameter = 0);

	//Zero Frame will be the first common (???) frame recognized by frameApart
	cv::Mat getFrame(unsigned long frameNumber);
	bool getFrame(unsigned long frameNumber, cv::Mat& leftFrame, cv::Mat& rightFrame);
	StereoFrame getNextFrame();
	bool getNextFrame(cv::Mat& leftFrame, cv::Mat& rightFrame);
	bool getNextFrame(StereoFrame& frame);

	FrameFeaturePoints getLeftFrameFeaturePoints(int frameNumber, std::string featureType);
	FrameFeaturePoints getRightFrameFeaturePoints(int frameNumber, std::string featureType);

//	AugmentedVideo& getLeft() { return left; }
//	AugmentedVideo& getRight() { return right; }

    void setFrameSynchronization(int synchParam, int frameNumber = 0)
    {
        frameApart = synchParam;
        cv::Mat l,r;
        getFrame(frameNumber, l, r );
    }

	virtual cv::Mat getDisplayableFrame(int frameNumber);
	virtual int getFramePerSecond() {
		//TODO : int fps = left.getFramePerSecond();
		//if (fps <= 0 ) fps = 25;
		//return fps;
		return 25;
	}

	virtual ~StereoAugmentedVideo();

private:
	AugmentedVideo left;
	AugmentedVideo right;

	/*
	 * defined by the number of frames difference between two videos of the same event.
	 * if event A occur at time t0 in real world, and it appears in frame t1 in left
	 * video and frame t2 in right video, frameApart will be t2 - t1.
	 * 		frameApart = t2 - t1 --> t1 = t2 - frameApart & t2 = frameApart + t1
	 * Positive frameApart value means right video is ahead in time and vice versa.
	 * */
	int frameApart;

//	int lw, lh, rw, rh;
//	cv::Mat currentStereoFrame;
//
//	cv::Mat roiLeft;
//	cv::Mat roiMargin;
//	cv::Mat roiRight;
//
//	cv::Mat combine(cv::Mat left, cv::Mat right);

};

} /* namespace Video */
} /* namespace HAR */

#endif /* STEREOAUGMENTEDVIDEO_H_ */
