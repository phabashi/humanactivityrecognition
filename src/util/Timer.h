//
//  Timer.h
//  Disparity
//
//  Created by pejman on 2014-10-19.
//  Copyright (c) 2014 pejman. All rights reserved.
//

#ifndef __Disparity__Timer__
#define __Disparity__Timer__

#include <stdio.h>
#include <ctime>

namespace HAR
{
    
    class Timer
    {
    public:
        Timer();
        void Start();
        void Stop();
        void pause();
        void Resume();
        double getPassedTime();
        clock_t getTicks();
        ~Timer();

    private:
        bool isActive;
        clock_t oldTime;
        double passedTime;
        
    };
    
}

#endif /* defined(__Disparity__Timer__) */
