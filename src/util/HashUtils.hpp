/*
 * HashUtils.hpp
 *
 *  Created on: Feb 14, 2016
 *      Author: Pejman
 *      Copyright (c) 2016 Pejman. All rights reserved.
 */

#ifndef SRC_UTIL_HASHUTILS_HPP_
#define SRC_UTIL_HASHUTILS_HPP_
#include <opencv2/opencv.hpp>
#include <unordered_set>
#include <unordered_map>
#include <exception>

namespace std {
template<class T>
struct hash<cv::Point_<T> > {
	std::size_t operator()(const cv::Point& point) const {
		return ( (hash<T>()(point.x)  >> 1)
				^ (hash<T>()(point.y)  << 1)
		) ;
	}
};
}

namespace HAR {
class indexNotFoundException : public std::exception {

};
}

#endif /* SRC_UTIL_HASHUTILS_HPP_ */
