/*
 * DiffEncoding.h
 *
 *  Created on: Dec 28, 2016
 *      Author: user
 */

#ifndef SRC_VIDEO_TRAJECTORYENCODING_DIFFENCODING_H_
#define SRC_VIDEO_TRAJECTORYENCODING_DIFFENCODING_H_

#include "IEncoding.h"
#include "I3DEncoding.h"

namespace HAR {
namespace Video {
namespace TrajectoryEncoding {

class DiffEncoding: public IEncoding, public I3DEncoding {
public:

	DiffEncoding(int N, bool normalize = false);

	virtual std::vector<float> encode(const std::vector<cv::Point2f>& points) const;
	virtual std::vector<float> encode3D(const std::vector<cv::Point3f>& trajectory3D) const;

	//used to get energy of 3D points
	static float get3DEnergy(const std::vector<cv::Point3f>& trajectory3D);


	virtual ~DiffEncoding();

private:
	static std::vector<float> normalizeVector(std::vector<float>& input, double vectorSize);

private:
	const int order;
	bool normalize;
};

} /* namespace TrajectoryEncoding */
} /* namespace Video */
} /* namespace HAR */

#endif /* SRC_VIDEO_TRAJECTORYENCODING_DIFFENCODING_H_ */
