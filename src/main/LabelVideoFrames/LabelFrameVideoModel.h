//
//  LabelFrameVideoModel.h
//  HARLinked
//
//  Created by pejman on 2015-07-08.
//  Copyright (c) 2015 pejman. All rights reserved.
//

#ifndef __HARLinked__LabelFrameVideoModel__
#define __HARLinked__LabelFrameVideoModel__

#include <stdio.h>
#include <string>
#include <map>

#include "../../Video/Video.h"

class LabelVideoModel
{
public:
    LabelVideoModel(std::string inputFileName,
                    std::string labelDefinitionFileName,
                    std::string outputFileName,
                    double scale = 1.0);
    
    cv::Mat getNextFrame();
    cv::Mat getCurrentFrame() { return currentFrame; }
    //setters
    bool setScale(double newScale);
    void upScale() { setScale( scale + 0.1); }
    void downScale() { setScale( scale - 0.1); }
    bool setFrameNumber(int newFrameNumber);
    bool seekVideo(int numberOfFrames);//Fast Forward/Rewind effect
    void setLastPressedKeyCode(int keyCode) { lastPressedKeyCode = keyCode; }
    void terminateDisplay() { playingFinished = true; }
    void TogglePlaying() { playingPaused = ! playingPaused; }
    void setErrorMsg(std::string msg) { lastErrorMsg = msg; }
    void setKeyBindingHelp(std::string msg){ keyBindinHelp = msg; }
    void AddToExistingLabels(std::string newLine);
    void RemoveLastLabel();
    
    //getters
    std::string getInputFileName() { return inFileName; }
    double getScale() { return scale; }
    int isPlayingFinished() { return playingFinished; }
    int isPlayigPaused() { return playingPaused; }
    int getCurrentFrameNumber() { return currentFrameNumber; }
    std::string getOutputFileName() { return outputFileName; }
    int getLastPressedKeyCode() { return lastPressedKeyCode; }
    std::string getLastErrorMsg() { return lastErrorMsg; }

    const std::vector< std::string > getLabelDefinitions() { return labelDefintionList; }
    
    const std::vector< std::string > getCurrentLabels() { return currentLabelList; }
    const char* getKeyBindingHelp() { return keyBindinHelp.c_str(); }
    
    std::string getLabel(int index) { return labelDefintionList[index]; }
    
private:
    std::string inFileName;
    std::string labelDefinitionFileName;
    std::string outputFileName;
    
    double scale;
    bool playingFinished;
    bool playingPaused;
    int currentFrameNumber;
    HAR::Video::Video theVideo;
    cv::Mat currentFrame;
    int lastPressedKeyCode;
    std::string lastErrorMsg;
    
    std::vector< std::string > labelDefintionList;
    std::vector< std::string > currentLabelList;
    
    std::string keyBindinHelp;
    
private://Utility Functions
    void UpdateVideoToFrame(std::string line);
    
    bool LoadLabelDefinitionsFromFile();
    bool LoadLabelsFromFile();
    
public:
    bool SaveLabelsToFile();
    void JumpVidepToLastIndex();
};


#endif /* defined(__HARLinked__LabelFrameVideoModel__) */
