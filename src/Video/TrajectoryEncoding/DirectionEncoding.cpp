/*
 * DirectionEncoding.cpp
 *
 *  Created on: Jun 1, 2016
 *      Author: pejman
 */

#include "DirectionEncoding.h"
#include <cmath>

namespace HAR {
namespace Video {
namespace TrajectoryEncoding {

DirectionEncoding::DirectionEncoding() {
	// TODO Auto-generated constructor stub

}

std::vector<float> DirectionEncoding::encode(
		const std::vector<cv::Point2f>& points) const {
	std::vector<float> result;
	result.reserve(points.size()- 1);
	const double PI = 2*acos(0.0);
	for (uint i = 1; i < points.size(); i++) {
		float dx = points[i].x - points[i - 1].x;
		float dy = points[i].y - points[i - 1].y;
		float angle=atan2(dy, dx);// atan2 takes into account the signs of dy and dx in callculating of atan
		if(angle > PI / 2)
			angle= PI-angle;
		if(angle < - PI / 2)
			angle= - PI - angle;
		result.push_back(angle);
	}
	return result;
}

DirectionEncoding::~DirectionEncoding() {
	// TODO Auto-generated destructor stub
}

} /* namespace TrajectoryEncoding */
} /* namespace Video */
} /* namespace HAR */
