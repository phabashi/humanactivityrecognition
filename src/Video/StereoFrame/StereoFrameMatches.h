/*
 * StereoFrameMatches.h
 *
 *  Created on: Feb 5, 2015
 *      Author: Pejman
 *      Copyright (c) 2015 Pejman. All rights reserved.
 */

#ifndef STEREOFRAMEMATCHES_H_
#define STEREOFRAMEMATCHES_H_

#include <opencv2/opencv.hpp>

#include "../Frame/FrameFeaturePoints.h"

namespace HAR {
namespace Video {

class StereoFrameMatches {
public:
	StereoFrameMatches() {}
	StereoFrameMatches(const std::vector<cv::DMatch>& initialMatches,
			const FrameFeaturePoints& originalSourceKeypoints,
			const FrameFeaturePoints& originalDestinationKeypoints);

	const unsigned long getSize() const { return singleMatches.size(); }

	const cv::DMatch& getMatches(int index) const { return singleMatches[index]; }
	const cv::KeyPoint& getSource(int index) const { return from[index]; }
	const cv::KeyPoint& getDestination(int index) const { return to[index]; }

	virtual ~StereoFrameMatches();

	const std::vector<cv::DMatch>& getMatches() const { return singleMatches; }
	const std::vector<cv::KeyPoint>& getSources() const { return from; }
	const std::vector<cv::KeyPoint>& getDestinations() const { return to; }
private:

private:

	std::vector<cv::DMatch> singleMatches;
	std::vector<cv::KeyPoint> from;
	std::vector<cv::KeyPoint> to;
};

} /* namespace Video */
} /* namespace HAR */

#endif /* STEREOFRAMEMATCHES_H_ */
