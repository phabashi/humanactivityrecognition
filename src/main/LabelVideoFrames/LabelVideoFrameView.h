//
//  LabelVideoFrameView.h
//  HARLinked
//
//  Created by pejman on 2015-07-08.
//  Copyright (c) 2015 pejman. All rights reserved.
//

#ifndef __HARLinked__LabelVideoFrameView__
#define __HARLinked__LabelVideoFrameView__

#include <stdio.h>
#include "LabelFrameVideoModel.h"

class LabelVideoView
{
public:
    LabelVideoView(LabelVideoModel& modelToDisplay);
    void Refresh();//Redraw information
    
    ~LabelVideoView();
    
private:
    LabelVideoModel& theModel;
    
    //Information Regarding the place of displaying information
    cv::Point2i frameNumberLocation;
    cv::Point2i fileNameLocation;
    cv::Point2i lastPressedKeyCodeLocation;
    cv::Point2i statusLocation;
    cv::Point2i scaleLocation;
    cv::Point2i errorMsgLocation;
    
    int width, height;
    
    void* informationWindow;
    void* errorWindow;
    void* labelDefinitionsWindow;
    void* outputWindow;
    void* helpWindow;
    
private://utility functions
    static void* create_newwin(int height, int width, int starty, int startx);
    static void destroy_win(void* window);
    void printInformation();
    void printError();
    void printHelp();
    void printLabelDefinitions();
    void printOutputLabels();
    
public://Input windows:
    std::string showStringInput(std::string message);
    void showString(std::string message);
    std::pair<int, int> showSetNewMapping();
};

#endif /* defined(__HARLinked__LabelVideoFrameView__) */
