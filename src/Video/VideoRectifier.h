/*
 * VideoRectifier.h
 *
 *  Created on: Mar 4, 2017
 *      Author: pejman
 */

#ifndef SRC_VIDEO_VIDEORECTIFIER_H_
#define SRC_VIDEO_VIDEORECTIFIER_H_

#include "AugmentedVideoBuffer.h"

class VideoRectifier {
public:
	VideoRectifier(HAR::Video::AugmentedVideoBuffer& left,
			HAR::Video::AugmentedVideoBuffer& right);

	void ExtractFeaturePoints();
	void MatchFeaturePoints(int frameNumber);
	void MatchFeaturePoints();
//	void FindFundamentalMatrxi();

	const std::vector<cv::Point2f>& getLeftPoints(int frameNumber) const { return leftPoints[frameNumber]; }
	const std::vector<cv::Point2f>& getRightPoints(int frameNumber) const { return rightPoints[frameNumber]; }

	const std::vector<cv::Point2f>& getLeftPoints() const { return allPointsLeft; }
	const std::vector<cv::Point2f>& getRightPoints() const { return allPointsRight; }

	virtual ~VideoRectifier();

private:
	HAR::Video::AugmentedVideoBuffer& left;
	HAR::Video::AugmentedVideoBuffer& right;

	std::vector<std::vector<cv::Point2f> > leftPoints;
	std::vector<std::vector<cv::Point2f> > rightPoints;

	std::vector<cv::Point2f> allPointsLeft;
	std::vector<cv::Point2f> allPointsRight;

	cv::Mat fundamentalMatrix;

};

#endif /* SRC_VIDEO_VIDEORECTIFIER_H_ */
