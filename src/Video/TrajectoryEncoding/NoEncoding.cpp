/*
 * NoEncoding.cpp
 *
 *  Created on: May 30, 2016
 *      Author: pejman
 */

#include "NoEncoding.h"

namespace HAR {
namespace Video {
namespace TrajectoryEncoding {

NoEncoding::NoEncoding() {
	// TODO Auto-generated constructor stub
}

std::vector<float> NoEncoding::encode(
		const std::vector<cv::Point2f>& points) const {
	std::vector<float> result;
	result.reserve(points.size() * 2);
	for (uint i = 0; i < points.size(); i++) {
		result.push_back(points[i].x);
		result.push_back(points[i].y);
	}
	return result;
}

NoEncoding::~NoEncoding() {
	// TODO Auto-generated destructor stub
}

std::vector<float> NoEncoding::encode3D(const std::vector<cv::Point3f>& trajectory3D) const {
	//TODO Impelement it again, with new interface!
	std::vector<float> result;
	result.reserve(trajectory3D.size() * 3);
	for (uint i = 0; i < trajectory3D.size(); i++) {
		result.push_back(trajectory3D[i].x);
		result.push_back(trajectory3D[i].y);
		result.push_back(trajectory3D[i].z);
	}

	return result;
}

} /* namespace TrajectoryEncoding */
} /* namespace Video */
} /* namespace HAR */

