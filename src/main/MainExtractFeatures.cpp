/*
 * MainExtractFeatures.cpp
 *
 *  Created on: May 1, 2015
 *      Author: Pejman
 *      Copyright (c) 2015 Pejman. All rights reserved.
 */


#include "../Video/AugmentedVideo.h"
#include "../Video/DisparityFlowDescriptorExtractor.h"
#include "../util/util.h"
#include "../Exceptions/ArgumentParseException.h"
#include "../Video/VideoBuffer.h"
#include "../Video/Display.h"

using namespace cv;
using namespace std;
using namespace HAR;
using namespace HAR::Exceptions;
using namespace HAR::Video;

class Arguments{
public:
	Arguments() {}
	Arguments(int argc, char* argv[]);
	static void printHelp();

	int getDescriptorLength() const { return descriptorLength; }
	int getFrameDifference() const {	return frameSynchronizationDifference; 	}
	const string& getInputFileLeft() const {	return inputFileLeft; }
	const string& getInputFileRight() const { return inputFileRight; }
	const string& getOutputFeatureFile() const {	return outputFeatureFile; }
	const string& getInterestPointDescriptor() const { return interestPointDescriptor; }
	const string& getInterestPointExtractor() const { return interestPointExtractor; }
	const string& getMatchingAlgorithm() const {	return matchingAlgorithm; }
	double getTheRatioToSelectBestMatches() const { return theRatioToSelectBestMatches; }
	bool IsRemoveHemogenousAreaSet() { return removeHemogenousArea; }

private:
	string inputFileLeft;
	string inputFileRight;
	string outputFeatureFile;
	int frameSynchronizationDifference;
	int descriptorLength;
	string interestPointExtractor;
	string interestPointDescriptor;
	string matchingAlgorithm;
	double theRatioToSelectBestMatches;
	bool removeHemogenousArea;

};

int main(int argc, char** argv) {
	Arguments args;
	try{
		args = Arguments(argc, argv);
	} catch(ArgumentParseException& exp)	{
		cout << "Error in parse of arguments!" << endl;
		args.printHelp();
		exit(1);
	}

//	VideoBuffer vb(args.getInputFileLeft(), args.getDescriptorLength());
//
//	HAR::Display::Show(vb);

	StereoAugmentedVideo inputFile(args.getInputFileLeft(), args.getInputFileRight(), args.getFrameDifference());

	HAR::Video::Video temp(args.getInputFileLeft());

	DisparityFlowDescriptorExtractor extractor(inputFile);

	bool videoReacheditsEnd = false;
	unsigned int start = 0;
	unsigned int length = 10 * args.getDescriptorLength();//(100 * frame)
	while(!videoReacheditsEnd)
	{
		cout <<"Try Extracting from : "<< start << " To " << start + length << endl;
		MyVideoDescrptor descriptor = extractor.ExtractMicroMovementDescriptors(
				args.getDescriptorLength(),				// The length of Descriptor (measured in frame)
				videoReacheditsEnd,						// If this is the last portion of video
				start, 									// The starting Frame Number
				length,									// The length of video to be processed (measured in frame)
				args.getInterestPointExtractor(),		// The interest point extractor
				args.getInterestPointDescriptor(),		// The interest point descriptor for matching left and right feature points
				args.getMatchingAlgorithm(),				// The matching algorithm for left and right feature points to be matched
				args.getTheRatioToSelectBestMatches(),
				args.IsRemoveHemogenousAreaSet());	// The portion in which the first found match should be better than the next one
		cout <<" Saving...." << endl;
		descriptor.Save(args.getOutputFeatureFile(), start == 0);//If start ==0 then overwrite the file!
		start += length;
		cout <<"Done. " << endl;
		if (videoReacheditsEnd) cout << "Finished." <<endl;
	}

	cout << "Done... Exiting..." << endl;
	return 0;
}

void Arguments::printHelp(){
	cout << "Usage: ExtractFeatures -l <infileL> -r <inFileR> -o outFile=<outputFeatueFile> -fs frames -fl length " << endl
		 << "-l <inFileL>: is the name of left input video file: (i.e. L.avi)" << endl
		 << "-r <inFileR>: is the name of right input video file: (i.e. R.avi)" << endl
		 << "-fs <frameSync>: is the value of difference between frame 0 of right video and left video measured in the number of frames. (e.g. -4) (default = 0)" << endl
		 << "-dl <featureLength>: is the length of descriptors that we are willing to extract. (default = 9)" << endl
		 << "-ip <AlgorithmName>: The Interest point extractor algorithm name: (Allowed: 'FAST' 'SIFT' 'Dense' 'SURF' ???? ) (default = 'FAST') " << endl
		 << "-ipd <AlgorithmName>: The Interest point descriptor Algorithm name: (Allowed: ??? ) (default = 'SIFT') " << endl
		 << "-ma <AlgorithmName>	: The matching algorithm name. (Allowed: ??? ) (default = 'BruteForce')" << endl
		 << "-ratio <ratio> 		: The ratio between 0 and 1 (default = 0.84" << endl
		 << "-rha [0,1]			: Either 0 or 1 and it specifies if the features in homogeneous areas should be removed (default 1 = true)"
		 << "-h 					: Show this help." << endl

		 << endl;
}

Arguments::Arguments(int argc, char* argv[]) {

	inputFileLeft = "";
	inputFileRight = "";
	outputFeatureFile = "";
	frameSynchronizationDifference = 0;
	descriptorLength = 9;
	interestPointExtractor = "FAST";
	interestPointDescriptor = "SIFT";
	matchingAlgorithm = "BruteForce";
	theRatioToSelectBestMatches = 0.84;
	removeHemogenousArea = true;

	for(int i=1; i< argc; i++) {
		if( strcmp(argv[i], "-l") == 0) {
			if(i < argc-1){
				inputFileLeft = argv[++i];
			} else {
				throw new ArgumentParseException("Ill formated input: switch '-l' The left input file name is missing.");
			}
		} else if( strcmp(argv[i], "-r") == 0) {
			if(i < argc-1) {
				inputFileRight = argv[++i];
			} else {
				throw new ArgumentParseException( "Ill formated input: switch '-r' The right input file name is missing.");
			}
		} else if( strcmp(argv[i], "-o") == 0){
			if(i < argc-1) {
				outputFeatureFile = argv[++i];
			} else {
				throw new ArgumentParseException( "Ill formated input: switch '-o' The ouput feature file name is missing.");
			}
		} else if( strcmp(argv[i], "-fs") == 0){
			if(i < argc-1) {
				frameSynchronizationDifference = stringTo<int>( argv[++i] );
			} else {
				throw new ArgumentParseException( "Ill formated input: switch '-fs' The frame synchronizatoin value is missing.");
			}
		} else if( strcmp(argv[i], "-dl") == 0){
			if(i < argc-1) {
				descriptorLength = stringTo<int>(argv[++i]);
			} else {
				throw new ArgumentParseException( "Ill formated input: switch '-dl' The descriptor length is missing.");
			}
		} else if( strcmp(argv[i], "-ip") == 0){
			if(i < argc-1) {
				interestPointExtractor = argv[++i];
			} else {
				throw new ArgumentParseException( "Ill formated input: switch '-ip' The interest point extraction algorithm name is missing.");
			}
		} else if( strcmp(argv[i], "-ipd") == 0){
			if(i < argc-1) {
				interestPointDescriptor = argv[++i];
			} else {
				throw new ArgumentParseException( "Ill formated input: switch '-ipd' The name of interest point descriptor algorithm is missing.");
			}
		} else if( strcmp(argv[i], "-ma") == 0){
			if(i < argc-1) {
				matchingAlgorithm = argv[++i];
			} else {
				throw new ArgumentParseException( "Ill formated input: switch '-ma' The matching algorithm name is missing.");
			}
		} else if( strcmp(argv[i], "-ratio") == 0){
			if(i < argc-1) {
				theRatioToSelectBestMatches = stringTo<double>(argv[++i]);
			} else {
				throw new ArgumentParseException( "Ill formated input: switch '-ratio' The ratio value is missing.");
			}
		} else if( strcmp(argv[i], "-rha") == 0){
			if(i < argc-1) {
				removeHemogenousArea = (stringTo<int>(argv[++i]) == 1)? true: false;
			} else {
				throw new ArgumentParseException( "Ill formated input: switch '-ratio' The ratio value is missing.");
			}
		} else if (strcmp (argv[i], "-h") == 0){
			printHelp();
			exit(1);
		}
	}
	//Check if minimum parameters have been provided:
	if(inputFileLeft == "")
		throw ArgumentParseException("You should provide left input file name with -l.");
	if (inputFileRight == "")
		throw ArgumentParseException("You should provide right input file name with -r.");
	if(outputFeatureFile == "")
		throw ArgumentParseException("You should provide output file name with -o.");
}

