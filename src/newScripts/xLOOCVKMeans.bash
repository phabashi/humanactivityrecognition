#!/bin/bash

#setting
inputFileList="./lists/FeatureFileList_Actor03_Activity_10_I5_Left.txt"
#Sample Generating file list
#ls ./xtest-data/features/ | grep -e "Left" | grep -f ./lists/ActivitySet_10.txt | grep -f ./lists/Actor_set3.txt | grep -f ./lists/Instances_5.txt
#length=15
source ~/HAR/newScripts/xdefault.bash

numberOfWords=${NOW:-40}
ActivityList="./lists/ActivitySet_10.txt"

featureFolderName="./xtest-data/Length_$length"
#SVMLearningParameters="-s 0 -t 3 -d 3 -c 1500 -q"
SVMLearningParameters="-s 0 -t 2 -c 1500 -q"
settingFile="SampleSetting.ini"

ExternalFeature=${EF:-"Trajectory"}
#ExternalFeature="HOF"

if [ $ExternalFeature = "Trajectory" ]; then
        FeatureNumbers='11-40' # Trajectory Features
elif [ $ExternalFeature = "HOG" ]; then
        FeatureNumbers='41-136' #HOG Features'
elif [ $ExternalFeature = "HOF" ]; then
        FeatureNumbers='137-244' #HOF Features'
elif [ $ExternalFeature = "MBHx" ]; then
        FeatureNumbers='245-340' #MBHx Features'
elif [ $ExternalFeature = "MBHy" ]; then
        FeatureNumbers='341-436' #MBHy Features'
elif [ $ExternalFeature = "MBH" ]; then
        FeatureNumbers='245-436' #MBHy Features'
fi

MicromovementFeaturesFolder=$featureFolderName/features/

leftVideoFileNames=(`cat $inputFileList`)
Actors=`cat $inputFileList | cut -f 1 -d "_" | uniq `

folderPrefix="./$featureFolderName/learning/$ExternalFeature/n-$numberOfWords-words" # find the file name part of $Activities

csvDataFolder="./$folderPrefix/data/csv"
SVMDataFolder="./$folderPrefix/data/svm"
SVMModelFolder="./$folderPrefix/models/svm"
KMeansModelFolder="./$folderPrefix/models/kmeans"
resultFolder="./$folderPrefix/results/"

mkdir -p $csvDataFolder
mkdir -p $SVMDataFolder
mkdir -p $SVMModelFolder
mkdir -p $KMeansModelFolder
mkdir -p $resultFolder

i=0

for Actor in ${Actors[@]}; do
	i=$(( i+1 ))
	echo "Do fold $i for actor $Actor"
	trainingSet=`cat $inputFileList | grep -v $Actor`
	testingSet=`cat $inputFileList | grep $Actor`

	modelFileName="$KMeansModelFolder/Fold-$Actor.yml"
	csvTrainFile="$csvDataFolder/Fold-$Actor.csv.train"
	csvTestFile="$csvDataFolder/Fold-$Actor.csv.test"
	svmTrainFile="$SVMDataFolder/Fold-$Actor.svm.train"
	svmTestFile="$SVMDataFolder/Fold-$Actor.svm.test"
	svmModelFileName="$SVMModelFolder/Fold-$Actor.svm.model"
	svmOutputFile="$SVMDataFolder/Fold-$Actor.svm.output"

	#make a KMeans model for this fold
	if [ ! -f $modelFileName ]; then
		echo "Make KMeans Model for fold $Actor"
		echo $trainingSet | tr ' ' '\n'  | ./bin/Merge -f $MicromovementFeaturesFolder | tr "\t" "," | cut -d ',' -f $FeatureNumbers | ./bin/KMeansClustering -c $numberOfWords -o $modelFileName as $settingFile -v  #-online  

		if [ $? != 0 ] ; then
			exit 1
		fi
	else
		echo "The moded file Exist! Skipping the clustering part!"
		echo 
		echo
		echo "Exists: $modelFileName"
	fi

	#Count The Words for this fold
	if [ ! -f $csvTrainFile ]; then
		echo "Count the words for trainig in fold $i"
		echo $trainingSet | tr ' ' '\n' | ./bin/CountTheWords -m $modelFileName -f $MicromovementFeaturesFolder -p 4 -c $csvTrainFile -lab $ActivityList -d tab -sel $FeatureNumbers
		if [ $? != 0 ] ; then
			exit 2
		fi 
	fi

	if [ ! -f $csvTestFile ]; then
		echo "Count the words for testing in fold $i"
		echo $testingSet  | tr ' ' '\n' | ./bin/CountTheWords -m $modelFileName -f $MicromovementFeaturesFolder -p 4 -c $csvTestFile -lab $ActivityList -d tab -sel $FeatureNumbers
		if [ $? != 0 ] ; then
			exit 3 
		fi 
	fi

	#Prepare data for libSVM
	#if [ ! -f $svmTrainFile ] || [ ! -f $svmTestFile ]; then
		echo "Preparing data for learning/testing with libSVM"
		./bin/csv2svm -i $csvTrainFile $csvTestFile -o $svmTrainFile $svmTestFile -n -lab $ActivityList
		if [ $? != 0 ] ; then
			echo "Erro Executing the following Command: "
			echo
			echo "./bin/csv2svm -i $csvTrainFile $csvTestFile -o $svmTrainFile $svmTestFile -n -lab $ActivityList"
			exit 4
		fi 
	#fi

	#Make a svm-model for the current data
	#if [ ! -f $svmModelFileName ]; then
		echo "Making SVM Model for the fold $i"
		./svmbin/svm-train $SVMLearningParameters $svmTrainFile $svmModelFileName
		if [ $? != 0 ] ; then
			exit 5
		fi 
	#fi

	#Measure the accuracy of svm-model for calssification
	#if [ ! -f $svmOutputFile ]; then
		echo "Measuring the accuracy for fold $i"
		./svmbin/svm-predict -q $svmTestFile $svmModelFileName $svmOutputFile
		if [ $? != 0 ] ; then
			exit 6
		fi 
	#fi 

done

echo "Making Report: "

expectedResult="$SVMDataFolder/all.expected"
outputResult="$SVMDataFolder/all.actual"

rm -f $expectedResult
rm -f $outputResult
j=0

makeReportForFold()
{
	i=$1
  j=$(( j + 1 ))

	expected="$resultFolder/Fold-$i.expected"
	aquired="$resultFolder/Fold-$i-out"
	svmTestFile="$SVMDataFolder/Fold-$i.svm.test"
	svmOutputFile="$SVMDataFolder/Fold-$i.svm.output"

	#echo "Making report for fold $i :"
	cat $svmTestFile | cut -d " " -f 1 > $expected
	cp $svmOutputFile $aquired
	#./bin/MakeReport -e $expected -o $aquired -lab $ActivityList
	cat $expected >> $expectedResult
	cat $aquired >> $outputResult
}

for i in ${Actors[@]}; do makeReportForFold "$i" ; done

./bin/MakeReport -e $expectedResult -o $outputResult -lab $ActivityList
echo "$folderPrefix"
