/*
 * FrameInformation.h
 *
 *  Created on: Feb 4, 2016
 *      Author: Pejman
 *      Copyright (c) 2016 Pejman. All rights reserved.
 */

#ifndef SRC_VIDEO_FRAME_FRAMEINFORMATION_H_
#define SRC_VIDEO_FRAME_FRAMEINFORMATION_H_

#include <opencv2/opencv.hpp>
#include <unordered_map>

#include "../../util/HashUtils.hpp"

namespace HAR {
namespace Video {

class FrameInformation {
public:
	FrameInformation();
	std::string Serialize() const;
	void deSerialize(std::string input);
	virtual ~FrameInformation();

public:
	void ClearRegionsOfInterest() { regionsOfInterest.clear(); }
	void ClearPointsOfInterest() { keypoints.clear(); interestPoints.clear(); }

	void AddToKeyPoints(cv::KeyPoint keypoint) {
		keypoints.push_back(keypoint);
		interestPoints.push_back(keypoint.pt);
		indexMap[keypoint.pt] = interestPoints.size() - 1;
	}

	void AddToRegionsOFInterest(cv::Rect_<double> rect ) { regionsOfInterest.push_back(rect); }

	const std::vector<cv::Rect_<float> >& getRegionsOfInterest() const { return regionsOfInterest; }
	const std::vector<cv::KeyPoint>& getKeypoints() { return keypoints; }
	const std::vector<cv::Point_<float> >& getPointsOfInterest() const { return interestPoints; }

	const cv::Mat& getDescriptors() const {
		return descriptors;
	}

	const cv::Mat getDescriptor(const cv::Point2f& point) const {
		auto ind =indexMap.find(point);
		if(ind == indexMap.end() )
			throw indexNotFoundException();
		return descriptors.row(ind->second);
	}

	const cv::Mat getDescriptor(int index) const {
		return descriptors.row(index);
	}

	void setDescriptors(const cv::Mat& descriptors) {
		this->descriptors = descriptors;
	}

private:
	std::vector<cv::Rect_<float> > regionsOfInterest;
	std::vector<cv::Point_<float> > interestPoints;
	std::vector<cv::KeyPoint> keypoints;
	std::unordered_map<cv::Point2f, int> indexMap;
	cv::Mat descriptors;

};

} /* namespace Video */
} /* namespace HAR */

#endif /* SRC_VIDEO_FRAME_FRAMEINFORMATION_H_ */
