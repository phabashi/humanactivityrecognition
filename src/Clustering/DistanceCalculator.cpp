/*
 * DistanceCalculator.cpp
 *
 *  Created on: Feb 14, 2016
 *      Author: Pejman
 *      Copyright (c) 2016 pejman. All rights reserved.
 */

#include "DistanceCalculator.h"

#include "../Logger/Logger.h"

#define ABS(A) (((A) > 0)? (A) : (- (A)) )

using namespace Pejman::Logger;

namespace HAR {

DistanceCalculator::DistanceCalculator() {
	// TODO Auto-generated constructor stub

}

//L1 Norm distance
float DistanceCalculator::ManhattanDistance(const cv::Point2f& p1,
		const cv::Point2f& p2) {
	float d = ABS(p1.x - p2.x) + ABS(p1.y - p2.y);
	return d;
}

//L2 Norm Distance
float DistanceCalculator::EucledeanDistance(const cv::Point2f& p1,
		const cv::Point2f& p2) {
	float dx = p1.x - p2.x;
	float dy = p1.y - p2.y;
	float d = dx * dx + dy * dy;
	return sqrt(d);
}

float DistanceCalculator::EucledeanDistance(const std::vector<float>& a,
		const std::vector<float>& b) {
	float d = 0;
	if (a.size() != b.size())
		throw DistanceCalculatorException(
				"The matrices should have the same size");
	for (unsigned int i = 0; i < a.size(); i++) {
		float dx = a[i] - b[i];
		d += dx * dx;
	}
	return sqrt(d);
}


//template < class T >
double DistanceCalculator::EucledeanDistance(const std::vector<double>& a,
		const std::vector<double>& b) {
	double sum = 0;
	if (a.size() != b.size())
		return std::numeric_limits<double>::max();
	int dimension = a.size();
	for (int i = 0; i < dimension; i++) {
		double x = a[i] - b[i];
		sum += x * x;
	}
	return sqrt(sum);
}

float DistanceCalculator::DTWDistance(const std::vector<float>& a,
		const std::vector<float>& b) {
	//Calculate the Dynamic Time warping distance!
	std::vector<std::vector<float> > cost(a.size(),
			std::vector<float>(b.size(), 0)); //initialize the matrix
	for (uint i = 0; i < a.size(); i++)
		for (uint j = 0; j < b.size(); j++) {
			float dx = a[i] - b[j];
			cost[i][j] = sqrt(dx * dx);
		}
	std::vector<std::vector<float> > accCost(a.size(),
			std::vector<float>(b.size(), 0)); //initialize the matrix
	for (uint i = 0; i < a.size(); i++) {
		accCost[i][0] = cost[i][0];
	}
	for (uint j = 0; j < b.size(); j++) {
		accCost[0][j] = cost[0][j];
	}

	for (uint i = 1; i < a.size(); i++)
		for (uint j = 1; j < b.size(); j++) {
			accCost[i][j] = std::min( { accCost[i - 1][j - 1],
					accCost[i - 1][j], accCost[i][j - 1] }) + cost[i][j];
		}

//	int i=a.size()-1;
//	int j=b.size()-1;
//	float totalCost=0;
//	while(i>0 && j >0){
//		if ( i == 0 )
//			j--;
//		if ( j == 0 )
//			i--;
//		float m = std::min( { accCost[i-1][j-1], accCost[i-1][j], accCost[i][j-1] } );
//		if ( m == accCost[i-1][j-1] ) {i--; j--; }
//		else if ( m == accCost[i][j-1] ) j--;
//		else i--;
//		totalCost += m;
//	}
//	while(i>0) totalCost += accCost[i--][0];
//	while(j>0) totalCost += accCost[0][j--];
	return accCost[a.size() - 1][b.size() - 1];
}

DistanceCalculator::~DistanceCalculator() {
	// TODO Auto-generated destructor stub
}

} /* namespace HAR */
