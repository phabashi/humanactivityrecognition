/*
 * StereoFrame.cpp
 *
 *  Created on: Jan 28, 2015
 *      Author: Pejman
 *      Copyright (c) 2015 Pejman. All rights reserved.
 */

#include "../StereoFrame/StereoFrame.h"

#include "../../Feature/FeatureDescriptorExtractor.h"
#include "../../Feature/FeatureDescriptorMatcher.h"
#include "../../Feature/FeatureDetector.h"
#include "../../OpticalFlow/OpticalFlow.h"
#include "../../OpticalFlow/OpticalFlowInformation.h"
#include "../../Logger/Logger.h"

using namespace HAR::Video::Features;
using namespace Pejman::Logger;

namespace HAR {
namespace Video {

StereoFrame::StereoFrame(cv::Mat& left, cv::Mat& right) {
	Logger::Log(LogLevel::VERBOSE, "Creating a new stereo Frame!");
	this->left = left;
	this->right = right;
	max = min = -1;
}

void StereoFrame::FindFeaturePoints(std::string algorithm)
{
	leftFeaturePoints = FeatureDetector::getInstance(algorithm).ExtractFeatureFromFrame(left);
	rightFeaturePoints = FeatureDetector::getInstance(algorithm).ExtractFeatureFromFrame(right);
}

void StereoFrame::UpdateFeaturePoints(const OpticalFlowInformation& leftFlowInfo, const OpticalFlowInformation& rightFlowInfo, const StereoFrameMatches& oldMatches)
{
	//        leftFeaturePoints = leftFlowInfo.getPreviousKeyPoints();
	//        rightFeaturePoints = rightFlowInfo.getPreviousKeyPoints();

	for (unsigned int i = 0; i < leftFlowInfo.getPreviousKeyPoints().size(); i++) {
		leftFeaturePoints.UpdateKeypointLocation(leftFlowInfo.getOriginialIndex(i), leftFlowInfo.getPreviousKeyPoints()[i].pt) ;
	}

	for (unsigned int i = 0; i < rightFlowInfo.getPreviousKeyPoints().size(); i++) {
		rightFeaturePoints.UpdateKeypointLocation(rightFlowInfo.getOriginialIndex(i), rightFlowInfo.getPreviousKeyPoints()[i].pt) ;
	}

	goodMatches = oldMatches;
	//        std::vector< cv::DMatch > gM;
	//        gM.clear();
	//        gM.reserve(oldMatches.getSize());
	//        for (int i =0; i<oldMatches.getSize(); i++)
	//        {
	//            cv::DMatch theMatch = oldMatches.getMatches(i);
	//            int source = theMatch.queryIdx;
	//            int destinaion = theMatch.trainIdx;
	//            //IF we tracked the sides of a stereo match in both images
	//            if(leftFlowInfo.getStatus()[source] && rightFlowInfo.getStatus()[destinaion])
	//            {
	//
	//                gM.push_back(theMatch);
	//            }
	//        }
	//        goodMatches = StereoFrameMatches(gM, leftFeaturePoints, rightFeaturePoints);

}

void StereoFrame::ExtractDescriptors(std::string algorithm)
{
	leftFeatureDescriptors = FeatureDescriptorExtractor::getInstance(algorithm).ExtractDescriptorsForFrame(left, leftFeaturePoints);
	rightFeatureDescriptors = FeatureDescriptorExtractor::getInstance(algorithm).ExtractDescriptorsForFrame(right, rightFeaturePoints);
}

void StereoFrame::MatchDescriptors(std::string algorithm)
{
	matches = FeatureDescriptorMatcher::getInstance(algorithm).MatchDescriptors(leftFeatureDescriptors, rightFeatureDescriptors);
}

void StereoFrame::DoubleMatchDescriptors(std::string algorithm)
{
	auto matchesLR = FeatureDescriptorMatcher::getInstance(algorithm).MatchDescriptors(leftFeatureDescriptors, rightFeatureDescriptors);
	auto matchesRL = FeatureDescriptorMatcher::getInstance(algorithm).MatchDescriptors(rightFeatureDescriptors, leftFeatureDescriptors);

}

/*
 * will remove all the points which located in homogenous area
 * based on a method which removes the IP if the eigen values of
 * auto-correlation matrix was smaller than a threshold [Reference 137 of papers]
 */
void StereoFrame::RemoveHomogenousAreaKeypoints()
{
	leftFeaturePoints.RemoveHomogenousAreaIP(left);
	rightFeaturePoints.RemoveHomogenousAreaIP(right);
}

const StereoFrameMatches& StereoFrame::KeepBestMatches(float ratio)
{
	std::vector< cv::DMatch > gM;
	for (unsigned int i = 0; i < matches.getMatches().size(); ++i)
	{
		//		for (int j = 0; j < matches.getMatches()[i].size(); j++)
		{
			/* The following make sure we select only the points that their second
			 * best match score is far enough from first match score. Note: We used
			 * Distance instead of match score for making the ratio: */
			if (matches.getMatches()[i][0].distance <= ratio * matches.getMatches()[i][1].distance)
			{
				gM.push_back(matches.getMatches()[i][0]);
			}
		}
	}
	goodMatches = StereoFrameMatches(gM, leftFeaturePoints, rightFeaturePoints);
	return goodMatches;
}

const StereoFrameMatches& StereoFrame::RemoveOutliersBasedOnTheirDistance(cv::Vec2f mean, cv::Vec2f var, double coeff)
{
	std::vector< cv::DMatch > gM;
	//	good_matches2.reserve(matches.getSize());

	auto stdDevX = sqrt(var[0]) * coeff / 2;
	auto stdDevY = sqrt(var[1]) * coeff / 2;

	for (size_t i = 0; i <  matches.getMatches().size(); i++)
	{
		for (unsigned int j = 0; j < matches.getMatches()[i].size(); j++)
		{
			cv::Point2f from = leftFeaturePoints.getKeyPoints()[matches.getMatches()[i][j].queryIdx].pt;
			cv::Point2f to = rightFeaturePoints.getKeyPoints()[matches.getMatches()[i][j].trainIdx].pt;

			//calculate local distance for each possible match
			//	        double dist = sqrt((from.x - to.x) * (from.x - to.x) + (from.y - to.y) * (from.y - to.y));
			double distx = ( from.x - to.x ) - mean[0] ;
			double disty = ( from.y - to.y ) - mean[1];

			//save as best match if local distance is in specified area and on same height
            if ( std::abs(distx) < stdDevX and std::abs(disty) < stdDevY)
			{
				gM.push_back(matches.getMatches()[i][j]);
				j = (int) matches.getMatches()[i].size();
			}
		}
	}
	goodMatches = StereoFrameMatches(gM, leftFeaturePoints, rightFeaturePoints);
	return goodMatches;
}

cv::Vec2f StereoFrame::calculateAllMatchesMean() const
{
	cv::Vec2f sum(0, 0);
	int count = 0;
	auto matches = this->matches.getMatches();
	for (auto it = matches.begin(); it != matches.end(); it ++)
	{
		for(auto ti = it->begin(); ti != it->end(); ti ++)
		{
			cv::Point2f from = leftFeaturePoints.getKeyPoint((*ti).queryIdx).pt;
			cv::Point2f to = rightFeaturePoints.getKeyPoint((*ti).trainIdx).pt;

			auto delta = cv::Vec2f(to.x - from.x, to.y - from.y);
			sum += delta;
			count ++;
		}
	}

	cv::Vec2f mean = cv::Vec2f( ((double) sum[0]) / count, ((double) sum[1]) / count);
	return mean;
}

cv::Vec2f  StereoFrame::calculateAllMatchesVar(cv::Vec2f mean) const
{
	cv::Vec2f sum(0, 0);
	auto matches = this->matches.getMatches();//matches.getMatches();
	int count = 0;
	for (auto it = matches.begin(); it != matches.end(); it ++)
	{
		for(auto ti = it->begin(); ti != it->end(); ti ++)
		{
			cv::Point2f from = leftFeaturePoints.getKeyPoints()[(*ti).queryIdx].pt;
			cv::Point2f to = rightFeaturePoints.getKeyPoint((*ti).trainIdx).pt;
			double dx = (to.x - from.x) - mean[0];
			double dy = (to.y - from.y) - mean[1];
			sum += cv::Vec2f(dx * dx, dy * dy);
			count ++;
		}
	}
	cv::Vec2f var =  sum / ( (double)count - 1);
	return var;
}

const cv::Mat& StereoFrame::getDisparityApproximationImage()
{
	return disparityImage;
}

void StereoFrame::calculateDisparity()
{
	auto temp = goodMatches.getMatches();// matches.getMatches();
	disparity.clear();
	if(temp.empty()) return;
	for(auto it = temp.begin(); it != temp.end(); it ++ )
	{
		cv::Point2f from = leftFeaturePoints.getKeyPoint((*it).queryIdx).pt;
		cv::Point2f to = rightFeaturePoints.getKeyPoint((*it).trainIdx).pt;

		//TODO What is the correct way of calculating disparity?

		/* Disparity may be the distance between two points in left an right image
		 * Or it can be only the x distance between two points if the cameras have
		 * only x displacement. Here for now I will use the distance!
		 *
		 */

		double dx = to.x - from.x;
		double dy = to.y - from.y;
		double d = sqrt(dx*dx + dy*dy);
		//        double d = dx;

		disparity.push_back(d);
	}
}

//TODO Remove
#include <iostream>
using namespace std;

void StereoFrame::calculateDisparityApproximationImage()
{
	int radious = 2;
	disparityImage = cv::Mat(left.rows, left.cols, CV_8UC3, cv::Scalar(255,255,255));

	if( disparity.size() == 0 ) return;
	//        int max = * (std::max_element(this->disparity.begin(), this->disparity.end()));
	//        int min = * (std::min_element(this->disparity.begin(), this->disparity.end()));

	//        int max = 30;
	//        int min = 0;
	//        cout << "MAX = " << max << "Min = " << min << endl;
	auto temp = goodMatches.getMatches();//matches.getMatches();
	for(auto it = temp.begin(); it != temp.end(); it ++ )
	{
		// for(auto ti = it->begin(); ti != it->end(); ti ++)
		{
			cv::Point2f from = leftFeaturePoints.getKeyPoints()[(*it).queryIdx].pt;
			cv::Point2f to = rightFeaturePoints.getKeyPoint((*it).trainIdx).pt;

			//			double dx = to.x - from.x;
			//			double dy = to.y - from.y;
			//			double d = sqrt(dx*dx + dy*dy);

			try
			{
				//                    double r =  (d - min) /(max-min) * 20;
				//                    r = 1 / r;
				//                    cout << r << endl;
				cv::circle(disparityImage, from, radious, cv::Scalar(0, 255, 0 ));
				cv::line(disparityImage, from, to, cv::Scalar(255, 0, 0));
				cv::circle(disparityImage, to, radious, cv::Scalar(0, 0, 255 ));
				//                    cv::circle(disparityImage, from, d, cv::Scalar(255));
				//                    cv::Mat roi = disparityImage( cv::Rect(from.x - halfPointSize, from.y - halfPointSize,  halfPointSize, halfPointSize));
				//			disparityImage.at<uchar>(from) = (d - min) /(max-min) * 255;
				//                    roi = (d - min) /(max-min);// * 255 ;
			}
			catch(cv::Exception& )
			{

			}
		}
	}
}

StereoFrame::~StereoFrame() {
	// TODO Auto-generated destructor stub
}

void StereoFrame::Scale(double scale)
{
	cv::resize(left, left, cv::Size(left.cols * scale, left.rows*scale));
	cv::resize(right, right, cv::Size(right.cols * scale, right.rows*scale));
}

void StereoFrame::CalculateDisparityOF()
{
	//FrameFeaturePoints fp;
	//TODO Load fp with dense features!
	FindFeaturePoints("FAST");

	info.setPointsForTracking(getLeftFeaturePoints());

	OpticalFlow::OpticalFlow opticalFlow(left);
	opticalFlow.Update(right, info);

	disparity.clear();

	for(int i=0; i< info.getMaxIndex(); i++ )
	{
		if (! info.NewPointsIndexIsValid(i) ) continue;//if the point already lost in tracking
		cv::Point2f from = info.getPreviousPoint(i);
		cv::Point2f to = info.getNextPoint(i);


		/* Disparity may be the distance between two points in left an right image
		 * Or it can be only the x distance between two points if the cameras have
		 * only x displacement. Here for now I will use the distance!
		 *
		 */

		double dx = to.x - from.x;
		double dy = to.y - from.y;
		double depth = sqrt(dx*dx + dy*dy);

		disparityOF[i] = depth;
	}
}


void StereoFrame::calculateDisparityApproximationImageOF()
{
	int radious = 2;
	disparityImage = cv::Mat(left.rows, left.cols, CV_8UC3, cv::Scalar(255,255,255));

	//if( disparity.size() == 0 ) return;

	for(int i=0; i< info.getMaxIndex(); i++ )
	{
		if (! info.NewPointsIndexIsValid(i) ) continue;//if the point already lost in tracking
		cv::Point2f from = info.getPreviousPoint(i);
		cv::Point2f to = info.getNextPoint(i);
       
		try
		{
            int intensity = 255 - 4* (disparityOF[i] - 50);
            radious = 3;// disparityOF[i] - 50;//(d - min) / (max-min) * 20;
			//                    double r =  (d - min) /(max-min) * 20;
			//                    r = 1 / r;
			//                    cout << r << endl;
			cv::circle(disparityImage, from, radious, cv::Scalar(0, intensity, 0 ));
			cv::line(disparityImage, from, to, cv::Scalar(intensity, 0, 0));
			cv::circle(disparityImage, to, radious, cv::Scalar(0, 0, intensity ));
			//                    cv::circle(disparityImage, from, d, cv::Scalar(255));
			//                    cv::Mat roi = disparityImage( cv::Rect(from.x - halfPointSize, from.y - halfPointSize,  halfPointSize, halfPointSize));
			//			disparityImage.at<uchar>(from) = (d - min) /(max-min) * 255;
			//                    roi = (d - min) /(max-min);// * 255 ;
		}
		catch(cv::Exception& )
		{

		}

	}
}

} /* namespace Video */
} /* namespace HAR */

