/*
 * NewOptialFlowInformation.h
 *
 *  Created on: May 21, 2015
 *      Author: Pejman
 *      Copyright (c) 2015 Pejman. All rights reserved.
 */

#ifndef SRC_OPTICALFLOW_NEWOPTICALFLOWINFORMATION_H_
#define SRC_OPTICALFLOW_NEWOPTICALFLOWINFORMATION_H_

#include <unordered_map>
#include "../Video/Frame/FrameFeaturePoints.h"

namespace HAR {

class NewOpticalFlowInformation {
public:
	NewOpticalFlowInformation();
	void setPointsForTracking(const FrameFeaturePoints& points);
	void UpdatePoints(std::vector<cv::Point2f>& nextFeaturePoints,
			std::vector<uchar>& status, const std::vector<float>& error);
	void ReplaceOldPointsWithNewOnes();
	std::vector<cv::Point2f> getPreviousPoints();

	long getMaxIndex() { return maxIndex; }

//	bool NewPointsIndexIsValid(int i) { return (nextPoints.find(i) != nextPoints.end()); }

    bool NewPointsIndexIsValid(int i) { return (validIndexes[i]); }

    cv::Point2f getPreviousPoint(int i) { return previousPoints[i]; }
	cv::Point2f getNextPoint(int i) { return nextPoints[i]; }

	virtual ~NewOpticalFlowInformation();

private:
	std::map<int, cv::Point2f> previousPoints;
	std::map<int, cv::Point2f> nextPoints;
    std::vector<bool> validIndexes;
    
	long maxIndex;

};

} /* namespace HAR */

#endif /* SRC_OPTICALFLOW_NEWOPTICALFLOWINFORMATION_H_ */
