/*
 * Trajectory.cpp
 *
 *  Created on: Feb 14, 2016
 *      Author: Pejman
 *      Copyright (c) 2016 Pejman. All rights reserved.
 */

#include "Trajectory.h"
#include <sstream>
#include "../Logger/Logger.h"

using namespace Pejman::Logger;

namespace HAR {
namespace Video {


HAR::Video::Trajectory::Trajectory(const Trajectory& other):
	startFrame{other.startFrame},
	points(other.points){
}

Trajectory::Trajectory(Trajectory&& other):
		Trajectory() {
	swap(*this, other);
}

Trajectory& Trajectory::operator =(Trajectory other) {
	swap(*this, other);
	return *this;
}

Trajectory::Trajectory(const cv::Mat& oneRowTrajectoryMat) {

	if(oneRowTrajectoryMat.rows !=1 ){
		Logger::Log(LogLevel::FATAL, "HAR::Video::Trajectory::Trajectory : We expect one row trajectory matrix!");
	}

	if(oneRowTrajectoryMat.cols % 2 !=1 ){
		Logger::Log(LogLevel::FATAL, "HAR::Video::Trajectory::Trajectory : We expect odd number of columns!");
	}
	startFrame = oneRowTrajectoryMat.at<float>(0,0);
	for(int j=1; j < oneRowTrajectoryMat.cols; j+=2){
		points.push_back(
				cv::Point2f(oneRowTrajectoryMat.at<float>(0, j),
						oneRowTrajectoryMat.at<float>(0, j+1)));
	}
}


std::string HAR::Video::Trajectory::Serialize( const cv::Size* frameSize , bool includeFrameNumber) const {
	std::stringstream result;
	if (includeFrameNumber)
		result << startFrame << " ";
	if(frameSize) {
		for (cv::Point2f p : points)
			result << p.x / frameSize->height << " " << p.y / frameSize->width << " ";
	} else{
		for (cv::Point2f p : points)
			result << p.x << " " << p.y << " ";
	}
	return result.str();
}

std::string HAR::Video::Trajectory::Serialize(
		TrajectoryEncoding::TEncoding& encodingAlgorithm) const {
	std::stringstream result;
	auto vec = encodingAlgorithm->encode(points);
	for (auto it = vec.begin(); it != vec.end(); it++) {
		result << *it << " ";
	}
	return result.str();
}

float Trajectory::getEnergy(const std::vector<cv::Point2f>& points) {
	float energy = 0;
	for (uint i = 1; i < points.size(); i++) {
		float dx = points[i].x - points[i - 1].x;
		float dy = points[i].y - points[i - 1].y;
		energy += dx * dx + dy * dy;
	}
	return sqrt(energy);
}

//std::string Trajectory::SerializeDeferentiation(bool includeFrameNumber,
//		bool normalize) const {
//	std::stringstream result;
//	if (includeFrameNumber)
//		result << startFrame << " ";
//
//	double m = (normalize)? getEnergy() : 1;
//
//	if (m == 0)
//		return "";
//
//	for (uint i = 1; i < points.size(); i++) {
//		result << (points[i].x - points[i - 1].x) / m << " "
//				<< (points[i].y - points[i - 1].y) / m << " ";
//	}
//	return result.str();
//}

float Trajectory::getEnergy() const {
	return getEnergy(points);
}

} /* namespace Video */
} /* namespace HAR */

