/*
 * MainMerge.cpp
 *
 *  Created on: May 1, 2015
 *      Author: Pejman
 *      Copyright (c) 2015 Pejman. All rights reserved.
 */

#include "../Video/Display.h"
#include "../util/util.h"
#include "../Logger/Logger.h"
#include "../util/CommaSeparatedReader.h"
#include <time.h>
#include <string>
#include <sstream>
#include <iostream>

using namespace std;
using namespace Pejman::Logger;
using namespace HAR::Video::TrajectoryEncoding;
using namespace HAR;

class Arguments {
public:
	Arguments(int argc, char* argv[]);

	void printHelp() {
		cout << "This program encode the uncoded trajectories."
			 << "Usage: encode [options]" << endl
			 << "options:" << endl
			 << " -e <encoding> 	: set the encoding" << endl
			 << "encoding Values:" << endl
			 << "\tDiff" << endl
			 << "\tNormDiff" << endl
			 << "\tDirection" << endl
			 << "\tCluster" << endl
			 << "\tSecondOrder" << endl
			 << "\tThirdOrder" << endl
			 << endl
			 << "The default encoding algorithm is NormDiff" << endl

			 << endl;
	}

	const string& getEncoding() const {
		return encoding;
	}

	const string& getInputFileName()  const {
		return inputFileName;
	}

	const string& getOutputFileName() const {
		return outputFileName;
	}

private:
	string encoding;
	string inputFileName;
	string outputFileName;
};

int main(int argc, char** argv) {
	Logger::getInstance().ChangeLevel(LogLevel::ERROR);
	Arguments args(argc, argv);
	TEncoding encoding = IEncoding::getEncoding(args.getEncoding());

	cv::Mat inputMat;
	if(args.getInputFileName() != "")
		inputMat = CommaSeparatedReader::ReadFloatMatrix(args.getInputFileName());
	else
		inputMat = CommaSeparatedReader::ReadFloatMatrix();

	Logger::Log(VERBOSE,"Input: %d x %d", inputMat.rows, inputMat.cols);

	cv::Mat outputMatrix = encoding->encode(inputMat);

	Logger::Log(VERBOSE,"Output: %d x %d", outputMatrix.rows, outputMatrix.cols);
	if (outputMatrix.cols == 0 || outputMatrix.rows == 0 ){
		Logger::Log(ERROR, "The returned matrix is empty! outputmatrix.size = %dx%d", outputMatrix.rows, outputMatrix.cols);
		return 0;
	}
	if(args.getOutputFileName() != "")
			CommaSeparatedReader::WriteFloatMatrix(args.getOutputFileName(), outputMatrix);
		else
			CommaSeparatedReader::WriteFloatMatrix(outputMatrix);

////	if()
//	while(cin){
//		stringstream input(line);
//		float x,y;
//
//		HAR::Video::Trajectory trajectory;
//		input >> x >> y;
//		while(input) {
//			trajectory.Add(cv::Point2d(x,y));
//			input >> x >> y;
//		}
//		cout << trajectory.Serialize(encoding) << endl;
//		std::getline(cin, line);
//	}
	return 0;
}

Arguments::Arguments(int argc, char* argv[]) :
		encoding("NormDiff"),
		inputFileName(""),
		outputFileName("")
{
	for (int i = 1; i < argc; i++) {
		if (strcmp(argv[i], "-h") == 0) {
			printHelp();
			exit(0);
		} else if( strcmp(argv[i], "-v") == 0 ){
			Logger::getInstance().ChangeLevel(LogLevel::VERBOSE);
		} else if (strcmp(argv[i], "-e") == 0) {
			if (i < argc - 1) {
				encoding = argv[++i];
			} else {
				cout << "Ill formated input: encoding algorithm should be specified after -e" 	<< endl;
			}
		} else if (strcmp(argv[i], "-o") == 0) {
			if (i < argc - 1) {
				outputFileName = argv[++i];
			} else {
				cout << "Ill formated input: Output file name should be specified after -o" << endl;
			}
		} else if (strcmp(argv[i], "-i") == 0) {
			if (i < argc - 1) {
				inputFileName = argv[++i];
			} else {
				cout << "Ill formated input: Input file name should be specified after -i" << endl;
			}
		} else {
				cout << "Invalid argument! '" << argv[i] << "' "
						<< endl;
				exit(0);
		}

	}//for
}
