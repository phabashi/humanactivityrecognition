/*
 * I3DEncoding.h
 *
 *  Created on: Dec 28, 2016
 *      Author: user
 */

#ifndef SRC_VIDEO_TRAJECTORYENCODING_I3DENCODING_H_
#define SRC_VIDEO_TRAJECTORYENCODING_I3DENCODING_H_

#include <memory>
#include <vector>
#include <opencv2/opencv.hpp>
#include "../Trajectory3D.h"

namespace HAR {
namespace Video {

class Trajectory;//forward declaration of trajectories

namespace TrajectoryEncoding {

class I3DEncoding {
public:
	// this function creates and returns an instance of 3DEncoding algotithm
	static std::unique_ptr<I3DEncoding> get3DEncoding(std::string algorithmName);


	//this function generates a 3D point from left and right points using disparity => TODO move to Trajectory3D
//	std::vector<cv::Point3f> Combine3D( const std::vector<cv::Point2f>& leftPoints,
//			const std::vector<cv::Point2f>& rightPoints);

	//encodes 3D points
	virtual std::vector<float> encode3D (const std::vector<cv::Point3f>& trajectory3D ) const = 0;

	cv::Mat encode3D(const cv::Mat& trajectories, cv::Size imgSize);

	const std::vector<bool>& getRectifiedCorrectly() const { return rectifiedCorrectly; }
	const std::vector<cv::Mat>& getH1() const { return H1; }
	const std::vector<cv::Mat>& getH2() const { return H2; }
	const std::vector<cv::Mat>& getF() const { return fundamentalMatrix; }

	virtual ~I3DEncoding();

protected:
	I3DEncoding();
	std::vector<cv::Mat> fundamentalMatrix;
	std::vector<cv::Mat> H1;
	std::vector<cv::Mat> H2;
	std::vector<bool> rectifiedCorrectly;

public:
	static std::vector<std::vector<Trajectory3D> > readTrajectoriesFromMatrix(
			const cv::Mat& trajectories);

};

typedef std::unique_ptr<I3DEncoding> T3DEncoding;

} /* namespace TrajectoryEncoding */
} /* namespace Video */
} /* namespace HAR */

#endif /* SRC_VIDEO_TRAJECTORYENCODING_I3DENCODING_H_ */
