/*
 * IOpticalFlow.cpp
 *
 *  Created on: Apr 30, 2016
 *      Author: pejman
 */

#include "IOpticalFlow.h"
#include "../Logger/Logger.h"

#include "FBOpticalFlow.h"
#include "LKOpticalFlow.h"
#include "IPOpticalFlow.h"

using namespace Pejman::Logger;

namespace HAR {
namespace Video {
namespace OpticalFlow {

std::unique_ptr<IOpticalFlow> IOpticalFlow::getOpticalFlowAlgorithm(std::string algorithmName) {
	Logger::Log(VERBOSE, "Checking the tracking algorithm.");
	IOpticalFlow* algorithm;
	if (algorithmName == "FB") {
		Logger::Log(VERBOSE, "Farn Back tracking algorithm have been selected.");
		algorithm = new FBOpticalFlow();
	} else if (algorithmName == "LK") {
		Logger::Log(VERBOSE, "Locaus Kanade Tracking algorithm have been selected.");
		algorithm = new LKOpticalFlow();
	} else if (algorithmName == "IP") {
		Logger::Log(VERBOSE, "Interest point Tracking algorithm have been selected.");
		algorithm = new IPOpticalFlow();
	} else {
		Logger::Log(LogLevel::FATAL, "Unknown optical flow algorithm '%s' have not been implemented!",
				algorithmName.c_str());
		throw std::runtime_error("Algorithm not found!");
	}
	return std::unique_ptr<IOpticalFlow>(algorithm);
}

IOpticalFlow::IOpticalFlow() {

}

IOpticalFlow::~IOpticalFlow() {
}

} /* namespace OpticalFlow */
} /* namespace Video */
} /* namespace HAR */
