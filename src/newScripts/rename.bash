#!/bin/bash
if [ $# != 2 ]; then
	echo "Usage $0 <activity> <class>"
	exit 0
fi
class="$2"
Activity="$1"

inputVideoFolder="/home/pejman/HAR/dataset/Windsor/selectedClips"
outputVideoFolder="/home/pejman/HAR/dataset/Windsor/test"

for file in `ls $inputVideoFolder/ | grep _${Activity}_`; do
	outFile="`echo $file | cut -f 1-5 -d "_"`_C_${class}_`echo $file | cut -f 6-7 -d "_"`"

	ln -s $inputVideoFolder/$file $outputVideoFolder/$outFile
done

