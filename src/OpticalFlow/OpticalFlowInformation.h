/*
 * OpticalFlowInformation.h
 *
 *  Created on: Feb 8, 2015
 *      Author: Pejman
 *      Copyright (c) 2015 Pejman. All rights reserved.
 */

#ifndef OPTICALFLOWINFORMATION_H_
#define OPTICALFLOWINFORMATION_H_

#include <vector>
#include <opencv2/opencv.hpp>
#include <unordered_map>

#include "../Video/Frame/FrameFeaturePoints.h"


namespace HAR {

class OpticalFlowInformation {
public:
	OpticalFlowInformation();
	const std::vector<cv::Point2f>& getPreviousPoints() const { return previousPoints; }
	const std::vector<cv::Point2f>& getNextPoints() const { return nextPoints; }
	const std::vector<uchar>& getStatus() const { return status; }
	const std::vector<float>& getError() const { return error; }

    int getOriginialIndex(unsigned int newIndex) const;

    void setPointsForTracking(const FrameFeaturePoints& featurePoints);
	void setPointsForTracking(const FrameFeaturePoints& featurePoints, std::vector<int>originalIndex);

	void ReplaceOldPointsWithNewOnes(std::vector<cv::Point2f>& nextFeaturePoints,
									std::vector<uchar>& status,
									const std::vector<float>& error);

	const std::vector<cv::KeyPoint>& getPreviousKeyPoints() const { return previousKeyPoints; }
	const std::vector<cv::KeyPoint>& getNextKeyPoints() const { return nextKeyPoints; }

	int getNewIndex(unsigned int oldIndex) const;
	virtual ~OpticalFlowInformation();

private:
	std::vector<cv::KeyPoint> previousKeyPoints;
	std::vector<cv::KeyPoint> nextKeyPoints;

    std::vector<cv::Point2f> previousPoints;
	std::vector<cv::Point2f> nextPoints;

	std::vector<uchar> status;
	std::vector<float> error;
    
    std::unordered_map<int, int> currentMapping;
    std::vector<int> originalMapping;
};

} /* namespace HAR */

#endif /* OPTICALFLOWINFORMATION_H_ */
