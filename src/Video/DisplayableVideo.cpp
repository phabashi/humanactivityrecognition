/*
 * DisplayableVideo.cpp
 *
 *  Created on: May 7, 2016
 *      Author: Pejman
 *      Copyright (c) 2016 Pejman. All rights reserved.
 */

#include "DisplayableVideo.h"

#include "../Logger/Logger.h"

using namespace Pejman::Logger;

namespace HAR {
namespace Video {

DisplayableVideo::DisplayableVideo():
		finished(false), paused(true), frameNumber(0), scale(1.0) {
}

DisplayableVideo::~DisplayableVideo() {
	// TODO Auto-generated destructor stub
}

cv::Mat DisplayableVideo::getResizedDisplayableFrame(int frameNumber) {
	cv::Mat tempFrame = getDisplayableFrame(frameNumber);
	if(!tempFrame.data) return tempFrame;
	Logger::Log(LogLevel::VERBOSE, "FrameSize(%d, %d)",frameSize.height, frameSize.width);
	if(frameSize.height == 0 || frameSize.width == 0) {
		//recalculate frameSize!
		frameSize = cv::Size( tempFrame.cols * scale, tempFrame.rows * scale);
	}
	cv::resize(tempFrame, tempFrame, frameSize);
	return tempFrame;
}


void DisplayableVideo::reset()  {
	setFinished(false);
	setPaused(true);
	frameNumber = 0;
	displayFrame = getResizedDisplayableFrame(frameNumber);
}

void DisplayableVideo::moveToNextDisplayabelFrame() {
	frameNumber++;
	displayFrame = getResizedDisplayableFrame(frameNumber);
}

void DisplayableVideo::setDisplayScale(double scale) {
	Logger::Log(LogLevel::VERBOSE, "Set Scale to %lf !", scale);
	this->scale = scale;
	frameSize = cv::Size( displayFrame.cols * scale, displayFrame.rows * scale);
	Logger::Log(LogLevel::VERBOSE, "FrameSize set! FrameSize(%d, %d)",frameSize.height, frameSize.width);

}

void DisplayableVideo::UpdateWithKey(int key) {
	key = (key & 0xFFFF);//to zero out special key controls
	switch (key) {
			case 27:
				finished = true;
				break;
			case 'T':
			case 't':
				Logger::Log( INFO, "Frame Number = %d", frameNumber);
				break;

			case ' ':
			case 'P':
			case 'p':
				paused = !paused;
				if(paused)
					Logger::Log( INFO, "This Frame Number = %d",  frameNumber);
				break;
			case 63234:// <-
				frameNumber -= 300;
				if(frameNumber < 0) frameNumber = 0;
				displayFrame = getResizedDisplayableFrame(frameNumber);//Set the frame number
				Logger::Log( INFO, "FB: Frame Number = %d", frameNumber);
				break;
			case 63235:// ->
				frameNumber += 300;
				displayFrame = getResizedDisplayableFrame(frameNumber);
				Logger::Log( INFO, "FF: Frame Number = %d", frameNumber);
				break;
			case '<' :
				frameNumber -= 10;
				displayFrame = getResizedDisplayableFrame(frameNumber);
				Logger::Log( INFO, "Tune Back: Frame Number = %d", frameNumber);
				break;

			case '>' :
				frameNumber += 10;
				displayFrame = getResizedDisplayableFrame(frameNumber);
				Logger::Log( INFO, "Tune Forward Frame Number = %d", frameNumber);
				break;

			case ',':
				frameNumber -= 1;
				displayFrame = getResizedDisplayableFrame(frameNumber);
				Logger::Log( INFO, "Previous Frame: %d", frameNumber);
				break;


				// Not so useful and not well designed feature! deleted.
//			case 'g':
//			case 'G':
//				cout << "Enter Frame Number : ";
//				cin >> frameNumber;
//				displayFrame = temp.getFrame(frameNumber);
//				Logger::Log( INFO,  "Got to Frame: ", frameNumber);
//				break;
			case '.':
				frameNumber += 1;
				displayFrame = getResizedDisplayableFrame(frameNumber);
				Logger::Log( INFO, "Next Frame: ", frameNumber);
				break;
			case -1: break; //Do nothing
			default:
				Logger::Log( WARNING, "Unknown Key = %d", key);
				break;
			}

}

} /* namespace Exceptions */
} /* namespace HAR */
