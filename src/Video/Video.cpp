/*
 * Video.cpp
 *
 *  Created on: Dec 18, 2014
 *      Author: Pejman
 *      Copyright (c) 2014 Pejman. All rights reserved.
 */

#include "Video.h"
#include "../Logger/Logger.h"

using namespace Pejman::Logger;

namespace HAR {
namespace Video {

Video::Video() {
	reSize = NULL;
	mirror = false;
	isGrayscale = true;
	scale=1.0;
}

Video::Video(std::string fileName, bool isGrayscale) {
	this->isGrayscale = isGrayscale;
	reSize = NULL;
	mirror = false;
	scale=1.0;
	LoadFromFile(fileName);
}

void Video::LoadFromFile(std::string fileName) {
	if (fileName == "camera0")
		cvCapture.open(0);
	else
		cvCapture.open(fileName);

	if(cvCapture.isOpened()){
		framePerSec = cvCapture.get(CV_CAP_PROP_FPS);
		width = cvCapture.get(CV_CAP_PROP_FRAME_WIDTH);
		height = cvCapture.get(CV_CAP_PROP_FRAME_HEIGHT);
		totalNumberOfFrames = cvCapture.get(CV_CAP_PROP_FRAME_COUNT);
	}
}

void Video::SaveToFile(std::string fileName) {
	//TODO Implement this method
	throw std::runtime_error(std::string("Not implemented exception!"));
}

const cv::Mat& Video::getNextFrame() {
	cvCapture >> currentFrame;
	if (!currentFrame.data)
		return currentFrame;
	if(! reSize && scale != 1.0) {
		Logger::Log(VERBOSE, "Ratio is %2.2f", scale);
		enableResize(cv::Size(currentFrame.cols * scale, currentFrame.rows * scale));
	}
	if (reSize)
		cv::resize(currentFrame, currentFrame, *reSize);
	if (mirror)
		cv::flip(currentFrame, currentFrame, 1);
	if (isGrayscale) {
		cv::cvtColor(currentFrame, currentFrame, CV_RGB2GRAY);
	}
	return currentFrame;
}

const cv::Mat& Video::getFrame(unsigned long frameNumber) {
	cvCapture.set(CV_CAP_PROP_POS_FRAMES, frameNumber);
	return Video::getNextFrame(); // do not call the derived class getNextFrame!
}

void Video::Reset() {
	cvCapture.set(CV_CAP_PROP_POS_FRAMES, 0);
}

Video::~Video() {
	if (reSize)
		delete reSize;
}

void Video::saveSubVideoToFile(std::string fileName, int startFrame,
		int endFrame) {
	cv::Mat theFrame = this->getFrame(startFrame);

	int fourcc = CV_FOURCC('M', 'P', 'E', 'G');
//	fourcc = this->cvCapture.get(CV_CAP_PROP_FOURCC);
	int fps = this->cvCapture.get(CV_CAP_PROP_FPS);
	cv::Size size(theFrame.cols, theFrame.rows);
	cv::VideoWriter writer;
	writer.open(fileName, fourcc, fps, size);
	for (int i = startFrame; i < endFrame; i++) {
		//imshow("test", theFrame);
		//cv::waitKey(10);
		writer << theFrame;
		theFrame = this->getNextFrame();
	}
}

void Video::enableResize(float ratio) {
	this->scale=ratio;
}

void Video::enableResize(cv::Size size) {
	if (reSize == NULL) {
		reSize = new cv::Size(size);
		InvalidateBuffer();
	} else if (size != *reSize) {
		delete reSize;		//remove old resize size
		reSize = new cv::Size(size);
		InvalidateBuffer();
	}
}

void Video::disableResize() {
	if (reSize) {
		delete reSize;
		reSize = NULL;
		InvalidateBuffer();
	}
}

void Video::enableMirorring() {
	if (!mirror) {
		mirror = true;
		InvalidateBuffer();
	}
}

void Video::dispableMirorring() {
	if (mirror) {
		mirror = false;
		InvalidateBuffer();
	}
}

void Video::setGrayscale(bool value) {
	if (isGrayscale != value) {
		isGrayscale = value;
		InvalidateBuffer();
	}
}
} /* namespace Video */
} /* namespace HAR */
