/*
 * ClusterModel.h
 *
 *  Created on: Nov 17, 2015
 *      Author: Pejman
 *      Copyright (c) 2015 Pejman. All rights reserved.
 */

#ifndef SRC_COUNTTHEWORDS_CLUSTERMODEL_H_
#define SRC_COUNTTHEWORDS_CLUSTERMODEL_H_

#include <opencv2/opencv.hpp>
#include "DistanceCalculator.h"

namespace HAR {

class ClusterModel {
public:
	ClusterModel(cv::Mat centers);
	inline int getCluster(const cv::Mat& vector) const;
	cv::Mat CountTheWords(cv::Mat samples) const;
	virtual ~ClusterModel();

	//double euclideanDistance(const cv::Mat& a, const cv::Mat& b) const;

private:
	cv::Mat centers;
};


inline int ClusterModel::getCluster(const cv::Mat& vector) const {
//	Logger::Log(LogLevel::VERBOSE, "Get Cluster function centeres= %d, vector = %d x %d", centers.row(0).cols,vector.rows, vector.cols);
	int mini = 0;
	double min = DistanceCalculator::EucledeanDistance(centers.row(mini), vector);
			//cv::sum( abs(centers.row(mini) - vector) )[0];
	for(int r = 0; r < centers.rows; r++)
	{
		//cv::Mat distance = abs(centers.row(r) - vector);// BUG: This was not the Euclidean distance!
	 	double s = DistanceCalculator::EucledeanDistance(centers.row(r), vector);//cv::sum(distance)[0];
		if(s<min)
		{
			min = s;
			mini = r;
		}
	}
	return mini;
}

} /* namespace HAR */


#endif /* SRC_COUNTTHEWORDS_CLUSTERMODEL_H_ */
