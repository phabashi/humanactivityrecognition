/*
 * Arguments.h
 *
 *  Created on: Nov 19, 2016
 *      Author: Dhawal Rank
 */

#ifndef SRC_EXTRACTTRAJECTORY3D_ARGUMENTS_H_
#define SRC_EXTRACTTRAJECTORY3D_ARGUMENTS_H_
#include <string>
#include "../../Exceptions/ArgumentParseException.h"

namespace HAR {
namespace NewExtractFeatures3D {

class Arguments {
public:
	Arguments() :
		inputFileLeft(""),
		inputFileRight(""),
		outputTrajectoryFile(""),
		frameSynchronizationDifference(0),
		descriptorLength(0),
		interestPointExtractor(""),
		interestPointDescriptor(""),
		matchingAlgorithm(""),
		theRatioToSelectBestMatches(0.65),
		removeHemogenousArea(false),
		displayEnabled(false),
		scale(false),
		outputFeatureFile(""),
		trackingAlgorithm(""),
		applicationSettingFileName(""),
//		trajectoryFileName(""),
//		normalizeOutput(false),
		trajectoryEncoding(""),
		detectMotion(true),
		minEnergy(0.0),
		isMultiscaleEnabled(false),
		samplingScale(1),
		initialScale(1),
		showMatches(false)
		{}

	Arguments(int argc, char* argv[]);
	static void printHelp();

	int getDescriptorLength() const {
		return descriptorLength;
	}
	int getFrameDifference() const {
		return frameSynchronizationDifference;
	}
	const std::string& getInputFileLeft() const {
		return inputFileLeft;
	}
	const std::string& getInputFileRight() const {
		return inputFileRight;
	}
	const std::string& getOutputFeatureFile() const {
		return outputFeatureFile;
	}
	const std::string& getOutputTrajectoryFile() const {
		return outputTrajectoryFile;
	}
	const std::string& getInterestPointDescriptor() const {
		return interestPointDescriptor;
	}
	const std::string& getInterestPointExtractor() const {
		return interestPointExtractor;
	}
	const std::string& getMatchingAlgorithm() const {
		return matchingAlgorithm;
	}
	double getTheRatioToSelectBestMatches() const {
		return theRatioToSelectBestMatches;
	}
	bool IsRemoveHemogenousAreaSet() const {
		return removeHemogenousArea;
	}
	bool IsDisplayEnabled() const {
		return displayEnabled;
	}
	const std::string& getTrackingAlgorithm() const {
		return trackingAlgorithm;
	}

//	const std::string& getTrajectoryFileName() const {
//		return trajectoryFileName;
//	}

	const std::string& getApplicationSettingFileName() const {
		return applicationSettingFileName;
	}

	float getScale() const {
		return scale;
	}

//	bool getNormalizeOutput() const {
//		return normalizeOutput;
//	}

	bool getDetectMotion() const {
		return detectMotion;
	}

	float getMinEnergy() const {
		return minEnergy;
	}
	bool IsMultiscaleEnabled() const {
		return isMultiscaleEnabled;
	}
	bool getShowMatches() const{
		return showMatches;
	}
	const std::string& getTrajectoryEncoding() const {
		return trajectoryEncoding;
	}

	int getSamplingScale() const {
		return samplingScale;
	}

	float getInitialScale() const {
		return initialScale;
	}

private:
	std::string inputFileLeft;
	std::string inputFileRight;
	std::string outputTrajectoryFile;
	int frameSynchronizationDifference;
	int descriptorLength;
	std::string interestPointExtractor;
	std::string interestPointDescriptor;
	std::string matchingAlgorithm;
	double theRatioToSelectBestMatches;
	bool removeHemogenousArea;
	bool displayEnabled;
	float scale;
	std::string outputFeatureFile;
	std::string trackingAlgorithm;
//	std::string trajectoryFileName;
	std::string applicationSettingFileName;
	std::string trajectoryEncoding;
//	bool normalizeOutput;
	bool detectMotion;
	float minEnergy;
	bool isMultiscaleEnabled;
	int samplingScale;
	float initialScale;
	bool showMatches;
};

} /* namespace NewExtractFeatures */
} /* namespace HAR */

#endif /* SRC_NEWEXTRACTFEATURES_ARGUMENTS_H_ */
