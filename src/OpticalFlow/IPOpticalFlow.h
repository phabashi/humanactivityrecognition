/*
 * IPOpticalFlow.h
 *
 *  Created on: Apr 30, 2016
 *      Author: pejman
 */

#ifndef SRC_OPTICALFLOW_IPOPTICALFLOW_H_
#define SRC_OPTICALFLOW_IPOPTICALFLOW_H_

#include "IOpticalFlow.h"

namespace HAR {
namespace Video {
namespace OpticalFlow {

class IPOpticalFlow: public IOpticalFlow {
public:
	IPOpticalFlow();

	virtual void ExtractOpticalFlow(AugmentedVideoBuffer& video, int length);

	virtual ~IPOpticalFlow();
private:
	float thresholdA;
	float distanceBetweenBestMatchAndSecondBest;
};

} /* namespace OpticalFlow */
} /* namespace Video */
} /* namespace HAR */

#endif /* SRC_OPTICALFLOW_IPOPTICALFLOW_H_ */
