/*
 * FrameInformation.cpp
 *
 *  Created on: Feb 4, 2016
 *      Author: Pejman
 *      Copyright (c) 2016 Pejman. All rights reserved.
 */

#include "FrameInformation.h"

#include <sstream>

namespace HAR {
namespace Video {

FrameInformation::FrameInformation() {
	// TODO Auto-generated constructor stub

}

std::string FrameInformation::Serialize() const {
	std::stringstream stream;
	stream << regionsOfInterest.size() << " ";
	for(auto i=regionsOfInterest.begin(); i!= regionsOfInterest.end(); i++ ){
		stream << i->x << " " << i->y << " " << i->height << " " << i->width << " ";
	}
//	stream << interestPoints.size() << " ";
//	for(auto i=interestPoints.begin(); i!= interestPoints.end(); i++ ){
//		stream << i->x << " " << i->y << " ";
//	}
	return stream.str();
}

void FrameInformation::deSerialize(std::string input) {
	regionsOfInterest.clear();
//	interestPoints.clear();
	std::stringstream stream(input);
	int size;
	stream >> size;
	for(int i=0; i<size; i++ ){
		cv::Rect_<double> r;
		stream >> r.x;
		stream >> r.y;
		stream >> r.height;
		stream >> r.width;
		regionsOfInterest.push_back(r);
	}
//	stream >> size;
//	for(int i=0; i<size; i++ ){
//		cv::Point_<double> p;
//		stream >> p.x;
//		stream >> p.y;
//		interestPoints.push_back(p);
//	}
}

FrameInformation::~FrameInformation() {
}

//cv::Rect FrameInformation::getRectangle(const cv::Rect_<double> rect,
//		const cv::Mat& frame) {
//	return cv::Rect(rect.x*frame.cols, rect.y*frame.rows, rect.width*frame.cols, rect.height*frame.rows);
//}
//
//cv::Point HAR::FrameInformation::getPosition(const cv::KeyPoint& keyPoint,
//		const cv::Mat& frame) {
//	return cv::Point(keyPoint.pt.x * frame.cols, keyPoint.pt.y * frame.rows);
//}

//const std::vector<cv::Point2f > FrameInformation::getPointsOfInterest(
//		const cv::Size& frameSize) const {
//	//TODO buffering might help speeding up, if we have call this function too many times!
//	std::vector<cv::Point2f > result;
//	for(int i=0; i< interestPoints.size(); i++){
//		result.push_back( cv::Point2f( interestPoints[i].x * frameSize.width,
//				interestPoints[i].y * frameSize.height) );
//	}
//	return result;
//}

} /* namespace Video */
} /* namespace HAR */
