#!/bin/bash
echo
echo "This script will make nessessary links to the dataset original video files"
echo 

if [ $# -ne 2 ]; then
	echo "Usage: $0 <fileMapping>  <pathToDatasetMainFolder>"
	exit 1
fi

fileMapping=$1
newPath=$2

linkNames=($( more $fileMapping | cut -d " " -f 1 ))
fileNames=($( more $fileMapping | cut -d " " -f 2 ))

echo "Number of instances are: " ${linkNames[0]}
for ((i=0;i<${#linkNames[@]};++i)); do
	fileName=${fileNames[i]/datasetPath/$newPath}
	linkName=${linkNames[i]}
	echo "Making link $linkName to $fileName"
	ln -s $fileName video/$linkName
done

