#~/bin/bash

export opticalFlowAlgorithm=${OFA:-"FB"}
export interestPointAlgorithm="FAST"
export descriptorAlgorithm="SIFT"
export normalizeOutput=${Norm:-"Yes"}
export detectMotion=${DM:-"Yes"}
export ratio="1"
export scale=${scale:-"5"} #scale the original video to half size in each direction
export length=${length:-13}
export numberOfWords=${NOW:-16}
export minEnergy=${minEnergy:-"-1"}

export MULTISCALE="-multiscale"
export encoding="NormDiff"
export samplingScale=${SS:-1}
