/*
 * MainMerge.cpp
 *
 *  Created on: May 1, 2015
 *      Author: Pejman
 *      Copyright (c) 2015 Pejman. All rights reserved.
 */

#include "Display.h"
#include "util.h"

using namespace std;
//using namespace cv;
//using namespace HAR;

int main(int argc, char** argv)
{
    if(argc <= 2)
    {
        cout << "Usage: Merge [List of input files] output" << endl;
        return -1;
    }
    vector<string> inputFileNames;
    for(int i = 1; i<argc -1; i++)
        inputFileNames.push_back(argv[i]);
    string outFileName = argv[argc-1];

    ofstream out(outFileName);
    bool headerWritten = false;
    for( auto inFile: inputFileNames)
    {
        string line;
        ifstream in(inFile);
        getline(in, line);//read header
        if(! headerWritten)
        {
            line = line.substr( line.find_first_of(',') + 1 );//remove the frameNumber
            out << line << endl;
            headerWritten = true;
        }
        while (in) {
            getline(in, line);//read a line
            line = line.substr( line.find_first_of(',') + 1 );//remove the frameNumber
            out << line << endl;
        }

    }

    return 0;
}
