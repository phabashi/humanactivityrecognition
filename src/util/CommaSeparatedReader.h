/*
 * CommaSeparatedReader.h
 *
 *  Created on: Nov 17, 2015
 *      Author: Pejman
 *      Copyright (c) 2015 Pejman. All rights reserved.
 */

#ifndef SRC_UTIL_COMMASEPARATEDREADER_H_
#define SRC_UTIL_COMMASEPARATEDREADER_H_

#include <opencv2/opencv.hpp>
#include <fstream>
#include <vector>
#include <set>

namespace HAR {


class CommaSeparatedReader {
private:
	static char delimiter;
	static std::string delimiters;

public:
	CommaSeparatedReader();
	static void setDelimiter(char newDelimiter) { delimiter = newDelimiter;  }
	static void addDelimiter(char newDelimiter) {
		std::size_t pos = delimiters.find_first_of(newDelimiter);
		if(pos == std::string::npos) //if the delimiter does not already exists
			delimiters += newDelimiter;
	}
	static void removeDelimiter(char oldDelimiter) {
		std::size_t pos = delimiters.find_first_of(oldDelimiter);
		if(pos == std::string::npos) return;//there is no such delimiter
		delimiters = delimiters.substr(0, pos-1) + delimiters.substr(pos+1);
	}

	static inline cv::Mat ReadFloatMatrix(){
		return ReadFloatMatrix(std::cin);
	}

	static inline cv::Mat ReadFloatMatrix(std::string fileName) {
		std::ifstream input(fileName);
		return ReadFloatMatrix(input);
	}

	static cv::Mat ReadFloatMatrix(std::istream& input);

	static cv::Mat ReadFloatLabeledMatrix(std::string fileName, std::string lebelDefinitionFileName);
	static cv::Mat ReadFloatLabeledMatrix(std::istream& input, std::string lebelDefinitionFileName);

	static std::vector<float> ReadLineFloat();
	static std::vector<float> ReadLineFloat(std::istream& input);

	static void WriteFloatMatrix(cv::Mat);
	static void WriteFloatMatrix(const std::string& fileName, cv::Mat);
	static void WriteFloatMatrix(std::ostream& ostream, cv::Mat);

	template <class T>
	static void WriteMatrix(cv::Mat);
	template <class T>
	static void WriteMatrix(std::string& fileName, cv::Mat);
	template <class T>
	static void WriteMatrix(std::ostream& ostream, cv::Mat);
	template <class T>
	static void WriteMatrixWithoutClassLabelInSVMFormat(std::ostream& ostream, cv::Mat);
	template <class T>
	static void WriteMatrixWithClassLabelInSVMFormat(std::ostream& ostream, cv::Mat);

	virtual ~CommaSeparatedReader();
};

} /* namespace HAR */

template<class T>
inline void HAR::CommaSeparatedReader::WriteMatrix(cv::Mat mat) {
	WriteMatrix<T>(std::cout, mat);
}

template<class T>
inline void HAR::CommaSeparatedReader::WriteMatrix(std::string& fileName,
		cv::Mat mat) {
	std::ofstream temp(fileName);
	WriteMatrix<T>(temp, mat);
}

template<class T>
inline void HAR::CommaSeparatedReader::WriteMatrix(std::ostream& ostream,
		cv::Mat mat) {
	for(int i=0; i<mat.rows; i++)
		{
			for(int j=0; j<mat.cols -1; j++)
				ostream << mat.at<T>(i, j) << ", ";
			ostream << mat.at<T>(i, mat.cols-1) << std::endl;
		}
}

template<class T>
inline void HAR::CommaSeparatedReader::WriteMatrixWithoutClassLabelInSVMFormat(std::ostream& ostream,
		cv::Mat mat) {
	bool flag = false;
	for(int i=0; i<mat.rows; i++){
		for(int j=0; j<mat.cols; j++)
			if( mat.at<T>(i, j)  != 0 ) {
				ostream << ((flag)? " " : "") << j << ":" << mat.at<T>(i, j);
				if (! flag ) flag = true;
			}
		if(i + 1 < mat.rows)
			ostream << std::endl;// To move to the next row!
	}
	ostream << std::endl;
}

template<class T>
inline void HAR::CommaSeparatedReader::WriteMatrixWithClassLabelInSVMFormat(std::ostream& ostream,
		cv::Mat mat) {

	for(int i=0; i<mat.rows; i++){
		ostream << mat.at<T>(i, 0);
		for(int j=1; j<mat.cols; j++)
			if( mat.at<T>(i, j)  != 0 ) {
				ostream << " " << j << ":" << mat.at<T>(i, j);
			}
		if(i + 1 < mat.rows)
			ostream << std::endl;// To move to the next row!
	}
	ostream << std::endl;
}
#endif /* SRC_UTIL_COMMASEPARATEDREADER_H_ */
