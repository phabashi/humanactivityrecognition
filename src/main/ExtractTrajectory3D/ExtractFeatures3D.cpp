/*
 * ExtractFeatures3D.cpp
 *
 *  Created on: Jan 1, 2017
 *      Author: dhawal
 */

#include "ExtractFeatures3D.h"

namespace HAR{
namespace ExtractFeatures3D{

void ExtractFeatures3D::ExtractFeatures(AugmentedVideoBuffer& leftVideo, AugmentedVideoBuffer& rightVideo, const Arguments& args) {

	if (args.getDetectMotion() == true) {
			MotionDetection mdr, mdl;
			Logger::Log(VERBOSE, "Detecting motion of left video object.");
			mdl.DetectMotion(leftVideo);
			Logger::Log(VERBOSE, "Detecting motion of right video object.");
			mdr.DetectMotion(rightVideo);
		}

		Logger::Log(VERBOSE, "Creating feature Detector object.");
		HAR::Video::Features::FeatureDetector& detector =
				HAR::Video::Features::FeatureDetector::getInstance(
						args.getInterestPointExtractor());

		if (args.getTrackingAlgorithm() == "IP") {
			Logger::Log(VERBOSE, "Extracting features from left video.");
			detector.ExtractFeatures(leftVideo, args.getDetectMotion());
			Logger::Log(VERBOSE, "Extracting features from right video.");
			detector.ExtractFeatures(rightVideo, args.getDetectMotion());

			HAR::Video::Features::FeatureDescriptorExtractor& extractor =
					HAR::Video::Features::FeatureDescriptorExtractor::getInstance(
							args.getInterestPointDescriptor());

			extractor.ExtractDescriptors(leftVideo);
			extractor.ExtractDescriptors(rightVideo);
		} else {
			Logger::Log(VERBOSE, "Extracting features from left video.");
			detector.ExtractFeatures(leftVideo, args.getDetectMotion(), args.getSamplingScale());
			Logger::Log(VERBOSE, "Extracting features from right video.");
			detector.ExtractFeatures(rightVideo, args.getDetectMotion(), args.getSamplingScale());
		}

		auto opticalFlow = IOpticalFlow::getOpticalFlowAlgorithm(args.getTrackingAlgorithm());
		opticalFlow->ExtractOpticalFlow(leftVideo, args.getDescriptorLength());
		opticalFlow->ExtractOpticalFlow(rightVideo, args.getDescriptorLength());


		//---------------------------- Removing Low Energy Trajectories --------------------------
		if(args.getMinEnergy() > 0 ) {
			int removedl = leftVideo.RemoveLowEnergyTrajectories(args.getMinEnergy());
			Logger::Log(INFO, "Hello! Can you hear me ? %d instances removed from left video!", removedl);
			int removedr = rightVideo.RemoveLowEnergyTrajectories(args.getMinEnergy());
			Logger::Log(INFO, "Hello! Can you hear me ? %d instances removed from right video!", removedr);

		}
		if (args.IsDisplayEnabled()) {
			Logger::Log(INFO, "Displaying is enabled, showing the output to the user.");
			leftVideo.setGrayscale(false);
			rightVideo.setGrayscale(false);
			Display::Show(leftVideo,  rightVideo);
		}
}
}
}
