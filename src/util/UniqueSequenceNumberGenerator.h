/*
 * UniqueSequenceNumberGenerator.h
 *
 *  Created on: Jan 6, 2016
 *      Author: Pejman
 *      Copyright (c) 2016 Pejman. All rights reserved.
 */

#ifndef SRC_UTIL_UNIQUESEQUENCENUMBERGENERATOR_H_
#define SRC_UTIL_UNIQUESEQUENCENUMBERGENERATOR_H_
#include <string>
#include <unordered_map>
namespace HAR {

class UniqueSequenceNumberGenerator {
public:
	UniqueSequenceNumberGenerator(std::string labelFileName);
	int getLabelIndex(std::string classLabel);

	std::string getLabel(int index);

	size_t size() { return labelMapper.size(); }

	virtual ~UniqueSequenceNumberGenerator();

private:
	std::string fixClassLabel(std::string classLabel);

private:
	std::unordered_map<std::string, int> labelMapper;
	std::unordered_map<int, std::string> label;

	//int counter;
};

} /* namespace HAR */

#endif /* SRC_UTIL_UNIQUESEQUENCENUMBERGENERATOR_H_ */
