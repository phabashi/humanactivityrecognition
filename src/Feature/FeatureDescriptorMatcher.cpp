/*
 * FeatureDescriptorMatcher.cpp
 *
 *  Created on: Jan 27, 2015
 *      Author: Pejman
 *      Copyright (c) 2015 Pejman. All rights reserved.
 */

#include "FeatureDescriptorMatcher.h"
#include "../Logger/Logger.h"

using namespace Pejman::Logger;

namespace HAR {
namespace Video {
namespace Features {

std::unordered_map<std::string, cv::Ptr<FeatureDescriptorMatcher> > FeatureDescriptorMatcher::instances;

FeatureDescriptorMatcher& FeatureDescriptorMatcher::getInstance(
		std::string featureType)
{
	if(instances.find(featureType) == instances.end())
	{
		instances[featureType] = new FeatureDescriptorMatcher();
		instances[featureType]->descriptorMatcher = cv::DescriptorMatcher::create(featureType);
		instances[featureType]->featureType = featureType;
	}
	return (* instances[featureType]);
}

void FeatureDescriptorMatcher::removeInstances()
{
//	for(auto temp = instances.begin(); temp!= instances.end(); temp++)
//		delete (*temp).second;
	instances.clear();
}

FrameMatches FeatureDescriptorMatcher::MatchDescriptors(
		const FrameFeatureDescriptors& descriptor1,
		const FrameFeatureDescriptors& descriptor2)
{
	std::vector< std::vector<cv::DMatch> > matches;
	//If either of descriptors is empty return empty match!
	if(descriptor1.getImage().rows == 0 || descriptor2.getImage().rows == 0 ){
		Logger::Log(LogLevel::ERROR, "one of the descriptor sets provided is empty!");
		if(descriptor1.getImage().rows == 0)
					Logger::Log(LogLevel::ERROR, "descriptor1.getImage().rows == 0");
		if(descriptor2.getImage().rows == 0 )
					Logger::Log(LogLevel::ERROR, "descriptor2.getImage().rows == 0 ");


		return FrameMatches(matches);
	}

	Logger::Log(LogLevel::VERBOSE,
			"Matching descriptors d1.size= (%d,%d), d2.size=(%d,%d)",
			descriptor1.getImage().rows, descriptor1.getImage().cols,
			descriptor2.getImage().rows, descriptor2.getImage().cols);


	descriptorMatcher->knnMatch(descriptor1.getImage(), descriptor2.getImage(), matches, 5);
	FrameMatches result(matches);

	return result;
}

std::vector< std::vector<cv::DMatch> > FeatureDescriptorMatcher::MatchDescriptors(
		const cv::Mat& descriptor1,
		const cv::Mat& descriptor2)
{
	std::vector< std::vector<cv::DMatch> > matches;

	//If either of descriptors is empty return empty match!
	if(descriptor1.rows == 0 || descriptor2.rows == 0 ){
			return matches;
	}

	Logger::Log(LogLevel::VERBOSE,
			"Matching descriptors d1.size= (%d,%d), d2.size=(%d,%d)",
			descriptor1.rows, descriptor1.cols, descriptor2.rows, descriptor2.cols);
	descriptorMatcher->knnMatch(descriptor1, descriptor2, matches, 5);
	return matches;
}

FeatureDescriptorMatcher::FeatureDescriptorMatcher() {
	// TODO Auto-generated constructor stub

}

FeatureDescriptorMatcher::~FeatureDescriptorMatcher() {
	// TODO Auto-generated destructor stub
}

FeatureDescriptorMatcher::FeatureDescriptorMatcher(FeatureDescriptorMatcher&)
{
}

FeatureDescriptorMatcher& FeatureDescriptorMatcher::operator =(
		FeatureDescriptorMatcher&)
{
	return *this;
}

} /* namespace Features */
} /* namespace Video */
} /* namespace HAR */
