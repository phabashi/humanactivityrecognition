/*
 * CommaSeparatedReadingException.h
 *
 *  Created on: Dec 22, 2015
 *      Author: Pejman
 *      Copyright (c) 2016 Pejman. All rights reserved.
 */

#ifndef SRC_EXCEPTIONS_COMMASEPARATEDREADINGEXCEPTION_H_
#define SRC_EXCEPTIONS_COMMASEPARATEDREADINGEXCEPTION_H_

namespace HAR {

class CommaSeparatedReadingException {
public:
	CommaSeparatedReadingException();
	 virtual const char* what() const throw()
	  {
	    return "ERROR: The first line of comma separated file can not be empty.";
	  }
	virtual ~CommaSeparatedReadingException();
};

} /* namespace HAR */

#endif /* SRC_EXCEPTIONS_COMMASEPARATEDREADINGEXCEPTION_H_ */
