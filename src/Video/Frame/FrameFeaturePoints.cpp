/*
 * FrameFeaturePoints.cpp
 *
 *  Created on: Dec 30, 2014
 *      Author: Pejman
 *      Copyright (c) 2014 Pejman. All rights reserved.
 */

#include "FrameFeaturePoints.h"

#include "ImageSizes.h"

using namespace cv;
using namespace std;

namespace HAR {

FrameFeaturePoints::FrameFeaturePoints() {
	frameNumber = 0;
//	thisFrameKeyPoints = 0;
}

FrameFeaturePoints::FrameFeaturePoints(std::vector<cv::Point_<float> > temp) {
//	thisFrameKeyPoints = temp;
	//TODO Implement: Convert the locations to cv::KeyPoints and save them to keyPoints
	for( size_t i = 0; i < temp.size(); i++ ) {
	  keyPoints.push_back(cv::KeyPoint(temp[i], 1.f));
	}
	frameNumber = 0;
}
FrameFeaturePoints::FrameFeaturePoints(std::vector<cv::KeyPoint> temp) {
		keyPoints = temp;
		frameNumber = 0;
}

void FrameFeaturePoints::DrawFeaturePointsOnImage(const cv::Mat& input, cv::Mat& output) const {
	cv::drawKeypoints(input, keyPoints, output, cv::Scalar(255,0,0));
}

bool FrameFeaturePoints::Write(cv::FileStorage& fs) const {
	unsigned int numberOfFeatures = (unsigned int) keyPoints.size();
	fs << "{";
	fs <<  "FeaturePointCount" << (int) numberOfFeatures;
	fs << "KeyPoints" <<  keyPoints;
	fs << "}";
	return true;
}

bool FrameFeaturePoints::Read(const cv::FileNode& currentNode) {

	unsigned int numberOfFeatures =  (int) currentNode["FeaturePointCount"];
	keyPoints.clear();
	keyPoints.resize(numberOfFeatures);
	cv::read(currentNode["KeyPoints"], keyPoints);
	return true;
}

cv::Vec2f FrameFeaturePoints::CalculateMean() const
{
	cv::Vec2f sum(0, 0);
	int count = 0;
	for (auto it = keyPoints.begin(); it != keyPoints.end(); it ++)
	{
		sum += cv::Vec2f(it->pt.x, it->pt.y);
		count ++;
	}

	cv::Vec2f mean = cv::Vec2f( ((double) sum[0]) / count, ((double) sum[1]) / count);
	return mean;
}

cv::Vec2f FrameFeaturePoints::CalculateVar(cv::Vec2f mean) const
{
	cv::Vec2f sum(0, 0);
	int count = 0;
	for (auto it = keyPoints.begin(); it != keyPoints.end(); it ++)
	{
		double dx = it->pt.x - mean[0];
		double dy = it->pt.y - mean[1];
		sum += cv::Vec2f(dx * dx, dy * dy);
		count ++;
	}

	cv::Vec2f var = cv::Vec2f( ((double) sum[0]) / count, ((double) sum[1]) / count);
	return var;
}

FrameFeaturePoints::~FrameFeaturePoints() {
//	if(thisFrameKeyPoints) delete thisFrameKeyPoints;
}

} /* namespace HAR */

std::vector<cv::Point2f> HAR::FrameFeaturePoints::CalculateAndGetLocations() const
{
	std::vector<cv::Point2f> result(keyPoints.size());
	int k=0;
	for(auto it = keyPoints.begin(); it != keyPoints.end(); it++)
		result[k++] = it->pt;
	return result;
}

void HAR::FrameFeaturePoints::RemoveHomogenousAreaIP(cv::Mat image)
{
	std::vector<cv::KeyPoint> goodKeypoints;
	ImageSizes sizes(image);
	for(auto it = keyPoints.begin(); it != keyPoints.end(); it++)
	{
		bool isGood = true;
		cv::Point2f cen = it->pt;
		//TODO Consider if this constant is important for optimization or what? (5)
		int radius = 5;

		//get the Rect containing the circle:
		cv::Rect r(cen.x-radius, cen.y-radius, radius*2,radius*2);
		if(! sizes.isInside(r))
		{
			//std::cout << "Ignoring an near edge IP at " << r << std::endl;
			continue;
		}
		cv::Mat roi(image, r);
		roi = roi.clone();
		cv::Point2f center(r.width/2, r.height /2);
		const int scale = 1;
		const int delta = 0;
		const int ddepth = CV_64F;//CV_16S
		Mat Ix, Iy;

		//TODO Consider if this constant is important for optimization or what? (Size(3,3))
		GaussianBlur( roi, roi, Size(3,3), 0, 0, BORDER_DEFAULT );

		/// Gradient X
		Sobel( roi, Ix, ddepth, 1, 0, 3, scale, delta, BORDER_DEFAULT );
		/// Gradient Y
		Sobel( roi, Iy, ddepth, 0, 1, 3, scale, delta, BORDER_DEFAULT );


		cv::Mat autoCorrelationMatrix(2,2,CV_64F);
		autoCorrelationMatrix.at<double>(0, 0) = Ix.at<double>(center) * Ix.at<double>(center);
		autoCorrelationMatrix.at<double>(0, 1) = Ix.at<double>(center) * Iy.at<double>(center);
		autoCorrelationMatrix.at<double>(1, 0) = Ix.at<double>(center) * Iy.at<double>(center);
		autoCorrelationMatrix.at<double>(1, 1) = Iy.at<double>(center) * Iy.at<double>(center);

		cv::Mat eigenValues, eigenVector;
		eigen(autoCorrelationMatrix, eigenValues, eigenVector);

		//TODO Consider if this constant is important for optimization or what? (500)
		if(abs(eigenValues.at<double>(0,0)) < 500)
		// abs(eigenValues.at<double>(1,0)) < 1.0E-60)
			isGood = false;
//			cout << "Eigen values = " << eigenValues << endl;

//		cout << "Auto Correlation Matrix = " << autoCorrelationMatrix << endl;
//		cout << "Eigen values = " << eigenValues << endl;
//		cout << "Eigen vectors = " << eigenVector << endl;
//
//		imshow("orig", roi);
//		imshow("Ix", Ix);
//		imshow("Iy", Iy);
		//waitKey(0);

		if(isGood)
			goodKeypoints.push_back(*it);
	}
	keyPoints = goodKeypoints;
}
