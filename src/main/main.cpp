/*
 * main.cpp
 *
 *  Created on: Dec 18, 2014
 *      Author: Pejman
 *      Copyright (c) 2014 Pejman. All rights reserved.
 */
#include <iostream>
#include <string>
#include <set>

#include "../util/CommaSeparatedReader.h"
#include "../Exceptions/CommaSeparatedReadingException.h"
#include "../util/util.h"
using namespace std;
using namespace HAR;

int main(int argc, char** argv) {
	cout << "This is a sample program, it is not intended for real use!" << endl;
	try{
		cv::Mat result= CommaSeparatedReader::ReadFloatMatrix("test.fet");
		cout << result<< endl;
	} catch (CommaSeparatedReadingException& e){
		cout << "Exception raised: " << e.what() << endl;
	}
	return 0;
}

