/*
 * Arguments.cpp
 *
 *  Created on: Nov 19, 2016
 *      Author: Pejman
 */

#include "Arguments.h"
#include "../../util/util.h"
#include <iostream>
#include <string.h>
#include "../../Logger/Logger.h"

using namespace std;
using namespace HAR::Exceptions;

namespace HAR {
namespace DisplayStereoVideo {

void Arguments::printHelp() {
	cout
			<< "Usage: displayStereoVideo -l <infileL> -r <inFileR> -t <stereoTrajectoryFile> -s <scale> -h " << endl
			<< "-l <inFileL>	: is the name of left input video file: (i.e. L.avi)" << endl
			<< "-r <inFileR>	: is the name of right input video file: (i.e. R.avi)" << endl
			<< "-h 	: Show this help." << endl
			<< "-s 	: Output video scale." << endl
			<< "-v 	: If you need verbose log." << endl
			<< endl;
}
Arguments::Arguments(int argc, char* argv[]) {

	inputFileLeft = "";
	inputFileRight = "";
	inputTrajectoryFile = "";
	scale = 1.0;

	for (int i = 1; i < argc; i++) {
		if (strcmp(argv[i], "-v") == 0) {
			Pejman::Logger::Logger::getInstance().ChangeLevel(
					Pejman::Logger::LogLevel::VERBOSE);
			Pejman::Logger::Logger::Log(Pejman::Logger::LogLevel::VERBOSE,
					"The logger set to verbose!");
		} else if (strcmp(argv[i], "-l") == 0) {
			if (i < argc - 1) {
				inputFileLeft = argv[++i];
			} else {
				throw ArgumentParseException(
						"Ill formated input: switch '-l' The left input file name is missing.");
			}
		} else if (strcmp(argv[i], "-r") == 0) {
			if (i < argc - 1) {
				inputFileRight = argv[++i];
			} else {
				throw ArgumentParseException(
						"Ill formated input: switch '-r' The left input file name is missing.");
			}
		} else if (strcmp(argv[i], "-t") == 0) {
			if (i < argc - 1) {
				inputTrajectoryFile = argv[++i];
			} else {
				throw ArgumentParseException(
						"Ill formated input: switch '-r' The left input file name is missing.");
			}
		} else if (strcmp(argv[i], "-s") == 0) {
			if (i < argc - 1) {
				scale = stringTo<float>(argv[++i]);
			} else {
				throw ArgumentParseException(
						"Ill formated input: switch '-s' The scale factor is missing.");
			}
		} else if (strcmp(argv[i], "-h") == 0) {
			printHelp();
			exit(1);
		} else {
			cout << "Unknown parameter ! " << argv[i] << endl << endl;
			printHelp();
			exit(1);
		}
	}
	//Check if minimum parameters have been provided:
	if (inputFileLeft == "")
		throw ArgumentParseException(
				"You should provide at least one input video file name using -l switch.");
	if (inputFileRight == "")
		throw ArgumentParseException(
				"You should provide right input file name with -r.");
}
} /* namespace NewExtractFeatures */
} /* namespace HAR */

