/*
 * MainEncode3D.cpp
 * Created on: Jan 25, 2017
 *  Author: Dhawal Rank
 */

#include "../../Video/Display.h"
#include "../../util/util.h"
#include "../../Logger/Logger.h"
#include "../../util/CommaSeparatedReader.h"
#include "../../Video/TrajectoryEncoding/I3DEncoding.h"
#include <time.h>
#include <string>
#include <sstream>
#include <iostream>

using namespace std;
using namespace Pejman::Logger;
using namespace HAR::Video::TrajectoryEncoding;
using namespace HAR;

class Arguments {
public:
	Arguments(int argc, char* argv[]);

	void printHelp() {
		cout << "This program encode the uncoded trajectories."
				<< "Usage: encode [options]" << endl
				<< "options:" << endl
				<< " -e <encoding> 	: set the encoding" << endl
				<< "encoding Values:" << endl
				<< "Off" << endl
				<< "\t0OrderN" << endl
				<< "\t1OrderN" << endl
				<< endl
				<< "The default encoding algorithm is No Encoding" << endl

				<< endl;
	}

	const string& getEncoding() const {
		return encoding;
	}

	const string& getInputFileName()  const {
		return inputFileName;
	}

	const string& getOutputFileName() const {
		return outputFileName;
	}

private:
	string encoding;
	string inputFileName;
	string outputFileName;
};

int main(int argc, char** argv) {
	Logger::getInstance().ChangeLevel(LogLevel::ERROR);
	Arguments args(argc, argv);

	T3DEncoding encoding = I3DEncoding::get3DEncoding(args.getEncoding());

	cv::Mat inputMat;
	if(args.getInputFileName() != "")
		inputMat = CommaSeparatedReader::ReadFloatMatrix(args.getInputFileName());
	else
		inputMat = CommaSeparatedReader::ReadFloatMatrix();

	Logger::Log(VERBOSE,"Input: %d x %d", inputMat.rows, inputMat.cols);
	//TODO IMPORTANT FIX The image Size issue!
	cv::Mat outputMatrix = encoding->encode3D(inputMat, cv::Size(1, 1));

	Logger::Log(VERBOSE,"Output: %d x %d", outputMatrix.rows, outputMatrix.cols);
	if (outputMatrix.cols == 0 || outputMatrix.rows == 0 ){
		Logger::Log(ERROR, "The returned matrix is empty! outputmatrix.size = %dx%d", outputMatrix.rows, outputMatrix.cols);
		return 0;
	}
	if(args.getOutputFileName() != "")
		CommaSeparatedReader::WriteFloatMatrix(args.getOutputFileName(), outputMatrix);
	else
		CommaSeparatedReader::WriteFloatMatrix(outputMatrix);

	////	if()
	//	while(cin){
	//		stringstream input(line);
	//		float x,y;
	//
	//		HAR::Video::Trajectory trajectory;
	//		input >> x >> y;
	//		while(input) {
	//			trajectory.Add(cv::Point2d(x,y));
	//			input >> x >> y;
	//		}
	//		cout << trajectory.Serialize(encoding) << endl;
	//		std::getline(cin, line);
	//	}
	return 0;
}

Arguments::Arguments(int argc, char* argv[]) :
				encoding("Off"),
				inputFileName(""),
				outputFileName("")
{
	for (int i = 1; i < argc; i++) {
		if (strcmp(argv[i], "-h") == 0) {
			printHelp();
			exit(0);
		} else if( strcmp(argv[i], "-v") == 0 ){
			Logger::getInstance().ChangeLevel(LogLevel::VERBOSE);
		} else if (strcmp(argv[i], "-e") == 0) {
			if (i < argc - 1) {
				encoding = argv[++i];
			} else {
				cout << "Ill formated input: encoding algorithm should be specified after -e" 	<< endl;
			}
		} else if (strcmp(argv[i], "-o") == 0) {
			if (i < argc - 1) {
				outputFileName = argv[++i];
			} else {
				cout << "Ill formated input: Output file name should be specified after -o" << endl;
			}
		} else if (strcmp(argv[i], "-i") == 0) {
			if (i < argc - 1) {
				inputFileName = argv[++i];
			} else {
				cout << "Ill formated input: Input file name should be specified after -i" << endl;
			}
		} else {
			cout << "Invalid argument! '" << argv[i] << "' "
					<< endl;
			exit(0);
		}

	}//for
}

