/*
 * NormalizedDifferentiationEncoding.cpp
 *
 *  Created on: May 31, 2016
 *      Author: pejman
 */

#include "NormalizedDifferentiationEncoding.h"

#include "../Trajectory.h"

namespace HAR {
namespace Video {
namespace TrajectoryEncoding {

NormalizedDifferentiationEncoding::NormalizedDifferentiationEncoding() {
	// TODO Auto-generated constructor stub

}

std::vector<float> NormalizedDifferentiationEncoding::encode(
		const std::vector<cv::Point2f>& points) const {
	std::vector<float> result;
	result.reserve((points.size()- 1) *2);

	float m = Trajectory::getEnergy(points);

	if (m == 0)
		return result;

	for (uint i = 1; i < points.size(); i++) {
		result.push_back((points[i].x - points[i - 1].x) / m);
		result.push_back((points[i].y - points[i - 1].y) / m);
	}
	return result;
}

NormalizedDifferentiationEncoding::~NormalizedDifferentiationEncoding() {
	// TODO Auto-generated destructor stub
}

} /* namespace TrajectoryEncoding */
} /* namespace Video */
} /* namespace HAR */
