/*
 * ApplicationSetting.h
 *
 *  Created on: Mar 15, 2016
 *      Author: Pejman
 *      Copyright (c) 2016 Pejman. All rights reserved.
 */

#ifndef PEJMAN_APPLICATIONSETTING_H_
#define PEJMAN_APPLICATIONSETTING_H_
#include <string>
#include <unordered_map>
#include  "../util/util.h"
#include "../Logger/Logger.h"

#include <iostream>

namespace Pejman {
namespace Application {

class GlobalSetting {
public:
	static bool LoadConfigurationFromIniFile(std::string fileName);

	template<class T>
	static T GetOptionAs(std::string category, std::string option) {
		return stringTo<T>(
				getInstance().categories[toLowerCase(category)][toLowerCase(
						option)]);
	}

	template<class T>
	static T GetOptionAs(const std::string& category,const std::string& option,
			T defaultValue) {
		T v;
		try {
			v = stringTo<T>(
					getInstance().categories[toLowerCase(category)][toLowerCase(
							option)]);
		} catch (ConversionException& e) {
			Logger::Logger::Log(Logger::LogLevel::VERBOSE,
					"The requested option (%s -> %s) could not be read! Set the default value instead!",
					category.c_str(), option.c_str());
			v = defaultValue;
		}
		return v;
	}

	virtual ~GlobalSetting();

private:
	//singletone implementation
	static GlobalSetting& getInstance() {
		static GlobalSetting theInstance;
		return theInstance;
	}
private:
	GlobalSetting();
	GlobalSetting(GlobalSetting&);
	GlobalSetting& operator=(GlobalSetting&);
	typedef std::unordered_map<std::string, std::string> SpecificConfigurations;
	std::unordered_map<std::string, SpecificConfigurations> categories;

private:
	//Test only code

	void PrintOptions() {
		for (auto s : categories) {
			std::cout << "[ " << s.first << " ]" << std::endl;
			for (auto p : s.second)
				std::cout << p.first << "=" << p.second << std::endl;
		}
	}
};

} /* namespace Applocation */
} /* namespace Pejman */

#endif /* PEJMAN_APPLICATIONSETTING_H_ */
