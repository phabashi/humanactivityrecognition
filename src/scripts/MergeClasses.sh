#!/bin/bash

if (( $# != 1 )) 
{
 echo "Wrong number of arguments. I need exactly one argument!" 
}

cat $1 | replaceholder \
 CA1 CrossingArm \
 CA2 CrossingArm \
 HC HandClapping \
 HF HighFive \
 HS HandShaking \
 HSWL HitSomething \
 HSWR HitSomething \
 HWL HandWaving \
 HWR HandWaving \
 JJFF JumpingJack \
 JJFS JumpingJack \
 JJSF JumpingJack \
 JJSS JumpingJack \
 JLR Jumping \
 JRL Jumping \
 KSWL Kicking \
 KSWR Kicking \
 SU SitUp \
 LD LayDown \
 PDSFF PickUpPutDownFloor \
 PUSFF PickUpPutDownFloor \
 PDSFS PickUpPutDownFloor \
 PUSFS PickUpPutDownFloor \
 PUST PickUpPutDownTable \
 PDST PickUpPutDownTable \
 PLOL PullPushHeavyObject \
 PSOR PullPushHeavyObject \
 PTBL Poiting \
 PTBR Poiting \
 PTCL Poiting \
 PTCR Poiting \
 PTLL Poiting \
 PTLR Poiting \
 PTRL Poiting \
 PTRR Poiting \
 RAC Running \
 RTC Running \
 RRTL Running \
 RLTR Running \
 RBHTC RaiseHand \
 RRHTC RaiseHand \
 RLHTC RaiseHand \
 SDF SitDown \
 SDS SitDown \
 SUF StandUp \
 SUS StandUp \
 SHL ScratchHead \
 SHR ScratchHead \
 SKF Skipping \
 SKS Skipping \
 TAC Turning \
 TTC Turning \
 THW TwoHandWaving \
 TOTCR ThroughObject \
 TOTCL ThroughObject \
 TOTRR ThroughObject \
 TOTRL ThroughObject \
 TOTLL ThroughObject \
 TOTLR ThroughObject \
 WTC Walking \
 WAC Walking \
 WRTL Walking \
 WLTR Walking \
 XOLR ExchangeObject \
 XORL ExchangeObject \
 KBTC KickBall \
 KBTR KickBall \
 KBTL KickBall \
 KBTB KickBall \
 KSTC Unknown \
 KSTR Unknown \
 JLR JumpGap \
 JRL JumpGap \
 JTC JumpGap \
 JAC JumpGap
