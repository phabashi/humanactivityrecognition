/*
 * FeatureExtractor.cpp
 *
 *  Created on: Dec 30, 2014
 *      Author: Pejman
 *      Copyright (c) 2014 Pejman. All rights reserved.
 */

#include "FeatureDetector.h"

#include "VideoFeaturePoints.h"
using namespace std;

#include <unordered_map>

namespace HAR {
namespace Video {
namespace Features {

std::unordered_map<std::string, FeatureDetector*> FeatureDetector::instances;

FeatureDetector& FeatureDetector::getInstance(std::string featureType) {
	if (instances.find(featureType) == instances.end()) {
		instances[featureType] = new FeatureDetector();
		instances[featureType]->featureDetector = cv::FeatureDetector::create(featureType);
		if(instances[featureType]->featureDetector == NULL){
			cout <<"Kaharam shoma ridi :D" << endl;
		}
		instances[featureType]->featureType = featureType;
	}
	return (*instances[featureType]);
}

FeatureDetector::FeatureDetector() {
//	featureDetector = NULL;
}

FrameFeaturePoints FeatureDetector::ExtractFeatureFromFrame(
		const cv::Mat input) {
	std::vector<cv::KeyPoint> keypoints; // = new std::vector<cv::KeyPoint>();
	featureDetector->detect(input, keypoints);
	return FrameFeaturePoints(keypoints);
}

void FeatureDetector::ExtractFeatures(AugmentedVideoBuffer& inputVideo,
		bool includeOnlyMotionArea, int samplingScale) {
	inputVideo.getCurrentFrameInformation().ClearPointsOfInterest();
	for (cv::Mat frame = inputVideo.getFrame(0); !frame.empty(); frame =
			inputVideo.getNextFrame()) {
		if(inputVideo.getCurrentFrameNumber() % samplingScale != 0)
			continue;//skip the frame
		auto rois =
				inputVideo.getCurrentFrameInformation().getRegionsOfInterest();
		if (includeOnlyMotionArea) {
			if (rois.size() != 0) {
				for (auto roi = rois.begin(); roi != rois.end(); roi++) {
					std::vector<cv::KeyPoint> keyPoints;
					featureDetector->detect(frame(*roi), keyPoints);
					for (uint i = 0; i < keyPoints.size(); i++) {
						keyPoints[i].pt.x += roi->x;
						keyPoints[i].pt.y += roi->y;

						inputVideo.getCurrentFrameInformation().AddToKeyPoints(
								keyPoints[i]);
					}
				}
			}
		} else { // Detect all video area features points
			std::vector<cv::KeyPoint> keyPoints;
			featureDetector->detect(frame, keyPoints);
			for (uint i = 0; i < keyPoints.size(); i++)
				inputVideo.getCurrentFrameInformation().AddToKeyPoints(
						keyPoints[i]);
		}
	}
}

VideoFeaturePoints* FeatureDetector::ExtractFeaturesFromVideo(
		Video inputVideo) {

	inputVideo.Reset();
	VideoFeaturePoints* result = new VideoFeaturePoints(getType());
	if (inputVideo.isValid()) {
		cv::Mat frame;
		frame = inputVideo.getNextFrame();
		int frameNumber = 0;
		while (frame.data) {
			FrameFeaturePoints frameFeaturePoints = ExtractFeatureFromFrame(
					frame);
			result->addFrameFeaturePoints(frameNumber++, &frameFeaturePoints);
			frame = inputVideo.getNextFrame();
		}
	}
	return result;
}

FeatureDetector::~FeatureDetector() {
}

FeatureDetector::FeatureDetector(FeatureDetector&) {
}

void FeatureDetector::removeInstances() {
	for (auto temp = instances.begin(); temp != instances.end(); temp++)
		delete (*temp).second;
}

FeatureDetector& FeatureDetector::operator=(FeatureDetector&) {
	return (*this);
}

} /* namespace Features */
} /* namespace Video */
} /* namespace HAR */
