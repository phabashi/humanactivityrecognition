/*
 * ClusterEncoding.cpp
 *
 *  Created on: Jul 7, 2016
 *      Author: pejman
 */

#include "ClusterEncoding.h"
#include "../Trajectory.h"
#include <iostream>

#define SIZE(a) (sqrt( (a).x * (a).x + (a).y * (a).y ))
#define SIGN(x) ((x > 0) - (x < 0))

using namespace std;

namespace HAR {
namespace Video {
namespace TrajectoryEncoding {

ClusterEncoding::ClusterEncoding() {
	// TODO Auto-generated constructor stub

}

std::vector<float> ClusterEncoding::encode(const std::vector<cv::Point2f>& points) const {
	std::vector<float> result;
	result.push_back(getCluster(points));
	return result;
}

int ClusterEncoding::getCluster(const std::vector<cv::Point2f>& points) const {
	cv::Point2f first = points[0];
	cv::Point2f last = points[points.size() - 1];
	float dy = last.y - first.y;
	float dx = last.x - first.x;
	float angle = atan2(dy, dx);
	static const double pi = 3.1415926535897;

	//Direction encoding
	const int directionBins = 8;
	int bin = ((angle / pi )+1) / 2 * directionBins;
	bin = (bin >= directionBins)? directionBins - 1: bin;

	//distance encoding
	static const int distanceBins =16;
	float distance = sqrt(dx*dx + dy*dy);
	int qd = distance/5;
	qd = (qd >= distanceBins)? qd % distanceBins : qd;

	//energy encoding
	double energy = Trajectory::getEnergy(points);
	static const int energyBins = 16;
	int qe = energy/5;
	qe = (qe >= energyBins)? qd % energyBins : qe;

	//Shape encoding
	// Using Heron equation to calculate S
	// Use regular triangle area formula S = (h * a) / 2 calculate h
	double max = 0;
	int maxi = 0;
	double a = SIZE(last - first);
	for(uint i=1; i < points.size() - 1; i++){
		cv::Point2f cur = points[i];

		double b = SIZE(cur - last);
		double c = SIZE(cur - first);
		double P = (a + b + c) / 2;
		double S = sqrt(P * (P - a) * (P - b) * (P - c) );
		double h = 2 * S / a;//The distance between current point and the line connecting two other points

		if (h > max) {
			max = h;
			maxi = i;
		}

	}

	//calculate the farthest point falls on which side of connecting point
	double side = (points[maxi].x - first.x)* (last.y - first.y) - (points[maxi].y - first.y) * (last.x - first.x);
	static const int shapeBins=8;
	double d = SIGN(side) * max / distance + (shapeBins / 2);

	int qs = (d) ;
	qs = (qs < 0) ? 0 : qs;
	qs =  qs % shapeBins;

	//Time Location encoding

	bin = (qe < 1)? 0 : bin;
	int clusterNumber =  (qs << 11) | (qe << 7) | (qd << 3) |  bin ;
	return clusterNumber;
}

ClusterEncoding::~ClusterEncoding() {
	// TODO Auto-generated destructor stub
}

} /* namespace TrajectoryEncoding */
} /* namespace Video */
} /* namespace HAR */
