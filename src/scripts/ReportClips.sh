#!/bin/bash

echo "This script will generate a report of the clips inside clips folder."

labelDef="./def/LabelsDefinitions.txt"

labels=( $(more $labelDef | cut -d " " -f 1))

echo -n "Subject"

for k in ${labels[@]}; do
	echo -n ", $k "
done
echo
for j in `seq 1 11`; do
	subjectName="";
	if (( $j < 10 )); then
		subjectName="S0$j"
	else
		subjectName="S$j"
	fi

	echo -n $subjectName " "

	for i in ${labels[@]}; do
		count=$(ls -R ./clips/ | grep "_Left_" | grep "$subjectName" | grep "_L_$i" | wc -w)
		echo -n ", $count "
	done
	echo
done
