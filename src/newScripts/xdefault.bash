#!/bin/bash


export length=${length:-15}
export NOW=${NOW:-16}


export ExternalFeature=${EF:-"Trajectory"}
#ExternalFeature="HOF"
TDL=$(( 2 * length ))
if [ $ExternalFeature = "Trajectory" ]; then
	start=11
	end=$(( TDL + start - 1 ))
        FeatureNumbers="$start-$end" # Trajectory Features
elif [ $ExternalFeature = "HOG" ]; then
	start=$(( 10 + TDL + 1))
	end=$(( start + 96 - 1 ))
        FeatureNumbers="$start-$end" #HOG Features'
elif [ $ExternalFeature = "HOF" ]; then
	start=$(( 10 + TDL + 96 + 1))
	end=$(( start + 108 - 1 ))	
        FeatureNumbers="$start-$end" #HOF Features'
elif [ $ExternalFeature = "MBHx" ]; then	
	start=$(( 10 + TDL + 96 + 108 + 1))
	end=$(( start + 96 - 1 ))
        FeatureNumbers="$start-$end" #MBHx Features'
elif [ $ExternalFeature = "MBHy" ]; then
	
	start=$(( 10 + TDL + 96 + 108 + 96 + 1))
	end=$(( start + 96 - 1 ))
        FeatureNumbers="$start-$end" #MBHy Features'
elif [ $ExternalFeature = "MBH" ]; then	
	start=$(( 10 + TDL + 96 + 108 + 1))
	end=$(( start + 2 * 96 - 1 ))
        FeatureNumbers="$start-$end" #MBHy Features'
fi

export FeatureNumbers
echo $FeatureNumbers
