/*
 * FrameMatches.cpp
 *
 *  Created on: Jan 27, 2015
 *      Author: Pejman
 *      Copyright (c) 2015 Pejman. All rights reserved.
 */

#include "FrameMatches.h"

namespace HAR {

FrameMatches::FrameMatches(std::vector< std::vector<cv::DMatch> > initialMatches)
{
	multipleMatches = initialMatches;
}

FrameMatches::~FrameMatches() {
}

} /* namespace HAR */
