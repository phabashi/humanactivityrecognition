/*
 * Arguments.h
 *
 *  Created on: Mar 6, 2016
 *      Author: Pejman
 *      Copyright (c) 2016 Pejman. All rights reserved.
 */

#ifndef SRC_NEWEXTRACTFEATURES_ARGUMENTS_H_
#define SRC_NEWEXTRACTFEATURES_ARGUMENTS_H_
#include <string>
#include "../../Exceptions/ArgumentParseException.h"

namespace HAR {
namespace NewExtractFeatures {

class Arguments {
public:
	Arguments() :
		inputFileLeft(""),
		inputFileRight(""),
		outputFeatureFile(""),
		frameSynchronizationDifference(0),
		descriptorLength(0),
		interestPointExtractor(""),
		interestPointDescriptor(""),
		matchingAlgorithm(""),
		theRatioToSelectBestMatches(0.0),
		removeHemogenousArea(false),
		displayEnabled(false),
		scale(false),
		trackingAlgorithm(""),
//		trajectoryFileName(""),
		applicationSettingFileName(""),
//		normalizeOutput(false),
		detectMotion(false),
		minEnergy(0.0),
		isMultiscaleEnabled(false),
		trajectoryEncoding(""),
		samplingScale(1),
		initialScale(1)
		{}

	Arguments(int argc, char* argv[]);
	static void printHelp();

	int getDescriptorLength() const {
		return descriptorLength;
	}
	int getFrameDifference() const {
		return frameSynchronizationDifference;
	}
	const std::string& getInputFileLeft() const {
		return inputFileLeft;
	}
	const std::string& getInputFileRight() const {
		return inputFileRight;
	}
	const std::string& getOutputFeatureFile() const {
		return outputFeatureFile;
	}
	const std::string& getInterestPointDescriptor() const {
		return interestPointDescriptor;
	}
	const std::string& getInterestPointExtractor() const {
		return interestPointExtractor;
	}
	const std::string& getMatchingAlgorithm() const {
		return matchingAlgorithm;
	}
	double getTheRatioToSelectBestMatches() const {
		return theRatioToSelectBestMatches;
	}
	bool IsRemoveHemogenousAreaSet() const {
		return removeHemogenousArea;
	}
	bool IsDisplayEnabled() const {
		return displayEnabled;
	}
	const std::string& getTrackingAlgorithm() const {
		return trackingAlgorithm;
	}

//	const std::string& getTrajectoryFileName() const {
//		return trajectoryFileName;
//	}

	const std::string& getApplicationSettingFileName() const {
		return applicationSettingFileName;
	}

	float getScale() const {
		return scale;
	}

//	bool getNormalizeOutput() const {
//		return normalizeOutput;
//	}

	bool getDetectMotion() const {
		return detectMotion;
	}

	float getMinEnergy() const {
		return minEnergy;
	}
	bool IsMultiscaleEnabled() const {
		return isMultiscaleEnabled;
	}

	const std::string& getTrajectoryEncoding() const {
		return trajectoryEncoding;
	}

	int getSamplingScale() const {
		return samplingScale;
	}

	float getInitialScale() const {
		return initialScale;
	}

private:
	std::string inputFileLeft;
	std::string inputFileRight;
	std::string outputFeatureFile;
	int frameSynchronizationDifference;
	int descriptorLength;
	std::string interestPointExtractor;
	std::string interestPointDescriptor;
	std::string matchingAlgorithm;
	double theRatioToSelectBestMatches;
	bool removeHemogenousArea;
	bool displayEnabled;
	float scale;
	std::string trackingAlgorithm;
//	std::string trajectoryFileName;
	std::string applicationSettingFileName;
	std::string trajectoryEncoding;
//	bool normalizeOutput;
	bool detectMotion;
	float minEnergy;
	bool isMultiscaleEnabled;
	int samplingScale;
	float initialScale;
};

} /* namespace NewExtractFeatures */
} /* namespace HAR */

#endif /* SRC_NEWEXTRACTFEATURES_ARGUMENTS_H_ */
