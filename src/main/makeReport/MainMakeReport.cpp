/*
 * MainMakeReport.cpp
 *
 *  Created on: Jan 10, 2016
 *      Author: Pejman
 *      Copyright (c) 2016 Pejman. All rights reserved.
 */
#include <string>
#include <string.h>
#include <iostream>
#include "../../util/util.h"
#include "ConfusionMatrix.h"
#include "../../util/UniqueSequenceNumberGenerator.h"
#include "../../Exceptions/ArgumentParseException.h"

#include "../../Logger/Logger.h"

using namespace std;
using namespace HAR::Exceptions;
using namespace Pejman::Logger;

class Arguments {
public:
	Arguments() {}
	Arguments(int argc, char* argv[]);

	string getExpectedFileName() { return expectedFileName; }
	string getOutputFileName() { return outputFileName; }
	string getLabelFileName() { return labelFileName; }

	void printHelp();

private:
	string expectedFileName;
	string outputFileName;
	string labelFileName;
};

using namespace std;
using namespace HAR;

int main(int argc, char* argv[]){

	Logger::getInstance().ChangeLevel(LogLevel::ERROR);

	Arguments args;

	try{
		args = Arguments(argc, argv);
	} catch (ArgumentParseException &e){
		cout << e.what() << endl;
	}

	UniqueSequenceNumberGenerator ug(args.getLabelFileName());
	ConfusionMatrix confusionMatrix(ug);

	confusionMatrix.makeConfusionMatrix(args.getExpectedFileName(), args.getOutputFileName());

	cout << confusionMatrix << endl;


	return 0;
}

Arguments::Arguments(int argc, char* argv[] ){
	expectedFileName = outputFileName = labelFileName = "";

	for(int i=1; i< argc; i++) {
		if( strcmp(argv[i], "-v") == 0) {
			Logger::getInstance().ChangeLevel(LogLevel::VERBOSE);

		}else if( strcmp(argv[i], "-e") == 0) {
			if(i < argc-1){
				expectedFileName = argv[++i];
			} else {
				throw new ArgumentParseException("Ill formated input: switch '-e' The expected file name is missing.");
			}
		} else if( strcmp(argv[i], "-o") == 0) {
			if(i < argc-1) {
				outputFileName = argv[++i];
			} else {
				throw new ArgumentParseException( "Ill formated input: switch '-o' The output file name is missing." );
			}
		} else if( strcmp(argv[i], "-lab") == 0) {
			if(i < argc-1) {
				labelFileName = argv[++i];
			} else {
				throw new ArgumentParseException( "Ill formated input: switch '-lab' The label Definition file name is missing." );
			}
		} else if (strcmp (argv[i], "-h") == 0){
			printHelp();
			exit(0);
		} else
		{
			printHelp();
			exit(1);
		}
	}
	//Check if minimum parameters have been provided:
	if(expectedFileName == "")
		throw ArgumentParseException("You should provide expected file name with -e.");
	if (outputFileName == "")
		throw ArgumentParseException("You should provide output file name with -o.");
	if(labelFileName == "")
		throw ArgumentParseException("You should provide the label definition file name with -lab.");
}

void Arguments::printHelp(){
	cout << "Usage: MakeReport -e <expected> -o <output> -lab <labelFileName>" << endl;
}
