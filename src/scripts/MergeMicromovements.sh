#/bin/bash

#This script will try to merge the micromovemnts into one big file. As one of the main parts of merging, this scripts will add the file name information to each descriptor (which will make dublicate information). 

micromovementFolder='./microMovements'

if (( $# != 2 )); then
	echo 'You should provide the file list as well as output.'
	echo "Usage: $0 fileNameList outputFile"
exit 1
fi

outputFileName=$2
rm $outputFileName
list=(`more $1`) 
i=0
j=0
lineNumber=0
for fileName in ${list[@]}; do
	i=`expr $i + 1`
	lineNumber=`expr $lineNumber + 1`
	echo "$lineNumber : Adding $fileName to the end of List."
	
	subject=`echo $fileName | cut -f 1 -d "_"`
	capture=`echo $fileName | cut -f 2 -d "_"`
	actionLabel=`echo $fileName | cut -f 4 -d "_"`
	instanceNumber=`echo $fileName | cut -f 6 -d "_" | cut -f 1 -d "."`

	j=0
	while FS='' read -r line || [[ -n "$line" ]]; do
		j=`expr $j + 1`
		if (($j == 1)); then
			#This is the first line of document, it is needed only in the first line of result
			if (( $i == 1 )); then
				#This means we are in the first line of result!
				echo "$line, Subject, Capture, actionLabel, instanceNumber" >> $outputFileName
			fi
		else
			line=`echo "$line , $subject, $capture, $actionLabel, $instanceNumber"| tr -d '\n' | tr -d '' `
			echo "$line" >> $outputFileName 
		fi
    		#echo "Text read from file: $line,"
	done < $micromovementFolder/$fileName

	echo "$subject, $capture, $actionLabel, $instanceNumber"
	echo "Done."
done
