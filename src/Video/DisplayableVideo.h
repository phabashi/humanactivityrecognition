/*
 * DisplayableVideo.h
 *
 *  Created on: May 7, 2016
 *      Author: Pejman
 *      Copyright (c) 2016 Pejman. All rights reserved.
 */

#ifndef SRC_VIDEO_DISPLAYABLEVIDEO_H_
#define SRC_VIDEO_DISPLAYABLEVIDEO_H_

#include <opencv2/opencv.hpp>

namespace HAR {
namespace Video {

class DisplayableVideo {
public:
	DisplayableVideo();

	virtual ~DisplayableVideo();

	void reset();

	void setDisplayScale(double scale);

	bool isFinished() const { return finished; }

	bool isPaused() const { 	return paused; }

	void UpdateWithKey(int key);

	int getDisplayableFrameNumber() const { return frameNumber; }

	void moveToNextDisplayabelFrame();
	virtual cv::Mat getDisplayableFrame(int frameNumber) = 0;

	virtual int getFramePerSecond() = 0;

	const cv::Mat& getDisplayableFrame() const { return displayFrame; }

private:
	virtual cv::Mat getResizedDisplayableFrame(int frameNumber);

protected:
	void setFinished(bool finished) {
		this->finished = finished;
	}

	void setPaused(bool paused) {
		this->paused = paused;
	}

protected:
	bool finished;
	bool paused;
	int frameNumber;
	cv::Mat displayFrame;

private:
	double scale;
	cv::Size frameSize;
};

} /* namespace Video */
} /* namespace HAR */

#endif /* SRC_VIDEO_DISPLAYABLEVIDEO_H_ */
