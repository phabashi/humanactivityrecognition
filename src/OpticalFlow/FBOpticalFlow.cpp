/*
 * FBOpticalFlow.cpp
 *
 *  Created on: Apr 30, 2016
 *      Author: pejman
 */

#include "FBOpticalFlow.h"
#include "../Video/VideoBuffer.h"
#include "../Pejman/GlobalSetting.h"
#include "../Logger/Logger.h"

#include <opencv2/opencv.hpp>

using namespace Pejman::Application;
using namespace Pejman::Logger;

namespace HAR {
namespace Video {
namespace OpticalFlow {

FBOpticalFlow::FBOpticalFlow() {
	//TODO Parameters to be optimized!
	windowSize = GlobalSetting::GetOptionAs<int>("OpticalFlowFarnBack", "windowSize", 5);
	level = GlobalSetting::GetOptionAs<int>("OpticalFlowFarnBack", "level", 5);
	scale = GlobalSetting::GetOptionAs<float>("OpticalFlowFarnBack","scale", 0.5);
	iteration = GlobalSetting::GetOptionAs<int>("OpticalFlowFarnBack", "iteration", 5);
	poly_n = GlobalSetting::GetOptionAs<int>("OpticalFlowFarnBack", "poly_n", 5);
	poly_sigma = GlobalSetting::GetOptionAs<double>("OpticalFlowFarnBack", "poly_sigma", 1.2);
	flags = GlobalSetting::GetOptionAs<int>("OpticalFlowFarnBack", "flags", 0);
	//	float jumptreshold = GlobalSetting::GetOptionAs<float>(
	//			"OpticalFlowFarnBack", "jumptreshold", 0); // This parameter optimization happen in the first of the main loop.
	termcrit= cv::TermCriteria(cv::TermCriteria::COUNT | cv::TermCriteria::EPS,
			GlobalSetting::GetOptionAs<int>("OpticalFlowFarnBack", "TermCritCount", 20),
			GlobalSetting::GetOptionAs<float>("OpticalFlowFarnBack", "TermCritEpsilon", 0.003));
}

void FBOpticalFlow::ExtractOpticalFlow(AugmentedVideoBuffer& video,
		int length) {
	video.ClearTrajectories();

	cv::Mat preImage;
	preImage = video.getFrame(0);
	cv::Size frameSize(preImage.cols, preImage.rows);

	std::vector<Trajectory> activeTracks; //Contains ongoing tracks
	std::vector<Trajectory> finishedTracks;

	for (cv::Mat frame = video.getFrame(1); frame.data;
			frame = video.getNextFrame()) {
		//		if (jumptreshold < 1) { // we have not calculated the threshold yet!
		//			//Optimize! Now it is fixed to 800 for 640x400 frame size
		//			float dx = (frame.cols / 32.0);
		//			float dy = (frame.rows / 20.0);
		//			jumptreshold = dx * dx + +dy * dy; // Example 20x20  for 640 and 400 => jump (10*10 + 10*10) = 200
		//		}
		Logger::Log(LogLevel::INFO, "Extracting Frame %d ", video.getCurrentFrameNumber());
		//				<< std::endl;
		//		const std::vector<cv::Point_<float> >& previousFramePointsOfInteres = video.getPreviousFrameInformation().getPointsOfInterest();
		Logger::Log(LogLevel::VERBOSE, "Try accessing this frame interest points.");
		const std::vector<cv::Point_<float> >& thisFramePointsOfInteres =
				video.getCurrentFrameInformation().getPointsOfInterest();

		Logger::Log(LogLevel::VERBOSE, "Creating new trajectories base on this frame feature points.");
		//Add new Tracking points for tracking
		for (const cv::Point2f p : thisFramePointsOfInteres) {
			activeTracks.push_back( Trajectory(video.getCurrentFrameNumber(), p)); // Add new point to tracking
		}

		if (activeTracks.empty()) { //If there is no point to find next points
			Logger::Log(LogLevel::WARNING, "Empty tracking set! skipping frame\n");
			continue;
		}
		cv::Mat flow;

		Logger::Log(LogLevel::VERBOSE, "Calculating farnback optical flow field");
		cv::calcOpticalFlowFarneback(preImage, frame, flow, scale, level,
				windowSize, iteration, poly_n, poly_sigma, flags);
//		cv::calcOpticalFlowFarneback(preImage, frame, flow, 0.5, 3,
//						15, 3, 5, 1.2, 0);
		preImage=frame.clone();
		std::vector<cv::Mat> flows;
		cv::split(flow, flows);
		cv::Mat dX = flows[0];
		cv::Mat dY = flows[1];

		cv::medianBlur(dX, dX, 5);
		cv::medianBlur(dY, dY, 5);

//		cv::imshow("fb DX", dX);
//		cv::imshow("fb DY", dY);
//		cv::imshow("frame", frame);
//		cv::waitKey(0);

		Logger::Log(LogLevel::VERBOSE, "Adding the result of farnback to the end of trajectories.");
		std::vector<Trajectory> notFinished;
		for (unsigned int i = 0; i < activeTracks.size(); i++) {
			Trajectory& t = activeTracks[i];
			const cv::Point2f& point = t.getLastPoint();

			int xLoc = point.x;
			int yLoc = point.y;
			float dx = flow.ptr<float>(yLoc)[2*xLoc];
			float dy = flow.ptr<float>(yLoc)[2*xLoc + 1];
			cv::Point2f thePoint(point.x + dx, point.y + dy);
			if ( //((dx * dx + dy * dy) < jumptreshold)
			thePoint.x >= 0 && thePoint.x < frame.cols && thePoint.y >= 0
					&& thePoint.y < frame.rows) {
				t.Add(thePoint);
				if (t.length() == length) // if we had reached the desired length
					video.AddTrajectory(t);
				else
					notFinished.push_back(t);
			}
		}
		activeTracks = notFinished;
	}
}

FBOpticalFlow::~FBOpticalFlow() {
	// TODO Auto-generated destructor stub
}

} /* namespace OpticalFlow */
} /* namespace Video */
} /* namespace HAR */
