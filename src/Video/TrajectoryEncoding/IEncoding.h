/*
 * TrajectoryEncoding.h
 *
 *  Created on: May 30, 2016
 *      Author: pejman
 */

#ifndef SRC_VIDEO_IENCODING_H_
#define SRC_VIDEO_IENCODING_H_

#include <memory>
#include <vector>
#include <opencv2/opencv.hpp>

namespace HAR {
namespace Video {
namespace TrajectoryEncoding {


//This is the base interface of encoding algorithm to be used when encoding the trajectory
class IEncoding {
public:
	//Factory desing pattern
	static std::unique_ptr<IEncoding> getEncoding(std::string algorithmName);

	virtual std::vector<float> encode(const std::vector<cv::Point2f>& points) const = 0;

	/* This function will encode a matrix containing arbitrary number of trajectories,   *
	 * It assumes that each row contains a trajectory and the first column contains the  *
	 * frame number of that trajectory (which usually ignored for encoding).             */
	virtual cv::Mat encode(const cv::Mat& trajectories) const;

	virtual ~IEncoding();

protected:
	IEncoding();
};

typedef std::unique_ptr<IEncoding> TEncoding;

} /* namespace TrajectoryEncoding */
} /* namespace Video */
} /* namespace HAR */

#endif /* SRC_VIDEO_IENCODING_H_ */
