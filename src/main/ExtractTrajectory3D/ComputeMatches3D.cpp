/*
 * ComputeMatches3D.cpp
 *
 *  Created on: Jan 1, 2017
 *      Author: dhawal
 */

#include "ComputeMatches3D.h"
#include "../../Pejman/GlobalSetting.h"
#include "../../Clustering/DistanceCalculator.h"

using namespace Pejman::Application;

namespace HAR {
namespace ComputeMatches {
	void ComputeMatches::ComputeMatches3D(AugmentedVideoBuffer& leftVideo, AugmentedVideoBuffer& rightVideo, const Arguments& args){
			Logger::Log(VERBOSE, "Creating FeatureDescriptorExtractor object.");
			HAR::Video::Features::FeatureDescriptorExtractor& extractor =
					HAR::Video::Features::FeatureDescriptorExtractor::getInstance(args.getInterestPointDescriptor());
			Logger::Log(VERBOSE, "Creating FeatureDescriptorMatcher object.");
			HAR::Video::Features::FeatureDescriptorMatcher & descriptorMatcher =
					HAR::Video::Features::FeatureDescriptorMatcher ::getInstance(args.getMatchingAlgorithm());
			leftVideo.setGrayscale(false);
			rightVideo.setGrayscale(false);
			int frameNumber = 0;
			double ratio = args.getTheRatioToSelectBestMatches();
			// itetrate through frames of left and right video and calculate the matches between key points
			for (cv::Mat lframe = leftVideo.getFrame(0), rframe = rightVideo.getFrame(0); !lframe.empty() && !rframe.empty();
					lframe = leftVideo.getNextFrame(), rframe = rightVideo.getNextFrame()){
				std::vector<Point2f> MatchedPointsL, MatchedPointsR;
				Logger::Log(INFO, "Generating matches for Frame %d ", frameNumber);
				vector<const Trajectory*> TrajectoryL;
				std::vector<KeyPoint> keyPointsL;
				Logger::Log(VERBOSE, "Generating KeyPoints for left Video and saving corresponding trajectories.");
				getTrajKeyPoints(leftVideo, &TrajectoryL, &keyPointsL);
				Logger::Log(VERBOSE, "%d keypoints detected for left.", keyPointsL.size());
				vector<const Trajectory*> TrajectoryR;
				std::vector<KeyPoint> keyPointsR;
				Logger::Log(VERBOSE, "Generating KeyPoints for right Video and saving corresponding trajectories.");
				getTrajKeyPoints(rightVideo, &TrajectoryR, &keyPointsR);
				Logger::Log(VERBOSE, "%d keypoints detected for right.", keyPointsL.size());

				Logger::Log(VERBOSE, "Creating Left Detector object.");
				//extract descriptors for right video
				FrameFeatureDescriptors leftFeatureDetector = extractor.ExtractDescriptorsForFrame(lframe, keyPointsL);
				if(leftFeatureDetector.getImage().rows != 0)
					Logger::Log(VERBOSE, "Creating Left Detector object SUCCEED.");
				else
					Logger::Log(ERROR, "RRRIID");

				Logger::Log(VERBOSE, "Creating Right Detector object.");
				//extract descriptors for left video
				FrameFeatureDescriptors rightFeatureDetector = extractor.ExtractDescriptorsForFrame(rframe, keyPointsR);
				if(leftFeatureDetector.getImage().rows != 0)
					Logger::Log(VERBOSE, "Creating Right Detector object SUCCEED.");
				else
					Logger::Log(ERROR, "RRRIID");


				Logger::Log(VERBOSE, "Generating Final Matches");
				//Generate final matches
				FrameMatches frameMatches = descriptorMatcher.MatchDescriptors(leftFeatureDetector, rightFeatureDetector);
				//---------------------Find Best Matches -------------------------------//
				std::vector<std::vector<cv::DMatch> > frameDMatch = frameMatches.getMatches();
				if(frameDMatch.size() == 0) {//We have no match,
					Logger::Log(INFO,"There is no matched points between left and right frame!");
					//TODO: Do approperiate thing :D I just skip this frame that might introduce some other bugs!
					continue;
				}
				cv::Mat descriptor = leftFeatureDetector.getImage();
				std::vector<cv::DMatch> frameGoodMatches;
				auto itf = frameDMatch.begin();
				auto itL = TrajectoryL.begin(), itR = TrajectoryR.begin();
				vector<const Trajectory*> SurvivedFrameTrajectoryL, SurvivedFrameTrajectoryR;
				//this loop iterates finds the best matches and saves the corresponding trajectories
				for( int i = 0; i < descriptor.rows; i++ ){
					// as generated matches are sorted in ascending order of their distance, first 2 match will be first_min_dist and second_min_distance respectively
					if ( itf[i].data()[0].distance < ratio * itf[i].data()[1].distance){
						Logger::Log(VERBOSE, "Best match %f vs %f accepted", itf[i].data()[0].distance, itf[i].data()[1].distance );
						frameGoodMatches.push_back(itf[i].data()[0]);
						//Replaced by Pejman, as we want only very good matches to survive
//						MatchedPointsL.push_back(keyPointsL[itf[i].data()[0].queryIdx].pt);
//						MatchedPointsR.push_back(keyPointsR[itf[i].data()[0].trainIdx].pt);
//						SurvivedFrameTrajectoryL.push_back(itL[itf[i].data()[0].queryIdx]);
//						SurvivedFrameTrajectoryR.push_back(itR[itf[i].data()[0].trainIdx]);
					}
					else{
						Logger::Log(VERBOSE, "Best match %f vs %f REJECTED!!!", itf[i].data()[0].distance, itf[i].data()[1].distance );
					}

				}


				Logger::Log(VERBOSE, "At the end we have %d good matches", frameGoodMatches.size());

				int i=0;
				double sumX=0, sumY=0;
				//Pejman Added these lines
				for(auto match : frameGoodMatches) {
					i++;
					auto left = keyPointsL[match.queryIdx].pt;
					auto right = keyPointsR[match.trainIdx].pt;

					Logger::Log(VERBOSE, "LEFT = (%6.2f, %6.2f) , RIGHT = (%6.2f, %6.2f) ", left.x, left.y,right.x, right.y);

					double sdX = std::abs(left.x - right.x);//* (left.x - right.x);
					double sdY = std::abs(left.y - right.y);// * (left.y - right.y);
					sumX += sdX;
					sumY +=sdY;
				}
				Logger::Log(VERBOSE, "SUM: SUM = (%6.2f, %6.2f) , # = %d, loop# = %d", sumX, sumY, frameGoodMatches.size(), i);
				double meanX = sumX/frameGoodMatches.size();
				double meanY = sumY/frameGoodMatches.size();

				double varXsum=0, varYsum=0;
				for(auto match : frameGoodMatches) {
					auto left = keyPointsL[match.queryIdx].pt;
					auto right = keyPointsR[match.trainIdx].pt;
					double sdX = std::abs(left.x - right.x);//* (left.x - right.x);
					double sdY = std::abs(left.y - right.y);

					varXsum += std::pow(sdX - meanX, 2);
					varYsum += std::pow(sdY - meanY, 2);
				}

				double varX = varXsum / frameGoodMatches.size();
				double varY = varYsum / frameGoodMatches.size();
				Logger::Log(VERBOSE, "mean and varianve of motion: M = (%6.2f, %6.2f) , V = (%6.2f, %6.2f)", meanX, meanY, varX, varY);
				double r = 2.0;//0.15;//consider anything outsde (r * sigma) area an outlier, r =2 shoul include 97% of normal data

				std::vector<cv::DMatch> frameVeryGoodMatches;
				for(auto match : frameGoodMatches) {
					auto left = keyPointsL[match.queryIdx].pt;
					auto right = keyPointsR[match.trainIdx].pt;
					double sdX = std::abs(left.x - right.x);//* (left.x - right.x);
					double sdY = std::abs(left.y - right.y);
					if( sdX > (meanX - varX * r / 2) && (sdX < meanX + varX * r / 2) &&
							sdY > (meanY - varY * r /2)  && (sdY < meanY + varY * r / 2)){
						frameVeryGoodMatches.push_back(match);
						// Moved here
						MatchedPointsL.push_back(keyPointsL[match.queryIdx].pt);
						MatchedPointsR.push_back(keyPointsR[match.trainIdx].pt);
						SurvivedFrameTrajectoryL.push_back(itL[match.queryIdx]);
						SurvivedFrameTrajectoryR.push_back(itR[match.trainIdx]);
					}
					else {
						cout << "Deleted !" << endl;
					}
				}
//				cout <<"Me an Match Distance = ( " << meanX << ", " << meanY << ") and sqrt(var) = ("<< std::sqrt(varX) << ", " << std::sqrt(varY) << ")" << endl;

				Logger::Log(VERBOSE, "At the end we have %d very good matches", frameVeryGoodMatches.size());

				//Pejman To here

				if(args.getShowMatches())
					Display::Show(lframe, rframe, keyPointsL, keyPointsR, frameVeryGoodMatches);


				matches.push_back(frameDMatch);
				goodMatches.push_back(frameVeryGoodMatches);
				SurvivedTrajectoryL.push_back(SurvivedFrameTrajectoryL);
				SurvivedTrajectoryR.push_back(SurvivedFrameTrajectoryR);
				frameNumber ++;
				leftMatchedPoints.push_back(MatchedPointsL);
				rightMatchedPoints.push_back(MatchedPointsR);
			}
	}


	void ComputeMatches::getTrajKeyPoints(AugmentedVideoBuffer& video,  vector<const Trajectory*>* trajectory , std::vector<KeyPoint>* keyPoints){
		std::vector<cv::Point2f> floatPoints;
		Logger::Log(VERBOSE, "Current frame number = %d ", video.getCurrentFrameNumber());
		auto trajPoints = video.getCurrentFrameTrajectoriesRange();
		//Logger::Log(INFO, "Current frame trajectories = %d", trajPoints.size());
		int i =0;
		for (auto it = trajPoints.first; it != trajPoints.second; ++it) {
			i++;
			floatPoints.push_back(it->second.getFirstPoint());
			trajectory->push_back(&(it->second));
		}
		Logger::Log(VERBOSE, "Loop runned = %d times", i);
//		Logger::Log(VERBOSE, "Current frame trajectories = %d", trajectory->size());
		KeyPoint::convert(floatPoints, *keyPoints);
	}
}
}
