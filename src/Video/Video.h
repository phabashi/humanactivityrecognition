/*
 * Video.h
 *
 *  Created on: Dec 18, 2014
 *      Author: Pejman
 *      Copyright (c) 2014 Pejman. All rights reserved.
 */

#ifndef VIDEO_H_
#define VIDEO_H_

#include <string>
#include <list>
#include <opencv2/opencv.hpp>

namespace HAR {
namespace Video{
class Video {
public:
	Video();
	Video(std::string fileName, bool isGrayscale = true);

	virtual void LoadFromFile(std::string fileName);
	virtual void SaveToFile(std::string fileName);

	virtual const cv::Mat& getFrame(unsigned long frameNumber);
	virtual const cv::Mat& getNextFrame();

	virtual void Reset();

	void enableMirorring();
	void dispableMirorring();

	void enableResize(float resize);
	void enableResize(cv::Size size);
	void disableResize();

	bool isValid() { return cvCapture.isOpened(); }
	virtual ~Video();

    void setGrayscale(bool value);
    
    void saveSubVideoToFile(std::string fileName, int startFrame, int endFrame);

    int getFramePerSecond() const { return framePerSec; }

    int getWidth() const { return width; }
    int getHeight() const { return height; }
    int getNumberOfFrames() const { return totalNumberOfFrames; }

protected:
    virtual void InvalidateBuffer() {} // In the buffered version of video, this function causes to reset buffer
private:
	cv::VideoCapture cvCapture;
	cv::Mat currentFrame;
	bool mirror;
	cv::Size* reSize;
	float scale;
    bool isGrayscale;

    int width;
    int height;
    int totalNumberOfFrames;
    int framePerSec;

};

} /* namespace Video */
} /* namespace HAR */

#endif /* VIDEO_H_ */
