/*
 * I3DEncoding.cpp
 *
 *  Created on: Dec 28, 2016
 *      Author: Pejman
 */

#include "I3DEncoding.h"
#include "NoEncoding.h"
#include "DiffEncoding.h"
#include "../../Logger/Logger.h"

#include <math.h>       /* sqrt */
#include "../Trajectory.h"
#include "../Trajectory3D.h"

#include <unordered_map>

using namespace std;
using namespace Pejman::Logger;

namespace HAR {
namespace Video {
namespace TrajectoryEncoding {

std::string encodingAlgorithmName;


vector<vector<Trajectory3D> > I3DEncoding::readTrajectoriesFromMatrix(const cv::Mat& trajectories) {
	vector<vector<Trajectory3D> > allTrajectories;
	allTrajectories.reserve(200);
	//Iterating through all trajectories, each run of loop cover left and right trajectories
	for (int i = 0; i < trajectories.rows; i += 2) {
		Trajectory leftTr(trajectories.row(i)); //left trajectory
		Trajectory rightTr(trajectories.row(i + 1)); // right trajectory
		Trajectory3D trajectory3D(std::move(leftTr), std::move(rightTr));
		int startFrame = trajectory3D.getStartFrame();
		while (startFrame >= (int) (allTrajectories.size())) {
			//push empty array at the end
			allTrajectories.push_back(vector<Trajectory3D>());
		}
		allTrajectories[startFrame].push_back(std::move(trajectory3D));
	}

	return allTrajectories;
}

//std::vector<cv::Point3f> I3DEncoding::Combine3D( const std::vector<cv::Point2f>& leftPoints, const std::vector<cv::Point2f>& rightPoints) {
//
//	std::vector<cv::Point3f> result;
//	if (! rectifiedCorrectly ) return result;
//	for(auto it = leftPoints.begin(), it1 = rightPoints.begin(); it != leftPoints.end()
//				&& it1 != rightPoints.end(); ++it, ++it1){
//			std::vector<cv::Point2f> leftPoint;
//			std::vector<cv::Point2f> rightPoint;
//			leftPoint.push_back( *it );
//			rightPoint.push_back( *it1 );
//			//cv::Mat lp(2,1, CV_64FC1), rp(2,1, CV_64FC1);
//			//lp.at<double>(0,0) = it->x;
//			//lp.at<double>(1,0) = it->y;
//			//lp.at<double>(2,0) = 1;
//			//rp.at<double>(0,0) = it1->x;
//			//rp.at<double>(1,0) = it1->y;
//			//rp.at<double>(2,0) = 1;
//
//			std::vector<cv::Point2f> rectifiedLeftPoint;
//			std::vector<cv::Point2f> rectifiedRightPoint;
//			perspectiveTransform(leftPoint, rectifiedLeftPoint, H1);
//			perspectiveTransform(rightPoint, rectifiedRightPoint, H2);
//			//lp = H1 * lp;
//			//rp = H2 * rp;
//
//
//			//rectified left point x and y
//			double lx = rectifiedLeftPoint[0].x;// / lp.at<double>(2,0);
//			double ly = rectifiedLeftPoint[0].y;// / lp.at<double>(2,0);
//
//			//rectified right point x and y
//			double rx = rectifiedRightPoint[0].x;// / rp.at<double>(2,0);
//			double ry = rectifiedRightPoint[0].y;// / rp.at<double>(2,0);
//
//			double dx = lx - rx;
//			double dy = ly - ry;
//			double disparity;
//			if(std::abs(dy) > 10) {
//				//TODO after rectification dy should be ideally zero
//				// dy larger than 10 might means it is not a good stereo match!
//				return std::vector<cv::Point3f>();//return empty matrix
//			};
//			//disparity = sqrtf(pow((rlx - rrx), 2) + pow((rly - rry), 2));
//			disparity = dx;//ignore dy as it should be ideally zero
//			//cout << "( rlx, rly, rrx, rry) = " << rlx << ", " << rly << ", " << rrx << ", " << rry << endl;
//			cv:: Point3f point3D;
//			point3D.x = it->x;//(lx) / disparity;
//			point3D.y = it->y;//(ly) / disparity;
//			point3D.z = disparity;
//			result.push_back(point3D);
//	}
//	return result;
//}


I3DEncoding::I3DEncoding() {

}

std::unique_ptr<I3DEncoding> I3DEncoding::get3DEncoding(std::string algorithmName) {
	I3DEncoding* algorithm;
	encodingAlgorithmName = algorithmName;
	if(algorithmName == "Off") {
		algorithm = new NoEncoding;
	} else if(algorithmName == "Spatial" or algorithmName == "0Order") {
		//algorithm = new SpatialEncoding();
		algorithm = new DiffEncoding(0, false);
	} else if(algorithmName == "Diff" or algorithmName == "1Order") {
		//algorithm = new DifferentiationEncoding;
		algorithm = new DiffEncoding(1, false);
	} else if(algorithmName == "SecondOrder" or algorithmName == "2Order") {
		//algorithm = new DifferentiationEncoding;
		algorithm = new DiffEncoding(2, false);
	} else if(algorithmName == "ThirdOrder" or algorithmName == "3Order") {
		//algorithm = new DifferentiationEncoding;
		algorithm = new DiffEncoding(3, false);
	} else if( algorithmName == "4Order") {
		//algorithm = new DifferentiationEncoding;
		algorithm = new DiffEncoding(4, false);
	} else if( algorithmName == "5Order") {
		//algorithm = new DifferentiationEncoding;
		algorithm = new DiffEncoding(5, false);
	} else if( algorithmName == "6Order") {
		//algorithm = new DifferentiationEncoding;
		algorithm = new DiffEncoding(6, false);
	} else if( algorithmName == "7Order") {
		//algorithm = new DifferentiationEncoding;
		algorithm = new DiffEncoding(7, false);
	} else if(algorithmName == "SpatialN" or algorithmName == "0OrderN") {
		//algorithm = new SpatialEncoding();
		algorithm = new DiffEncoding(0, true);
	} else if(algorithmName == "DiffN" or algorithmName == "1OrderN") {
		//algorithm = new DifferentiationEncoding;
		algorithm = new DiffEncoding(1, true);
	} else if(algorithmName == "SecondOrderN" or algorithmName == "2OrderN") {
		//algorithm = new DifferentiationEncoding;
		algorithm = new DiffEncoding(2, true);
	} else if(algorithmName == "ThirdOrderN" or algorithmName == "3OrderN") {
		//algorithm = new DifferentiationEncoding;
		algorithm = new DiffEncoding(3, true);
	} else if( algorithmName == "4OrderN") {
		//algorithm = new DifferentiationEncoding;
		algorithm = new DiffEncoding(4, true);
	} else if( algorithmName == "5OrderN") {
		//algorithm = new DifferentiationEncoding;
		algorithm = new DiffEncoding(5, true);
	} else if( algorithmName == "6OrderN") {
		//algorithm = new DifferentiationEncoding;
		algorithm = new DiffEncoding(6, true);
	} else if( algorithmName == "7OrderN") {
		//algorithm = new DifferentiationEncoding;
		algorithm = new DiffEncoding(7, true);
	} else {
			Logger::Log(LogLevel::ERROR,
					"The requested encoding algorithm %s is unknown! Using 'No Encoding' Instead.",
					algorithmName.c_str());
			algorithm = new NoEncoding;
	}
	Logger::Log(LogLevel::VERBOSE,
						"The requested encoding algorithm %s Is set.",
						algorithmName.c_str());
	return std::unique_ptr<I3DEncoding>(algorithm);
}

cv::Mat I3DEncoding::encode3D(const cv::Mat& trajectories, cv::Size imgSize) {
	//Each line in the Trajectories contains 2D trajectories from left and right video respectively!
	Logger::Log(LogLevel::INFO, "I3DEncoding::encode3D()");
	int rows = trajectories.rows;
	int cols = trajectories.cols;
	if (rows % 2 != 0 ){
		// make a scene
		Logger::Log(LogLevel::FATAL, "FATAL ERROR : We expect even number of rows");
		return cv::Mat();
	}

	cv::Mat result;
	auto allTrajectories = readTrajectoriesFromMatrix(trajectories);
	Logger::Log(LogLevel::INFO, "Given %dx%d matrix translated into %d frame trajectories.", trajectories.rows, trajectories.cols,(int) allTrajectories.size());
	for (int i=0; i< allTrajectories.size(); i++) {
		Logger::Log(LogLevel::INFO, "The frame %d has %d trajectories.", i, allTrajectories[i].size());
	}
	int trajectoryLength = ((cols -1 )/ 2) ;
	int numberOfFrames = allTrajectories.size() + trajectoryLength;

	for(int i=0; i< numberOfFrames; i++) {
		rectifiedCorrectly.push_back(false);
		H1.push_back(cv::Mat());
		H2.push_back(cv::Mat());
	}

	//TODO To rectify before encoding??? I moved update rectifying information to stereoRecftifyableVideo
	//UpdateRectifyInformationForAllFrames(allTrajectories, numberOfFrames, imgSize);
	int rowToWrite = 0;
	for(auto frame : allTrajectories){
		for ( auto trajectory : frame){
			trajectory.Combine(rectifiedCorrectly, H1, H2);
			auto temp = trajectory.get3DPoints();
			if (temp.size()>0){//if the combine did not reject this trajectory
				std::vector<float> encoded = this->encode3D(temp);
				if( result.empty() ){//if the matrix is not initialized
					result = cv::Mat( trajectories.rows, encoded.size(), CV_32FC1 );
				}
				for(int i=0; i< encoded.size(); i++)
					result.at<float>(rowToWrite, i) = encoded[i];
				rowToWrite ++;
			}
		}
	}
	return result(cv::Rect(0,0, result.cols, rowToWrite));
}

//cv::Mat I3DEncoding::encode3D(const cv::Mat& trajectories) const {
//	if(trajectories.rows == 0 || trajectories.cols < 2)//trajectory length should be greater than 2
//		return cv::Mat();
//
//	int start=1, end = trajectories.cols -1;
//	if(trajectories.cols % 3 == 0) //if the frame number is not saved (3D trajectories have 3*l length)
//		start = 0;
//	Logger::Log(VERBOSE,"Start = %d,  end = %d", start, end);
//	int trajectorylegth=(end - start)/3;
//	std::vector<float> firstEncoded;
//	int numberOfColumns = 0;
//	int rowToRead=0;
//
//	while(numberOfColumns == 0) {
//		std::vector<cv::Point3f> points;
//		points.reserve(trajectorylegth);
//		for(int j=start; j <= end; j+=3){
//			if(encodingAlgorithmName=="Off")
//				points.push_back(cv::Point3f(trajectories.at<float>(rowToRead,j),trajectories.at<float>(rowToRead,j+1), trajectories.at<float>(rowToRead,j+2)));
//			else {
//				points.push_back(cv::Point3f(trajectories.at<float>(rowToRead,j),trajectories.at<float>(rowToRead,j+1), trajectories.at<float>(rowToRead,j+2)));
//				points.push_back(cv::Point3f(trajectories.at<float>(rowToRead,j+1),trajectories.at<float>(rowToRead,j+2), trajectories.at<float>(rowToRead,j+3)));
//			}
//		}
//		firstEncoded = encode3D(points);
//		numberOfColumns = firstEncoded.size();
//		rowToRead++;
//	}
//	Logger::Log(VERBOSE,"Generate result matrix");
//	cv::Mat result( trajectories.rows, numberOfColumns, CV_32FC1 );
//
//	for(int j=0; j < numberOfColumns; j++ )
//		result.at<float>(0,j) = firstEncoded[j];
//	int rowToWrite=1;
//
//	for(; rowToRead < trajectories.rows; rowToRead++){
//		std::vector<cv::Point3f> points;
//		points.reserve(trajectorylegth);
//		for(int j=start; j <= end; j+=3){
//			if(encodingAlgorithmName=="Off")
//				points.push_back(cv::Point3f(trajectories.at<float>(rowToRead,j),trajectories.at<float>(rowToRead,j+1), trajectories.at<float>(rowToRead,j+2)));
//			else {
//				points.push_back(cv::Point3f(trajectories.at<float>(rowToRead,j),trajectories.at<float>(rowToRead,j+1), trajectories.at<float>(rowToRead,j+2)));
//				points.push_back(cv::Point3f(trajectories.at<float>(rowToRead,j+1),trajectories.at<float>(rowToRead,j+2), trajectories.at<float>(rowToRead,j+3)));
//			}
//		}
//		std::vector<float> encoded = encode3D(points);
//
//		if(encoded.size() != 0 ){
//			for(uint j=0; j < encoded.size(); j++ )
//				result.at<float>(rowToWrite,j) = encoded[j];
//			rowToWrite++;
//		}
//	}
//
//	return result(cv::Range(0, rowToWrite), cv::Range(0, numberOfColumns));
//}

I3DEncoding::~I3DEncoding() {
}


} /* namespace TrajectoryEncoding */
} /* namespace Video */
} /* namespace HAR */
