/*
 * VideoFeaturePoints.cpp
 *
 *  Created on: Dec 30, 2014
 *      Author: Pejman
 *      Copyright (c) 2014 Pejman. All rights reserved.
 */

#include "VideoFeaturePoints.h"

#include <opencv2/core/core.hpp>

namespace HAR {
namespace Video {
namespace Features {

VideoFeaturePoints::VideoFeaturePoints() {
	this->keypointType = "NOTINITIALIZED";
}

VideoFeaturePoints::VideoFeaturePoints(std::string keypointType) {
	this->keypointType = keypointType;
}

FrameFeaturePoints* VideoFeaturePoints::operator[](int index) {
	return getFrameFeaturePoints(index);
}

FrameFeaturePoints* VideoFeaturePoints::getFrameFeaturePoints(unsigned int frameNumber) const{
	if(frameNumber >= this->videoFeaturePoints.size())
		return 0;
	return videoFeaturePoints[frameNumber];
}

bool VideoFeaturePoints::Write(std::string fileName, std::string channel)
{
	int v = cv::FileStorage::APPEND;// 1;//cv::FileStorage::WRITE;
	cv::FileStorage fs(fileName, v);

	fs << channel << "{";
	unsigned long frameCount = videoFeaturePoints.size();
	fs << "FrameCount" << (int)frameCount;

	fs << "FeatureKeyPointType" << keypointType;

	for(unsigned int i = 0; i< frameCount; i++)
	{
		std::stringstream name;
		name << "Frame" << i;
		fs << name.str();
		videoFeaturePoints[i]->Write(fs);
	}

	fs << "}";

	fs.release();
	return true;
}

bool VideoFeaturePoints::Read(std::string fileName, std::string channel)
{

	int v = cv::FileStorage::READ;
	cv::FileStorage storage(fileName, v);

	if(storage[channel].empty())
		return false;

	ReadVideoFromFileNode(storage[channel]);

	storage.release();
	return true;
}

bool VideoFeaturePoints::ReadVideoFromFileNode(cv::FileNode node)
{
	unsigned int frameCount = (int) node["FrameCount"];
	videoFeaturePoints.resize(frameCount);

	node["FeatureKeyPointType"] >> keypointType;
	for(unsigned int i = 0; i< frameCount; i++)
	{
		std::stringstream name;
		name << "Frame" << i;
		if(! videoFeaturePoints[i])
			videoFeaturePoints[i] = new FrameFeaturePoints();
		videoFeaturePoints[i]->Read(node[name.str()]);
	}
	return true;
}

VideoFeaturePoints::~VideoFeaturePoints() {
	for(unsigned int i = 0; i<videoFeaturePoints.size(); i++)
		if(videoFeaturePoints[i]) delete videoFeaturePoints[i];
}

void VideoFeaturePoints::addFrameFeaturePoints(unsigned int frameNumber,
		FrameFeaturePoints* frameFeaturePoints) {
	if(frameNumber < videoFeaturePoints.size())
	{
		printf("Frame %d Exists\n", frameNumber);
		videoFeaturePoints[frameNumber] = frameFeaturePoints;
	}
	else
	{
		videoFeaturePoints.resize(frameNumber + 1);
		videoFeaturePoints[frameNumber] = frameFeaturePoints;
	}
}

} /* namespace Features */
} /* namespace Video */
} /* namespace HAR */
