/*
 * MainMerge.cpp
 *
 *  Created on: May 1, 2015
 *      Author: Pejman
 *      Copyright (c) 2015 Pejman. All rights reserved.
 */

#include "../Video/Display.h"
#include "../util/util.h"
#include "../Logger/Logger.h"
#include <time.h>

using namespace std;
using namespace Pejman::Logger;

class Arguments {
public:
	Arguments(int argc, char* argv[]);
	string getOutputFileName() {
		return outputFileName;
	}
	bool outputFileNameIsSet() {
		return (outputFileName != "");
	}
	string getInputFolderName() {
		return inputFolder;
	}

	void printHelp() {
		cout << "This program reads the file names from standard input and concatenate the content of the files into standard output." << endl
		<< endl << "       Usage : Merge [-f <folderName>] [-s <SampleSize in K>] " << endl << "another way of doing it is to send the file names trough piping: " << endl
		<< endl <<" -s <size> 	: Is useful for sampling from merged instances. " << endl
		<< endl <<" Sample Usage 1 : ls ./features/ | Merge -f ./features/ " << endl
		<< endl <<" Sample Usage 2 : ls ./features/ | Merge -f ./features/ -s 10 \t\t selects 10K lines from all files" << endl
		<< endl <<" Sample Usage 3 : ls ./features/* | Merge " << endl << endl;
	}

	long getSampleSize() const {
		return sampleSize;
	}

private:
	string outputFileName;
	string inputFolder;
	long sampleSize;
};

int main(int argc, char** argv) {
	Logger::getInstance().ChangeLevel(LogLevel::ERROR);
	Arguments args(argc, argv);
	string outputFileCommand = "";
	if (args.outputFileNameIsSet()) {
		Logger::Log(LogLevel::INFO, "Setting the output file name to %s",
				args.getOutputFileName().c_str());
		outputFileCommand = " >> " + args.getOutputFileName();
	}

	srand(time(NULL));

	if(args.getSampleSize() == 0 ) {//there is no sampling

		string fileName;
		cin >> fileName;
		while (cin) {
			Logger::Log(LogLevel::INFO, "Printing the content of  %s",
					(args.getInputFolderName() + fileName + outputFileCommand).c_str());
			system(
					("cat " + args.getInputFolderName() + fileName
							+ outputFileCommand).c_str());
			cin >> fileName;
		}
	} else { //we are going to sample the input with size: args.getSampleSize()

		vector<string> fileNames;
		string fileName;
		cin >> fileName;
		while (cin) {
			fileNames.push_back(fileName);
			cin >> fileName;
		}
		int remainingFiles = fileNames.size();
		long remaining = args.getSampleSize();
		for(string fileName: fileNames){
			ifstream inputFile(args.getInputFolderName() + fileName);
			vector<string> lines;
			lines.reserve(100000);// To speed up I might waste some memory!
			while(inputFile){
				string line;
				getline(inputFile, line);
				lines.push_back(line);
			}
			uint thisfileShare = remaining / remainingFiles;

			float p = (float) thisfileShare / lines.size();
			for(uint i=0; i<lines.size(); i++){
				float r = ((float) rand()) / RAND_MAX;
				if(r <= p){
					cout << lines[i] << endl;
					remaining --;
				}
			}

			remainingFiles--;//We are processing one file
		}
	}

	return 0;
}

Arguments::Arguments(int argc, char* argv[]) :
		outputFileName(""),
		inputFolder(""),
		sampleSize(0)
{
	for (int i = 1; i < argc; i++) {
		if (strcmp(argv[i], "-h") == 0) {
			printHelp();
			exit(0);
		} else if (strcmp(argv[i], "-o") == 0) {
			if (i < argc - 1) {
				outputFileName = argv[++i];
			} else {
				cout << "Ill formated input: The output file name is missing"
						<< endl;
			}
		} else if (strcmp(argv[i], "-f") == 0) {
			if (i < argc - 1) {
				inputFolder = argv[++i];
			} else {
				cout << "Ill formated input: The input folder name is missing"
						<< endl;
			}
		} else if (strcmp(argv[i], "-s") == 0) {
			if (i < argc - 1) {
				sampleSize = atoi(argv[++i]) * 1000;
			} else {
				cout << "Ill formated input: The sampling size is missing."
						<< endl;
			}
		}
	}//for
}
