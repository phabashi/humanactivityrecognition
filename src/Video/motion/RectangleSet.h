/*
 * RectangleSet.h
 *
 *  Created on: Jan 30, 2016
 *      Author: Pejman
 *      Copyright (c) 2016 Pejman. All rights reserved.
 */

#ifndef SRC_VIDEO_MOTION_RECTANGLESET_H_
#define SRC_VIDEO_MOTION_RECTANGLESET_H_

#include <vector>
#include <opencv2/opencv.hpp>

namespace HAR {

class RectangleSet {
public:
	RectangleSet();
	void AddRectangle(cv::Rect r);

	const std::vector<cv::Rect>& getFinalRectangles() { return finalRects; }

	void MergeCloseRectangles(int distance);

	virtual ~RectangleSet();

private:
	void MakeAdjacencyMartix(int distance);
	void ColorTheGraph();
	void PutRectanglesIntoDifferentSets();
	void CalculateFinalRectangles();

private:
	std::vector<cv::Rect> rects;
	std::vector< std::vector<int> > a;
	int c;
	int *color;
	std::vector<std::vector<cv::Rect> > rs;
	std::vector<cv::Rect> finalRects;

private: // Helper Functions

	static int distance;

	static inline bool inside(cv::Rect r, cv::Point p){
		return (p.x > r.x && p.x < r.x + r.width && p.y > r.y && p.y < r.y + r.height );
	}

	static inline bool near(cv::Rect r1, cv::Rect r2) {
		cv::Point p1(r2.x, r2.y);
		cv::Point p2(r2.x + r2.width, r2.y + r2.height);

		cv::Rect paddedRectangle(r1.x - distance , r1.y - distance , r1.width + 2 * distance, r1.height + 2 * distance);
		return ( inside(paddedRectangle, p1) || inside(paddedRectangle, p2) );
	}

	static inline const cv::Rect Merge(cv::Rect r1, cv::Rect  r2){
		int x0 = std::min(r1.x, r2.x);
		int y0 = std::min(r1.y, r2.y);
		int x1 = std::max(r1.x + r1.width, r2.x + r2.width);
		int y1 = std::max(r1.y + r1.height, r2.y + r2.height);
		return cv::Rect(x0, y0, x1-x0, y1-y0);
	}
};

} /* namespace HAR */

#endif /* SRC_VIDEO_MOTION_RECTANGLESET_H_ */
