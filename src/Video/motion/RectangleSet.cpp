/*
 * RectangleSet.cpp
 *
 *  Created on: Jan 30, 2016
 *      Author: Pejman
 *      Copyright (c) 2016 Pejman. All rights reserved.
 */

#include "RectangleSet.h"
#include <queue>
#include "../../Logger/Logger.h"

using namespace Pejman::Logger;

namespace HAR {

int RectangleSet::distance = 10;

RectangleSet::RectangleSet() {
	color = NULL;
	c = 0;
}

void RectangleSet::AddRectangle(cv::Rect r) {
	rects.push_back(r);
}

void RectangleSet::MakeAdjacencyMartix(int distnace) {
	//Make a graph based on the closeness of rectangles
	this->distance = distnace;
	int size = rects.size();
	a = std::vector< std::vector<int> >( size , std::vector<int>(size, 0));
	for(uint i=0; i<rects.size(); i++)
		for(uint j=0; j<rects.size(); j++)
			a[j][i] = a[i][j] = ( near(rects[i], rects[j]) ) ? 1 : 0;
}


RectangleSet::~RectangleSet() {
	if ( color != NULL) delete[] color;
}

void RectangleSet::ColorTheGraph() {
	//Color the Graphs
	if (color !=NULL) delete []color;
	color = new int[rects.size()];
	for (uint i=0; i< rects.size(); i++ )
		color[i] = 0;//initialize the colors
	c=0;

	for(uint i=0;i<rects.size(); i++){
		if(color[i] != 0)
			continue;// The point is already in a cluster, no need for further processing!

		//Use BFS to color the graph
		std::queue<int> toProcess;
		toProcess.push(i);

		color[i] = ++c;//make a new color for the freshly found node!

		while(! toProcess.empty()){
			int current = toProcess.front();
			toProcess.pop();
			for(uint j=0;j<rects.size(); j++){//go through all nodes
				if(a[current][j] == 1 ) { // If i and j are connected
					assert(color[j] == 0 || color[current] == color[j]);
					if(color[j] == 0){ // We have not visit this node before!
						color[j] = color[i]; // paint them in the same color: Process the node
						toProcess.push(j); // add it the queue further processing
					}
				}
			}
		}
	}
}

void RectangleSet::PutRectanglesIntoDifferentSets() {
	rs.clear();
	rs = std::vector<std::vector<cv::Rect> >(c, std::vector<cv::Rect>());

	for (uint i = 0; i < rects.size(); i++){
		rs[ color[i] - 1 ].push_back( rects[i] );//put rectangles in different sets based on the color
	}

}

void RectangleSet::CalculateFinalRectangles() {
	finalRects.clear();
	for(uint i=0; i<rs.size(); i++){
		std::vector<cv::Rect> cluster = rs[i];
		if(cluster.size() == 0) {
			Logger::Log( LogLevel::ERROR ,"EMPTY CLUSTER! i = %d\n", i);
		continue;
		}
		cv::Rect f = * cluster.begin();
		for(auto r = cluster.begin(); r != cluster.end(); r ++){
			f = Merge(f, *r);
		}
		finalRects.push_back(f);
	}
}


void RectangleSet::MergeCloseRectangles(int distance) {
	try{
		MakeAdjacencyMartix(distance);
		ColorTheGraph();
		PutRectanglesIntoDifferentSets();
		CalculateFinalRectangles();
	}
	catch(std::length_error& t){
		Logger::Log( LogLevel::ERROR,"Catch you!\n");
	}
}

} /* namespace HAR */
