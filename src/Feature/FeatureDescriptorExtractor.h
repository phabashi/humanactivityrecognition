/*
 * FeatureDescriptorExtractor.h
 *
 *  Created on: Jan 26, 2015
 *      Author: Pejman
 *      Copyright (c) 2015 Pejman. All rights reserved.
 */

#ifndef FEATUREDESCRIPTOREXTRACTOR_H_
#define FEATUREDESCRIPTOREXTRACTOR_H_


#include <opencv2/core/core.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <string>
#include <unordered_map>
#include "../Video/AugmentedVideoBuffer.h"
#include "../Video/Frame/FrameFeatureDescriptors.h"
#include "../Video/Frame/FrameFeaturePoints.h"

namespace HAR {
namespace Video {
namespace Features {

class FeatureDescriptorExtractor {
public:
	static FeatureDescriptorExtractor& getInstance(std::string featureType);
	static void removeInstances();

public:
	void ExtractDescriptors(AugmentedVideoBuffer& inputVideo);
	FrameFeatureDescriptors ExtractDescriptorsForFrame(const cv::Mat image, const FrameFeaturePoints& featurePoints);
	std::string getType() { return featureType; }
	virtual ~FeatureDescriptorExtractor();

private:
	FeatureDescriptorExtractor();
	FeatureDescriptorExtractor(FeatureDescriptorExtractor&);
	FeatureDescriptorExtractor& operator=(FeatureDescriptorExtractor&);

private:
	std::string featureType;
	cv::Ptr<cv::DescriptorExtractor> descriptorExtractor;

private:
	static std::unordered_map<std::string, cv::Ptr<FeatureDescriptorExtractor> > instances;


};

} /* namespace Feature */
} /* namespace Video */
} /* namespace HAR */

#endif /* FEATUREDESCRIPTOREXTRACTOR_H_ */
