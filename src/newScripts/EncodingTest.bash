#!/bin/bash

echo "Running test on Different Configurations"
i=0
printf " ##    OFA   length  Normalized  Motion-Detected    NOW     scale    Accuracy\n"
for OFA in FB; do
	export OFA
	for length in 11; do
		export length
		for Norm in Yes; do
			export Norm
			for DM in Yes; do
				export DM
				#. ./newScripts/test.bash
				#for NOW in 4000; do
					for scale in "5" ; do
						for encoding in Spatial 4Order 5Order 6Order 7Order; do
	
							i=$(( i + 1 ))
							printf  "%3d     %2s     %-4s       %-3s         %-8s      %5s    %-6s " $i $OFA $length $Norm $DM $scale $NOW
							. ./newScripts/ExtractFeatures.bash Windsor-short ./rawFeatureFolder & 
							. ./newScripts/Encode.bash ./rawFeatureFolder ./encodedFeatures &
							. ./newScripts/CV.bash ./encodedFeatures
							wait
						done 
					done
				#done
			done	
		done		
	done
done 
