/*
 * UniqueSequenceNumberGenerator.cpp
 *
 *  Created on: Jan 6, 2016
 *      Author: Pejman
 *      Copyright (c) 2016 Pejman. All rights reserved.
 */

#include "UniqueSequenceNumberGenerator.h"

#include <fstream>
#include <sstream>
#include "../util/util.h"
#include "../Exceptions/ArgumentParseException.h"

#include <iostream>

#include "../Exceptions/LabelNotFoundException.h"
#include "../Logger/Logger.h"

using namespace std;
using namespace HAR::Exceptions;
using namespace Pejman::Logger;

namespace HAR {

std::string UniqueSequenceNumberGenerator::fixClassLabel(
		std::string classLabel) {
	//TODO [IMPORTANT]: Class labels do not have underline anymore, find out how the following code works.
	std::string result;
	result = trim(classLabel);
	int i = result.find_last_of('_');
	result = result.substr(0, i);
	i = result.find_last_of('_');
	result = result.substr(i + 1);
	return result;
}

UniqueSequenceNumberGenerator::UniqueSequenceNumberGenerator(std::string labelFileName) {
	std::ifstream input(labelFileName);
	std::string classLabel;
	int counter = 0;

	input >> classLabel;
	while (input) {
		classLabel = fixClassLabel(classLabel);

		auto labelID = labelMapper.find(classLabel);
		if (labelID != labelMapper.end())
			throw ArgumentParseException(
					"Repeated label in your activity list! You should have only one instance of each activity in your activity list.");

		if (classLabel != "") {
			counter++;
			labelMapper[classLabel] = counter;
			label[counter] = classLabel;
			Logger::Log(LogLevel::INFO, "Assigning label '%s' value '%d'\n",
					classLabel.c_str(), counter);
		}
		input >> classLabel;
	};
}

UniqueSequenceNumberGenerator::~UniqueSequenceNumberGenerator() {
	// TODO Auto-generated destructor stub
}

int UniqueSequenceNumberGenerator::getLabelIndex(std::string classLabel) {
	auto labelID = labelMapper.find(classLabel);
	if (labelID == labelMapper.end()) {
		//label have not been found in predefined label list, Most probably a fatal error
		throw LabelNotFoundException(classLabel);
	} else {
		return labelID->second;
	}
}

// OLD Implementation, I keep it just is case.
//int UniqueSequenceNumberGenerator::getUniqueSequenceNumber(std::string classLabel){
//	auto labelID = labelMapper.find(classLabel);
//	if( labelID == labelMapper.end()){
//		//New label have been found
//		labelMapper[classLabel] = ++counter;
//		return labelMapper[classLabel];
//	} else {
//		return labelID->second;
//	}
//}
std::string UniqueSequenceNumberGenerator::getLabel(int index) {
	auto labelID = label.find(index);
	if (labelID == label.end()) {
		//label have not been found in predefined label list, Most probably a fatal error
		std::stringstream msg;
		msg << "Reverse Index Not found: '" << index << "'";
		throw LabelNotFoundException(msg.str());
	} else {
		return labelID->second;
	}
}

} /* namespace HAR */
