/*
 * ImageSizes.h
 *
 *  Created on: Feb 10, 2015
 *      Author: Pejman
 *      Copyright (c) 2015 Pejman. All rights reserved.
 */

#ifndef IMAGESIZES_H_
#define IMAGESIZES_H_

#include <opencv2/opencv.hpp>

namespace HAR {

class ImageSizes {
public:
	ImageSizes(float width, float height );
	ImageSizes(cv::Mat image);
	bool isInside(cv::Point2f p);
	bool isInside(cv::Rect r);
	virtual ~ImageSizes();

private:
	float w;
	float h;
};

} /* namespace HAR */

#endif /* IMAGESIZES_H_ */
