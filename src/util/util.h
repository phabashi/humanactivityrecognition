/*
 * util.h
 *
 *  Created on: May 1, 2015
 *      Author: Pejman
 *      Copyright (c) 2015 Pejman. All rights reserved.
 */

#ifndef SRC_UTIL_UTIL_H_
#define SRC_UTIL_UTIL_H_

#include <string>
#include <sstream>
#include <algorithm>
#include <exception>
#include <vector>
#include <set>



class ConversionException : public std::exception
{
	  virtual const char* what() const throw()
	  {
	    return "Conversion Exception happened!";
	  }
};

static inline std::string toLowerCase(std::string str) {
	std::transform(str.begin(), str.end(), str.begin(), ::tolower);
	return str;
}

template <class T>
T stringTo(std::string str)
{
	T value;
	std::stringstream temp(str);
	temp >> value;
	//char c;
	if (temp.fail() /*|| temp.get(c)*/)
	{
		ConversionException exc;
		//std::cout << "\"" << temp.str() << "\"" << std::endl;
		throw exc;
	}
	return value;
}

// trim from start
static inline std::string &ltrim(std::string &s) {
    s.erase(s.begin(), std::find_if(s.begin(), s.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
    return s;
}

// trim from end
static inline std::string &rtrim(std::string &s) {
    s.erase(std::find_if(s.rbegin(), s.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
    return s;
}

// trim from both ends
static inline std::string &trim(std::string &s) {
    return ltrim(rtrim(s));
}

static inline void chreplace(std::string& str, char ch, char otherCh)
{
    for(unsigned int i=0; i< str.length(); i++)
    {
        if(str[i] == ch)
            str[i] = otherCh;
    }
}

static inline std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems) {
    std::stringstream ss(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        elems.push_back(item);
    }
    return elems;
}


static inline std::vector<std::string> split(const std::string &s, char delim) {
    std::vector<std::string> elems;
    split(s, delim, elems);
    return elems;
}

static inline std::vector<std::string> split(const std::string &line, std::string delimiter) {
    std::vector<std::string> elems;
    elems.reserve(100);
    std::size_t prev=0, pos;
    //std::string delimiter = "";
    //for(auto item : delim)
    //	delimiter += item;
    while((pos = line.find_first_of(delimiter, prev)) != std::string::npos) {
    	if(pos > prev){
    		elems.push_back(line.substr(prev, pos-prev));
    	} else {
    		break;
    	}
    	prev=line.find_first_not_of(delimiter,pos);
    	if(prev==std::string::npos || prev < pos)
    		break;
		//Pejman::Logger::Logger::Log(Pejman::Logger::LogLevel::VERBOSE,
			//	"prev = %d pos = %d", prev, pos);
    }
    if(prev < line.length()){
    	elems.push_back(line.substr(prev));
    }
    return elems;
}



#endif /* SRC_UTIL_UTIL_H_ */
