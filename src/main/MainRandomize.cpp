/*
 * MainRandomize.cpp
 *
 *  Created on: Nov 15, 2015
 *      Author: Pejman
 *      Copyright (c) 2015 Pejman. All rights reserved.
 */
#include <iostream>
#include <vector>
#include <string>

using namespace std;

int main(int argc, char*argv[])
{
	srand(time(NULL));
	rand();
	vector<string> lines;

	while(cin)
	{
		string theLine;
		cin >> theLine;
		if (!cin) break;
		lines.push_back(theLine);
	}

	for(int i=lines.size() - 1; i >= 1 ; i --)
	{
		int r = rand() % i;
		string temp = lines[i];
		lines[i] = lines[r];
		lines[r] = temp;
	}

	for(uint i =0; i < lines.size(); i++)
	{
		cout << lines[i];
		cout << endl;
	}
	return 0;
}

