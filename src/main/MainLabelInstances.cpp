/*
 * MainLabelInstances.cpp
 *
 *  Created on: May 1, 2015
 *      Author: Pejman
 *      Copyright (c) 2015 Pejman. All rights reserved.
 */


//#include "util.h"
#include <iostream>
#include <fstream>
#include <sstream>

using namespace std;

int main(int argc, char** argv)
{
    string csvFileName, labelFileName, csvOutFileName;
    if (argc == 4)
    {
        csvFileName = argv[1];
        labelFileName = argv[2];
        csvOutFileName = argv[3];
    }
    else
    {
        cout << "Usage: LabelInstances csvFileName labelFileName outFileName" << endl;
        return -1;
    }

    ifstream labelFile(labelFileName);
    ifstream infile(csvFileName);
    ofstream outfile(csvOutFileName);

    int lastFrameWithThisLabel;
    string className;
    labelFile >> lastFrameWithThisLabel;
    labelFile >> className;

    string theLine;
    getline(infile, theLine);
    theLine += ", instance, class";
    outfile << theLine << endl;

    int activityInstanceNumber = 0;
    string instanceText = "I";
    string instance ;
    stringstream instanceStream;
    instanceStream << instanceText  << activityInstanceNumber;
    instance = instanceStream.str();

    int i =0;
    while (infile) {
        std::getline( infile, theLine);i++;
        if(theLine.find_first_of(',') == string::npos)
        {
        	cout << "Empty Line detected ( Line " << i <<" ), skipping it. " << endl;
        	continue; //empty line detected
        }

        stringstream temp(theLine);
        int frameNumber;
        temp >> frameNumber;

        while (labelFile && frameNumber >= lastFrameWithThisLabel)
        {
            labelFile >> lastFrameWithThisLabel;
            labelFile >> className;
            activityInstanceNumber ++;
            stringstream instanceStream;
            instanceStream << instanceText  << activityInstanceNumber;
            instance = instanceStream.str();
        }

        if(! labelFile)//we reach the end of label file
        {
        	cout << "Input has finished!" << endl;
            break;
        }

        if(className != "U")
        {
        	theLine.erase(theLine.find_last_not_of(" \n\r\t")+1);
            theLine += ", " + instance + ", " + className;
            outfile << theLine << endl;
        }

    }

    return 0;
}

