/*
 * ConfusionMatrix.h
 *
 *  Created on: Jan 10, 2016
 *      Author: Pejman
 *      Copyright (c) 2016 Pejman. All rights reserved.
 */

#ifndef SRC_CONFUSIONMATRIX_H_
#define SRC_CONFUSIONMATRIX_H_

#include <set>
#include <string>
#include <unordered_map>
#include <vector>
#include <unordered_map>
#include <ostream>

#include "../../util/UniqueSequenceNumberGenerator.h"

namespace HAR {

class ConfusionMatrix {
public:
	ConfusionMatrix(UniqueSequenceNumberGenerator& labels);
	void makeConfusionMatrix(std::string expectedFileName, std::string actualFileName);
	void makeConfusionMatrix(std::vector<int> expected, std::vector<int> actual);
	void CalculateStatistics();
	std::string toString();
	virtual ~ConfusionMatrix();

private:
	UniqueSequenceNumberGenerator& knownLabels;
	std::vector< std::vector <int> > theMatrix;

	//Statistics
	std::vector<double> precision;
	std::vector<double> recall;
	double accuracy;

};

std::ostream& operator <<(std::ostream& out, HAR::ConfusionMatrix& matrix);

} /* namespace HAR */

#endif /* SRC_CONFUSIONMATRIX_H_ */
