/*
 * VideoBuffer.cpp
 *
 *  Created on: Feb 3, 2016
 *      Author: Pejman
 *      Copyright (c) 2016 Pejman. All rights reserved.
 */

#include "VideoBuffer.h"
#include "../Logger/Logger.h"

using namespace Pejman::Logger;

namespace HAR {
namespace Video {

VideoBuffer::VideoBuffer(std::string inputVideoFileName,  int maxBufferSize, bool isGrayscale)
: Video(inputVideoFileName, isGrayscale)
{
	this->maxBufferSize = maxBufferSize;
	bufferStartFrame = 0;
	currentFrameNumber = 0;
	if(maxBufferSize > 0)
		buffer.reserve(maxBufferSize);
	//LoadBuffer(bufferStartFrame);//I had to diable it to make the resize effective :D
}

//return if it was successful to load the given frame number into the buffer
bool VideoBuffer::LoadBuffer(int frameOfinterest) {
	if(frameOfinterest < bufferStartFrame + (int) buffer.size())
		return true; // We have the frameOfInterest in the buffer
	if(maxBufferSize == -1)
	{
		if( buffer.size() != 0){
			return false;//We have loaded the whole video already, but this frame number could not be found!
		}
		int frameNumber = 0;
		//Load all video into the buffer
		bufferStartFrame = 0;
		Video::Reset();
		while(true){
			cv::Mat temp = Video::getNextFrame();
			if(temp.empty()) break;

			buffer.push_back(temp.clone());
			frameNumber++;
		}
		frameSize = cv::Size(buffer[0].cols, buffer[0].rows);
		return (frameOfinterest < (int)buffer.size());//return if the frame number is valid

	} else {
		bufferStartFrame = frameOfinterest;
		//We start filling buffer with the framOfInterest be the first one
		if(buffer.size() == 0)
			buffer.push_back(Video::getFrame(frameOfinterest).clone());
		else
			buffer[0] = Video::getFrame(frameOfinterest).clone();

		uint i;
		for(i =1; i<buffer.size() && !buffer[i-1].empty(); i++)
			buffer[i] = Video::getNextFrame().clone();

		if(buffer[i-1].empty()){// we reached the end of video
			//We should remove any old frames in the buffer from this point!
			while(buffer.size() != i-1){
				buffer.pop_back();//remove instances after last frame
			}
		} else { // we reached the end of buffer, expand the buffer if it is possible
			for(; (int)i < maxBufferSize && !buffer[i-1].empty(); i++){
				buffer.push_back(Video::getNextFrame().clone());
			}
			if(buffer[i-1].empty())
				buffer.pop_back();
		}
		frameSize = cv::Size(buffer[0].cols, buffer[0].rows);
		return (frameOfinterest < bufferStartFrame + (int)buffer.size());
	}

}

const cv::Mat& VideoBuffer::getFrame(unsigned long frameNumber) {
	if( LoadBuffer(frameNumber) ) {//If it is not in the buffer load it into buffer
		currentFrameNumber = frameNumber;
		return (buffer[frameNumber - bufferStartFrame]);//return the frame from buffer
	} else {
		return emptyFrame;//return an empty frame
	}
}

const cv::Mat& VideoBuffer::getNextFrame() {
	currentFrameNumber ++;
	if(	LoadBuffer(currentFrameNumber) ) {//Make sure current frame is in buffer
		return (buffer[currentFrameNumber - bufferStartFrame]);//return current frame from buffer
	} else {
		Logger::Log(VERBOSE, "VIDEO ENDED!");
		currentFrameNumber --;
		return emptyFrame;//return an empty frame
	}
}

const cv::Mat& VideoBuffer::getCurrentFrame() {
	if(	LoadBuffer(currentFrameNumber) ) {//Make sure current frame is in buffer
		return (buffer[currentFrameNumber - bufferStartFrame]);//return current frame from buffer
	} else {
		return emptyFrame;//return an empty frame
	}
}

VideoBuffer::~VideoBuffer() {
	// TODO Auto-generated destructor stub
}


void VideoBuffer::InvalidateBuffer() {
	buffer.clear();
}

} /* namespace Video */
} /* namespace HAR */

