/*
 * MotionDetection.h
 *
 *  Created on: Jan 30, 2016
 *      Author: Pejman
 *      Copyright (c) 2016 Pejman. All rights reserved.
 */

#ifndef MOTIONDETECTION_H_
#define MOTIONDETECTION_H_

#include <vector>
#include "../AugmentedVideoBuffer.h"

namespace HAR {
namespace Video {
namespace Motion {

class MotionDetection {
public:
	MotionDetection();
	void DetectMotion(AugmentedVideoBuffer& video);
	virtual ~MotionDetection();

private:

	cv::Mat frame;
	cv::Mat back;
	cv::Mat fore;

	cv::BackgroundSubtractorMOG2 bg;
	std::vector < std::vector < cv::Point > >contours;

};

} /* namespace Motion */
} /* namespace Video */
} /* namespace HAR */

#endif /* MOTIONDETECTION_H_ */
