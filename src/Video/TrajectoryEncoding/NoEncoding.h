/*
 * NoEncoding.h
 *
 *  Created on: May 30, 2016
 *      Author: pejman
 */

#ifndef SRC_VIDEO_TRAJECTORYENCODING_NOENCODING_H_
#define SRC_VIDEO_TRAJECTORYENCODING_NOENCODING_H_

#include "IEncoding.h"
#include "I3DEncoding.h"

namespace HAR {
namespace Video {
namespace TrajectoryEncoding {

class NoEncoding: public IEncoding, public I3DEncoding {
public:
	NoEncoding();

	virtual std::vector<float> encode(const std::vector<cv::Point2f>& points) const;
	virtual std::vector<float>  encode3D(const std::vector<cv::Point3f>& trajectory3D) const;

	virtual ~NoEncoding();
};

} /* namespace TrajectoryEncoding */
} /* namespace Video */
} /* namespace HAR */

#endif /* SRC_VIDEO_TRAJECTORYENCODING_NOENCODING_H_ */
