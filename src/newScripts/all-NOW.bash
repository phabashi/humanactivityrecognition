#!/bin/bash

echo "Running test on Different Configurations"
i=0
printf " ##    OFA   length  Normalized  Motion-Detected    NOW     scale    Accuracy\n"
for OFA in FB; do
	export OFA
	for length in 21; do
		export length
		for Norm in Yes; do
			export Norm
			for DM in Yes; do
				export DM
				#. ./newScripts/test.bash
				for NOW in 16; do
					export NOW 
					for scale in "5" ; do
					export scale
						for encoding in Diff SecondOrder ThirdOrder NormDiff; do

							i=$(( i + 1 ))
							printf  "%3d     %2s     %-4s       %-3s         %-8s      %5s    %-6s " $i $OFA $length $Norm $DM $scale $NOW
							. ./newScripts/ExtractFeatures.bash Windsor-short-R ./rawFeatureFolder & 
							. ./newScripts/Encode.bash ./rawFeatureFolder ./encodedFeatures &
							. ./newScripts/CV.bash ./encodedFeatures
							wait 
						done
					done
				done
			done	
		done		
	done
done 
