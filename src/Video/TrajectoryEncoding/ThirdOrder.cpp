/*
 * ThirdOrder.cpp
 *
 *  Created on: Sep 26, 2016
 *      Author: pejman
 */

#include "ThirdOrder.h"

#include "../Trajectory.h"
namespace HAR {
namespace Video {
namespace TrajectoryEncoding {

ThirdOrder::ThirdOrder() {
	// TODO Auto-generated constructor stub

}

std::vector<float> ThirdOrder::encode(const std::vector<cv::Point2f>& points) const {
	std::vector<float> result;
	std::vector<cv::Point2f> first;
	std::vector<cv::Point2f> second;
	result.reserve((3*(points.size()- 1) - 3) * 2);

	float m = Trajectory::getEnergy(points);

	if (m == 0)
		return result;

	for (uint i = 1; i < points.size(); i++) {//first order
		cv::Point2f thePoint=points[i] - points[i - 1];
		result.push_back(thePoint.x);
		result.push_back(thePoint.y);
		first.push_back(thePoint);
	}

	for (uint i = 1; i < first.size(); i++) {//second order
		cv::Point2f thePoint = first[i]-first[i-1];
		result.push_back(thePoint.x);
		result.push_back(thePoint.y);
		second.push_back(thePoint);
	}

	for (uint i = 1; i < second.size(); i++) {//second order
		cv::Point2f thePoint = second[i]-second[i-1];
		result.push_back(thePoint.x);
		result.push_back(thePoint.y);
		//third.push_back(thePoint);
	}

	return result;

}

ThirdOrder::~ThirdOrder() {
	// TODO Auto-generated destructor stub
}

} /* namespace TrajectoryEncoding */
} /* namespace Video */
} /* namespace HAR */
