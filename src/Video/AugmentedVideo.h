/*
 * AugmentedVideo.h
 *
 *  Created on: Jan 16, 2015
 *      Author: Pejman
 *      Copyright (c) 2015 Pejman. All rights reserved.
 */

#ifndef AUGMENTEDVIDEO_H_
#define AUGMENTEDVIDEO_H_

#include <unordered_map>

#include "../Feature/VideoFeaturePoints.h"
#include "Video.h"

namespace HAR {
namespace Video {

class AugmentedVideo {
public:
	AugmentedVideo(std::string videoFileName, bool isGrayScale = false);

	Video getVideo() const { return inputVideo; }

	const Features::VideoFeaturePoints& getFeaturePoints(std::string featureType);

	bool ReadFeaturesFromXMLFile(std::string xmlFileName, std::string featureType);

	bool WriteFeaturesToXMLFile(std::string xmlFileName, std::string featureType);

	void CalculateFeatures(std::string featureType);

	virtual ~AugmentedVideo();

private:
	Video inputVideo;
	std::string videoFileName;
	std::string xmlFileName;

//	VideoFeaturePoints siftFeaturePoints;
//	VideoFeaturePoints surfFeaturePoints;

	std::unordered_map<std::string, Features::VideoFeaturePoints*> featurePoints;

//private:
//	void CalculateSift();
//	void CalculateSurf();
//	bool WriteSiftFeaturesToXMLFile(std::string xmlFileName);
//	bool WriteSurfFeaturesToXMLFile(std::string xmlFileName);
//	bool ReadSiftFeaturesFromXMLFile(std::string xmlFileName);
//	bool ReadSurfFeaturesFromXMLFile(std::string xmlFileName);
//	const VideoFeaturePoints& getSiftFeaturePoints();
//	const VideoFeaturePoints& getSurfFeaturePoints();
};

} /* namespace Video */
} /* namespace HAR */

#endif /* AUGMENTEDVIDEO_H_ */
