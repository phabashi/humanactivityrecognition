/*
 * SecondOrder.h
 *
 *  Created on: Sep 25, 2016
 *      Author: Pejman
 *      Copyright (c) 2016 Pejman. All rights reserved.
 */

#ifndef SRC_VIDEO_TRAJECTORYENCODING_SECONDORDER_H_
#define SRC_VIDEO_TRAJECTORYENCODING_SECONDORDER_H_

#include "IEncoding.h"

namespace HAR {
namespace Video {
namespace TrajectoryEncoding {

class SecondOrder: public IEncoding  {
public:
	SecondOrder();

	virtual std::vector<float> encode(const std::vector<cv::Point2f>& points) const;

	virtual ~SecondOrder();
};

} /* namespace TrajectoryEncoding */
} /* namespace Video */
} /* namespace HAR */

#endif /* SRC_VIDEO_TRAJECTORYENCODING_SECONDORDER_H_ */
