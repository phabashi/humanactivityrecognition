#!/bin/bash

#settings
export inputFileList="./lists/VideoFileList.txt"
export opticalFlowAlgorithm="IP"
export interestPointAlgorithm="FAST"
export descriptorAlgorithm="SIFT"
#matchingAlgorithm="BruteForce"
ratio="1"
export scale="0.5" #scale the original video to half size in each direction
removeHomogenousAreas="0"
export featureFolderName="./test-data/$opticalFlowAlgorithm/$interestPointAlgorithm/$descriptorAlgorithm/$matchingAlgorithm/scale_$scale/rha_$removeHomogenousAreas"

#Configuration Section
export numberOfWords=40

# Folder definition Section
ExternalFeaturesFolder="./featuresx/" #This folder is input
MicromovementFeaturesFolder="./microMovements/"
folderPrefix="./test-data/$(echo $ActivitiesFileName | cut -d . -f 1)/$ExternalFeature/n-$numberOfWords-words" # find the file name part of $ActivitiesFileName

csvDataFolder="./$folderPrefix/data/csv"
SVMDataFolder="./$folderPrefix/data/svm"
SVMModelFolder="./$folderPrefix/models/svm"
KMeansModelFolder="./$folderPrefix/models/kmeans"
resultFolder="./$folderPrefix/results/"

# Automated generating parameters section, should not be modified usually.

ActivityList="./lists/$ActivitiesFileName"
externalFeatureFlag=true

SVMLearningParameters="-s 0 -t 3 -d 3 -c 500"

if [ $ExternalFeature = "Trajectory" ]; then
	FeatureNumbers='11-40' # Trajectory Features
elif [ $ExternalFeature = "HOG" ]; then
	FeatureNumbers='41-136' #HOG Features'
elif [ $ExternalFeature = "HOF" ]; then
	FeatureNumbers='137-244' #HOF Features'
elif [ $ExternalFeature = "MBHx" ]; then
	FeatureNumbers='245-340' #MBHx Features'
elif [ $ExternalFeature = "MBHy" ]; then
	FeatureNumbers='341-436' #MBHy Features'
else
	externalFeatureFlag=false # we are dealing with a Micromovement
	if [ $ExternalFeature = "Micromovement-39" ]; then
		FeatureNumbers='2-115' #Micromovemetn with length 39
		MMLength="_L39."
	elif [ $ExternalFeature = "Micromovement-37" ]; then
		FeatureNumbers='2-109' #Micromovemetn with length 37
		MMLength="_L37."
	elif [ $ExternalFeature = "Micromovement-35" ]; then
		FeatureNumbers='2-103' #Micromovemetn with length 35
		MMLength="_L35."
	elif [ $ExternalFeature = "Micromovement-33" ]; then
		FeatureNumbers='2-97' #Micromovemetn with length 33
		MMLength="_L33."
	elif [ $ExternalFeature = "Micromovement-31" ]; then
		FeatureNumbers='2-91' #Micromovemetn with length 31
		MMLength="_L31."
	elif [ $ExternalFeature = "Micromovement-29" ]; then
		FeatureNumbers='2-85' #Micromovemetn with length 29
		MMLength="_L29."
	elif [ $ExternalFeature = "Micromovement-27" ]; then
		FeatureNumbers='2-79' #Micromovemetn with length 27
		MMLength="_L27."
	elif [ $ExternalFeature = "Micromovement-25" ]; then
		FeatureNumbers='2-73' #Micromovemetn with length 25
		MMLength="_L25."
	elif [ $ExternalFeature = "Micromovement-23" ]; then
		FeatureNumbers='2-67' #Micromovemetn with length 23
		MMLength="_L23-"
	elif [ $ExternalFeature = "Micromovement-21" ]; then
		FeatureNumbers='2-61' #Micromovemetn with length 21
		MMLength="_L21-"
	elif [ $ExternalFeature = "Micromovement-19" ]; then
		FeatureNumbers='2-55' #Micromovemetn with length 19
		MMLength="_L19-"
	elif [ $ExternalFeature = "Micromovement-17" ]; then
		FeatureNumbers='2-49' #Micromovemetn with length 17
		MMLength="_L17-"
	elif [ $ExternalFeature = "Micromovement-15" ]; then
		FeatureNumbers='2-43' #Micromovemetn with length 15
		MMLength="_L15-"
	elif [ $ExternalFeature = "Micromovement-13" ]; then
		FeatureNumbers='2-37' #Micromovemetn with length 13
		MMLength="_L13-"
	elif [ $ExternalFeature = "Micromovement-11" ]; then
		FeatureNumbers='2-31' #Micromovemetn with length 11
		MMLength="_L11-"
	elif [ $ExternalFeature = "Micromovement-9" ]; then
		FeatureNumbers='2-25' #Micromovemetn with length 9
		MMLength="_L9-"
	elif [ $ExternalFeature = "Micromovement-7" ]; then
		FeatureNumbers='2-19' #Micromovemetn with length 7
		MMLength="_L7-"
	elif [ $ExternalFeature = "Micromovement-5" ]; then
		FeatureNumbers='2-13' #Micromovemetn with length 5
		MMLength="_L5-"
	elif [ $ExternalFeature = "Micromovement-3" ]; then
		FeatureNumbers='2-7' #Micromovemetn with length 3
		MMLength="_L3-"
	else
		echo "The External Feature can not be found!"
		exit
	fi
fi
