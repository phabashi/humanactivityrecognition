//
//  LabelVideoFramesController.cpp
//  HARLinked
//
//  Created by pejman on 2015-07-08.
//  Copyright (c) 2015 pejman. All rights reserved.
//

#include "LabelVideoFramesController.h"

LabelVideoController::LabelVideoController(LabelVideoModel& model,
                                           LabelVideoView& view): theModel(model), view(view)
{
    keyBinding['0'] = 0;
    setKeyBindings(0);
    theModel.setKeyBindingHelp(getKeyBindingHelp());
    si = 0;
}

bool LabelVideoController::setKeyBindings(int startIndex)
{
    int keyBindingSize = 5;
    if (startIndex<0 || startIndex > (int)theModel.getLabelDefinitions().size() - keyBindingSize) return false;
    
    for (int i = 1 ; i<keyBindingSize; i++) {
        keyBinding['0'+i] = startIndex + i;
    }
    theModel.setKeyBindingHelp(getKeyBindingHelp());
    return true;
}

void LabelVideoController::Update(int keyCode)
{
    if(keyCode != -1)
    {
        theModel.setLastPressedKeyCode(keyCode);
        theModel.setErrorMsg("");
    }
    
    auto kb = keyBinding.find(keyCode);
    if(kb != keyBinding.end())
    {
        std::stringstream newLine;
        newLine << theModel.getCurrentFrameNumber() << " " << theModel.getLabel(kb->second);
        theModel.AddToExistingLabels(newLine.str());
    }
    else switch (keyCode) {
        case 27:
        {
            std::string temp = view.showStringInput("Are you saved everything you need?");
            if(temp == "yes") theModel.terminateDisplay();
            else theModel.setErrorMsg("You have not answered yes.");
            break;
        }
        case ' ':
        case 'P':
        case 'p':
            theModel.TogglePlaying();
            break;
        case 63234:// <-
            theModel.seekVideo(-300);
            break;
            
        case 63235:// ->
            theModel.seekVideo(+300);
            break;
        case '<' :
            theModel.seekVideo(-10);
            break;
            
        case '>' :
            theModel.seekVideo(+10);
            break;
            
        case ',':
            theModel.seekVideo(-1);
            break;
            
        case '.':
            theModel.seekVideo(+1);
            break;
            
        case 63232 ://uparrow
            theModel.upScale();
            break;
            
        case 63233://down arrow
            theModel.downScale();
            break;
            
        case 'a':
        case 'A':
        {
            std::pair<int, int> newMapping = view.showSetNewMapping();
            updateKeyBinding(newMapping);
            theModel.setKeyBindingHelp(getKeyBindingHelp());
            break;
        }
        case 's':
        case 'S':
            theModel.SaveLabelsToFile();
            theModel.setErrorMsg("Saved successfully to "+ theModel.getOutputFileName());
            break;
            /*   case 'g':
             case 'G':
             cout << "Enter Frame Number : ";
             cin >> frameNumber;
             frame = temp.getFrame(frameNumber);
             cout << "Got to Frame: " << frameNumber << endl;
             break;
             */
        case 127:
            theModel.RemoveLastLabel();
            break;
            
        case 'j':
        case 'J':
            theModel.JumpVidepToLastIndex();
            break;
            
        case 'q':
        case 'Q':
            if(setKeyBindings(si-1)) si--;
            
            break;
            
        case 'w':
        case 'W':
            if(setKeyBindings(si+1)) si++;
            break;
            
        default:
            char buff[100];
            sprintf(buff, "Unknown Key code: code = %d", keyCode);
            if(keyCode != -1)
                theModel.setErrorMsg(buff);
            break;
    }
}


int LabelVideoController::getKeyBindings(int i)
{
    auto temp = keyBinding.find(i);
    if(temp != keyBinding.end())
        return temp->second;
    return -1;
}

std::string LabelVideoController::getKeyBindingHelp()
{
    std::string result;
    auto kbind = getKeyBinding();
    for(auto binding= kbind.begin(); binding != kbind.end(); binding++)
    {
        char str[100];
        if(binding->second != -1)
        {
            sprintf(str, "[%c-%s]", binding->first,  theModel.getLabel( binding->second ).c_str());
            result += str;
        }
    }
    return result;
}
