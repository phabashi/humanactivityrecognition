#include <iostream>
#include <string>
#include <opencv2/opencv.hpp>
#include <inttypes.h>
#include "../util/util.h"
#include "../util/CommaSeparatedReader.h"
#include "../Exceptions/CommaSeparatedReadingException.h"
#include "../Clustering/OnlineKMeans.h"
#include "../Exceptions/ArgumentParseException.h"
#include "../Logger/Logger.h"
#include "../Pejman/GlobalSetting.h"

using namespace std;
using namespace cv;
using namespace HAR;
using namespace HAR::Exceptions;
using namespace Pejman::Logger;
using namespace Pejman::Application;

class Arguments {
public:
	Arguments(int argc, char* argv[]);

	int getNumberOfClusters() {
		return numberOfClusters;
	}
	string getModelFileName() {
		return ouputModelFileName;
	}
	bool isShuffleSet() {
		return shuffleIsSet;
	}
	bool isVerboseSet() {
		return verbose;
	}
	bool isOnlineAlgorithmSet() {
		return onlineAlgorithm;
	}
	char getDelimiterCharacter() {
		return delimiter[0];
	}

	const string getSettingFileName() {
		return settingFileName;
	}

private:
	int numberOfClusters = 0;
	string ouputModelFileName;
	bool shuffleIsSet;
	bool verbose;
	bool onlineAlgorithm;
	string delimiter;
	string settingFileName;
};

int main(int argc, char* argv[]) {
	Arguments args(argc, argv);

	if (!GlobalSetting::LoadConfigurationFromIniFile(
			args.getSettingFileName())) {
		Logger::Log(LogLevel::FATAL, "Cannot Load setting from file : '%s' .... Exiting...",
				args.getSettingFileName().c_str());
		exit(1);
	}

	const int clusterCount = args.getNumberOfClusters();
	const string ouputModelFileName = args.getModelFileName();

	Mat centers;
	FileStorage fs(ouputModelFileName, FileStorage::WRITE);

	if (args.isOnlineAlgorithmSet()) {
		std::vector<float> observation;
		bool readIsNotSuccessful = true;
		while (readIsNotSuccessful)
			try {
				observation = CommaSeparatedReader::ReadLineFloat(cin);
				readIsNotSuccessful = false;
			} catch (ConversionException& exp) {
				//cerr << "can not read the line! skipping the line" << endl;
				readIsNotSuccessful = true;
			}

		try {
			OnlineKMeans onlinekmeans(args.getNumberOfClusters(), observation);
//		int i=0;
			while (cin) {
				//cout << "reading " << ++i << endl;
				try {
					observation = CommaSeparatedReader::ReadLineFloat(cin);
				} catch (ConversionException& exp) {
					//cerr << "can not read the line! skipping the line" << endl;
					continue;
				}
				if (!cin)
					break; //if we reach the end of input file
				onlinekmeans.Update(observation);
			}
			cout << "Writing the centers!" << endl;

			auto t = onlinekmeans.getDistribution();

			for (uint i = 0; i < t.size(); i++)
				cout << "count[ " << i << " ] = " << t[i] << endl;

			centers = onlinekmeans.getCentersInMatrixFormat();
			cout << "Centers: Size: " << centers.size() << endl;
		} catch (HAR::CommaSeparatedReadingException& e) {
			cerr << "Reading Exception: " << e.what() << endl;
		}

	} else {
		HAR::CommaSeparatedReader::setDelimiter(args.getDelimiterCharacter());
		Mat inputData = HAR::CommaSeparatedReader::ReadFloatMatrix();
		//cout << inputData << endl;
		//cin.get();
		if (args.isShuffleSet()) {
			cout << "Shuffling... " << endl;
			srand(time(NULL));
			for (int i = inputData.rows - 1; i >= 1; i--) {
				int r = rand() % i;
				Mat temp = inputData.row(r).clone();
				inputData.row(i).copyTo(inputData.row(r));
				temp.copyTo(inputData.row(i));
			}
			cout << "Done." << endl;
			//cout << "Shuffle Input Data = " << inputData << endl;
		}

		Mat labels;
		int attempts = GlobalSetting::GetOptionAs<int>("KMEANS", "attempts", 8);
		int iterations = GlobalSetting::GetOptionAs<int>("KMEANS", "iterations",
				10000);
		float epsilon = GlobalSetting::GetOptionAs<float>("KMEANS", "epsilon",
				0.00001);
		float densFactor = kmeans(inputData, clusterCount, labels,
				TermCriteria(CV_TERMCRIT_ITER | CV_TERMCRIT_EPS, iterations,
						epsilon), attempts, KMEANS_PP_CENTERS, centers);
		cout << "Dense factor is: " << densFactor << endl;
		//cout << labels.t() << endl;
		cout << "Centers: Size: " << centers.size() << endl;
	}

	fs << "Centers" << centers;
	//cout << centers << endl;
}

Arguments::Arguments(int argc, char* argv[]) :
		numberOfClusters(2), ouputModelFileName("TheModel.yml"), shuffleIsSet(
				false), onlineAlgorithm(false) {
	delimiter = ",";
	for (int i = 1; i < argc; i++) {
		if (strcmp(argv[i], "-c") == 0) {
			if (i < argc - 1)
				numberOfClusters = strtol(argv[++i], NULL, 10);
			else {
				cout << "-c options needs a value" << endl;
				exit(1);
			}

			if (numberOfClusters == 0) {
				cout << "Ill formatted input. The number of clusters is zero!"
						<< endl;
				exit(1);
			}
		} else if (strcmp(argv[i], "-o") == 0) {
			if (i < argc - 1) {
				ouputModelFileName = argv[++i];
			} else {
				cout << "-o options needs a value" << endl;
				exit(1);
			}
		} else if (strcmp(argv[i], "-s") == 0) {
			shuffleIsSet = true;
		} else if (strcmp(argv[i], "-online") == 0) {
			onlineAlgorithm = true;
		} else if (strcmp(argv[i], "-d") == 0) {
			if (i + 1 < argc) {
				delimiter = string(argv[++i]);
				if (delimiter.length() != 1) {
					Logger::Log(LogLevel::FATAL,
							"The provided delimiter is not a character!");
				}
			} else
				throw ArgumentParseException("-d option needs a delimiter!");
		} else if (strcmp(argv[i], "as") == 0) {
			if (i + 1 < argc) {
				settingFileName = string(argv[++i]);
			} else
				throw ArgumentParseException(
						"as option needs a setting file name!");
		} else if (strcmp(argv[i], "-h") == 0) {
			cout << "Usage: " << argv[0]
					<< "[options]                              " << endl
					<< "Valid Options:                                  "
					<< endl
					<< "-c <n>           Set the numbr of clusters to n "
					<< endl
					<< "-o <outFileName> Set ouput file name            "
					<< endl
					<< "-h               Print out the help             "
					<< endl
					<< "-s               Shuffle                        "
					<< endl
					<< "-d <delimiter>   Change delimiter (default ',') "
					<< endl << "-online			 Use Online version of KMeans   "
					<< endl;
			exit(1);
		} else if (strcmp(argv[i], "-v") == 0) {
			verbose = true;
		} else {
			cout << "Unknown option '" << argv[i] << "'" << endl
					<< " Use -h to get help. " << endl;
			exit(1);
		}
	}
}
