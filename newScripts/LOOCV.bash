#!/bin/bash

#setting
inputFileList="./lists/FeatureFileList.txt"
opticalFlowAlgorithm="IP"
interestPointAlgorithm="FAST"
descriptorAlgorithm="SIFT"
length=9
#matchingAlgorithm="BruteForce"
ratio="1"
scale="1" #scale the original video to half size in each direction
#removeHomogenousAreas="0"
numberOfWords=400
ActivityList="./lists/ActivitySet_15.txt"
featureFolderName="./test-data/$opticalFlowAlgorithm/$interestPointAlgorithm/$descriptorAlgorithm/$matchingAlgorithm/scale_$scale/rha_$removeHomogenousAreas/Length_$length/"
SVMLearningParameters="-s 0 -t 2 -d 3 -c 500 -q"

MicromovementFeaturesFolder=$featureFolderName/features/

leftVideoFileNames=(`cat $inputFileList`)
Actors=`cat $inputFileList | cut -f 1 -d "_" | uniq `

folderPrefix="./$featureFolderName/learning/n-$numberOfWords-words" # find the file name part of $Activities

csvDataFolder="./$folderPrefix/data/csv"
SVMDataFolder="./$folderPrefix/data/svm"
SVMModelFolder="./$folderPrefix/models/svm"
KMeansModelFolder="./$folderPrefix/models/kmeans"
resultFolder="./$folderPrefix/results/"

mkdir -p $csvDataFolder
mkdir -p $SVMDataFolder
mkdir -p $SVMModelFolder
mkdir -p $KMeansModelFolder
mkdir -p $resultFolder

i=0

for Actor in ${Actors[@]}; do
	i=$(( i+1 ))
	echo "Do fold $i for actor $Actor"
	trainingSet=`cat $inputFileList | grep -v $Actor`
	testingSet=`cat $inputFileList | grep $Actor`

	modelFileName="$KMeansModelFolder/Fold-$Actor.yml"
	csvTrainFile="$csvDataFolder/Fold-$Actor.csv.train"
	csvTestFile="$csvDataFolder/Fold-$Actor.csv.test"
	svmTrainFile="$SVMDataFolder/Fold-$Actor.svm.train"
	svmTestFile="$SVMDataFolder/Fold-$Actor.svm.test"
	svmModelFileName="$SVMModelFolder/Fold-$Actor.svm.model"
	svmOutputFile="$SVMDataFolder/Fold-$Actor.svm.output"

	#make a KMeans model for this fold
	echo "Make KMeans Model for fold $Actor"
	echo $trainingSet | tr ' ' '\n'  | ./bin/Merge -f $MicromovementFeaturesFolder | tr " " "," | ./bin/KMeansClustering -c $numberOfWords -o $modelFileName #-online

	#Count The Words for this fold
	echo "Count the words for trainig in fold $i"
	echo $trainingSet | tr ' ' '\n' | ./bin/CountTheWords -m $modelFileName -f $MicromovementFeaturesFolder -p 3 -c $csvTrainFile -lab $ActivityList -d " " 

	echo "Count the words for testing in fold $i"
	echo $testingSet  | tr ' ' '\n' | ./bin/CountTheWords -m $modelFileName -f $MicromovementFeaturesFolder -p 3 -c $csvTestFile -lab $ActivityList -d " "

	#Prepare data for libSVM
	echo "Preparing data for learning/testing with libSVM"
	./bin/csv2svm -i $csvTrainFile $csvTestFile -o $svmTrainFile $svmTestFile -n -lab $ActivityList

	#Make a svm-model for the current data
	echo "Making SVM Model for the fold $i"
	./svmbin/svm-train $SVMLearningParameters $svmTrainFile $svmModelFileName

	#Measure the accuracy of svm-model for calssification
	echo "Measuring the accuracy for fold $i"
	./svmbin/svm-predict -q $svmTestFile $svmModelFileName $svmOutputFile

done

echo "Making Report: "

expectedResult="$SVMDataFolder/all.expected"
outputResult="$SVMDataFolder/all.actual"

rm -f $expectedResult
rm -f $outputResult
j=0

makeReportForFold()
{
	i=$1
  j=$(( j + 1 ))

	expected="$resultFolder/Fold-$i.expected"
	aquired="$resultFolder/Fold-$i-out"
	svmTestFile="$SVMDataFolder/Fold-$i.svm.test"
	svmOutputFile="$SVMDataFolder/Fold-$i.svm.output"

	#echo "Making report for fold $i :"
	cat $svmTestFile | cut -d " " -f 1 > $expected
	cp $svmOutputFile $aquired
	#./bin/MakeReport -e $expected -o $aquired -lab $ActivityList
	cat $expected >> $expectedResult
	cat $aquired >> $outputResult
}

for i in ${Actors[@]}; do makeReportForFold "$i" ; done

./bin/MakeReport -e $expectedResult -o $outputResult -lab $ActivityList
