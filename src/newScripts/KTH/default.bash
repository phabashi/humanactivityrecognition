#~/bin/bash

export opticalFlowAlgorithm=${OFA:-"FB"}
export interestPointAlgorithm="FAST"
export descriptorAlgorithm="SIFT"
export normalizeOutput=${Norm:-"Yes"}
export detectMotion=${DM:-"No"}
export ratio="1"
export scale=${scale:-"3"} #scale the original video to half size in each direction
export length=${length:-13}
export numberOfWords=${NOW:-16}
export minEnergy=${minEnergy:-"1"}


if [[ $detectMotion == "No" ]]; then
	motionOption="-dm"
	motionFolderName="Disabled"
else
	motionOption="-em"
	motionFolderName="Enabled"
fi
#normalized="-nd"
#foldn="Not"
#if [[ $normalizeOutput == "Yes" ]]; then
#	normalized="-ne"
#	foldn=""
#fi

export encoding="NormDiff"

export motionOption
export motionFolderName
export normalized
export foldn

export featureFolderName="./test-data/KTH/${motionFolderName}Motion/$opticalFlowAlgorithm/$interestPointAlgorithm/$descriptorAlgorithm/$matchingAlgorithm/scale_$scale/$encoding/Length_$length"

export settingFile="./SampleSetting.ini"

# Sample making file List:

#export MULTISCALE="-multiscale"
