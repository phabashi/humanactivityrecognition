/*
 * ArgumentParseException.h
 *
 *  Created on: Mar 6, 2016
 *      Author: Pejman
 *      Copyright (c) 2016 Pejman. All rights reserved.
 */

#ifndef SRC_UTIL_ARGUMENTPARSEEXCEPTION_H_
#define SRC_UTIL_ARGUMENTPARSEEXCEPTION_H_
#include <exception>
#include <string>

namespace HAR {
namespace Exceptions {

class ArgumentParseException : public std::exception
{
public:
	ArgumentParseException(std::string unknownArgument) { ua = unknownArgument; }
	  virtual const char* what() const throw()
	  {
	    return ("Argument Parse Exception:  " + ua ).c_str();
	  }

private:
	  std::string ua;
};

} /* namespace Exceptions */
} /* namespace HAR */

#endif /* SRC_UTIL_ARGUMENTPARSEEXCEPTION_H_ */
