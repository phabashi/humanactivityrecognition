/*
 * Trajectory3D.cpp
 *
 *  Created on: Feb 13, 2017
 *      Author: pejman
 */

#include "Trajectory3D.h"
#include <unordered_map>
#include "../Logger/Logger.h"

using namespace Pejman::Logger;

namespace HAR {
namespace Video {

Trajectory3D::Trajectory3D() {

}

Trajectory3D::Trajectory3D(Trajectory left, Trajectory right) {
	if (leftTrajectory.getStartFrame() != rightTrajectory.getStartFrame() ){
		Logger::Log(LogLevel::FATAL, "The starting frame for left and right trajectory should be the same");
	}
	leftTrajectory = std::move(left);
	rightTrajectory = std::move(right);

}

Trajectory3D::~Trajectory3D() {
}

std::size_t Trajectory3D::getHashCode() const {
	return ( leftTrajectory.getHashCode() )
			^ ( rightTrajectory.getHashCode() );
}

void Trajectory3D::Combine(
		const std::vector<bool>& isValid,
		const std::vector<cv::Mat>& h1,
		const std::vector<cv::Mat>& h2) {
	//std::vector<cv::Point3f> result;
	Logger::Log(LogLevel::INFO, "Trajectory3D::Combine()");
	trajectory3D.clear();
	int frameNumber = getStartFrame();
	cv::Mat H1;
	cv::Mat H2;

	//if(! isValid[frameNumber]) {
	//	Logger::Log(LogLevel::INFO, "The given rectification is not valid, ignoring this instance...");
	//	return;
	//}
	//H1 = h1[frameNumber];
	//H2 = h2[frameNumber];
	for (auto it = leftTrajectory.getAllPoints().begin(),
			it1 =  rightTrajectory.getAllPoints().begin();

			it != leftTrajectory.getAllPoints().end()
			&& it1 != rightTrajectory.getAllPoints().end();

			++it, ++it1, frameNumber++) {
		Logger::Log(LogLevel::VERBOSE, "Combining frame number %d", frameNumber);
		std::vector<cv::Point2f> leftPoint;
		std::vector<cv::Point2f> rightPoint;
		leftPoint.push_back( *it );
		rightPoint.push_back( *it1 );

//		std::vector<cv::Point2f> rectifiedLeftPoint;
//		std::vector<cv::Point2f> rectifiedRightPoint;
//		if(isValid[frameNumber]){
//			H1 = h1[frameNumber];
//			H2 = h2[frameNumber];
//		}
//		Logger::Log(LogLevel::INFO, "Try Rectification ...");
//		perspectiveTransform(leftPoint, rectifiedLeftPoint, H1);
//		perspectiveTransform(rightPoint, rectifiedRightPoint, H2);

		//std::cout << leftPoint[0] << " ==> " << rightPoint[0] << std::endl;
		//std::cout << rectifiedLeftPoint[0] << " ==> " << rectifiedRightPoint[0] << std::endl;

		//std:: cout << "H1 = " << H1 << "\n, H2= " << H2 << std::endl;
		//lp = H1 * lp;
		//rp = H2 * rp;

		Logger::Log(LogLevel::VERBOSE, "Accessing something dangerous...", frameNumber);
		//rectified left point x and y
		double lx = leftPoint[0].x;// / lp.at<double>(2,0);
		double ly = leftPoint[0].y;// / lp.at<double>(2,0);

		//rectified right point x and y
		double rx = rightPoint[0].x;// / rp.at<double>(2,0);
		double ry = rightPoint[0].y;// / rp.at<double>(2,0);

		double dx = lx - rx;
		double dy = ly - ry;
		double disparity;
		Logger::Log(LogLevel::VERBOSE, "I'm alive ...", frameNumber);
		if(std::abs(dy) > 0.010) {// 10 pixel in 1000 pixel
			//TODO after rectification dy should be ideally zero
			// dy larger than 10 might means it is not a good stereo match!
			//std::cout << "DY = " << dy << ", ";
			Logger::Log(LogLevel::WARNING,
					"Bad stereo match in frame %d!, ignoring it,"
							"dx = %lf, dy = %lf, starting fram = %d"
							"L(%f, %f) => R(%f, %f), Unrectified: L(%f, %f)=>(%f, %f)",
					frameNumber, dx, dy, getStartFrame(), lx, ly, rx, ry, it->x, it->y, it1->x, it1->y);
			trajectory3D.clear();
			return;// do not continue the process.
		} else {
			Logger::Log(LogLevel::VERBOSE, "GOOD stereo match!, dy = %lf after rectification.", dy);

		}
		//disparity = sqrtf(pow((rlx - rrx), 2) + pow((rly - rry), 2));
		disparity = dx;//ignore dy as it should be ideally zero
		//cout << "( rlx, rly, rrx, rry) = " << rlx << ", " << rly << ", " << rrx << ", " << rry << endl;
		cv:: Point3f point3D;
		point3D.x = it->x ;//(lx) / disparity;
		point3D.y = it->y ;//(ly) / disparity;
		point3D.z = disparity;
		trajectory3D.push_back(point3D);
	}
	//return trajectory3D;
}

} /* namespace Video */
} /* namespace HAR */

