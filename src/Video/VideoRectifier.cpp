/*
 * VideoRectifier.cpp
 *
 *  Created on: Mar 4, 2017
 *      Author: pejman
 */

#include "VideoRectifier.h"
#include "../Feature/FeatureDescriptorExtractor.h"
#include "../Feature/FeatureDetector.h"
#include "../Feature/FeatureDescriptorMatcher.h"

#include "../Logger/Logger.h"
#include <opencv2/opencv.hpp>

using Pejman::Logger::Logger;
using Pejman::Logger::LogLevel;

VideoRectifier::VideoRectifier(HAR::Video::AugmentedVideoBuffer& left,
		HAR::Video::AugmentedVideoBuffer& right) :
		left(left), right(right) {

}

void VideoRectifier::ExtractFeaturePoints() {
	using HAR::Video::Features::FeatureDetector;
	using HAR::Video::Features::FeatureDescriptorExtractor;
	FeatureDetector& detector = FeatureDetector::getInstance("ORB");

	Logger::Log(LogLevel::VERBOSE, "Detecting feature points.");
	detector.ExtractFeatures(right, false);
	detector.ExtractFeatures(left, false);

	FeatureDescriptorExtractor& extractor =
			HAR::Video::Features::FeatureDescriptorExtractor::getInstance("ORB");

	Logger::Log(LogLevel::VERBOSE,
			"Extracting descriptors for detected feature points Left...");
	extractor.ExtractDescriptors(left);
	extractor.ExtractDescriptors(right);
}

void VideoRectifier::MatchFeaturePoints(int frameNumber) {
	while( leftPoints.size() <= frameNumber){
		leftPoints.push_back (std::vector<cv::Point2f>());
		rightPoints.push_back(std::vector<cv::Point2f>());
	}

	Logger::Log(LogLevel::VERBOSE, "Hello");
	using HAR::Video::Features::FeatureDescriptorMatcher;
	FeatureDescriptorMatcher& descriptorMatcher =
			FeatureDescriptorMatcher::getInstance("BruteForce");

	auto leftFI = left.getFrameInformation(frameNumber);
	auto rightFI = right.getFrameInformation(frameNumber);

	auto frameMatches = descriptorMatcher.MatchDescriptors(
			leftFI.getDescriptors(), rightFI.getDescriptors());

	Logger::Log(LogLevel::VERBOSE, "can you here me?");
	//check for first and second best matches
	std::vector<cv::DMatch> frameGoodMatches;
	double ratio = 0.8;
	for (auto it : frameMatches) {
		// as generated matches are sorted in ascending order of their distance, first 2 match will be first_min_dist and second_min_distance respectively
		if (it[0].distance < ratio * it[1].distance) {
			frameGoodMatches.push_back(it[0]);
		}
	}

	Logger::Log(LogLevel::VERBOSE, "Hello");
	//Check the symetry
	auto reverseFrameMatches = descriptorMatcher.MatchDescriptors(
			rightFI.getDescriptors(), leftFI.getDescriptors());

	std::vector<cv::DMatch> reverseGoodMatches;
//	double ratio = 0.5;
	for (auto it : reverseFrameMatches) {
		// as generated matches are sorted in ascending order of their distance, first 2 match will be first_min_dist and second_min_distance respectively
		if (it[0].distance < ratio * it[1].distance) {
			reverseGoodMatches.push_back(it[0]);
		}
	}
	Logger::Log(LogLevel::VERBOSE, "I'm the boy");
	std::vector<cv::DMatch> symetricMatches;
	for (auto match : frameGoodMatches) {
		for (auto rmatch : reverseGoodMatches) {
			if (match.trainIdx == rmatch.queryIdx
					&& match.queryIdx == rmatch.trainIdx) {
				symetricMatches.push_back(match);
				break;
			}
		}
	}

	//leftPoints.clear();
	//rightPoints.clear();
	Logger::Log(LogLevel::VERBOSE, "Calling from callifornia");
	for (auto match : symetricMatches) {
		auto leftInterestPoint = leftFI.getKeypoints()[match.trainIdx].pt;
		auto rightInterestPoint = rightFI.getKeypoints()[match.queryIdx].pt;

		leftPoints[frameNumber].push_back(leftInterestPoint);
		rightPoints[frameNumber].push_back(rightInterestPoint);

		allPointsLeft.push_back(leftInterestPoint);
		allPointsRight.push_back(rightInterestPoint);
	}

	cv::Mat leftI = left.getFrame(frameNumber);
	cv::Mat rightI = right.getFrame(frameNumber);
	cv::Mat result;

//	Logger::Log(LogLevel::VERBOSE, "NO, it's windsor");
//	cv::drawMatches(leftI, leftFI.getKeypoints(), rightI,
//			rightFI.getKeypoints(), symetricMatches, result);
//
//	Logger::Log(LogLevel::VERBOSE,
//			"%d matches survived from first test, while %d survived from symetry test.",
//			frameGoodMatches.size(), symetricMatches.size());
//	cv::imshow("Dahanet!", result);
//	cv::waitKey();
//	Logger::Log(LogLevel::VERBOSE, "Godbye Erro prone functgion :D");
}

void VideoRectifier::MatchFeaturePoints() {
	int i=0;
	for(auto lframe = left.getFrame(0), rframe = right.getFrame(0);
			!lframe.empty() && !rframe.empty();
			lframe = left.getNextFrame(), rframe = right.getNextFrame()){
		this->MatchFeaturePoints(i++);
	}
}

VideoRectifier::~VideoRectifier() {
}

