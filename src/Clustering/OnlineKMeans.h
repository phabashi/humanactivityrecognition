/*
 * OnlineKMeans.h
 *
 *  Created on: Jan 17, 2016
 *      Author: habashi
 */

#ifndef SRC_ONLINEKMEANS_H_
#define SRC_ONLINEKMEANS_H_

#include <vector>
#include <opencv2/core/core.hpp>


namespace HAR {

typedef enum {Eucledean, } DistanceMeasure;

class OnlineKMeans {
public:
	OnlineKMeans(int numberOfClasses, std::vector<float> sample);
	int Update(std::vector<float> anObservation); //get the nearest cluster and update the center as well
	int getNearestCenter(std::vector<float> anObservation);
	cv::Mat getCentersInMatrixFormat();
	const std::vector<std::vector<float> > getCenters() const { return centers; }
	std::vector<int> getDistribution();

	virtual ~OnlineKMeans();

private:
	//float euclideanDistance(const std::vector<float>& a, const std::vector<float>& b) const;


private:
	std::vector<std::vector<float> > centers;
	std::vector<int> counts;

	int numberOfClusters;
	unsigned int dimension;
};

}
#endif /* SRC_ONLINEKMEANS_H_ */
