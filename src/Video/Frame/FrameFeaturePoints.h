/*
 * FrameFeaturePoints.h
 *
 *  Created on: Dec 30, 2014
 *      Author: Pejman
 *      Copyright (c) 2014 Pejman. All rights reserved.
 */

#ifndef FRAMEFEATUREPOINTS_H_
#define FRAMEFEATUREPOINTS_H_

#include <opencv2/opencv.hpp>
//#include <opencv2/core/core.hpp>

#include <vector>

namespace HAR {

class FrameFeaturePoints {
public:
	FrameFeaturePoints();
	FrameFeaturePoints(std::vector<cv::KeyPoint> temp);
	FrameFeaturePoints(std::vector<cv::Point_<float> > temp);
	const std::vector<cv::KeyPoint>& getKeyPoints() const{ return keyPoints; }
	const cv::KeyPoint getKeyPoint(unsigned int i) const { return (i < keyPoints.size())? keyPoints[i] : cv::KeyPoint(); }
    void UpdateKeypointLocation(int index, cv::Point2f newLocation) { keyPoints[index].pt = newLocation; }
	
    void DrawFeaturePointsOnImage(const cv::Mat& input, cv::Mat& output) const;

	std::vector<cv::Point2f> CalculateAndGetLocations() const;
    
	bool Write(cv::FileStorage& fs) const;
	bool Read(const cv::FileNode& currentNode);

	void RemoveHomogenousAreaIP(cv::Mat image);

	cv::Vec2f CalculateMean() const;
	cv::Vec2f CalculateVar(cv::Vec2f mean) const;


	virtual ~FrameFeaturePoints();

private:
	unsigned long int frameNumber;
	std::vector<cv::KeyPoint> keyPoints;
	//std::vector<cv::Mat> points; We do not need this
};

} /* namespace HAR */
//TODO Decide if we need the following definitions.
//namespace cv
//{
//
//static void write(cv::FileStorage& fs, const std::string& name,const HAR::FrameFeaturePoints& frameFeturePoints)
//{
//	fs << name;
//	frameFeturePoints.Write(fs);
//}
//
//static void read(cv::FileNode& node, HAR::FrameFeaturePoints& frameFeaturePoints, const HAR::FrameFeaturePoints defaultValue = HAR::FrameFeaturePoints())
//{
//	if(node.empty())
//		frameFeaturePoints = defaultValue;
//	else
//		frameFeaturePoints.Read(node);
//}
//
//static cv::FileStorage& operator<<(cv::FileStorage& fs, const HAR::FrameFeaturePoints& frameFeturePoints)
//{
//	frameFeturePoints.Write(fs);
//	return fs;
//}
//
//}
#endif /* FRAMEFEATUREPOINTS_H_ */
