/*
 * MainExtractFeatures.cpp
 *
 *  Created on: May 1, 2015
 *      Author: Pejman
 *      Copyright (c) 2015 Pejman. All rights reserved.
 */

#include <stdlib.h>
#include <unordered_set>
#include <ostream>
#include <fstream>

#include "../../Video/AugmentedVideo.h"
///#include "../../Video/DisparityFlowDescriptorExtractor.h"
//#include "../../util/util.h"
//#include "../../Video/VideoBuffer.h"
#include "../../Video/Display.h"
#include "../../Video/motion/MotionDetection.h"
//#include "../../Video/VideoBuffer.h"
#include "../../Video/Display.h"
#include "../../Feature/FeatureDetector.h"
#include "../../Feature/FeatureDescriptorExtractor.h"
#include "../../OpticalFlow/IOpticalFlow.h"
#include "../../OpticalFlow/OpticalFlow.h"
#include "../../Logger/Logger.h"
#include "../../Pejman/GlobalSetting.h"
#include "Arguments.h"
#include "../../Video/TrajectoryEncoding/IEncoding.h"

using namespace cv;
using namespace std;
using namespace HAR;
using namespace Pejman::Logger;
using namespace Pejman::Application;
using namespace HAR::NewExtractFeatures;
using namespace HAR::Exceptions;
using namespace HAR::Video;
using namespace HAR::Video::Motion;
using namespace HAR::Video::OpticalFlow;

void ExtractFeatures(AugmentedVideoBuffer& leftVideo, const Arguments& args);

int main(int argc, char** argv) {
	Logger::getInstance().ChangeLevel(LogLevel::ERROR);
	Arguments args;
	try {
		args = Arguments(argc, argv);
	} catch (ArgumentParseException& exp) {
		Logger::Log(LogLevel::FATAL, "Error in parsing of the arguments!");
		args.printHelp();
		exit(1);
	}

	if (!GlobalSetting::LoadConfigurationFromIniFile(args.getApplicationSettingFileName())) {
		Logger::Log(LogLevel::FATAL, "Can not load the configuration file '%s'.", args.getApplicationSettingFileName().c_str());
		exit(1);
	} else {
		Logger::Log(LogLevel::INFO, "Setting File '%s' loaded successfully.", args.getApplicationSettingFileName().c_str());
	}

	int bufferSize = GlobalSetting::GetOptionAs<int>("Video", "BufferSize", -1); //-1 load whole video into the buffer
	bool isGrayscale = GlobalSetting::GetOptionAs<int>("Video", "isGrayscale", (args.getTrackingAlgorithm() == "FB"));


	if(args.IsMultiscaleEnabled()) {
		int n = args.getScale();
		if (n <= 1) {
			Logger::Log(LogLevel::FATAL, "You are running algorithm for multiscale and you should provide the number of scales with -s parameter.");
			exit(1);
		}
//		if (args.getTrajectoryFileName() != "") {
//			Logger::Log(FATAL, "Not Implemented Feature! We are in multiscale implementation and we can not save this!.");
//			exit(1);
//			Logger::Log(VERBOSE, "Saving the trajectories into the file.");
//			ofstream outfile(args.getTrajectoryFileName());
//			outfile << leftVideo.SerializeTrajectories();
//			outfile.close();
//		}

		std::unique_ptr<ofstream> outfile;
		if (args.getOutputFeatureFile() != "") {
			Logger::Log(VERBOSE, "Saving the features into the file.");

			outfile = std::unique_ptr<ofstream>(new ofstream(args.getOutputFeatureFile()) );
			//outfile << leftVideo.SerializeTrajectoriesDeferentiation( args.getNormalizeOutput() );
		}

		float scale= args.getInitialScale();
		for(int i=0; i<n; i++) {
			AugmentedVideoBuffer leftVideo(args.getInputFileLeft(), bufferSize, isGrayscale);
			if (!leftVideo.isValid()) {
				Logger::Log(FATAL, "Can not open the requested Video file : '%s'", args.getInputFileLeft().c_str());
				exit(1);
			}
			//	AugmentedVideoBuffer rightVideo(args.getInputFileRight(), bufferSize, isGrayscale);
			leftVideo.enableResize(scale);

			TrajectoryEncoding::TEncoding encodingAlgorithm =
					TrajectoryEncoding::IEncoding::getEncoding(
							args.getTrajectoryEncoding());

			ExtractFeatures(leftVideo, args);
			if (args.getOutputFeatureFile() == "") {
				Logger::Log(VERBOSE, "Printing out the features.");
				cout << leftVideo.SerializeTrajectories( encodingAlgorithm );
			} else {
				Logger::Log(VERBOSE, "Saving the features into the file.");
				(*outfile) << leftVideo.SerializeTrajectories( encodingAlgorithm );
			}
			scale *= 1/sqrt(2);
		}

		if (args.getOutputFeatureFile() != "") {
			outfile->close();
		}


	} else { //Run program in single scale mode!

		AugmentedVideoBuffer leftVideo(args.getInputFileLeft(), bufferSize, isGrayscale);
		if (!leftVideo.isValid()) {
			Logger::Log(FATAL, "Can not open the requested Video file : '%s'", args.getInputFileLeft().c_str());
			exit(1);
		}
		//	AugmentedVideoBuffer rightVideo(args.getInputFileRight(), bufferSize, isGrayscale);
		leftVideo.enableResize(args.getScale());
		//	rightVideo.enableResize(temp);

		ExtractFeatures(leftVideo, args);
		//------------------------------ Saving Trajectories  to File  ----------------------------

//		if (args.getTrajectoryFileName() != "") {
//			Logger::Log(VERBOSE, "Saving the trajectories into the file.");
//			ofstream outfile(args.getTrajectoryFileName());
//			outfile << leftVideo.SerializeTrajectories();
//			outfile.close();
//		}

		TrajectoryEncoding::TEncoding encodingAlgorithm =
				TrajectoryEncoding::IEncoding::getEncoding(
						args.getTrajectoryEncoding());

		if (args.getOutputFeatureFile() == "") {
			Logger::Log(VERBOSE, "Printing out the features.");
			cout << leftVideo.SerializeTrajectories( encodingAlgorithm );
		} else {
			Logger::Log(VERBOSE, "Saving the features into the file.");
			ofstream outfile(args.getOutputFeatureFile());
			outfile << leftVideo.SerializeTrajectories( encodingAlgorithm );
			outfile.close();
		}
	}
	return 0;
}

void ExtractFeatures(AugmentedVideoBuffer& leftVideo, const Arguments& args){

	if (args.getDetectMotion() == true) {
		Logger::Log(VERBOSE, "Detecting motion of left video object.");
		MotionDetection md;
		md.DetectMotion(leftVideo);
	}
//	md.DetectMotion(rightVideo);

	Logger::Log(VERBOSE, "Creating feature Detector object.");
	HAR::Video::Features::FeatureDetector& detector =
			HAR::Video::Features::FeatureDetector::getInstance(
					args.getInterestPointExtractor());

	if (args.getTrackingAlgorithm() == "IP") {
		Logger::Log(VERBOSE, "Extracting features from left video.");
		detector.ExtractFeatures(leftVideo, args.getDetectMotion());
	//	detector.ExtractFeaturesFromVideo(rightVideo);
		HAR::Video::Features::FeatureDescriptorExtractor& extractor =
				HAR::Video::Features::FeatureDescriptorExtractor::getInstance(
						args.getInterestPointDescriptor());
		extractor.ExtractDescriptors(leftVideo);
	} else {
		Logger::Log(VERBOSE, "Extracting features from left video.");
		detector.ExtractFeatures(leftVideo, args.getDetectMotion(), args.getSamplingScale());
	//	detector.ExtractFeaturesFromVideo(rightVideo);
	}

	auto opticalFlow = IOpticalFlow::getOpticalFlowAlgorithm(args.getTrackingAlgorithm());
	opticalFlow->ExtractOpticalFlow(leftVideo, args.getDescriptorLength());

	//---------------------------- Removing Low Energy Trajectories --------------------------
	if(args.getMinEnergy() > 0 ) {
		int removed = leftVideo.RemoveLowEnergyTrajectories(args.getMinEnergy());
		Logger::Log(INFO, "Hello! Can you hear me ? %d instances removed!", removed);
	}

	//leftVideo.ClearTrajectories();
	if (args.IsDisplayEnabled()) {
		Logger::Log(INFO, "Displaying is enabled, showing the output to the user.");
		//leftVideo.disableResize();
//		leftVideo.setGrayscale(false);
		Display::Show((DisplayableVideo&) leftVideo);
		//	Display::Show(rightVideo);
	}
}

