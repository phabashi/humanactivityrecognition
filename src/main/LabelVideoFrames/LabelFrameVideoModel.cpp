//
//  LabelFrameVideoModel.cpp
//  HARLinked
//
//  Created by pejman on 2015-07-08.
//  Copyright (c) 2015 pejman. All rights reserved.
//

#include "LabelFrameVideoModel.h"

#include <fstream>
//#include "util.h"

LabelVideoModel::LabelVideoModel(std::string inputFileName,
                                  std::string labelDefinitionFileName,
                                  std::string outputFileName,
                                  double scale)
{
    this->inFileName = inputFileName;
    this->scale = scale;
    this->playingFinished = false;
    this->playingPaused = false;
    this->currentFrameNumber = 0;
    this->theVideo = HAR::Video::Video(inputFileName);
    theVideo.setGrayscale(false);
    this->labelDefinitionFileName = labelDefinitionFileName;
    this->outputFileName = outputFileName;
    
    LoadLabelDefinitionsFromFile();
    LoadLabelsFromFile();

}

cv::Mat LabelVideoModel::getNextFrame()
{
    currentFrameNumber++;
    currentFrame = theVideo.getNextFrame();
    return currentFrame;
}

bool LabelVideoModel::setScale(double newScale)
{
    if(newScale > 0.1 && newScale < 2.0)
    {
        scale = newScale;
        return true;
    }
    return false;
}

bool LabelVideoModel::setFrameNumber(int newFrameNumber)
{
    if(newFrameNumber  < 0) newFrameNumber = 0;

    currentFrameNumber = newFrameNumber;
    currentFrame = theVideo.getFrame(currentFrameNumber);
    return (! currentFrame.empty());
}

bool LabelVideoModel::seekVideo(int numberOfFrames)
{
    return setFrameNumber(currentFrameNumber + numberOfFrames);
}

bool LabelVideoModel::LoadLabelDefinitionsFromFile()
{
    std::ifstream infile(labelDefinitionFileName);
    if(!infile) return false;
    char theLine[256];
    
    labelDefintionList.clear();
    
    while (infile) {
        
        infile.getline(theLine, 256);
        std::string line(theLine);
        line = line.substr(0,line.find(' '));
        //line = trim(line);
        if(line != "")
            labelDefintionList.push_back(line);
    }
    
    return true;
}

bool LabelVideoModel::LoadLabelsFromFile()
{
    std::ifstream infile(outputFileName);
    if(!infile) return false;
    char theLine[256];
    
    currentLabelList.clear();
    
    while (infile) {
        
        infile.getline(theLine, 256);
        std::string line(theLine);
        if(line!= "")
            currentLabelList.push_back(line);
    }
    
    UpdateVideoToFrame(currentLabelList.back());
    
    return true;
}

bool LabelVideoModel::SaveLabelsToFile()
{
    std::ofstream outfile(outputFileName);
    if(!outfile) return false;
    
    for (uint i=0; i< currentLabelList.size(); i++) {
        
        outfile << currentLabelList[i] << std::endl;
    }
    
    return true;
}

void LabelVideoModel::AddToExistingLabels(std::string newLine)
{
    currentLabelList.push_back(newLine);
}

void LabelVideoModel::RemoveLastLabel()
{
    if(currentLabelList.size() > 0)
        currentLabelList.pop_back();
}

void LabelVideoModel::UpdateVideoToFrame(std::string line)
{
    std::stringstream temp(line);
    int frame;
    temp >> frame;
    setFrameNumber(frame);
}

void LabelVideoModel::JumpVidepToLastIndex()
{
    UpdateVideoToFrame(currentLabelList.back());
}

