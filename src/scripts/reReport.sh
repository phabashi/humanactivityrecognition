#!/bin/bash

if [ $# != 1 ]; then
	echo "You should provide the feature set you want: (HOG, HOF, Trajectory, MBHx, MBHy) "
	exit
fi
#Configuration Section
ExternalFeature=$1
Activities="ActivitySet_15.txt"
ActorList=(S01 S02 S03 S04 S05 S06 S07 S08 S09 S10 S11) # I assumend the list is always constant during my tests
numberOfWords=40

# Folder definition Section
ExternalFeaturesFolder="./featuresx/" #This folder is input
MicromovementFeaturesFolder="./microMovements/"
folderPrefix="./test-data/$(echo $Activities | cut -d . -f 1)/$ExternalFeature/n-$numberOfWords-words" # find the file name part of $Activities

csvDataFolder="./$folderPrefix/data/csv"
SVMDataFolder="./$folderPrefix/data/svm"
SVMModelFolder="./$folderPrefix/models/svm"
KMeansModelFolder="./$folderPrefix/models/kmeans"
resultFolder="./$folderPrefix/results/"

# Automated generating parameters section, should not be modified usually.

ActivityList="./lists/$Activities" 
externalFeatureFlag=true

SVMLearningParameters="-s 0 -t 3 -d 3 -c 500"

if [ $ExternalFeature = "Trajectory" ]; then
	FeatureNumbers='11-40' # Trajectory Features
elif [ $ExternalFeature = "HOG" ]; then
	FeatureNumbers='41-136' #HOG Features'
elif [ $ExternalFeature = "HOF" ]; then
	FeatureNumbers='137-244' #HOF Features'
elif [ $ExternalFeature = "MBHx" ]; then
	FeatureNumbers='245-340' #MBHx Features'
elif [ $ExternalFeature = "MBHy" ]; then
	FeatureNumbers='341-436' #MBHy Features'
else
	externalFeatureFlag=false # we are dealing with a Micromovement
	if [ $ExternalFeature = "Micromovement-23" ]; then
		FeatureNumbers='2-67' #Micromovemetn with length 23
		MMLength="_L23-"
	elif [ $ExternalFeature = "Micromovement-21" ]; then
		FeatureNumbers='2-61' #Micromovemetn with length 21
		MMLength="_L21-"
	elif [ $ExternalFeature = "Micromovement-19" ]; then
		FeatureNumbers='2-55' #Micromovemetn with length 19
		MMLength="_L19-"
	elif [ $ExternalFeature = "Micromovement-17" ]; then
		FeatureNumbers='2-49' #Micromovemetn with length 17
		MMLength="_L17-"
	elif [ $ExternalFeature = "Micromovement-15" ]; then
		FeatureNumbers='2-43' #Micromovemetn with length 15
		MMLength="_L15-"
	elif [ $ExternalFeature = "Micromovement-13" ]; then
		FeatureNumbers='2-37' #Micromovemetn with length 13
		MMLength="_L13-"
	elif [ $ExternalFeature = "Micromovement-11" ]; then
		FeatureNumbers='2-31' #Micromovemetn with length 11
		MMLength="_L11-"
	elif [ $ExternalFeature = "Micromovement-9" ]; then
		FeatureNumbers='2-25' #Micromovemetn with length 9
		MMLength="_L9-"
	elif [ $ExternalFeature = "Micromovement-7" ]; then
		FeatureNumbers='2-19' #Micromovemetn with length 7
		MMLength="_L7-"
	elif [ $ExternalFeature = "Micromovement-5" ]; then
		FeatureNumbers='2-13' #Micromovemetn with length 5
		MMLength="_L5-"
	elif [ $ExternalFeature = "Micromovement-3" ]; then
		FeatureNumbers='2-7' #Micromovemetn with length 3
		MMLength="_L3-"
	else
		echo "The External Feature can not be found!"
		exit
	fi
fi

# Creating the folders if they are not exists already:
mkdir -p $csvDataFolder
mkdir -p $SVMDataFolder
mkdir -p $SVMModelFolder
mkdir -p $KMeansModelFolder
mkdir -p $resultFolder

echo "Setting : Feature = $ExternalFeature , FeatureNumbers = $FeatureNumbers , NumberOfWords = $numberOfWords "

makeReportForFold()
{
	i=$1

	expected="$resultFolder/Fold-$i.expected"
	aquired="$resultFolder/Fold-$i.out"
	svmTestFile="$SVMDataFolder/Fold-$i.svm.test"
	svmOutputFile="$SVMDataFolder/Fold-$i.svm.output"

	#echo "Making report for fold $i :"
	cat $svmTestFile | cut -d " " -f 1 > $expected
	cp $svmOutputFile $aquired
	#./bin/MakeReport -e $expected -o $aquired -lab $ActivityList
	cat $expected >> $expectedResult
	cat $aquired >> $outputResult
}

#Make a report

echo "Making Report: "

expectedResult="$SVMDataFolder/all.expected"
outputResult="$SVMDataFolder/all.actual"

rm -f $expectedResult
rm -f $outputResult

for i in ${ActorList[@]}; do makeReportForFold "$i" ; done

./bin/MakeReport -e $expectedResult -o $outputResult -lab $ActivityList

