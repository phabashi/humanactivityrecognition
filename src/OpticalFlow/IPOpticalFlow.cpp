/*
 * IPOpticalFlow.cpp
 *
 *  Created on: Apr 30, 2016
 *      Author: pejman
 */

#include "IPOpticalFlow.h"
#include "float.h"
#include <unordered_set>

#include "../Pejman/GlobalSetting.h"
#include "../Logger/Logger.h"
#include "../Clustering/DistanceCalculator.h"

using namespace Pejman::Logger;
using namespace Pejman::Application;

namespace HAR {
namespace Video {
namespace OpticalFlow {

IPOpticalFlow::IPOpticalFlow() {
	thresholdA = GlobalSetting::GetOptionAs<float>("OpticalFlowIP",
			"thresholdA", 20); //pixels
	distanceBetweenBestMatchAndSecondBest = GlobalSetting::GetOptionAs<
				float>("OpticalFlowIP", "distanceBetweenBestMatchAndSecondBest",
				0.96);
}

void IPOpticalFlow::ExtractOpticalFlow(AugmentedVideoBuffer& video,
		int length) {

	video.ClearTrajectories();
	cv::Mat preImage;
	preImage = video.getFrame(0);
	cv::Size frameSize(preImage.cols, preImage.rows);

	std::unordered_set<Trajectory> survivedTracking; //Contains ongoing tracks
	std::unordered_set<Trajectory> activeTracks; //Contains ongoing tracks
	//	std::unordered_set<Track> finishedTracks;//Contains finished tracks

	//TODO The frame will never be used during the loop, it might be faster to replace actual reading with a simulation
	for (cv::Mat frame = video.getFrame(1); frame.data;
			frame = video.getNextFrame()) {
		const std::vector<cv::Point_<float> >& previousFramePointsOfInteres =
				video.getPreviousFrameInformation().getPointsOfInterest();
		const std::vector<cv::Point_<float> >& thisFramePointsOfInteres =
				video.getCurrentFrameInformation().getPointsOfInterest();

		std::unordered_set<cv::Point2f> PointsThatAreCurrentlyTracking;
		//add new location of interest points to the tracking points
		for (Trajectory track : activeTracks)
			PointsThatAreCurrentlyTracking.insert(track.getLastPoint());

		//Look if new feature points are different from tracked ones, create new trackings for them
		for (cv::Point2f newPoint : previousFramePointsOfInteres) {
			if (PointsThatAreCurrentlyTracking.find(newPoint)
					== PointsThatAreCurrentlyTracking.end()) {
				//Create New tracking for the current point from previous frame (we are still looking in previous frames
				activeTracks.insert(
						Trajectory(video.getPreviousFrameNumber(), newPoint));
			}
		}

		if (activeTracks.empty()) {
			Logger::Log(LogLevel::WARNING, "Empty Tracks set!\n");
			continue;
		}

		for (Trajectory track : activeTracks) {
			cv::Point2f oldPoint = track.getLastPoint();
			cv::Mat descriptor1;
			try {
				descriptor1 = video.getPreviousFrameInformation().getDescriptor(
						oldPoint);
			} catch (indexNotFoundException& excp) {
				Logger::Log(LogLevel::WARNING,
						"The (%f, %f) point could not be located in the existing frame informations point of previous frame, skipping it.\n",
						oldPoint.x, oldPoint.y);
				continue;	//skip this track
			}
			float min = FLT_MAX;
			float secondMin = FLT_MAX;
			cv::Point2f bestMatch(-1, -1);
			cv::Point2f secondBestMatch(-1, -1);

			for (cv::Point2f newPoint : thisFramePointsOfInteres) {
				if (DistanceCalculator::ManhattanDistance(oldPoint, newPoint)
						< thresholdA) {
					cv::Mat descriptor2;
					try {
						descriptor2 =
								video.getCurrentFrameInformation().getDescriptor(
										newPoint);
					} catch (indexNotFoundException& exc) {
						Logger::Log(LogLevel::WARNING,
								"The (%f, %f) point could not be located in the existing frame informations point of previous frame, skipping it.\n",
								newPoint.x, newPoint.y);
						continue;
					}

					try {
						float distance = DistanceCalculator::EucledeanDistance(
								descriptor1, descriptor2);
						if (distance < min) {
							secondMin = min;
							secondBestMatch = bestMatch;

							min = distance;
							bestMatch = newPoint;
						} else if (distance < secondMin) {
							secondMin = distance;
							secondBestMatch = newPoint;
						}
					} catch (DistanceCalculatorException& exp) {
						Logger::Log(LogLevel::ERROR, exp.what());
					}
				}
			}
//			Logger::Log(LogLevel::VERBOSE, "min = %f,  smin = %f \n", min,
//					secondMin);

			if ((bestMatch != cv::Point2f(-1, -1))
					&& (min <= distanceBetweenBestMatchAndSecondBest * secondMin)) {
				//We could find a good match!
				track.Add(bestMatch);
				if (track.length() == length)
					video.AddTrajectory(track);
				else
					survivedTracking.insert(track);

			} else {
				//Best match did not found, assuming the interest point is not moving or deleting it from tracking
				Logger::Log(LogLevel::WARNING,
						"The best match did not found! Ignoring the trajectory!\n");
//				Logger::Log(LogLevel::INFO,
//						"The best match did not found! Adding point (%3.2f, %3.2f) to track!\n",
//						oldPoint.x, oldPoint.y);
//				track.Add(oldPoint);
//				if (track.length() == length)
//					video.AddTrajectory(track);
//				else
//					survivedTracking.insert(track);
			}

		}
		//replace survivedTracking with actual tracking.
		Logger::Log(LogLevel::VERBOSE, "Survived = %d ALL = %d \n",
				survivedTracking.size(), video.getAllTrajectories().size());
		activeTracks = std::move(survivedTracking);
		survivedTracking.clear();
	}	//main for loop
}

IPOpticalFlow::~IPOpticalFlow() {
}

} /* namespace OpticalFlow */
} /* namespace Video */
} /* namespace HAR */
