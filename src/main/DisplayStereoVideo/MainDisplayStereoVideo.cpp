#include <iostream>
#include "Arguments.h"
#include "../../Logger/Logger.h"
#include "../../Video/StereoAugmentedVideo.h"
#include "../../Video/Display.h"
#include "../../util/CommaSeparatedReader.h"
#include "../../Video/TrajectoryEncoding/I3DEncoding.h"

#include "../../Video/VideoRectifier.h"
#include "../../Video/StereoRectifyableVideo.h"

using namespace std;
using namespace HAR;
using namespace HAR::DisplayStereoVideo;
using namespace Pejman::Logger;
using namespace HAR::Exceptions;
using namespace HAR::Video::TrajectoryEncoding;

int main(int argc, char* argv[]){
	Arguments args;
	try {
		args = Arguments(argc, argv);
	} catch (ArgumentParseException& exp) {
		Logger::Log(LogLevel::FATAL, "Error in parsing of the arguments!");
		args.printHelp();
		exit(1);
	}

	Logger::Log(LogLevel::VERBOSE, "Calling Stereo Augmented Video!");
	HAR::Video::StereoRectifyableVideo A(args.getInputFileLeft(), args.getInputFileRight());
	HAR::Video::AugmentedVideoBuffer left(args.getInputFileLeft());
	HAR::Video::AugmentedVideoBuffer right(args.getInputFileRight());

	cv::Mat inputMat;
	T3DEncoding encoding = I3DEncoding::get3DEncoding("Off");//important :D otherwise the memory will be released while it is still needed.

	if(args.getTrajectoryFileName() != ""){
		inputMat = CommaSeparatedReader::ReadFloatMatrix(args.getTrajectoryFileName());
		auto allTrajectories = I3DEncoding::readTrajectoriesFromMatrix(inputMat);
		int trajectoryLength { ((inputMat.cols -1 )/ 2)} ;
		int numberOfFrames = allTrajectories.size() + trajectoryLength;
		cv::Mat temp = A.getFrame(0);
		cv::Size imgSize((temp.cols -1 ) / 2, temp.rows);//This is stereo frame, the width is twice the frame plus 2 pixel blue ribbon in between
		A.RectifyVideo(allTrajectories, numberOfFrames, imgSize);
		//encoding->UpdateRectifyInformationForAllFrames(allTrajectories, numberOfFrames, imgSize);

		//A.setRectificationInformation(&encoding->getRectifiedCorrectly(),
		//		&encoding->getH1(), &encoding->getH2(), & encoding->getF() );
	} else {
		A.RectifyVideo();

		Logger::Log(LogLevel::VERBOSE, "Done!");
	}
	Logger::Log(LogLevel::VERBOSE, "Calling Set Scale!");
	A.setDisplayScale(args.getScale());
	Logger::Log(LogLevel::VERBOSE, "Calling Display::Show!");
	HAR::Video::Display::Show(A);
	cout << args.getInputFileLeft() <<  args.getInputFileRight() << args.getScale() << endl;
	return 0;
}
