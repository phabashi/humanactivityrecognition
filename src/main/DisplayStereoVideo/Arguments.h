/*
 * Arguments.h
 *
 *  Created on: Nov 19, 2016
 *      Author: Pejman
 */

#ifndef SRC_EXTRACTTRAJECTORY3D_ARGUMENTS_H_
#define SRC_EXTRACTTRAJECTORY3D_ARGUMENTS_H_
#include <string>
#include "../../Exceptions/ArgumentParseException.h"

namespace HAR {
namespace DisplayStereoVideo {

class Arguments {
public:
	Arguments() :
		inputFileLeft(""),
		inputFileRight(""),
		inputTrajectoryFile(""),
		scale(1)
		{}

	Arguments(int argc, char* argv[]);
	static void printHelp();

	const std::string& getInputFileLeft() const {
		return inputFileLeft;
	}

	const std::string& getInputFileRight() const {
		return inputFileRight;
	}

	const std::string& getTrajectoryFileName() const {
		return inputTrajectoryFile;
	}

	float getScale() const {
		return scale;
	}

private:
	std::string inputFileLeft;
	std::string inputFileRight;
	std::string inputTrajectoryFile;
	double scale;
};

} /* namespace NewExtractFeatures */
} /* namespace HAR */

#endif /* SRC_NEWEXTRACTFEATURES_ARGUMENTS_H_ */
