#!/bin/bash

echo "This script will go trough all the clips in ./clips folder, search for Left vidoe and run extract featues on it and its corresponding right vido. "

#ls clips/ | grep -f ./lists/Actor_set3.txt | grep -f ./lists/ActivitySet_15.txt | grep Left

cores=7

source ~/HAR/newScripts/KTH/default.bash

# inputFileList=`ls ./clips/ | grep -e "Left" | grep -f ./lists/ActivitySet_10.txt | grep -f ./lists/Actor_set3.txt | grep -f ./lists/Instances_5.txt > ./lists/VideoFileList_Actor03_Activity_10_I5.txt` 
inputFileList="./lists/KTH/VideoFileList_Actor25_Activity_10_I5.txt"

mkdir -p $featureFolderName

leftVideoFileNames=(`cat $inputFileList`)

#for length in ${featureLengthes[@]};do

completeFeatureFileName="$featureFolderName/features"
mkdir -p $completeFeatureFileName

count=${#leftVideoFileNames[@]}
fileNumber="1"
for i in ${leftVideoFileNames[@]}; do
	#ExtractFeatures $i &
	fileNumber=$(( $fileNumber + 1 ))
	#echo "Running $fileNumber/ $count = [$(( fileNumber * 100 / count ))%]"
	leftVideo="./dataset/KTH/clips/$i"
	name="${i/.avi/.fet}"
	echo
	echo "'$i' -> '$name'"
	echo
	featureFileName="$completeFeatureFileName/$name"
	if [ -e $featureFileName ]; then
		echo "File Exsists, skipping $featureFileName..."
	else
		echo "[$(( fileNumber * 100 / count ))%] Extracting features of '$leftVideo' with lenght $length and save it to '$featureFileName'"
		./bin/newExtractFeatures -l $leftVideo -o $featureFileName -dl $length -ip $interestPointAlgorithm -ipd $descriptorAlgorithm -ofa $opticalFlowAlgorithm -s $scale -te $encoding $motionOption -minE $minEnergy $MULTISCALE -is 6 as $settingFile&
		if [[ $? != 0 ]]; then
			echo "Some error occured! Exiting "
			exit 1
		fi
	fi
	#sleep 1 &
	#echo $fileNumber &
	#wait %1
	if (( $fileNumber % $cores == 0 )); then
		echo "Waiting for childs!"
		wait
	fi
done
#done
