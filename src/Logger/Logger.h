/*
 * Logger.h
 *
 *  Created on: Mar 6, 2016
 *      Author: Pejman
 *      Copyright (c) 2016 Pejman. All rights reserved.
 */

#ifndef SRC_LOGGER_LOGGER_H_
#define SRC_LOGGER_LOGGER_H_
#include <string>
#include <stdarg.h>

namespace Pejman {
namespace Logger {

enum LogLevel {FATAL, ERROR, EXCEPTION, WARNING, INFO, VERBOSE};

class Logger {
public://Singleton implementation
	void ChangeLevel(LogLevel level) {
		getInstance().logLevel = level;
	}

	static Logger& getInstance(){
			static Logger instance(LogLevel::EXCEPTION);
			return instance;
		}

	static void Log(LogLevel type, const char* fmt, ...){
		va_list tmp;
		va_start(tmp, fmt);
		getInstance().LogFormattedString(type, fmt, tmp);
		va_end(tmp);
	}
private:
	Logger(LogLevel level);

public:
	void LogFormattedString(LogLevel type, const char* fmt, ...);
	void LogFormattedString(LogLevel type, const char* fmt, va_list args);


	virtual ~Logger();
private:
	//void ChangeLevel(LogLevel level) { logLevel = level; }


	LogLevel logLevel;
	std::string levelName[6];
};

} /* namespace Logger */
} /* namespace Pejman */

#endif /* SRC_LOGGER_LOGGER_H_ */
