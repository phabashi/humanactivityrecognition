#!/bin/bash

if [ $# -lt 1 ]; then
	echo "Usage 1: $0 <3DTrajectoryFolder>"
	echo "OR"
	echo "Usage 2: $0 <inputFifoName> [outputFifoName]"

	exit 1
fi

if [ -d $1 ]; then
	folderName=$1
	fileList=`ls $folderName`
else
	if [ -e $1 ]; then
		folderName=`cat $1`
		fileList = folderName
	else
		mkfifo $1
		folderName=`cat $1`
		fileList = folderName
	fi
fi


encoding=${encoding:-"3Order"}

outputFolder="$folderName/../$encoding/features"
mkdir -p $outputFolder
bc=0
for file in $fileList; do
	if [ ! -f "$outputFolder/$file" ]; then
		bc=$(( bc + 1 ))
		echo "encode3D: $file with $encoding and save it to $outputFolder/$file"
		./bin/encode3D -e $encoding -i "$folderName/$file" -o "$outputFolder/$file -v " &
	else
		#echo "File alreasy exists: $outputFolder/$file"
		echo -n "."
	fi
	if [ $bc == 7 ]; then
		wait
		bc=0
	fi
done

#send the parameter to next program
if [ "$2" != "" ]; then
        echo send the parameter to next program through \"$2\"
        if [ ! -e "$2" ]; then
                mkfifo "$2"
        fi
        echo "$outputFolder/../" > $2
fi
