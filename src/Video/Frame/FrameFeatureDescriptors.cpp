/*
 * FrameFeatureDescriptors.cpp
 *
 *  Created on: Jan 26, 2015
 *      Author: Pejman
 *      Copyright (c) 2015 Pejman. All rights reserved.
 */

#include "FrameFeatureDescriptors.h"

namespace HAR {

FrameFeatureDescriptors::FrameFeatureDescriptors(cv::Mat descriptors)
{
	this->descriptor = descriptors;
}

FrameFeatureDescriptors::~FrameFeatureDescriptors() {
	// TODO Auto-generated destructor stub
}

} /* namespace HAR */
