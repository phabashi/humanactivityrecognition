/*
 * MyDescriptor.cpp
 *
 *  Created on: Feb 12, 2015
 *      Author: Pejman
 *      Copyright (c) 2016 Pejman. All rights reserved.
 */

#include "MyDescriptor.h"
#include "../../Logger/Logger.h"
#include <sstream>
using namespace Pejman::Logger;

namespace HAR {

MyDescriptor::MyDescriptor(std::vector<cv::Point3f> microMovement,
		int startingFrame) : MyDescriptor(startingFrame)
{
	this->microMovement = microMovement;
}

MyDescriptor::~MyDescriptor() {
	// TODO Auto-generated destructor stub
}

MyDescriptor::MyDescriptor(int startingFrame)
{
	this->startingFrame = startingFrame;
	terminated = false;
}

void MyDescriptor::Update(cv::Point3f newLocation)
{
	if(! terminated)
		microMovement.push_back(newLocation);
}


    void MyDescriptor::Save(std::ofstream& outFile) const
    {
        std::string temp = ToString();
        outFile << temp;
    }
    
    void parse(std::string input, float& value)
    {
        std::string ignoreCharacter = "[],=>\r";
        for(int i=0; input[i]; i++)
            if(ignoreCharacter.find(input[i]) != std::string::npos )
                input[i] = ' ';
        std::stringstream valueString(input);
        valueString >> value;
        
    }
    
    bool MyDescriptor::Parse(std::string input)
    {
        if(input == "") return false;
        std::stringstream text(input);
        text >> startingFrame;
        std::string ignore;
        text >> ignore;
        Logger::Log( LogLevel::VERBOSE,  "ignoring: '%s'", ignore.c_str() );
        
        while (text) {
        
            cv::Point3f aPoint;
            std::string xs,ys,zs;
            text >> xs;
            if(xs == "=>" || xs == "" ) continue;
            parse(xs, aPoint.x);
            text >> ys; parse(ys, aPoint.y);
            text >> zs; parse(zs, aPoint.z);
            
            Update(aPoint);
        }
        Terminate();
        return true;
    }
    
std::string MyDescriptor::ToString() const
{
	std::stringstream temp;
    temp << startingFrame << " : " ;
	for(unsigned int i =0; i< microMovement.size(); i ++)
		temp << microMovement[i] << ((i < microMovement.size() - 1)? " => ": "");
	return temp.str();
}

    double  MyDescriptor::CalculateEnergy()
    {
        double energy = 0;
        cv::Point3f previous;
        for(auto point: microMovement)
        {
            if(previous == cv::Point3f())
            {
                //This is the first point
                previous = point;
                continue;
            }
            double dx = point.x - previous.x;
            double dy = point.y - previous.y;
            double dz = point.z - previous.z;
            
            energy +=  sqrt(dx*dx + dy*dy + dz*dz);
        }
        return energy / microMovement.size();
    }
    
    std::string MyDescriptor::getHeader()
    {
        std::stringstream out;
        out << "fn" ;
        for(unsigned int i=1;i<=microMovement.size() - 1;i++)
            out << ", vx" << i << ", vy" << i << ", vz"<< i;
        
        out << ", dx" << ", dy" << ", dz";
        out << ", energy";
        
        out << std::endl;
        
        return out.str();
    }
    
    std::string MyDescriptor::ToMicromovementString()
    {
        double epsilon = 1e-30;
        std::stringstream result;
        result << startingFrame << ", " ;
        cv::Point3f previous, first, last;
        double energy = 0;
        for(auto point: microMovement)
        {
            if(std::abs(point.z) < epsilon)
                return "";
            if(previous == cv::Point3f())
            {
                //This is the first point
                previous = point;
                double x = point.x / point.z;
                double y = point.y / point.z;
                double z = 1 / point.z;
                
                first = cv::Point3f(x,y,z);
                continue;
            }
            
            //normalize points values to represent real world micromivements
            double x = point.x / point.z;
            double y = point.y / point.z;
            double z = 1 / point.z;
            last = cv::Point3f(x,y,z);
            double ox = previous.x / previous.z;
            double oy = previous.y / previous.z;
            double oz = 1 / previous.z;
            
            double dx = x - ox;
            double dy = y - oy;
            double dz = z - oz;
            
            energy +=  dx*dx + dy*dy + dz*dz;
            
            result << dx << ", ";
            result << dy << ", ";
            result << dz << ", ";
        }
        
        result << last.x - first.x;
        result << ", " << last.y - first.y;
        result << ", " << last.z - first.z;
        
        result << ", " << energy;
        
        return result.str();
    }

} /* namespace HAR */


