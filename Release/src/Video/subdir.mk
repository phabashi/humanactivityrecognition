################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/Video/AugmentedVideoDraw.cpp \
../src/Video/DisplayableVideo.cpp 

OBJS += \
./src/Video/AugmentedVideoDraw.o \
./src/Video/DisplayableVideo.o 

CPP_DEPS += \
./src/Video/AugmentedVideoDraw.d \
./src/Video/DisplayableVideo.d 


# Each subdirectory must supply rules for building sources it contributes
src/Video/%.o: ../src/Video/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -I../__GXX_EXPERIMENTAL_CXX0X__ -I/home/habashi/Development/opencv2/include -I/usr/local/include/ -O3 -Wall -c -fmessage-length=0 -std=c++11 -MT"$@" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


