/*
 * DisparityFlowDescriptorExtractor.h
 *
 *  Created on: Feb 12, 2015
 *      Author: Pejman
 *      Copyright (c) 2015 Pejman. All rights reserved.
 */

#ifndef DISPARITYFLOWDESCRIPTOREXTRACTOR_H_
#define DISPARITYFLOWDESCRIPTOREXTRACTOR_H_

#include "../OpticalFlow/OpticalFlowInformation.h"
#include "Descriptor/MyVideoDescrptor.h"
#include "StereoAugmentedVideo.h"

namespace HAR {
namespace Video {

class DisparityFlowDescriptorExtractor {
public:
	DisparityFlowDescriptorExtractor(StereoAugmentedVideo theSynchronizedVideo);

	/*
	 * Extract descriptors of length frameLength from each frameLength Number of Frames. This can
	 * be changed to extract descriptors of frameLength for each frame.
	 */
private:
	MyVideoDescrptor ExtractDescriptors(int frameLength);
public:
    MyVideoDescrptor ExtractMicroMovementDescriptors(
    		unsigned int frameLength, bool& finished,
    		unsigned int startFrame = 0,
		unsigned int legth = 0,
		std::string FeaturePointExtractionMethod = "FAST",
		std::string FeaturePointDescriptionMethod = "SIFT",
		std::string FeaturePointMatchingAlgorithm = "BruteForce",
		double theRatioToSelectBestMatches =  0.95 ,
		bool removeHemogenousArea = true);

	virtual ~DisparityFlowDescriptorExtractor();


private:
	StereoAugmentedVideo theVideo;

	void AddMicroMovementsToResult(
			std::vector<MyDescriptor>& micromovements,
			MyVideoDescrptor& result);
	void InitializeMicromovements(unsigned long size,
			int frameCount, std::vector<MyDescriptor>& micromovements);
	void UpdateMicroMovements(
			OpticalFlowInformation& flowInfoLeft,
			std::vector<MyDescriptor>& micromovements,
			MyVideoDescrptor& result);
	void AddPointsToMicromovement(
			const OpticalFlowInformation& flowInfoLeft,
			std::vector<MyDescriptor>& micromovements,
			StereoFrame& aFrame);
};

} /* namespace Video */
} /* namespace HAR */

#endif /* DISPARITYFLOWDESCRIPTOREXTRACTOR_H_ */
